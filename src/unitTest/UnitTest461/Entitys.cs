﻿using CNative.Dapper.Utils;
//using CNative.DbUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest461
{
    [TableMap(Schema = "test", TableName = "persons")]
    public class Entity_persons
    {
        #region 构造函数

        //public Entity_persons()
        //{ }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        [DbFieldMap(CloumnName = "id", Type = "int", IsNullable = false, IsPrimaryKey = true)]
        public int id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }

        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

        public int age { get; set; }

        public string adress { get; set; }
        public string remark { get; set; }


        public string getPublic()
        {
            return "getPublic";
        }
        internal string getInternal()
        {
            return "getInternal";
        }
        protected string getProtected()
        {
            return "getProtected";
        }
        private string getPrivate()
        {
            return "getPrivate";
        }

        private string getPrivate(string str = "", string str1 = "")
        {
            return "getPrivatestrstr1";
        }
        private string getPrivate(string str = "", int? id = 0)
        {
            return "getPrivatestrid";
        }
    }

    public class Entity_persons2 : Entity_persons
    {
        #region 构造函数

        public Entity_persons2(string _name)
        { name = _name; }
        #endregion


        public new string getPublic()
        {
            return name + " getPublic";
        }
        internal new string getInternal()
        {
            return name + " getInternal";
        }
        protected new string getProtected()
        {
            return name + " getProtected";
        }
        private string getPrivate()
        {
            return name + " getPrivate";
        }

        private string getPrivate(string str = "", string str1 = "")
        {
            return name + " getPrivatestrstr1";
        }
        private string getPrivate(string str = "", int? id = 0)
        {
            return name + " getPrivatestrid";
        }
    }

    [TableMap(Schema = "test", TableName = "DM_JGXXB")]
    public class Entity_DMJGXXB
    {
        #region 构造函数

        public Entity_DMJGXXB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// OrgId
        /// </summary>
        [DbFieldMap(CloumnName = "OrgId", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int OrgId { get; set; }
        /// <summary>
        /// jgdm
        /// </summary>
        public string jgdm { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        /// <summary>
        /// jgjc
        /// </summary>
        public string jgjc { get; set; }
        /// <summary>
        /// sjjgid
        /// </summary>
        public int sjjgid { get; set; }
        /// <summary>
        /// jgjpm
        /// </summary>
        public string jgjpm { get; set; }
        /// <summary>
        /// yhzh
        /// </summary>
        public string yhzh { get; set; }
        /// <summary>
        /// yhmc
        /// </summary>
        public string yhmc { get; set; }
        /// <summary>
        /// zt
        /// </summary>
        public int zt { get; set; }
        /// <summary>
        /// bz
        /// </summary>
        public string bz { get; set; }
        /// <summary>
        /// CreateId
        /// </summary>
        public int CreateId { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// UpdateId
        /// </summary>
        public int UpdateId { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_KSXXB")]
    public class Entity_DMKSXXB
    {
        #region 构造函数

        public Entity_DMKSXXB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// ksid
        /// </summary>
        [DbFieldMap(CloumnName = "ksid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int ksid { get; set; }
        /// <summary>
        /// sjksid
        /// </summary>
        public int sjksid { get; set; }
        /// <summary>
        /// ksjc
        /// </summary>
        public string ksjc { get; set; }
        /// <summary>
        /// ksmc
        /// </summary>
        public string ksmc { get; set; }
        /// <summary>
        /// ksdm
        /// </summary>
        public string ksdm { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// CreateID
        /// </summary>
        public int CreateID { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// UpdateID
        /// </summary>
        public int UpdateID { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        #endregion

        /// <summary>
        /// jgdm
        /// </summary>
        public string jgdm { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        /// <summary>
        /// jgjc
        /// </summary>
        public string jgjc { get; set; }
    }
    [TableMap(Schema = "test", TableName = "DM_CZYXXB")]
    public class Entity_DMCZYXXB
    {
        #region 构造函数

        public Entity_DMCZYXXB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// czyid
        /// </summary>
        [DbFieldMap(CloumnName = "czyid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int czyid { get; set; }
        /// <summary>
        /// czygh
        /// </summary>
        public string czygh { get; set; }
        /// <summary>
        /// czyxm
        /// </summary>
        public string czyxm { get; set; }
        /// <summary>
        /// xb
        /// </summary>
        public int xb { get; set; }
        /// <summary>
        /// CreateId
        /// </summary>
        public int CreateId { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// UpdateId
        /// </summary>
        public int UpdateId { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// srm
        /// </summary>
        public string srm { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_CZYJGDYB")]
    public class Entity_DMCZYJGDYB
    {
        #region 构造函数

        public Entity_DMCZYJGDYB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// czyjgdyid
        /// </summary>
        [DbFieldMap(CloumnName = "czyjgdyid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int czyjgdyid { get; set; }
        /// <summary>
        /// czyid
        /// </summary>
        public int czyid { get; set; }
        /// <summary>
        /// czyxm
        /// </summary>
        public string czyxm { get; set; }
        /// <summary>
        /// ksid
        /// </summary>
        public int ksid { get; set; }
        /// <summary>
        /// ksmc
        /// </summary>
        public string ksmc { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_CZYJGDYB2")]
    public class Entity_DMCZYJGDYB2
    {
        #region 构造函数

        public Entity_DMCZYJGDYB2()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// czyjgdyid
        /// </summary>
        [DbFieldMap(CloumnName = "czyjgdyid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int czyjgdyid { get; set; }
        /// <summary>
        /// czyid
        /// </summary>
        public int czyid { get; set; }
        /// <summary>
        /// czyxm
        /// </summary>
        public string czyxm { get; set; }
        /// <summary>
        /// ksid
        /// </summary>
        public int ksid { get; set; }
        /// <summary>
        /// ksmc
        /// </summary>
        public string ksmc { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_GJDMB")]
    public class Entity_DMGJDMB
    {
        #region 构造函数

        public Entity_DMGJDMB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// id
        /// </summary>
        [DbFieldMap(CloumnName = "id", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int id { get; set; }
        /// <summary>
        /// gjmc
        /// </summary>
        public string gjmc { get; set; }
        /// <summary>
        /// gjjc
        /// </summary>
        public string gjjc { get; set; }
        /// <summary>
        /// gjjpm
        /// </summary>
        public string gjjpm { get; set; }
        /// <summary>
        /// gjwbm
        /// </summary>
        public string gjwbm { get; set; }
        /// <summary>
        /// xsxh
        /// </summary>
        public int xsxh { get; set; }
        /// <summary>
        /// bz
        /// </summary>
        public string bz { get; set; }
        #endregion
    }

    [TableMap(Schema = "", TableName = "sys_menu")]
    public class Entity_sysmenu
    {
        #region 构造函数
        public Entity_sysmenu()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// Id
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Pid
        /// </summary>
        public Int64 Pid { get; set; }
        /// <summary>
        /// Pids
        /// </summary>
        public string Pids { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// Icon
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// Router
        /// </summary>
        public string Router { get; set; }
        /// <summary>
        /// Component
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// Permission
        /// </summary>
        public string Permission { get; set; }
        /// <summary>
        /// Application
        /// </summary>
        public string Application { get; set; }
        /// <summary>
        /// OpenType
        /// </summary>
        public int OpenType { get; set; }
        /// <summary>
        /// Visible
        /// </summary>
        public string Visible { get; set; }
        /// <summary>
        /// Link
        /// </summary>
        public string Link { get; set; }
        /// <summary>
        /// Redirect
        /// </summary>
        public string Redirect { get; set; }
        /// <summary>
        /// Weight
        /// </summary>
        public int Weight { get; set; }
        /// <summary>
        /// Sort
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// CreatedTime
        /// </summary>
        public string CreatedTime { get; set; }
        /// <summary>
        /// UpdatedTime
        /// </summary>
        public string UpdatedTime { get; set; }
        /// <summary>
        /// CreatedUserId
        /// </summary>
        public int? CreatedUserId { get; set; }
        /// <summary>
        /// CreatedUserName
        /// </summary>
        public string CreatedUserName { get; set; }
        /// <summary>
        /// UpdatedUserId
        /// </summary>
        public int? UpdatedUserId { get; set; }
        /// <summary>
        /// UpdatedUserName
        /// </summary>
        public string UpdatedUserName { get; set; }
        /// <summary>
        /// IsDeleted
        /// </summary>
        public int IsDeleted { get; set; }
        #endregion
    }

    [TableMap(Schema = "PHARMACY", TableName = "DRUG_STOCK")]
    public class Entity_DRUGSTOCK
    {
        #region 构造函数

        public Entity_DRUGSTOCK()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// 库存管理单位：库房代码，见库存单位字典
        /// </summary>
        [DbFieldMap(CloumnName = "STORAGE", Type = "VARCHAR2", MaxLength = 8, IsNullable = false, IsPrimaryKey = true, Description = "库存管理单位：库房代码，见库存单位字典")]
        public string STORAGE { get; set; }
        /// <summary>
        /// 药品代码：由药品字典定义的代码
        /// </summary>
        [DbFieldMap(CloumnName = "DRUG_CODE", Type = "VARCHAR2", MaxLength = 10, IsNullable = false, IsPrimaryKey = true, Description = "药品代码：由药品字典定义的代码")]
        public string DRUGCODE { get; set; }
        /// <summary>
        /// 规格：由药品字典定义的规格
        /// </summary>
        [DbFieldMap(CloumnName = "DRUG_SPEC", Type = "VARCHAR2", MaxLength = 20, IsNullable = false, IsPrimaryKey = true, Description = "规格：由药品字典定义的规格")]
        public string DRUGSPEC { get; set; }
        /// <summary>
        /// 单位：对应剂型及规格，使用规范名称，见4.32计量单位字典
        /// </summary>
        public string UNITS { get; set; }
        /// <summary>
        /// 批号：使用“XX/XX/XXXXXX”
        /// </summary>
        [DbFieldMap(CloumnName = "BATCH_NO", Type = "VARCHAR2", MaxLength = 16, IsNullable = false, IsPrimaryKey = true, Description = "批号：使用“XX/XX/XXXXXX”")]
        public string BATCHNO { get; set; }
        /// <summary>
        /// 有效期：药品的有效截止日期
        /// </summary>
        [DbFieldMap(CloumnName = "EXPIRE_DATE", Type = "DATE", MaxLength = 7, Description = "有效期：药品的有效截止日期")]
        public DateTime? EXPIREDATE { get; set; }
        /// <summary>
        /// 厂家标识：反映生产厂家，见药品生产厂家字典
        /// </summary>
        [DbFieldMap(CloumnName = "FIRM_ID", Type = "VARCHAR2", MaxLength = 10, IsNullable = false, IsPrimaryKey = true, Description = "厂家标识：反映生产厂家，见药品生产厂家字典")]
        public string FIRMID { get; set; }
        /// <summary>
        /// 进货价：购买价，以包装单位记单价
        /// </summary>
        [DbFieldMap(CloumnName = "PURCHASE_PRICE", Type = "NUMBER", MaxLength = 22, Description = "进货价：购买价，以包装单位记单价")]
        public decimal PURCHASEPRICE { get; set; }
        /// <summary>
        /// 折扣：该药品购入时的折扣率。百分数，只记录数值部分
        /// </summary>
        public decimal DISCOUNT { get; set; }
        /// <summary>
        /// 包装规格：反映药品含量及包装信息，如0.25g*30
        /// </summary>
        [DbFieldMap(CloumnName = "PACKAGE_SPEC", Type = "VARCHAR2", MaxLength = 20, IsNullable = false, IsPrimaryKey = true, Description = "包装规格：反映药品含量及包装信息，如0.25g*30")]
        public string PACKAGESPEC { get; set; }
        /// <summary>
        /// 数量：以包装规格及包装单位所计的现库存数量，每次出库，该数量核减
        /// </summary>
        public decimal QUANTITY { get; set; }
        /// <summary>
        /// 包装单位：对应包装规格的计量单位，可使用任一级管理上方便的包装
        /// </summary>
        [DbFieldMap(CloumnName = "PACKAGE_UNITS", Type = "VARCHAR2", MaxLength = 8, Description = "包装单位：对应包装规格的计量单位，可使用任一级管理上方便的包装")]
        public string PACKAGEUNITS { get; set; }
        /// <summary>
        /// 存放包装1：上述一个包装单位中包含的小包装数量，为空或1表示为无此级包装
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_1", Type = "NUMBER", MaxLength = 22, Description = "存放包装1：上述一个包装单位中包含的小包装数量，为空或1表示为无此级包装")]
        public decimal SUBPACKAGE1 { get; set; }
        /// <summary>
        /// 存放包装1单位：对应内含包装1的单位
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_UNITS_1", Type = "VARCHAR2", MaxLength = 8, Description = "存放包装1单位：对应内含包装1的单位")]
        public string SUBPACKAGEUNITS1 { get; set; }
        /// <summary>
        /// 存放包装1规格：对应内含包装1的规格
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_SPEC_1", Type = "VARCHAR2", MaxLength = 20, Description = "存放包装1规格：对应内含包装1的规格")]
        public string SUBPACKAGESPEC1 { get; set; }
        /// <summary>
        /// 存放包装2：内含包装1中包含的小包装数量，为空或1表示为无此级包装
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_2", Type = "NUMBER", MaxLength = 22, Description = "存放包装2：内含包装1中包含的小包装数量，为空或1表示为无此级包装")]
        public decimal SUBPACKAGE2 { get; set; }
        /// <summary>
        /// 存放包装2单位：对应内含包装2的单位
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_UNITS_2", Type = "VARCHAR2", MaxLength = 8, Description = "存放包装2单位：对应内含包装2的单位")]
        public string SUBPACKAGEUNITS2 { get; set; }
        /// <summary>
        /// 存放包装2规格：对应内含包装2的规格
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_SPEC_2", Type = "VARCHAR2", MaxLength = 20, Description = "存放包装2规格：对应内含包装2的规格")]
        public string SUBPACKAGESPEC2 { get; set; }
        /// <summary>
        /// 存放库房：一个库存管理单位内的存放库房
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_STORAGE", Type = "VARCHAR2", MaxLength = 8, Description = "存放库房：一个库存管理单位内的存放库房")]
        public string SUBSTORAGE { get; set; }
        /// <summary>
        /// 货位：描述存放该批药品的位置，自由描述
        /// </summary>
        public string LOCATION { get; set; }
        /// <summary>
        /// 入库单号：该药品对应的入库单号，当多次入库的药品合并记录时，该项为空
        /// </summary>
        [DbFieldMap(CloumnName = "DOCUMENT_NO", Type = "VARCHAR2", MaxLength = 10, Description = "入库单号：该药品对应的入库单号，当多次入库的药品合并记录时，该项为空")]
        public string DOCUMENTNO { get; set; }
        /// <summary>
        /// 供应标志：反映该药品当前是否可供使用，0-不可供 1-可供
        /// </summary>
        [DbFieldMap(CloumnName = "SUPPLY_INDICATOR", Type = "NUMBER", MaxLength = 22, Description = "供应标志：反映该药品当前是否可供使用，0-不可供 1-可供")]
        public decimal SUPPLYINDICATOR { get; set; }
        /// <summary>
        /// 说明书
        /// </summary>
        public string INSTRUCTION { get; set; }
        #endregion


    }

    [TableMap(Schema = "", TableName = "Orders")]
    public class Entity_Orders
    {
        #region 构造函数

        public Entity_Orders()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// Same entry as in Customers table.
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// Same entry as in Employees table.
        /// </summary>
        public int EmployeeID { get; set; }
        /// <summary>
        /// Freight
        /// </summary>
        public decimal Freight { get; set; }
        /// <summary>
        /// OrderDate
        /// </summary>
        public DateTime? OrderDate { get; set; }
        /// <summary>
        /// Unique order number.
        /// </summary>
        [DbFieldMap(CloumnName = "OrderID", Type = "Integer", IsNullable = false, IsPrimaryKey = true, Description = "Unique order number.")]
        public int OrderID { get; set; }
        /// <summary>
        /// RequiredDate
        /// </summary>
        public DateTime? RequiredDate { get; set; }
        /// <summary>
        /// Street address only -- no post-office box allowed.
        /// </summary>
        public string ShipAddress { get; set; }
        /// <summary>
        /// ShipCity
        /// </summary>
        public string ShipCity { get; set; }
        /// <summary>
        /// ShipCountry
        /// </summary>
        public string ShipCountry { get; set; }
        /// <summary>
        /// Name of person or company to receive the shipment.
        /// </summary>
        public string ShipName { get; set; }
        /// <summary>
        /// ShippedDate
        /// </summary>
        public DateTime? ShippedDate { get; set; }
        /// <summary>
        /// ShipPostalCode
        /// </summary>
        public string ShipPostalCode { get; set; }
        /// <summary>
        /// State or province.
        /// </summary>
        public string ShipRegion { get; set; }
        /// <summary>
        /// Same as Shipper ID in Shippers table.
        /// </summary>
        public int ShipVia { get; set; }
        #endregion


        #region 属性
        /// <summary>
        /// Discount
        /// </summary>
        public Single Discount { get; set; }
        /// <summary>
        /// Same as Product ID in Products table.
        /// </summary>
        public int ProductID { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// UnitPrice
        /// </summary>
        public decimal UnitPrice { get; set; }
        #endregion
    }


    [TableMap(Schema = "", TableName = "Order Details")]
    public class Entity_Order_Details
    {
        #region 构造函数

        public Entity_Order_Details()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// OrderID
        /// </summary>
        [DbFieldMap(CloumnName = "OrderID", Type = "Integer", IsNullable = false, IsPrimaryKey = true)]
        public long OrderID { get; set; }
        /// <summary>
        /// ProductID
        /// </summary>
        [DbFieldMap(CloumnName = "ProductID", Type = "Integer", IsNullable = false, IsPrimaryKey = true)]
        public long ProductID { get; set; }
        /// <summary>
        /// UnitPrice
        /// </summary>
        public decimal UnitPrice { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// Discount
        /// </summary>
        public float Discount { get; set; }

        #endregion

    }


}
