﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.EventBusRabbitMQ
{
   public  enum QueueConsumerMode
    {
        Normal = 0,
        Retry,
        Fail,
    }
}
