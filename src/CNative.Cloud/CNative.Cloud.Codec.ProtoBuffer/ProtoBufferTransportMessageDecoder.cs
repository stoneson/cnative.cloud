﻿using CNative.Cloud.Codec.ProtoBuffer.Messages;
using CNative.Cloud.Codec.ProtoBuffer.Utilities;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Transport.Codec;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Codec.ProtoBuffer
{
   public sealed class ProtoBufferTransportMessageDecoder : ITransportMessageDecoder
    {
        #region Implementation of ITransportMessageDecoder

        public TransportMessage Decode(byte[] data)
        {
            var message = SerializerUtilitys.Deserialize<ProtoBufferTransportMessage>(data);
            return message.GetTransportMessage();
        }

        #endregion Implementation of ITransportMessageDecoder
    }
} 