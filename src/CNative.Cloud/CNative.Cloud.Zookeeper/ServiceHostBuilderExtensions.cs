﻿using System;
using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using CNative.Cloud.CPlatform.Cache;
using CNative.Cloud.CPlatform.Module;
using CNative.Cloud.CPlatform.Mqtt;
using CNative.Cloud.CPlatform.Routing;
using CNative.Cloud.CPlatform.Runtime.Client;
using CNative.Cloud.CPlatform.Runtime.Server;
using CNative.Cloud.CPlatform.Serialization;
using CNative.Cloud.CPlatform.Support;
using CNative.Cloud.Zookeeper.Configurations;
using CNative.Cloud.Zookeeper.Internal;
using CNative.Cloud.Zookeeper.Internal.Cluster.HealthChecks;
using CNative.Cloud.Zookeeper.Internal.Cluster.HealthChecks.Implementation;
using CNative.Cloud.Zookeeper.Internal.Cluster.Implementation.Selectors;
using CNative.Cloud.Zookeeper.Internal.Cluster.Implementation.Selectors.Implementation;
using CNative.Cloud.Zookeeper.Internal.Implementation;

namespace CNative.Cloud.Zookeeper
{
   public static class ServiceHostBuilderExtensions
    {
        public static IHostBuilder UseZookeeper(this IHostBuilder hostBuilder)
        {
            return hostBuilder.ConfigureContainer<ContainerBuilder>(containerBuilder =>
            {
           
                var configInfo = new ConfigInfo(null);
                containerBuilder.RegisterInstance(GetConfigInfo(configInfo));
                containerBuilder.RegisterType<ZookeeperRandomAddressSelector>().As<IZookeeperAddressSelector>().SingleInstance();
                containerBuilder.RegisterType<DefaultHealthCheckService>().As<IHealthCheckService>().SingleInstance();
                containerBuilder.RegisterType<DefaultZookeeperClientProvider>().As<IZookeeperClientProvider>();
                containerBuilder.RegisterType<ZooKeeperServiceRouteManager>().As<IServiceRouteManager>();
                containerBuilder.RegisterType<ZooKeeperMqttServiceRouteManager>().As<IMqttServiceRouteManager>();
                containerBuilder.RegisterType<ZookeeperServiceCacheManager>().As<IServiceCacheManager>();
                containerBuilder.RegisterType<ZookeeperServiceCommandManager>().As<IServiceCommandManager>();
                containerBuilder.RegisterType<ZooKeeperServiceSubscribeManager>().As<IServiceSubscribeManager>();

            });

        }
        private static ConfigInfo GetConfigInfo(ConfigInfo config)
        {
            ZookeeperOption option = null;
            var section = CPlatform.AppConfig.GetSection("Zookeeper");
            if (section.Exists())
                option = section.Get<ZookeeperOption>();
            else if (AppConfig.Configuration != null)
                option = AppConfig.Configuration.Get<ZookeeperOption>();
            if (option != null)
            {
                var sessionTimeout = config.SessionTimeout.TotalSeconds;
                var connectionTimeout = config.ConnectionTimeout.TotalSeconds;
                var operatingTimeout = config.OperatingTimeout.TotalSeconds;
                if (option.SessionTimeout > 0)
                {
                    sessionTimeout = option.SessionTimeout;
                }
                if (option.ConnectionTimeout > 0)
                {
                    connectionTimeout = option.ConnectionTimeout;
                }
                if (option.OperatingTimeout > 0)
                {
                    operatingTimeout = option.OperatingTimeout;
                }
                config = new ConfigInfo(
                    option.ConnectionString,
                    TimeSpan.FromSeconds(sessionTimeout),
                    TimeSpan.FromSeconds(connectionTimeout),
                    TimeSpan.FromSeconds(operatingTimeout),
                    option.RoutePath ?? config.RoutePath,
                    option.SubscriberPath ?? config.SubscriberPath,
                    option.CommandPath ?? config.CommandPath,
                    option.CachePath ?? config.CachePath,
                    option.MqttRoutePath ?? config.MqttRoutePath,
                    option.ChRoot ?? config.ChRoot,
                    option.ReloadOnChange != null ? bool.Parse(option.ReloadOnChange) :
                    config.ReloadOnChange,
                    option.EnableChildrenMonitor != null ? bool.Parse(option.EnableChildrenMonitor) :
                    config.EnableChildrenMonitor
                   );
            }
            return config;
        }

    }
}
