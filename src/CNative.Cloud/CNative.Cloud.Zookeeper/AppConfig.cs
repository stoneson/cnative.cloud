﻿using Microsoft.Extensions.Configuration;
using CNative.Cloud.Zookeeper.Configurations;

namespace CNative.Cloud.Zookeeper
{
   public class AppConfig
    {
        public static IConfigurationRoot Configuration { get; set; }

        public static ConfigInfo Config { get; internal set; }
    }
}
