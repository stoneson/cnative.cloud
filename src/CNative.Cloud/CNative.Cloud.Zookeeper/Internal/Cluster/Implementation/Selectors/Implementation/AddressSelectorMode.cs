﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Zookeeper.Internal.Cluster.Implementation.Selectors.Implementation
{
   public enum AddressSelectorMode
    {
        Polling,
        Random
    }
}
