﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CNative.Cloud.CPlatform.Cache;
using CNative.Cloud.CPlatform.Module;
using CNative.Cloud.CPlatform.Mqtt;
using CNative.Cloud.CPlatform.Routing;
using CNative.Cloud.CPlatform.Runtime.Client;
using CNative.Cloud.CPlatform.Runtime.Server;
using CNative.Cloud.CPlatform.Serialization;
using CNative.Cloud.CPlatform.Support;
using CNative.Cloud.Zookeeper.Configurations;
using CNative.Cloud.Zookeeper.Internal;
using CNative.Cloud.Zookeeper.Internal.Cluster.HealthChecks;
using CNative.Cloud.Zookeeper.Internal.Cluster.HealthChecks.Implementation;
using CNative.Cloud.Zookeeper.Internal.Cluster.Implementation.Selectors;
using CNative.Cloud.Zookeeper.Internal.Cluster.Implementation.Selectors.Implementation;
using CNative.Cloud.Zookeeper.Internal.Implementation;
using System;

namespace CNative.Cloud.Zookeeper
{
    public class ZookeeperModule : EnginePartModule
    {
        protected override void RegisterBuilder(ContainerBuilderWrapper builder)
        {
           
            var configInfo = new ConfigInfo(null);
            builder.RegisterInstance(GetConfigInfo(configInfo));
            builder.RegisterType<ZookeeperRandomAddressSelector>().As<IZookeeperAddressSelector>().SingleInstance();
            builder.RegisterType<DefaultHealthCheckService>().As<IHealthCheckService>().SingleInstance();
            builder.RegisterType<DefaultZookeeperClientProvider>().As<IZookeeperClientProvider>();
            builder.RegisterType<ZooKeeperServiceRouteManager>().As<IServiceRouteManager>();
            builder.RegisterType<ZooKeeperMqttServiceRouteManager>().As<IMqttServiceRouteManager>();
            builder.RegisterType<ZookeeperServiceCacheManager>().As<IServiceCacheManager>();
            builder.RegisterType<ZookeeperServiceCommandManager>().As<IServiceCommandManager>();
            builder.RegisterType<ZooKeeperServiceSubscribeManager>().As<IServiceSubscribeManager>();
        }
        
        private static ConfigInfo GetConfigInfo(ConfigInfo config)
        {
            ZookeeperOption option = null;
            var section = CPlatform.AppConfig.GetSection("Zookeeper");
            if (section.Exists())
                option = section.Get<ZookeeperOption>();
            else if (AppConfig.Configuration != null)
                option = AppConfig.Configuration.Get<ZookeeperOption>();
            if (option != null)
            {
                var sessionTimeout = config.SessionTimeout.TotalSeconds;
                var connectionTimeout = config.ConnectionTimeout.TotalSeconds;
                var operatingTimeout = config.OperatingTimeout.TotalSeconds;
                if (option.SessionTimeout > 0)
                {
                    sessionTimeout = option.SessionTimeout;
                }
                if (option.ConnectionTimeout > 0)
                {
                    connectionTimeout = option.ConnectionTimeout;
                }
                if (option.OperatingTimeout > 0)
                {
                    operatingTimeout = option.OperatingTimeout;
                }
                config = new ConfigInfo(
                    option.ConnectionString,
                    TimeSpan.FromSeconds(sessionTimeout),
                    TimeSpan.FromSeconds(connectionTimeout),
                    TimeSpan.FromSeconds(operatingTimeout),
                    option.RoutePath ?? config.RoutePath,
                    option.SubscriberPath ?? config.SubscriberPath,
                    option.CommandPath ?? config.CommandPath,
                    option.CachePath ?? config.CachePath,
                    option.MqttRoutePath ?? config.MqttRoutePath,
                    option.ChRoot ?? config.ChRoot,
                    option.ReloadOnChange != null ? bool.Parse(option.ReloadOnChange) :
                    config.ReloadOnChange,
                    option.EnableChildrenMonitor != null ? bool.Parse(option.EnableChildrenMonitor) :
                    config.EnableChildrenMonitor
                   );
            }
            return config;
        }
    }
}
