﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Protocol.WS.Runtime
{
    public interface IWSServiceEntryProvider
    {
        IEnumerable<WSServiceEntry> GetEntries();
    }
}
