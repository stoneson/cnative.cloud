﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Module
{
    public interface IModuleProvider
    {
        List<AbstractModule> Modules { get; }

        string[] VirtualPaths { get; }
        void Initialize();
    }
}
