﻿using CNative.Cloud.CPlatform.Address;
using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime;
using CNative.Cloud.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.CPlatform.Support.Attributes;
using System.Threading.Tasks;

namespace CNative.Cloud.CPlatform.Module
{
    [ServiceBundle("")]
    public interface IEchoService: IServiceKey
    {
        [Command(ShuntStrategy = AddressSelectorMode.HashAlgorithm)]
        [HttpGet]
        Task<IpAddressModel> Locate(string key,string routePath, HttpMethod httpMethod);
    }
}
