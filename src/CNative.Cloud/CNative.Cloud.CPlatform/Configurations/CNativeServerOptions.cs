﻿using CNative.Cloud.CPlatform.Support;
using CNative.Cloud.CPlatform.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;

namespace CNative.Cloud.CPlatform.Configurations
{
    public  partial class CNativeServerOptions: ServiceCommand
    {
        public string Ip { get; set; }

        public string MappingIP { get; set; }

        public int MappingPort { get; set; }

        public string WanIp { get; set; }

        public bool IsModulePerLifetimeScope { get; set; }

        public int WatchInterval { get; set; } = 20;
        
        public int HealthCheckWatchIntervalInSeconds { get; set; } = 15;

        public bool EnableHealthCheck { get; set; } = true;

        public int AllowServerUnhealthyTimes { get; set; } = 5;

        public bool Libuv { get; set; } = false;

        public int SoBacklog { get; set; } = 8192;
        public DockerDeployMode DockerDeployMode { get; set; } = DockerDeployMode.Standard;
        public IPEndPoint IpEndpoint { get; set; }

        public List<ModulePackage> Packages { get; set; } = new List<ModulePackage>();

        public CommunicationProtocol Protocol { get; set; }
        public string RootPath { get; set; }

        public string WebRootPath { get; set; } = Cloud.CPlatform.Utilities.EnvironmentHelper.BaseDirectory;

        public int Port { get; set; }

        public bool DisableServiceRegistration { get; set; }

        public bool DisableDiagnostic { get; set; }
        
        public ProtocolPortOptions Ports { get; set; } = new  ProtocolPortOptions();

        public string Token { get; set; }

        public string NotRelatedAssemblyFiles { get; set; }

        public string RelatedAssemblyFiles { get; set; } = "";

        public RuntimeEnvironment Environment { get; set; } = RuntimeEnvironment.Production;

        public bool ForceDisplayStackTrace { get; set; }

        public int RpcConnectTimeout { get; set; } = 500;

        private string _hostName;
        public string HostName {
            get 
            {
                if (_hostName.IsNullOrEmpty()) 
                {
                    var hostAssembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(p => p.GetName().Name.ToLower().Contains("host") || p.GetName().Name.ToLower().Contains("server"));
                    if (hostAssembly != null) 
                    {
                        return string.Join(".", hostAssembly.GetName().Name.Split(".").Take(ProjectSegment));
                    }
                    return "";
                  
                }
                return _hostName;

            }
            set { _hostName = value; }
        }
        
        public int ProjectSegment { get; set; } = 3;

        /// <summary>
        /// 业务模块路径 继承于 IServiceKey，IServiceBehavior
        /// </summary>
        public string ModulesRootPath { get; set; } = "Modules";
        /// <summary>
        /// 组件模块路径 继承于AbstractModule的组件
        /// </summary>
        public string ComponentsRootPath { get; set; } = "Components";
        /// <summary>
        /// 附加组件路径
        /// </summary>
        public string AddOnsRootPath { get; set; } = "AddOns";
        /// <summary>
        ///WCF服务模块接口，多个用‘;’号隔开
        /// </summary>
        public string WcfUsingServices { get; set; } = "";
    }
}
