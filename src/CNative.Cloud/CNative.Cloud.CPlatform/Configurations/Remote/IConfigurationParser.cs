﻿using System.Collections.Generic;
using System.IO;

namespace CNative.Cloud.CPlatform.Configurations.Remote
{
    public interface IConfigurationParser
    {
        IDictionary<string, string> Parse(Stream input, string initialContext);
    }
}
