﻿namespace CNative.Cloud.CPlatform.Configurations
{
    public class ModulePackage
    {
        /// <summary>
        /// EnginePartModule
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// ${UseEngineParts}|DotNettyModule;KestrelHttpModule;WSProtocolModule;MqttProtocolModule;MessagePackModule;ConsulModule;EventBusRabbitMQModule;CachingModule;ServiceProxyModule;ApiGeteWayModule;NLogModule;ExceptionlessModule;SkywalkingModule
        /// </summary>
        public string Using { get; set; }
    }
}
