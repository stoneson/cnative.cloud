﻿using CNative.Cloud.CPlatform.Configurations.Watch;

namespace CNative.Cloud.CPlatform.Configurations
{
    public  interface IConfigurationWatchManager
    {
        void Register(ConfigurationWatch watch);
    }
}
