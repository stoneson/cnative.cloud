﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Cache
{
   public  interface ICacheNodeProvider
    {
        IEnumerable<ServiceCache> GetServiceCaches();
    }
}
