﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Exceptions
{
    public class RegisterConnectionException : CPlatformException
    {
        public RegisterConnectionException(string message, Exception innerException = null) : base(message, innerException, StatusCode.RegisterConnection)
        {
        }
    }
}