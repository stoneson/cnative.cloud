﻿using System;

namespace CNative.Cloud.CPlatform.Exceptions
{
    public class UserFriendlyException : BusinessException
    {

        public UserFriendlyException(string message) : base(message, StatusCode.UserFriendly)
        {

        }
    }
}
