﻿using System;

namespace CNative.Cloud.CPlatform.Exceptions
{
    public class ValidateException : BusinessException
    {

        public ValidateException(string message) : base(message, StatusCode.ValidateError)
        {

        }
    }
}
