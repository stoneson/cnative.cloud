﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Exceptions
{
    public class DataAccessException : CPlatformException
    {
        public DataAccessException(string message, Exception innerException = null) : base(message, innerException,StatusCode.DataAccessError)
        {
        }
    }
}
