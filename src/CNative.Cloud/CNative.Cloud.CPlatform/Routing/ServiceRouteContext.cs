﻿using CNative.Cloud.CPlatform.Address;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Routing.Implementation;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Routing
{
    public class ServiceRouteContext 
    { 
        public ServiceRoute  Route { get; set; }

        public RemoteInvokeResultMessage ResultMessage { get; set; }

        public RemoteInvokeMessage InvokeMessage { get; set; }
    }
}
