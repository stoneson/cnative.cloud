﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CNative.Cloud.CPlatform.Address;
using CNative.Cloud.CPlatform.Module;
using CNative.Cloud.CPlatform.Routing;
using CNative.Cloud.CPlatform.Runtime.Server;
using CNative.Cloud.CPlatform.Support;
using CNative.Cloud.CPlatform.Utilities;

namespace CNative.Cloud.CPlatform
{
    public class ServerHostedService : IHostedService
    {
        private readonly IServiceTokenGenerator _serviceTokenGenerator;
        private readonly IServiceCommandManager _serviceCommandManager;
        private readonly IServiceRouteProvider _serviceRouteProvider;
        private readonly IModuleProvider _moduleProvider;
        public ServerHostedService(IServiceTokenGenerator serviceTokenGenerator,
            IServiceCommandManager serviceCommandManager,
            IServiceRouteProvider serviceRouteProvider, 
            IServiceProvider serviceProvider, 
            IModuleProvider moduleProvider)
        {
            _serviceTokenGenerator = serviceTokenGenerator;
            _serviceCommandManager = serviceCommandManager;
            _serviceRouteProvider = serviceRouteProvider;
            _moduleProvider = moduleProvider;

            if (ServiceLocator.Current == null)
                ServiceLocator.Current = serviceProvider.GetAutofacRoot();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
           
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", AppConfig.ServerOptions.Environment.ToString());
            Environment.SetEnvironmentVariable("DOTNET_ENVIRONMENT", AppConfig.ServerOptions.Environment.ToString());
       
            _serviceTokenGenerator.GeneratorToken(AppConfig.ServerOptions.Token);
            int _port = AppConfig.ServerOptions.Port = AppConfig.ServerOptions.Port == 0 ? 100 : AppConfig.ServerOptions.Port;
            _port = AppConfig.ServerOptions.Port = AppConfig.ServerOptions.IpEndpoint?.Port ?? _port;
            string _ip =  AppConfig.ServerOptions.Ip ?? "0.0.0.0";
            //_ip = AppConfig.ServerOptions.Ip = AppConfig.ServerOptions.IpEndpoint?.Address.ToString() ?? _ip;
            //_ip = NetUtils.GetHostAddress(_ip);
            _moduleProvider.Initialize();
            if (!AppConfig.ServerOptions.DisableServiceRegistration)
            {
                await _serviceCommandManager.SetServiceCommandsAsync();
                if (AppConfig.ServerOptions.Protocol == CommunicationProtocol.Tcp ||
                    AppConfig.ServerOptions.Protocol == CommunicationProtocol.None)
                {
                    await _serviceRouteProvider.RegisterRoutes(Math.Round(Convert.ToDouble(Process.GetCurrentProcess().TotalProcessorTime.TotalSeconds), 2, MidpointRounding.AwayFromZero));
                } 
            }

            var serviceHosts = ServiceLocator.Current.Resolve<IList<Runtime.Server.IServiceHost>>();
            await Task.Factory.StartNew(async () =>
            {
                foreach (var serviceHost in serviceHosts)
                    await serviceHost.StartAsync(_ip, _port);

                ServiceLocator.Current.Resolve<Engines.IServiceEngineLifetime>().NotifyStarted();
            }, cancellationToken);
          
        }
        public static async Task ConfigureRoute(ILifetimeScope mapper)
        {
            if (AppConfig.ServerOptions.Protocol == CommunicationProtocol.Tcp ||
             AppConfig.ServerOptions.Protocol == CommunicationProtocol.None)
            {
                var routeProvider = mapper.Resolve<IServiceRouteProvider>();
                //if (AppConfig.ServerOptions.EnableRouteWatch)
                //    new ServiceRouteWatch(mapper.Resolve<CPlatformContainer>(),
                //        async () => await routeProvider.RegisterRoutes(
                //        Math.Round(Convert.ToDecimal(Process.GetCurrentProcess().TotalProcessorTime.TotalSeconds), 2, MidpointRounding.AwayFromZero)));
                //else
                    await routeProvider.RegisterRoutes(0);
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            var serviceHosts = ServiceLocator.Current.Resolve<IList<Runtime.Server.IServiceHost>>();
            foreach (var serviceHost in serviceHosts)
            {
                serviceHost.Dispose();
            }
        }
    }
}