﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform
{
   public enum CommunicationProtocol
    {
        None,
        Tcp,
        Http,
        WS,
        Mqtt,
        Dns,
        Udp
    }
}
