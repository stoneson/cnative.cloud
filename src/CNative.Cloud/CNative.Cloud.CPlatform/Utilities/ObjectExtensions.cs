﻿using CNative.Cloud.CPlatform.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

namespace CNative.Cloud.CPlatform.Utilities
{
    public static class ObjectExtensions
    {
        public static T As<T>(this object obj)
            where T : class
        {
            return (T)obj;
        }

        public static T To<T>(this object obj)
            where T : struct
        {
            if (typeof(T) == typeof(Guid))
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(obj.ToString());
            }
            if (typeof(T).IsEnum)
            {
                return (T)Enum.Parse(typeof(T), obj as string);
            }

            return (T)Convert.ChangeType(obj, typeof(T), CultureInfo.InvariantCulture);
        }

        public static T ConventTo<T>(this object obj)
        {
            if (typeof(T).IsValueType)
            {
                if (typeof(T) == typeof(Guid))
                {
                    return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(obj.ToString());
                }
                if (typeof(T).IsEnum)
                {
                    return (T)Enum.Parse(typeof(T), obj as string);
                }

                return (T)Convert.ChangeType(obj, typeof(T), CultureInfo.InvariantCulture);
            }
            return (T)obj;
        }

        public static bool IsIn<T>(this T item, params T[] list)
        {
            return item.In_(list);
        }

        public static T DeepCopy<T>(this object obj)
        {
            var serializer = ServiceLocator.GetService<ISerializer<string>>();
            return (T)serializer.Deserialize(serializer.Serialize(obj), typeof(T));

        }

		// 检测验证操作

		#region Contains_
		/// <summary>验证集合中是否包含特定值</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="array">要验证的集合</param>
		/// <param name="value">用于验证的值</param>
		/// <returns>指示集合中是否包含特定值</returns>
		public static bool Contains_<T>(this IEnumerable<T> array, T value)
		{
			return value.In_(array);
		}

		/// <summary>验证数组中是否包含特定值</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="array">要验证的数组</param>
		/// <param name="value">用于验证的值</param>
		/// <returns>指示数组中是否包含特定值</returns>
		public static bool Contains_<T>(this T[] array, T value)
		{
			return value.In_(array);
		}

		/// <summary>验证数组中是否包含特定字符串</summary>
		/// <param name="strArray">要比较的字符串数组</param>
		/// <param name="value">要比较的字符串</param>
		/// <param name="ignoreCase">是否区分大小写</param>
		/// <param name="full">是否完整匹配</param>
		/// <param name="atArray">非完整匹配时，true 为验证数组中的字符串是否包含要匹配的字符串，false 为验证要匹配的字符串是否包含数组里的字符串</param>
		/// <returns>指示数组中是否包含特定字符串</returns>
		public static bool Contains_(this string[] strArray, string value, bool ignoreCase = true, bool full = true, bool atArray = false)
		{
			return value.In_(ignoreCase, full, atArray, strArray);
		}
		#endregion

		#region In_
		/// <summary>验证对象是否存在于集合中</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="obj">要验证的对象</param>
		/// <param name="array">用于验证的集合</param>
		/// <returns>指示对象是否存在于集合中</returns>
		public static bool In_<T>(this T obj, IEnumerable<T> array)
		{
			if (null == obj || IsNullOrEmpty_(array)) return false;
			return array.Contains(obj);
		}

		/// <summary>验证对象是否存在于集合中</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="obj">要验证的对象</param>
		/// <param name="array">用于验证的集合</param>
		/// <returns>指示对象是否存在于集合中</returns>
		public static bool In_<T>(this T obj, params T[] array)
		{
			return obj.In_(array as IEnumerable<T>);
		}

		/// <summary>验证字符串是否跟数组中的字符串匹配</summary>
		/// <param name="str">要比较的字符串</param>
		/// <param name="ignoreCase">是否区分大小写</param>
		/// <param name="full">是否完整匹配</param>
		/// <param name="atArray">非完整匹配时，true 为验证数组中的字符串是否包含要匹配的字符串，false 为验证要匹配的字符串是否包含数组里的字符串</param>
		/// <param name="strArray">要比较的字符串数组</param>
		/// <returns>指示字符串是否跟数组中的字符串匹配</returns>
		/// <remarks>
		/// 非完整匹配时，atArray 为 true 时验证数组中的字符串是否包含要匹配的字符串，为 false 时验证要匹配的字符串是否包含数组里的字符串
		/// </remarks>
		public static bool In_(this string str, bool ignoreCase = true, bool full = true, bool atArray = false, params string[] strArray)
		{
			if (string.IsNullOrEmpty(str) || IsNullOrEmpty_(strArray)) return false;

			StringComparison _sc = ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;
			string _tmp;
			return strArray.Any(s =>
			{
				if (s.IsNullOrEmpty_()) return false;

				_tmp = s.Trim();

				if (full)
				{
					if (str.Equals(_tmp, _sc)) return true;
				}
				else
				{
					if (atArray)
					{
						if (_tmp.IndexOf(str, _sc) > -1) return true;
					}
					else
					{
						if (str.IndexOf(_tmp, _sc) > -1) return true;
					}
				}
				return false;
			});
		}

		/// <summary>验证字符串是否跟数组中的字符串匹配</summary>
		/// <param name="str">要比较的字符串</param>
		/// <param name="list">要比较的字符串列表</param>
		/// <param name="split">列表分隔符</param>
		/// <param name="ignoreCase">是否区分大小写</param>
		/// <returns>指示字符串是否跟数组中的字符串匹配</returns>
		/// <remarks>默认的 list 以半角逗号（,）拆分；默认忽略大小写。</remarks>
		public static bool In_(this string str, string list, string split = ",", bool ignoreCase = true)
		{
			return str.In_(ignoreCase, true, false, list.Split_(split));
		}
		#endregion

		#region 拆分字符串
		/// <summary>拆分字符串</summary>
		/// <param name="str">要拆分的字符串</param>
		/// <param name="split">用于分隔的字符串</param>
		/// <param name="splitOption">拆分选项</param>
		/// <returns>拆分后的字符串数组</returns>
		public static string[] Split_(this string str, string split, StringSplitOptions splitOption = StringSplitOptions.None)
		{
			if (str.IsNull_()) return EmptyArray<string>();
			if (splitOption == StringSplitOptions.RemoveEmptyEntries && str.IsEmpty_()) return EmptyArray<string>();
			if (split.IsNullOrEmpty_() || !str.Contains(split)) return GetArray(str);
			if (split.Length == 1)
				return str.Split(GetArray(split[0]), splitOption);
			return str.Split(GetArray(split), splitOption);
		}

		/// <summary>以给出的正则表达式拆分字符串</summary>
		/// <param name="str">用于拆分的字符串</param>
		/// <param name="pattern">正则表达式。可以使用内联 (?imnsx-imnsx:) 分组构造或 (?imnsx-imnsx) 其他构造设置选项，一个选项或一组选项前面的减号 (-) 用于关闭这些选项。例如，内联构造 (?ix-ms)
		/// <para>* i 指定不区分大小写的匹配</para>
		/// <para>* m 指定多行模式。更改 ^ 和 $ 的含义，以使它们分别与任何行的开头和结尾匹配，而不只是与整个字符串的开头和结尾匹配</para>
		/// <para>* n 指定唯一有效的捕获是显式命名或编号的 (?&lt;name&gt;…) 形式的组。这允许圆括号充当非捕获组，从而避免了由 (?:…) 导致的语法上的笨拙</para>
		/// <para>* s 指定单行模式。更改句点字符 (.) 的含义，以使它与每个字符（而不是除 \n 之外的所有字符）匹配</para>
		/// <para>* x 指定从模式中排除非转义空白并启用数字符号 (#) 后面的注释</para></param>
		/// <param name="ignoreCase">是否忽略大小写</param>
		/// <returns>拆分后的字符串数组</returns>
		public static string[] Split_(this string str, string pattern, bool ignoreCase)
		{
			if (str.IsNullOrEmpty_()) return EmptyArray<string>();
			if (pattern.IsNullOrEmpty_()) return GetArray(str);
			return Regex.Split(str, pattern, ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None);
		}
		/// <summary>返回空数组</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <returns>一个长度为 0 的空数组</returns>
		public static T[] EmptyArray<T>()
		{
			return new T[0];
		}
		/// <summary>以传入的参数包装成数组</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="value">要包装的参数</param>
		/// <returns>以传入的参数组成的数组</returns>
		public static T[] GetArray<T>(params T[] value)
		{
			return value;
		}

		#endregion

		#region IsNullOrEmpty_
		/// <summary>验证字符是否属于空白字符</summary>
		/// <param name="c">要验证的字符</param>
		/// <returns>指示是否属于空白字符</returns>
		public static bool IsWhiteSpace_(this char c)
		{
			if (char.IsWhiteSpace(c)) return true;
			if (c == '﻿') return true;
			return false;
		}
		/// <summary>验证字符串是否为 null 或为空</summary>
		/// <param name="source">要验证的字符串</param>
		/// <returns>指示字符串是否为 null 或为空</returns>
		public static bool IsNullOrEmpty_(this string source)
		{
			return string.IsNullOrEmpty(source);
		}

		/// <summary>验证字符串是否为 null 或为空白字符串</summary>
		/// <param name="source">要验证的字符串</param>
		/// <returns>指示字符串是否为 null 或为空白字符串</returns>
		public static bool IsNullOrWhiteSpace_(this string source)
		{
			if (null == source) return true;
			for (int i = 0, len = source.Length; i < len; i++)
				if (!source[i].IsWhiteSpace_()) return false;
			return true;
		}

		/// <summary>验证对象是否为 null</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="source">要验证的对象</param>
		/// <returns>指示对象是否为 null</returns>
		public static bool IsNull_<T>(this T source)
		{
			return null == source;
		}

		/// <summary>验证字符串是否为空</summary>
		/// <param name="source">要验证的字符串</param>
		/// <returns>指示字符串是否为空</returns>
		public static bool IsEmpty_(this string source)
		{
			return source.Length == 0;
		}

		/// <summary>验证 GUID 是否为空</summary>
		/// <param name="guid">要验证的 GUID</param>
		/// <returns>指示 GUID 是否为空</returns>
		public static bool IsEmpty_(this Guid guid)
		{
			return guid == Guid.Empty;
		}

		/// <summary>验证 GUID 是否为 null 或为空</summary>
		/// <param name="guid">要验证的 GUID</param>
		/// <returns>指示 GUID 是否为 null 或为空</returns>
		public static bool IsNullOrEmpty_(this Guid guid)
		{
			return guid == null || guid == Guid.Empty;
		}

		/// <summary>验证集合是否为 null 或为空</summary>
		/// <param name="source">要验证的集合</param>
		/// <returns>指示集合是否为 null 或为空</returns>
		public static bool IsNullOrEmpty_(this IEnumerable source)
		{
			if (null == source) return true;
			var _source = source as ICollection;
			if (null != _source) return _source.Count == 0;
			foreach (var s in source) return false;
			return false;
		}

		/// <summary>验证集合是否为 null 或为空</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="source">要验证的集合</param>
		/// <returns>指示集合是否为 null 或为空</returns>
		public static bool IsNullOrEmpty_<T>(this IEnumerable<T> source)
		{
			if (null == source) return true;

			var _source = source as ICollection<T>;
			if (null != _source) return _source.Count == 0;
			var _source2 = source as ICollection;
			if (null != _source2) return _source2.Count == 0;
			foreach (T _item in source) return false;
			return true;
		}

		/// <summary>验证数组是否为 null 或为空</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="source">要验证的数组</param>
		/// <returns>指示数组是否为 null 或为空</returns>
		public static bool IsNullOrEmpty_<T>(this T[] source)
		{
			return null == source || source.Length == 0;
		}

		/// <summary>验证对象是否为数据库字段 null 值</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="value">要验证的对象</param>
		/// <returns>指示对象是否为数据库字段 null 值</returns>
		public static bool IsDBNull_<T>(this T value)
		{
			return Convert.IsDBNull(value);
		}

		/// <summary>验证对象是否为 null 或数据库字段 null 值</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="value">要验证的对象</param>
		/// <returns>指示对象是否为 null 或数据库字段 null 值</returns>
		public static bool IsNullOrDBNull_<T>(this T value)
		{
			return null == value || Convert.IsDBNull(value);
		}
		#endregion

		#region 检测元素项是否是集合中的第一或最后项
		/// <summary>检测元素项是否是集合中的第一项</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="source">集合</param>
		/// <param name="item">要检测的元素项</param>
		/// <param name="comparer">相等比较器</param>
		/// <returns>指示是否为集合中的第一项</returns>
		public static bool IsFirstItem_<T>(this IEnumerable<T> source, T item, IEqualityComparer<T> comparer = null)
		{
			if (null == source) return false;
			return comparer.Equals(source.FirstOrDefault(), item);
		}

		/// <summary>检测元素项是否是集合中的最后项</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="source">集合</param>
		/// <param name="item">要检测的元素项</param>
		/// <param name="comparer">相等比较器</param>
		/// <returns>指示是否为集合中的最后项</returns>
		public static bool IsLastItem_<T>(this IEnumerable<T> source, T item, IEqualityComparer<T> comparer = null)
		{
			if (null == source) return false;
			comparer = comparer ?? EqualityComparer<T>.Default;
			return comparer.Equals(source.LastOrDefault(), item);
		}
		#endregion

		#region 验证是否为结构数据
		/// <summary>验证是否为结构数据</summary>
		/// <param name="type">要验证的类型</param>
		/// <returns>指示是否为结构数据</returns>
		public static bool IsStruct_(this Type type)
		{
			return ((type.IsValueType && !type.IsEnum) && (!type.IsPrimitive && !type.IsSerializable));
		}

		/// <summary>验证是否为结构数据</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="value">要验证的对象</param>
		/// <returns>指示是否为结构数据</returns>
		public static bool IsStruct_<T>(this T value)
		{
			return typeof(T).IsStruct_();
		}
		#endregion

		#region 验证是否为数字
		/// <summary>
		/// 验证是否为数字（sbyte、byte、short、ushort、int、uint、long、ulong、float、double、decimal）
		/// </summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="n">要验证的对象</param>
		/// <returns>指示是否为数字</returns>
		public static bool IsNumber_<T>(this T n)
		{
			var tc = Type.GetTypeCode(typeof(T));
			return tc >= TypeCode.SByte && tc <= TypeCode.Decimal;
		}

		/// <summary>验证是否为整数（sbyte、byte、short、ushort、int、uint、long、ulong）</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="n">要验证的对象</param>
		/// <returns>指示是否为整数</returns>
		public static bool IsInt_<T>(this T n)
		{
			var tc = Type.GetTypeCode(typeof(T));
			return tc >= TypeCode.SByte && tc <= TypeCode.UInt64;
		}

		/// <summary>验证是否为浮点数（float、double、decimal）</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="n">要验证的对象</param>
		/// <returns>指示是否为浮点数</returns>
		public static bool IsFloat_<T>(this T n)
		{
			var tc = Type.GetTypeCode(typeof(T));
			return tc >= TypeCode.Single && tc <= TypeCode.Decimal;
		}
		#endregion

		#region 验证是否为虚成员
		/// <summary>验证是否为虚属性</summary>
		/// <param name="property">属性信息</param>
		/// <returns>指示是否为虚属性</returns>
		public static bool IsVirtual_(this PropertyInfo property)
		{
			var get = property.GetGetMethod();
			if (get != null && get.IsVirtual) return true;
			var set = property.GetSetMethod();
			if (set != null && set.IsVirtual) return true;
			return false;
		}
		#endregion

		#region 验证是否为可 null 类型
		/// <summary>验证是否为可 null 类型</summary>
		/// <param name="type">要验证的类型</param>
		/// <returns>指示是否为可 null 类型</returns>
		public static bool IsNullable_(this Type type)
		{
			return (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
		}

		/// <summary>验证是否为可 null 类型</summary>
		/// <typeparam name="T">数据类型</typeparam>
		/// <param name="value">要验证的对象</param>
		/// <returns>指示是否为可 null 类型</returns>
		public static bool IsNullable_<T>(this T value)
		{
			return value.GetType().IsNullable_();
		}
		#endregion
	}
}