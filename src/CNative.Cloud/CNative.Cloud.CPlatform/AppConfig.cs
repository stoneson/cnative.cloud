﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CNative.Cloud.CPlatform.Configurations;
using CNative.Cloud.CPlatform.DependencyResolution;
using CNative.Cloud.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using CNative.Cloud.CPlatform.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CNative.Cloud.CPlatform
{
    public class AppConfig
    {
        #region 字段
        private static AddressSelectorMode _loadBalanceMode = AddressSelectorMode.Polling;
        private static CNativeServerOptions _serverOptions = new CNativeServerOptions();
        private static IEnumerable<MapRoutePathOption> _mapRoutePathOptions = new List<MapRoutePathOption>();
        #endregion

        public static IConfigurationRoot Configuration { get; internal set; }

        /// <summary>
        /// 负载均衡模式
        /// </summary>
        public static AddressSelectorMode LoadBalanceMode
        {
            get
            {
                AddressSelectorMode mode = _loadBalanceMode; ;
                if (Configuration != null
                    && Configuration["AccessTokenExpireTimeSpan"] != null
                    && !Enum.TryParse(Configuration["AccessTokenExpireTimeSpan"], out mode))
                {
                    mode = _loadBalanceMode;
                }
                return mode;
            }
            internal set
            {
                _loadBalanceMode = value;
            }
        }

        public static IConfigurationSection GetSection(string name)
        {
            return Configuration?.GetSection(name);
        }


        public static CNativeServerOptions ServerOptions
        {
            get
            {
                return _serverOptions;
            }
            internal set
            {
                _serverOptions = value;
            }
        }


        public static IEnumerable<MapRoutePathOption> MapRoutePathOptions
        {
            get
            {
                return _mapRoutePathOptions;
            }
            internal set 
            {
                _mapRoutePathOptions = value;
            }
        
        }

        public static string PayloadKey { get; } = "payload";
    }
}
