﻿namespace CNative.Cloud.CPlatform.Runtime
{
    public enum HttpMethod
    {
        GET,

        POST,

        PUT,

        DELETE,

        PATCH,

        OPTION,

        HEAD
    }
}
