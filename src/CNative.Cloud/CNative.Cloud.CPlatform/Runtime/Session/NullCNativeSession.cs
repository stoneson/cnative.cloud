﻿namespace CNative.Cloud.CPlatform.Runtime.Session
{
    public class NullCNativeSession : CNativeSessionBase
    {
        private NullCNativeSession()
        {
        }

        public static ICNativeSession Instance { get; } = new RpcContextSession();

        public override long? UserId { get; } = Instance.UserId;
        public override string UserName { get; } = Instance.UserName;

        public override long? OrgId { get; } = Instance.OrgId;
        public override long[] DataPermissionOrgIds { get; } = Instance.DataPermissionOrgIds;

        public override bool IsAllOrg { get; } = Instance.IsAllOrg;

        public override long? TenantId { get; } = Instance.TenantId;

    }
}
