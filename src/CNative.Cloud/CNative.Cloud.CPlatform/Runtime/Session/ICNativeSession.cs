﻿
namespace CNative.Cloud.CPlatform.Runtime.Session
{
    public interface ICNativeSession
    {
        long? UserId { get; }

        long? OrgId { get; }
        
        bool IsAllOrg { get; }

        long[] DataPermissionOrgIds { get;  }

        string UserName { get; }
        
        long?  TenantId { get; }

    }
}
