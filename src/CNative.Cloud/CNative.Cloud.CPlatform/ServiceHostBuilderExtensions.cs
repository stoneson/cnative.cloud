﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using CNative.Cloud.CPlatform.Configurations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using CNative.Cloud.CPlatform.Engines;
using CNative.Cloud.CPlatform.Exceptions;
using CNative.Cloud.CPlatform.Utilities;

namespace CNative.Cloud.CPlatform
{
    public static class ServiceHostBuilderExtensions
    {
        public static IHostBuilder RegisterMicroServices(this IHostBuilder hostBuilder)
        {
           
            return hostBuilder
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureContainer<ContainerBuilder>(containerBuilder =>
                {
                    containerBuilder.GetServiceBuilder()
                                 .AddServiceRuntime()
                                 .AddRelateServiceRuntime()                                
                                ;
                })
                .ConfigureLogging(logging =>
                {
                    try
                    {
                        if (AppConfig.Configuration.GetSection("Logging")?.Exists() == true)
                        {
                            logging.AddConfiguration(AppConfig.Configuration.GetSection("Logging"));
                        }
                    }
                    catch (Exception ex) { ConsoleC.WriteLine(ex, " RegisterMicroServices.ConfigureLogging "); }
                })
                ;
        }
        //public static void BuildServiceEngine(this IContainer container)
        //{
        //    if (container.IsRegistered<IServiceEngine>())
        //    {
        //        var builder = new ContainerBuilder();

        //        container.Resolve<IServiceEngineBuilder>().Build(builder);
        //        //var configBuilder = container.Resolve<IConfigurationBuilder>();
        //        //var appSettingPath = Path.Combine(AppConfig.ServerOptions.RootPath, "appsettings.json");
        //        //configBuilder.AddCPlatformFile("${appsettingspath}|" + appSettingPath, optional: false, reloadOnChange: true);
        //        //builder.Update(container);
        //    }
        //}
        public static IHostBuilder UseEngine<T>(this IHostBuilder hostBuilder) where T: IServiceEngine
        {
            return hostBuilder.UseEngine(typeof(T));
        }
        
        
        public static IHostBuilder UseEngine(this IHostBuilder hostBuilder, Type type)
        {
            if (!typeof(IServiceEngine).IsAssignableFrom(type))
            {
                throw new CPlatformException($"设置的服务引擎类型必须继承IServiceEngine接口");
            }

            return hostBuilder.ConfigureContainer<ContainerBuilder>(containerBuilder =>
            {
                containerBuilder.GetServiceBuilder().AddServiceEngine(type);
            });
        }
        

        public static IHostBuilder UseServer(this IHostBuilder hostBuilder)
        {
            return hostBuilder.ConfigureServices((hostContext,services) =>
            {
                services.AddHostedService<ServerHostedService>();
                
            });
        }

        public static IHostBuilder UseServer(this IHostBuilder hostBuilder, Action<CNativeServerOptions> options)
        {
            var serverOptions = new CNativeServerOptions();
            options.Invoke(serverOptions);
            AppConfig.ServerOptions = serverOptions;
            return hostBuilder.UseServer();
        }

        public static IHostBuilder UseClient(this IHostBuilder hostBuilder)
        {
            return hostBuilder.ConfigureServices((hostContext,services) =>
            {
                services.AddHostedService<ClientHostedService>();
            });
        }

    }
}
