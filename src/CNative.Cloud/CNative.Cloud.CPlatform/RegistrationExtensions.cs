﻿using CNative.Cloud.CPlatform.Filters;
using CNative.Cloud.CPlatform.Module;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform
{
    public static class RegistrationExtensions
    { 
        public static void AddFilter(this ContainerBuilderWrapper builder, Type filter)
        {
           
            if (typeof(IExceptionFilter).IsAssignableFrom(filter))
            {
                builder.RegisterType(filter).As<IExceptionFilter>().SingleInstance();
            }
            else if (typeof(IAuthorizationFilter).IsAssignableFrom(filter))
            {
                builder.RegisterType(filter).As<IAuthorizationFilter>().SingleInstance();
            }
        }

       
    }
}
