﻿using CNative.Cloud.CPlatform.Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace CNative.Cloud.CPlatform.Diagnostics
{
    public static class DiagnosticListenerExtensions
    {
        public const string DiagnosticListenerName = "CNativeDiagnosticListener";
        public const string Prefix = "CNative.Cloud.";
        public const string CNativeBeforeTransport = Prefix +".{0}."+ nameof(WriteTransportBefore);
        public const string CNativeAfterTransport= Prefix + ".{0}." + nameof(WriteTransportAfter);
        public const string CNativeErrorTransport = Prefix + ".{0}." + nameof(WriteTransportError);

        public static void WriteTransportBefore(this DiagnosticListener diagnosticListener,TransportType transportType, TransportEventData eventData)
        {
            var ldname = string.Format(CNativeBeforeTransport, transportType);
            if (diagnosticListener.IsEnabled(ldname))
            {
                eventData.Headers = new TracingHeaders();
                diagnosticListener.Write(ldname, eventData);
            }
        }

        public static void WriteTransportAfter(this DiagnosticListener diagnosticListener, TransportType transportType, ReceiveEventData eventData)
        {
            var ldname = string.Format(CNativeAfterTransport, transportType);
            if (diagnosticListener.IsEnabled(ldname))
            {
                eventData.Headers = new TracingHeaders();
                diagnosticListener.Write(ldname, eventData);
            }
        }

        public static void WriteTransportError(this DiagnosticListener diagnosticListener, TransportType transportType, TransportErrorEventData eventData)
        {
            var ldname = string.Format(CNativeErrorTransport, transportType);
            if (diagnosticListener.IsEnabled(ldname))
            {
                eventData.Headers = new TracingHeaders();
                diagnosticListener.Write(ldname, eventData);
            }
        }

    }
}
