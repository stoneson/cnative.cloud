﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Diagnostics
{
   public  class DiagnosticParameters
    {
        public const string PREFIX = "CNative.Cloud.";

        public const string DiagnosticListenerName = "CNativeDiagnosticListener";
    }
}
