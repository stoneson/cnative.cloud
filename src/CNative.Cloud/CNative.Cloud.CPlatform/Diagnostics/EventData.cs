﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Diagnostics
{
    public class EventData
    {
        public EventData(Guid operationId)
        {
            OperationId = operationId; 
        }

        public Guid OperationId { get; set; }

    }
}
