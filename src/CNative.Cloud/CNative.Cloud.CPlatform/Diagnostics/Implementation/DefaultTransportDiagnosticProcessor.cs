﻿using CNative.Cloud.CPlatform.Diagnostics;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Serialization;
using CNative.Cloud.CPlatform.Utilities;
using System;
using System.Collections.Concurrent;
using System.Text;
using CNativeEvents = CNative.Cloud.CPlatform.Diagnostics.DiagnosticListenerExtensions;

namespace CNative.Cloud.CPlatform.Diagnostics.Implementation
{
   public class DefaultTransportDiagnosticProcessor : ITracingDiagnosticProcessor
    {
        protected Func<TransportEventData, string> _transportOperationNameResolver;
        public virtual string ListenerName => CNativeEvents.DiagnosticListenerName;


        protected readonly ConcurrentDictionary<string, SegmentContext> _resultDictionary =
            new ConcurrentDictionary<string, SegmentContext>();

        protected readonly ISerializer<string> _serializer;
        protected readonly ITracingContext _tracingContext;

        protected TransportType transportType = TransportType.Rest;

        public virtual Func<TransportEventData, string> TransportOperationNameResolver
        {
            get
            {
                return _transportOperationNameResolver ??
                       (_transportOperationNameResolver = (data) => transportType.ToString()+"-Transport:: " + data.Message.MessageName);
            }
            set => _transportOperationNameResolver =
                value ?? throw new ArgumentNullException(nameof(TransportOperationNameResolver));
        }

        public DefaultTransportDiagnosticProcessor(ITracingContext tracingContext,ISerializer<string> serializer)
        {
            _tracingContext = tracingContext;
            _serializer = serializer;
        }

        [DiagnosticName(CNativeEvents.CNativeBeforeTransport, TransportType.Rest)]
        public virtual void TransportBefore([Object] TransportEventData eventData)
        {
            var message = eventData.Message.GetContent<HttpMessage>();
            var operationName = TransportOperationNameResolver(eventData);
            var context = _tracingContext.CreateEntrySegmentContext(operationName,
                new DefaultTransportCarrierHeaderCollection(eventData.Headers));
            context.TraceId = ConvertUniqueId(eventData).ToString();
            context.Span.AddLog(LogEvent.Message($"Worker running at: {DateTime.Now}"));
            context.Span.SpanLayer = SpanLayer.HTTP;
            context.Span.Peer = new StringOrIntValue(eventData.RemoteAddress);
            context.Span.AddTag(Tags.REST_METHOD, eventData.Method.ToString());
            context.Span.AddTag(Tags.REST_PARAMETERS, _serializer.Serialize(message.Parameters));
            context.Span.AddTag(Tags.REST_LOCAL_ADDRESS, NetUtils.GetHostAddress().ToString());
            _resultDictionary.TryAdd(eventData.OperationId.ToString(), context);
        }

        [DiagnosticName(CNativeEvents.CNativeAfterTransport, TransportType.Rest)]
        public virtual void TransportAfter([Object] ReceiveEventData eventData)
        {
            _resultDictionary.TryRemove(eventData.OperationId.ToString(), out SegmentContext context);
            if (context != null)
            {
                if (eventData != null && eventData.Message != null && eventData.Message.Content != null)
                    context.Span.AddLog(LogEvent.Reponse(_serializer.Serialize(eventData.Message.Content)));
                _tracingContext.Release(context);
            }
        }

        [DiagnosticName(CNativeEvents.CNativeErrorTransport, TransportType.Rest)]
        public virtual void TransportError([Object] TransportErrorEventData eventData)
        {
            _resultDictionary.TryRemove(eventData.OperationId.ToString(), out SegmentContext context);
            if (context != null)
            {
                context.Span.ErrorOccurred(eventData.Exception);
                _tracingContext.Release(context);
            }
        }

        public virtual UniqueId ConvertUniqueId(TransportEventData eventData)
        {
            long part1 = 0, part2 = 0, part3 = 0;
            UniqueId uniqueId = new UniqueId();
            var bytes = Encoding.Default.GetBytes(eventData.TraceId);
            part1 = BitConverter.ToInt64(bytes, 0);
            if (eventData.TraceId.Length > 8)
                part2 = BitConverter.ToInt64(bytes, 8);
            if (eventData.TraceId.Length > 16)
                part3 = BitConverter.ToInt64(bytes, 16);
            if (!string.IsNullOrEmpty(eventData.TraceId))
                uniqueId = new UniqueId(part1, part2, part3);
            return uniqueId;
        }
    }
}