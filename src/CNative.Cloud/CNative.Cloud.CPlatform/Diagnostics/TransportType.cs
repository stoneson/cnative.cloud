﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Diagnostics
{
    public  enum TransportType
    {
        Rpc,
        Rest,
        Mqtt,
    }
}
