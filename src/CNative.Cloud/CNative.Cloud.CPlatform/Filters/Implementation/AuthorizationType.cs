﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Filters.Implementation
{
    public enum AuthorizationType
    {
        JWT,
        AppSecret
    }
}
