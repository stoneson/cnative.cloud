﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.CPlatform.Filters
{
   public interface IFilter
    {

        bool AllowMultiple { get; }  
    }
}
