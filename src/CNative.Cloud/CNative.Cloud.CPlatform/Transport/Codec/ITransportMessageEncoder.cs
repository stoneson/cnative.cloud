﻿using CNative.Cloud.CPlatform.Messages;

namespace CNative.Cloud.CPlatform.Transport.Codec
{
    public interface ITransportMessageEncoder
    {
        byte[] Encode(TransportMessage message);
    }
}