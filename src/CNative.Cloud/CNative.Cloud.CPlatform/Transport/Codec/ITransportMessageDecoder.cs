﻿using CNative.Cloud.CPlatform.Messages;

namespace CNative.Cloud.CPlatform.Transport.Codec
{
    public interface ITransportMessageDecoder
    {
        TransportMessage Decode(byte[] data);
    }
}