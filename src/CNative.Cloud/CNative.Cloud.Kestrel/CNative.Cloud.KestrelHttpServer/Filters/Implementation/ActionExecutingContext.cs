﻿using Microsoft.AspNetCore.Http;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.KestrelHttpServer.Filters.Implementation
{
   public  class ActionExecutingContext
    {
        public HttpMessage Message { get; internal set; }

        public ServiceRoute Route { get; internal set; }

        public HttpResultMessage<object> Result { get; set; }
          
        public HttpContext Context { get; internal set; }
    }
}
