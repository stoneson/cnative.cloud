﻿using CNative.Cloud.KestrelHttpServer.Filters.Implementation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.KestrelHttpServer.Filters
{
   public interface IExceptionFilter
    { 
          Task OnException(ExceptionContext context);
    }
}
