﻿using CNative.Cloud.CPlatform.Diagnostics;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Serialization;
using CNative.Cloud.CPlatform.Utilities;
using System;
using System.Collections.Concurrent;
using System.Text;
using CNativeEvents = CNative.Cloud.CPlatform.Diagnostics.DiagnosticListenerExtensions;

namespace CNative.Cloud.KestrelHttpServer.Diagnostics
{
    public class RestTransportDiagnosticProcessor : CPlatform.Diagnostics.Implementation.DefaultTransportDiagnosticProcessor
    {

        public RestTransportDiagnosticProcessor(ITracingContext tracingContext, ISerializer<string> serializer) 
            : base(tracingContext, serializer)
        {
            transportType = TransportType.Rest;
        }

    }
}