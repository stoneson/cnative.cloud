﻿using Microsoft.AspNetCore.Http;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Runtime.Server;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.KestrelHttpServer
{
    public  class ActionContext
    {
        public ActionContext()
        {

        }

        public HttpContext HttpContext { get; set; }

        public TransportMessage Message { get; set; }
         
    }
}
