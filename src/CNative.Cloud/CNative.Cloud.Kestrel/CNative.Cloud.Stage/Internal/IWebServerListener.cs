﻿using CNative.Cloud.KestrelHttpServer;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Stage.Internal
{
    public interface IWebServerListener
    {
        void Listen(WebHostContext context);
    }
}
