﻿namespace CNative.Cloud.SwaggerGen
{
    public interface ISchemaRegistryFactory
    {
        ISchemaRegistry Create();
    }
}
