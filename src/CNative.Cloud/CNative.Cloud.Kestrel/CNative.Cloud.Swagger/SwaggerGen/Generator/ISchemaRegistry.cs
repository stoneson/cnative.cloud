﻿using CNative.Cloud.Swagger;
using System;
using System.Collections.Generic;
namespace CNative.Cloud.SwaggerGen
{
    public interface ISchemaRegistry
    {
        Schema GetOrRegister(Type type);

        Schema GetOrRegister(string parmName, Type type);

        IDictionary<string, Schema> Definitions { get; }
    }
}
