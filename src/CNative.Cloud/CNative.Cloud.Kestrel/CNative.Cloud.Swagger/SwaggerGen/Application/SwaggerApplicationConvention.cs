﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace CNative.Cloud.SwaggerGen
{
    public class SwaggerApplicationConvention : IApplicationModelConvention
    {
        public void Apply(ApplicationModel application)
        {
            application.ApiExplorer.IsVisible = true;
        }
    }
}