﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Module;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.KestrelHttpServer;
using CNative.Cloud.Nlog;



namespace CNative.Cloud.Kestrel.Nlog
{
   public class KestrelNLogModule : KestrelHttpModule
    {
        private string nlogConfigFile = "${LogPath}|NLog.config";
        public override void Initialize(AppModuleContext context)
        {
  
        }

        public override void RegisterBuilder(WebHostContext context)
        { 
        }

        public override void Initialize(ApplicationInitializationContext context)
        {
            var serviceProvider = context.Builder.ApplicationServices;
            base.Initialize(context);
            var section = AppConfig.GetSection("Logging");
            nlogConfigFile = EnvironmentHelper.GetEnvironmentVariable(nlogConfigFile);

            NLog.LogManager.LoadConfiguration(nlogConfigFile);
            serviceProvider.GetService<ILoggerFactory>().AddProvider(new NLogProvider());
        }

        public override void RegisterBuilder(ConfigurationContext context)
        {
             context.Services.AddLogging();
        }

        protected override void RegisterBuilder(ContainerBuilderWrapper builder)
        {
           
        }
    }
}
