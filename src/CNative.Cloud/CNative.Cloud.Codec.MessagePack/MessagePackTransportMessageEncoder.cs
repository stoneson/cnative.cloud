﻿using CNative.Cloud.Codec.MessagePack.Messages;
using CNative.Cloud.Codec.MessagePack.Utilities;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Transport.Codec;
using System.Runtime.CompilerServices;

namespace CNative.Cloud.Codec.MessagePack
{
   public sealed class MessagePackTransportMessageEncoder:ITransportMessageEncoder
    {
        #region Implementation of ITransportMessageEncoder

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] Encode(TransportMessage message)
        {
            var transportMessage = new MessagePackTransportMessage(message)
            {
                Id = message.Id,
                ContentType = message.ContentType,
            };
            return SerializerUtilitys.Serialize(transportMessage);
        }
        #endregion Implementation of ITransportMessageEncoder
    }
}
