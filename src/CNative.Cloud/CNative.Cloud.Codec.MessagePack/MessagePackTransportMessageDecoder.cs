﻿using CNative.Cloud.Codec.MessagePack.Messages;
using CNative.Cloud.Codec.MessagePack.Utilities;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Transport.Codec;
using System.Runtime.CompilerServices;

namespace CNative.Cloud.Codec.MessagePack
{
    public sealed class MessagePackTransportMessageDecoder : ITransportMessageDecoder
    {
        #region Implementation of ITransportMessageDecoder

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public TransportMessage Decode(byte[] data)
        {
            var message = SerializerUtilitys.Deserialize<MessagePackTransportMessage>(data);
            return message.GetTransportMessage();
        }

        #endregion Implementation of ITransportMessageDecoder
    }
}
