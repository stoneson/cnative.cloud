﻿using System.Collections.Generic;

namespace CNative.Cloud.Quartz.Schedule.Runtime
{
    public interface IJobEntityProvider
    {
        IEnumerable<JobEntity> GetJobEntities();
    }
}
