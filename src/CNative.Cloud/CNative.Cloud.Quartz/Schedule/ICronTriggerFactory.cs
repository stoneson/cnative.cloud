﻿using Quartz;
using CNative.Cloud.CPlatform.Ioc;

namespace CNative.Cloud.Quartz.Schedule
{
    public interface ICronTriggerFactory : ITransientDependency
    {
        ITrigger CreateTrigger(string cronExpression);
    }
}
