﻿using Quartz;
using CNative.Cloud.Quartz.Configurations;

namespace CNative.Cloud.Quartz.Schedule
{
    public interface ISurgingTriggerFactory
    {
        ITrigger CreateTrigger(JobOption jobOption);
    }
}
