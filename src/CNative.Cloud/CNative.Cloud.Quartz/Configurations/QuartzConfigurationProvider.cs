﻿using Microsoft.Extensions.Configuration;
using CNative.Cloud.CPlatform.Configurations.Remote;
using System.IO;

namespace CNative.Cloud.Quartz.Configurations
{
    public class QuartzConfigurationProvider : FileConfigurationProvider
    {
        public QuartzConfigurationProvider(QuartzConfigurationSource source) : base(source)
        {
        }

        public override void Load(Stream stream)
        {
            var parser = new JsonConfigurationParser();
            this.Data = parser.Parse(stream, null);
        }
    }
}
