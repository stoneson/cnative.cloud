﻿namespace CNative.Cloud.Quartz.Configurations
{
    public enum QuartzClusterDbType
    {
        SqlServer = 1,

        MySql = 2,

        Oracle = 3
    }
}
