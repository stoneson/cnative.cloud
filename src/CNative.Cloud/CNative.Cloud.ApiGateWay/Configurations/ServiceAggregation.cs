﻿using CNative.Cloud.CPlatform.Runtime;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.ApiGateWay.Configurations
{
   public class ServiceAggregation
    {
        public string RoutePath { get; set; }

        public HttpMethod HttpMethod { get; set; }

        public string ServiceKey { get; set; }

        public Dictionary<string, object> Params { get; set; }

        public string Key { get; set; }
    }
}
