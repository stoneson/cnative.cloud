﻿using CNative.Cloud.ApiGateWay.ServiceDiscovery.Implementation;
using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Address;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.ApiGateWay.ServiceDiscovery
{
   public interface IServiceDiscoveryProvider
    {
        Task<IEnumerable<ServiceAddressModel>> GetAddressAsync(string condition = null);

        Task<IEnumerable<ServiceDescriptor>> GetServiceDescriptorAsync(string address, string condition = null);
        
        Task EditServiceToken(AddressModel address);
    }
}
