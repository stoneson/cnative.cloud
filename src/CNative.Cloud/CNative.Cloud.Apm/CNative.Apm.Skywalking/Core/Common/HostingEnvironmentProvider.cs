﻿using CNative.Apm.Skywalking.Abstractions;
using SkyApm;

namespace SkyApm.Common
{
    internal class HostingEnvironmentProvider : IEnvironmentProvider
    {
        public string EnvironmentName { get; }

        public HostingEnvironmentProvider()
        {
            EnvironmentName ="";
        }
    }
}
