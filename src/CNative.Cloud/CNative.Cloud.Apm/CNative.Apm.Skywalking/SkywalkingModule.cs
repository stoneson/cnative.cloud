﻿
using CNative.Apm.Skywalking.Abstractions.Tracing;
using SkyApm.Transport;
using SkyApm.Sampling;
using SkyApm.Transport.Grpc;
using CNative.Cloud.CPlatform.Module;
using Autofac;
using CNative.Apm.Skywalking.Abstractions.Common.Tracing;
using SkyApm.Tracing;
using SkyApm;
using SkyApm.Service;
using SkyApm.Diagnostics;
using SkyApm.Config;
using CNative.Apm.Skywalking.Configuration;
using SkyApm.Common;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using CNative.Cloud.CPlatform.Diagnostics;

namespace CNative.Apm.Skywalking
{
    public class SkywalkingModule : EnginePartModule
    {
        public override void Initialize(AppModuleContext context)
        {
            base.Initialize(context);
            var isp = context.ServiceProvoider.Resolve<IInstrumentStartup>();
            isp.StartAsync();
        }

        /// <summary>
        /// Inject dependent third-party components
        /// </summary>
        /// <param name="builder"></param>
        protected override void RegisterBuilder(ContainerBuilderWrapper builder)
        {
            base.RegisterBuilder(builder);
            builder.RegisterType<AsyncQueueSegmentDispatcher>().As<ISegmentDispatcher>().SingleInstance();
            builder.RegisterType<RegisterService>().As<IExecutionService>().SingleInstance();
            builder.RegisterType<PingService>().As<IExecutionService>().SingleInstance();
            builder.RegisterType<CLRStatsService>().As<IExecutionService>().SingleInstance();
            builder.RegisterType<SegmentReportService>().As<IExecutionService>().SingleInstance();
            builder.RegisterType<InstrumentStartup>().As<IInstrumentStartup>().SingleInstance();
            builder.Register(p => RuntimeEnvironment.Instance).SingleInstance();
            builder.RegisterType<TracingDiagnosticProcessorObserver>().SingleInstance();
            builder.RegisterType<ConfigAccessor>().As<IConfigAccessor>().SingleInstance();
            builder.RegisterType<ConfigurationFactory>().As<IConfigurationFactory>().SingleInstance();
            builder.RegisterType<HostingEnvironmentProvider>().As<IEnvironmentProvider>().SingleInstance();
            AddTracing(builder).AddSampling(builder).AddGrpcTransport(builder);
        }

        private SkywalkingModule AddTracing(ContainerBuilderWrapper builder)
        {
            builder.RegisterType<TracingContext>().As<CNative.Cloud.CPlatform.Diagnostics.ITracingContext>().SingleInstance();
            builder.RegisterType<CarrierPropagator>().As<ICarrierPropagator>().SingleInstance();
            builder.RegisterType<Sw8CarrierFormatter>().As<ICarrierFormatter>().SingleInstance();
            builder.RegisterType<SegmentContextFactory>().As<ISegmentContextFactory>().SingleInstance();
            builder.RegisterType<EntrySegmentContextAccessor>().As<IEntrySegmentContextAccessor>().SingleInstance();
            builder.RegisterType<LocalSegmentContextAccessor>().As<ILocalSegmentContextAccessor>().SingleInstance();
            builder.RegisterType<ExitSegmentContextAccessor>().As<IExitSegmentContextAccessor>().SingleInstance();
            builder.RegisterType<SamplerChainBuilder>().As<ISamplerChainBuilder>().SingleInstance();
            builder.RegisterType<UniqueIdGenerator>().As<IUniqueIdGenerator>().SingleInstance();
            builder.RegisterType<UniqueIdParser>().As<IUniqueIdParser>().SingleInstance();
            builder.RegisterType<SegmentContextMapper>().As<ISegmentContextMapper>().SingleInstance();
            builder.RegisterType<Base64Formatter>().As<IBase64Formatter>().SingleInstance();
            return this;
        }

        private SkywalkingModule AddSampling(ContainerBuilderWrapper builder)
        {
            builder.RegisterType<SimpleCountSamplingInterceptor>().SingleInstance();
            builder.Register<ISamplingInterceptor>(p => p.Resolve<SimpleCountSamplingInterceptor>()).SingleInstance();
            builder.Register<IExecutionService>(p => p.Resolve<SimpleCountSamplingInterceptor>()).SingleInstance();
            builder.RegisterType<RandomSamplingInterceptor>().As<ISamplingInterceptor>().SingleInstance();
            builder.RegisterType<IgnorePathSamplingInterceptor>().As<ISamplingInterceptor>().SingleInstance();
            return this;
        }

        private SkywalkingModule AddGrpcTransport(ContainerBuilderWrapper builder)
        {
            builder.RegisterType<SegmentReporter>().As<ISegmentReporter>().SingleInstance();
            builder.RegisterType<CLRStatsReporter>().As<ICLRStatsReporter>().SingleInstance();
            builder.RegisterType<ConnectionManager>().SingleInstance();
            builder.RegisterType<PingCaller>().As<IPingCaller>().SingleInstance();
            builder.RegisterType<ServiceRegister>().As<IServiceRegister>().SingleInstance();
            builder.RegisterType<ConnectService>().As<IExecutionService>().SingleInstance();
            return this;
        }

        //private SkywalkingModule AddSkyApmLogging(ContainerBuilderWrapper builder)
        //{
        //    builder.RegisterType<DefaultLoggerFactory>().As<ILoggerFactory>().SingleInstance();
        //    return this;
        //}
    }
}
