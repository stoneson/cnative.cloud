﻿//using CNative.Cloud.Caching.Configurations;
using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Configurations;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.EventBusRabbitMQ.Configurations;
using CNative.Cloud.ProxyGenerator;
//using CNative.Cloud.System;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CNative.Cloud.ProxyClientPRC
{
    /// <summary>
    /// 客户端工厂
    /// </summary>
    public class RPCClientFactory
    {
        private static ConcurrentDictionary<string, object> _services { get; } = new ConcurrentDictionary<string, object>();

        public static async Task MainRun(Action action)
        {
            var host = new HostBuilder()
                  .RegisterMicroServices()
                  .ConfigureAppConfiguration((hostContext, configure) =>
                  {
                      configure.AddCPlatformFile("${CNativePath}|cnativeSettings.json", optional: false, reloadOnChange: true);
                      //configure.AddCacheFile("${cachePath}|cacheSettings.json", optional: false, reloadOnChange: true);
                      configure.AddEventBusFile("${eventBusPath}|eventBusSettings.json", optional: false, reloadOnChange: true);
                  })
               .UseClient()
               //.UseDefaultCacheInterceptor()
               .Build();

            //host.StartAsync();
            using (host)
            {
                try
                {
                    await host.StartAsync();
                    action?.Invoke();
                    await host.WaitForShutdownAsync();
                }
                catch (Exception ex)
                {
                    ConsoleC.WriteLine(ex);
                }
            }
        }
        
        /// <summary>
        /// 获取客户端代码
        /// </summary>
        /// <typeparam name="T">接口定义类型</typeparam>
        /// <returns></returns>
        public static T CreateProxy<T>() where T : class
        {
            var serviceProxyFactory = ServiceLocator.GetService<IServiceProxyFactory>();
            return serviceProxyFactory.CreateProxy<T>();
        }

        //public static Task<T> Invoke<T>(string routePath, IDictionary<string, object> parameters, Cloud.CPlatform.Runtime.HttpMethod httpMethod= CPlatform.Runtime.HttpMethod.GET)
        //{
        //    var serviceProxyProvider = ServiceLocator.GetService<IServiceProxyProvider>();
        //    return serviceProxyProvider.Invoke<T>(parameters, routePath, httpMethod);
        //}

        //public static Task<T> Invoke<T>(string routePath, IDictionary<string, object> parameters, string serviceKey, Cloud.CPlatform.Runtime.HttpMethod httpMethod = CPlatform.Runtime.HttpMethod.GET)
        //{
        //    var serviceProxyProvider = ServiceLocator.GetService<IServiceProxyProvider>();
        //    return serviceProxyProvider.Invoke<T>(parameters, routePath, httpMethod, serviceKey);
        //}
    }
}
