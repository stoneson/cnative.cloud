﻿using System;
using Autofac;
using Microsoft.Extensions.Hosting;
using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Exceptions;
using CNative.Cloud.ProxyGenerator;
using CNative.Cloud.ProxyGenerator.Interceptors;
using CNative.Cloud.System.Intercept;

namespace CNative.Cloud.System
{
    public static class ServiceHostBuilderExtensions
    {
        public static IHostBuilder UseCacheInterceptor(this IHostBuilder hostBuilder, Type type) 
        {
            if (!typeof(CacheInterceptor).IsAssignableFrom(type))
            {
                throw new CPlatformException($"设置的服务引擎类型必须继承IServiceEngine接口");
            }
  
            return hostBuilder.ConfigureContainer<ContainerBuilder>(containerBuilder =>
            {
                containerBuilder.GetServiceBuilder().AddClientIntercepted(type);
            });
        }
        
        public static IHostBuilder UseCacheInterceptor<T>(this IHostBuilder hostBuilder) where T: CacheInterceptor
        {
            return hostBuilder.UseCacheInterceptor(typeof(T));
        }
           
        public static IHostBuilder UseDefaultCacheInterceptor(this IHostBuilder hostBuilder)
        {
            return hostBuilder.UseCacheInterceptor(typeof(CacheProviderInterceptor));
        }

    }
}