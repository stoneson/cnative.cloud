﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Exceptionless
{
   public class AppConfig
    {
        public static ExceptionlessOption ExceptionlessOption
        {
            get; internal set;
        }
    }
    public class ExceptionlessOption
    {
        public string UseExceptionless { get; set; } = "false";

        public string ApiKey { get; set; }

        public string ServerUrl { get; set; }
        public string LogLevel { get; set; } = "Error";
    }
}
