﻿namespace CNative.Cloud.AutoMapper
{
    public interface IAutoMapperBootstrap
    {
        void Initialize();
    }
}
