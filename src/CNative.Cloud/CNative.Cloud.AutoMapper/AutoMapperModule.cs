﻿using Autofac;
using Microsoft.Extensions.Configuration;
using CNative.Cloud.CPlatform.Module;
using CPlatformAppConfig = CNative.Cloud.CPlatform.AppConfig;

namespace CNative.Cloud.AutoMapper
{
    public class AutoMapperModule : EnginePartModule
    {

        public override void Initialize(AppModuleContext context)
        {
            base.Initialize(context);
            context.ServiceProvoider.Resolve<IAutoMapperBootstrap>().Initialize();
        }

        protected override void RegisterBuilder(ContainerBuilderWrapper builder)
        {
            var configAssembliesStr = CPlatformAppConfig.GetSection("Automapper:Assemblies").Get<string>();
            if (!string.IsNullOrEmpty(configAssembliesStr))
            {
                AppConfig.AssembliesStrings = configAssembliesStr.Split(';');
            }
            builder.RegisterType<AutoMapperBootstrap>().As<IAutoMapperBootstrap>();
        }


    }
}
