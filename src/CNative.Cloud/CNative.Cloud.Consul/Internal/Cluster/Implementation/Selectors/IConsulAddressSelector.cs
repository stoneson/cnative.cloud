﻿using CNative.Cloud.CPlatform.Address;
using CNative.Cloud.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.Consul.Internal.Cluster.Implementation.Selectors
{
    public interface IConsulAddressSelector : IAddressSelector
    { 
    }
}
