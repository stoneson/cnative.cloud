﻿using Autofac;
using CNative.Cloud.CPlatform.EventBus.Events;
using CNative.Cloud.CPlatform.EventBus.Implementation;
using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.ProxyGenerator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.Protocol.Udp.Runtime
{
    public abstract class UdpBehavior : ProxyServiceBase
    {
        #region old
        //public T CreateProxy<T>(string key) where T : class
        //{
        //    return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<T>(key);
        //}

        //public object CreateProxy(Type type)
        //{
        //    return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy(type);
        //}

        //public object CreateProxy(string key, Type type)
        //{
        //    return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy(key, type);
        //}

        //public T CreateProxy<T>() where T : class
        //{
        //    return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<T>();
        //}

        //public T GetService<T>(string key) where T : class
        //{
        //    if (ServiceLocator.Current.IsRegisteredWithKey<T>(key))
        //        return ServiceLocator.GetService<T>(key);
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<T>(key);
        //}

        //public T GetService<T>() where T : class
        //{
        //    if (ServiceLocator.Current.IsRegistered<T>())
        //        return ServiceLocator.GetService<T>();
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<T>();

        //}

        //public object GetService(Type type)
        //{
        //    if (ServiceLocator.Current.IsRegistered(type))
        //        return ServiceLocator.GetService(type);
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy(type);
        //}

        //public object GetService(string key, Type type)
        //{
        //    if (ServiceLocator.Current.IsRegisteredWithKey(key, type))
        //        return ServiceLocator.GetService(key, type);
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy(key, type);

        //}

        //public void Publish(IntegrationEvent @event)
        //{
        //    GetService<IEventBus>().Publish(@event);
        //}
        #endregion

        public abstract Task<bool> Dispatch(IEnumerable<byte> bytes);
    }
}