﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Protocol.Udp.Runtime
{
    public interface IUdpServiceEntryProvider
    {
        UdpServiceEntry GetEntry();
    }
}
