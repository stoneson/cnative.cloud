﻿using CNative.Cloud.Protocol.Mqtt.Internal.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Protocol.Mqtt.Internal.Runtime
{
   public interface IMqttBehaviorProvider
    {
        MqttBehavior GetMqttBehavior();
    }
}
