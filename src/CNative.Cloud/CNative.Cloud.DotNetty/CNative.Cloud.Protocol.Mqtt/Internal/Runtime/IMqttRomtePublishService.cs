﻿using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.CPlatform.Support.Attributes;
using CNative.Cloud.Protocol.Mqtt.Internal.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.Protocol.Mqtt.Internal.Runtime
{
    [ServiceBundle("Device")]
    public interface IMqttRomtePublishService : IServiceKey
    {
        [Command(ShuntStrategy = AddressSelectorMode.HashAlgorithm)]
        Task Publish(string deviceId, MqttWillMessage message);
    }
} 
