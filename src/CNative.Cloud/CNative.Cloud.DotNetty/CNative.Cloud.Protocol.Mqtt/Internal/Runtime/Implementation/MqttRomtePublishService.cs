﻿using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.Protocol.Mqtt.Internal.Messages;
using CNative.Cloud.Protocol.Mqtt.Internal.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.Protocol.Mqtt.Internal.Runtime.Implementation
{
    public class MqttRomtePublishService : ServiceBase, IMqttRomtePublishService
    {
       public async Task Publish(string deviceId, MqttWillMessage message)
        {
            await ServiceLocator.GetService<IChannelService>().Publish(deviceId, message);
        }
    }
}
