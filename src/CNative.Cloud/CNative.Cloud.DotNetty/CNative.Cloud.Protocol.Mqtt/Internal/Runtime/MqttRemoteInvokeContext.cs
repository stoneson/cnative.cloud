﻿using CNative.Cloud.CPlatform.Runtime.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Protocol.Mqtt.Internal.Runtime
{
    public class MqttRemoteInvokeContext: RemoteInvokeContext
    {
         public string topic { get; set; }
    }
}
 