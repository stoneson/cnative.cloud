﻿using Autofac;
using CNative.Cloud.CPlatform.EventBus.Events;
using CNative.Cloud.CPlatform.EventBus.Implementation;
using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.Protocol.Mqtt.Internal.Enums;
using CNative.Cloud.Protocol.Mqtt.Internal.Messages;
using CNative.Cloud.ProxyGenerator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.Protocol.Mqtt.Internal.Services
{
    public abstract class MqttBehavior: ProxyServiceBase
    {
        #region old
        //public override T GetService<T>(string key)
        //{
        //    if (ServiceLocator.Current.IsRegisteredWithKey<T>(key))
        //        return base.GetService<T>(key);
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<T>(key);
        //}

        //public override T GetService<T>()
        //{
        //    if (ServiceLocator.Current.IsRegistered<T>())
        //        return base.GetService<T>();
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<T>();

        //}

        //public override object GetService(Type type)
        //{
        //    if (ServiceLocator.Current.IsRegistered(type))
        //        return base.GetService(type);
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy(type);
        //}

        //public override object GetService(string key, Type type)
        //{
        //    if (ServiceLocator.Current.IsRegisteredWithKey(key, type))
        //        return base.GetService(key, type);
        //    else
        //        return ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy(key, type);

        //}

        //public void Publish(IntegrationEvent @event)
        //{
        //    GetService<IEventBus>().Publish(@event);
        //}
        #endregion

        public async Task Publish(string deviceId, MqttWillMessage willMessage)
        {
            await GetService<IChannelService>().Publish(deviceId, willMessage);
        }

        public async Task RemotePublish(string deviceId, MqttWillMessage willMessage)
        {
            await GetService<IChannelService>().RemotePublishMessage(deviceId, willMessage);
        }

        public async Task<bool> GetDeviceIsOnine(string deviceId)
        {
           return  await this.GetService<IChannelService>().GetDeviceIsOnine(deviceId);
        }

        public abstract Task<bool> Authorized(string username, string password);
    }
}
