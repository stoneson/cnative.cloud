﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.Protocol.Mqtt.Internal.Enums
{
    public enum Behavior
    {
        Publish,
        Suscribe,
    }
}
