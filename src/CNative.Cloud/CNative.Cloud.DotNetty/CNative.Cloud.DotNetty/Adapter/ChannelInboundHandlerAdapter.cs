﻿using System.Net;
using DotNetty.Handlers.Timeout;
using DotNetty.Transport.Channels;
using CNative.Cloud.CPlatform.Address;
using CNative.Cloud.CPlatform.Runtime.Client.HealthChecks;
using CNative.Cloud.CPlatform.Utilities;

namespace CNative.Cloud.DotNetty.Adapter
{
    public class ChannelInboundHandlerAdapter : ChannelHandlerAdapter
    {

        private readonly IHealthCheckService _healthCheckService;

        public ChannelInboundHandlerAdapter()
        {
            _healthCheckService = ServiceLocator.GetService<IHealthCheckService>();
        }

        public async override void UserEventTriggered(IChannelHandlerContext context, object evt) 
        {
            if (evt is IdleStateEvent @event)
            {
                if (@event.State == IdleState.ReaderIdle)
                {
                    var iPEndPoint = context.Channel.RemoteAddress as IPEndPoint;
                    var ipAddressModel = new IpAddressModel(iPEndPoint.Address.MapToIPv4().ToString(),
                        iPEndPoint.Port);
                    await _healthCheckService.MarkHealth(ipAddressModel);
                    if (context.Channel.Active)
                    {
                        await context.Channel.CloseAsync();
                    }
                    
                }

            }
            else
            {
                base.UserEventTriggered(context, evt);
            }
        }

        
    }
}
