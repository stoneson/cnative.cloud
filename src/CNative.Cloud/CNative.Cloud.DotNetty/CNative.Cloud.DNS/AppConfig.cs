﻿using Microsoft.Extensions.Configuration;
using CNative.Cloud.DNS.Configurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.DNS
{
    public static  class AppConfig
    {
        public static IConfigurationRoot Configuration { get; set; }

        public static DnsOption DnsOption { get; set; }
    }
}
