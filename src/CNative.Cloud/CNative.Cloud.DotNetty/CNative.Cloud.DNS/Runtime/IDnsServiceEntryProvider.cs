﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.DNS.Runtime
{
    public interface IDnsServiceEntryProvider
    {
        DnsServiceEntry GetEntry();
    }
}
