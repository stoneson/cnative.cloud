﻿namespace CNative.Cloud.ProxyGenerator.Interceptors.Implementation.Metadatas
{
    public enum CacheTargetType
    {
        Redis,
        CouchBase,
        Memcached,
        MemoryCache,
    }
}
