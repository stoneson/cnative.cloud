﻿namespace CNative.Cloud.ProxyGenerator.Interceptors.Implementation.Metadatas
{
    public enum CachingMethod
    { 
        Get, 
        Put, 
        Remove
    }
}
