﻿using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Runtime;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.ProxyGenerator
{
    /// <summary>
    /// 代理服务接口
    /// </summary>
   public interface  IServiceProxyProvider
    {
        
        Task<T> Invoke<T>(IDictionary<string, object> parameters, string routePath, HttpMethod httpMethod);

        Task<T> Invoke<T>(IDictionary<string, object> parameters, string routePath, HttpMethod httpMethod, string serviceKey);
    }
}
