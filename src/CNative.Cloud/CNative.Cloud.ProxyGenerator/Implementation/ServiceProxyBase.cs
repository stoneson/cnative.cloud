﻿using Newtonsoft.Json.Linq;
using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Convertibles;
using CNative.Cloud.CPlatform.Messages;
using CNative.Cloud.CPlatform.Runtime.Client;
using CNative.Cloud.CPlatform.Support;
using CNative.Cloud.ProxyGenerator.Interceptors;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using CNative.Cloud.CPlatform.Utilities;
using System.Linq;
using Microsoft.Extensions.Logging;
using CNative.Cloud.CPlatform.Exceptions;
using CNative.Cloud.CPlatform.Transport.Implementation;

namespace CNative.Cloud.ProxyGenerator.Implementation
{
    /// <summary>
    /// 一个抽象的服务代理基类。
    /// </summary>
    public abstract class ServiceProxyBase : System.Dynamic.DynamicObject
    {
        #region Field
        protected readonly IRemoteInvokeService _remoteInvokeService;
        protected readonly ITypeConvertibleService _typeConvertibleService;
        protected readonly string _serviceKey;
        protected readonly CPlatformContainer _serviceProvider;
        protected readonly IServiceCommandProvider _commandProvider;
        protected readonly IBreakeRemoteInvokeService _breakeRemoteInvokeService;
        protected readonly IEnumerable<IInterceptor> _interceptors;
        protected readonly IInterceptor _cacheInterceptor;
        protected readonly ILogger<ServiceProxyBase> _logger;
        #endregion Field

        #region Constructor

        protected ServiceProxyBase(IRemoteInvokeService remoteInvokeService,
            ITypeConvertibleService typeConvertibleService, String serviceKey, CPlatformContainer serviceProvider)
        {
            _remoteInvokeService = remoteInvokeService;
            _typeConvertibleService = typeConvertibleService;
            _serviceKey = serviceKey;
            _serviceProvider = serviceProvider;
            _commandProvider = serviceProvider.GetInstances<IServiceCommandProvider>();
            _breakeRemoteInvokeService = serviceProvider.GetInstances<IBreakeRemoteInvokeService>();
            _logger = serviceProvider.GetInstances<ILogger<ServiceProxyBase>>();
            _interceptors = new List<IInterceptor>();
            if (serviceProvider.Current.IsRegistered<IInterceptor>())
            {
                var interceptors = serviceProvider.GetInstances<IEnumerable<IInterceptor>>();
                _interceptors = interceptors.Where(p => !typeof(CacheInterceptor).IsAssignableFrom(p.GetType()));
                _cacheInterceptor = interceptors.Where(p => typeof(CacheInterceptor).IsAssignableFrom(p.GetType())).FirstOrDefault();
            }


        }
        #endregion Constructor

        #region Protected Method
        /// <summary>
        /// 远程调用。
        /// </summary>
        /// <typeparam name="T">返回类型。</typeparam>
        /// <param name="parameters">参数字典。</param>
        /// <param name="serviceId">服务Id。</param>
        /// <returns>调用结果。</returns>
        protected async Task<T> Invoke<T>(IDictionary<string, object> parameters, string serviceId)
        {
            object result = default(T);
            var command = await _commandProvider.GetCommand(serviceId);
            RemoteInvokeResultMessage message = null;
            var decodeJOject = typeof(T) == UtilityType.ObjectType;
            IInvocation invocation = null;
            var existsInterceptor = _interceptors.Any();
            if ((_cacheInterceptor == null || !command.RequestCacheEnabled) && !existsInterceptor)
            {
                message = await _breakeRemoteInvokeService.InvokeAsync(parameters, serviceId, _serviceKey, decodeJOject);
                if (message == null || !message.IsSucceedRemoteInvokeCalled())
                {
                    return await FallBackRetryInvoke<T>(parameters, serviceId, command);
                }
            }
            if (_cacheInterceptor != null && command.RequestCacheEnabled)
            {
                invocation = GetCacheInvocation(parameters, serviceId, typeof(T));
                if (invocation != null)
                {
                    var interceptReuslt = await Intercept(_cacheInterceptor, invocation);
                    message = interceptReuslt.Item1;
                    result = interceptReuslt.Item2 == null ? default(T) : interceptReuslt.Item2;
                }
                else
                {
                    message = await _breakeRemoteInvokeService.InvokeAsync(parameters, serviceId, _serviceKey, decodeJOject);
                    if (message == null || !message.IsSucceedRemoteInvokeCalled())
                    {
                        return await FallBackRetryInvoke<T>(parameters, serviceId, command);
                    }
                }

            }
            if (existsInterceptor)
            {
                invocation = invocation == null ? GetInvocation(parameters, serviceId, typeof(T)) : invocation;
                foreach (var interceptor in _interceptors)
                {
                    var interceptReuslt = await Intercept(interceptor, invocation);
                    message = interceptReuslt.Item1;
                    result = interceptReuslt.Item2 == null ? default(T) : interceptReuslt.Item2;
                }
            }
            if (message != null)
            {
                result = await GetInvokeResult<T>(message);
            }
            return (T)result;
        }
        /// <summary>
        /// 远程调用。
        /// </summary>
        /// <param name="returnType">返回类型。</param>
        /// <param name="parameters">参数字典。</param>
        /// <param name="serviceId">服务Id。</param>
        /// <returns>调用结果。</returns>
        protected async Task<object> Invoke(Type returnType, IDictionary<string, object> parameters, string serviceId)
        {
            object result = null;
            var command = await _commandProvider.GetCommand(serviceId);
            RemoteInvokeResultMessage message = null;
            var decodeJOject = returnType == UtilityType.ObjectType;
            IInvocation invocation = null;
            var existsInterceptor = _interceptors.Any();
            if ((_cacheInterceptor == null || !command.RequestCacheEnabled) && !existsInterceptor)
            {
                message = await _breakeRemoteInvokeService.InvokeAsync(parameters, serviceId, _serviceKey, decodeJOject);
                if (message == null || !message.IsSucceedRemoteInvokeCalled())
                {
                    return await FallBackRetryInvoke(returnType, parameters, serviceId, command);
                }
            }
            if (_cacheInterceptor != null && command.RequestCacheEnabled)
            {
                invocation = GetCacheInvocation(parameters, serviceId, returnType);
                if (invocation != null)
                {
                    var interceptReuslt = await Intercept(_cacheInterceptor, invocation);
                    message = interceptReuslt.Item1;
                    result = interceptReuslt.Item2 == null ? null : interceptReuslt.Item2;
                }
                else
                {
                    message = await _breakeRemoteInvokeService.InvokeAsync(parameters, serviceId, _serviceKey, decodeJOject);
                    if (message == null || !message.IsSucceedRemoteInvokeCalled())
                    {
                        return await FallBackRetryInvoke(returnType, parameters, serviceId, command);
                    }
                }
            }
            if (existsInterceptor)
            {
                invocation = invocation == null ? GetInvocation(parameters, serviceId, returnType) : invocation;
                foreach (var interceptor in _interceptors)
                {
                    var interceptReuslt = await Intercept(interceptor, invocation);
                    message = interceptReuslt.Item1;
                    result = interceptReuslt.Item2 == null ? null : interceptReuslt.Item2;
                }
            }
            //---------------------------------------------------------------------------------------------------------
            if (message != null)
            {
                if (message.StatusCode == StatusCode.Success)
                {
                    if (message.Result != null)
                    {
                        if (returnType.BaseType == typeof(Task))
                            result = message.Result;// _typeConvertibleService.Convert(message.Result, returnType); //Task.FromResult(message.Result);
                        else
                            result = _typeConvertibleService.Convert(message.Result, returnType);
                    }
                    else
                    {
                        result = message.Result;
                    }
                }
                else
                {
                    throw message.GetExceptionByStatusCode();
                }

                return result;
            }
            return result;
        }

        /// <summary>
        /// 远程调用。
        /// </summary>
        /// <param name="parameters">参数字典。</param>
        /// <param name="serviceId">服务Id。</param>
        /// <returns>调用任务。</returns>
        protected async Task Invoke(IDictionary<string, object> parameters, string serviceId)
        {
            var existsInterceptor = _interceptors.Any();
            RemoteInvokeResultMessage message = null;
            if (!existsInterceptor)
                message = await _breakeRemoteInvokeService.InvokeAsync(parameters, serviceId, _serviceKey, false);
            else
            {
                var invocation = GetInvocation(parameters, serviceId, typeof(Task));
                foreach (var interceptor in _interceptors)
                {
                    var interceptReuslt = await Intercept(interceptor, invocation);
                    message = interceptReuslt.Item1;
                }
            }
            if (message == null || !message.IsSucceedRemoteInvokeCalled())
            {
                var command = await _commandProvider.GetCommand(serviceId);
                await FallBackRetryInvoke(parameters, serviceId, command);
            }
        }

        public async Task<object> CallInvoke(IInvocation invocation)
        {
            var cacheInvocation = invocation as ICacheInvocation;
            var parameters = invocation.Arguments;
            var serviceId = invocation.ServiceId;
            var type = invocation.ReturnType;
            var message = await _breakeRemoteInvokeService.InvokeAsync(parameters, serviceId, _serviceKey, type == typeof(Task) ? false : true);
            if (message == null || !message.IsSucceedRemoteInvokeCalled())
            {
                var command = await _commandProvider.GetCommand(serviceId);
                return await CallInvokeBackFallBackRetryInvoke(parameters, serviceId, command, type, type == typeof(Task) ? false : true);
            }
            if (type == typeof(Task)) return message;
            return await GetInvokeResult(message, invocation.ReturnType);
        }

        private async Task<Tuple<RemoteInvokeResultMessage, object>> Intercept(IInterceptor interceptor, IInvocation invocation)
        {
            await interceptor.Intercept(invocation);
            return new Tuple<RemoteInvokeResultMessage, object>(invocation.RemoteInvokeResultMessage, invocation.ReturnValue);
        }

        private IInvocation GetInvocation(IDictionary<string, object> parameters, string serviceId, Type returnType)
        {
            var invocation = _serviceProvider.GetInstances<IInterceptorProvider>();
            return invocation.GetInvocation(this, parameters, serviceId, returnType);
        }

        private IInvocation GetCacheInvocation(IDictionary<string, object> parameters, string serviceId, Type returnType)
        {
            var invocation = _serviceProvider.GetInstances<IInterceptorProvider>();
            return invocation.GetCacheInvocation(this, parameters, serviceId, returnType);
        }

        private Task<T> GetInvokeResult<T>(RemoteInvokeResultMessage message)
        {
            return Task.Run(() =>
            {
                object result = default(T);
                if (message.StatusCode == StatusCode.Success)
                {
                    if (message.Result != null)
                    {
                        result = _typeConvertibleService.Convert(message.Result, typeof(T));
                    }
                }
                else
                {
                    throw message.GetExceptionByStatusCode();
                }

                return (T)result;
            });
        }

        private Task<object> GetInvokeResult(RemoteInvokeResultMessage message, Type returnType)
        {
            return Task.Run(() =>
            {
                object result;
                if (message.StatusCode == StatusCode.Success)
                {
                    if (message.Result != null)
                    {
                        result = _typeConvertibleService.Convert(message.Result, returnType);
                    }
                    else
                    {
                        result = message.Result;
                    }

                }
                else
                {
                    throw message.GetExceptionByStatusCode();
                }

                return result;

            });
        }

        private async Task<T> FallBackRetryInvoke<T>(IDictionary<string, object> parameters, string serviceId, ServiceCommand command)
        {
            if (command.FallBackName != null && _serviceProvider.IsRegistered<IFallbackInvoker>(command.FallBackName) && command.Strategy == StrategyType.FallBack)
            {
                var invoker = _serviceProvider.GetInstances<IFallbackInvoker>(command.FallBackName);
                return await invoker.Invoke<T>(parameters, serviceId, _serviceKey);
            }
            else
            {
                var invoker = _serviceProvider.GetInstances<IClusterInvoker>(command.Strategy.ToString());
                return await invoker.Invoke<T>(parameters, serviceId, _serviceKey, typeof(T) == UtilityType.ObjectType);
            }
        }
        private async Task<object> FallBackRetryInvoke(Type returnType, IDictionary<string, object> parameters, string serviceId, ServiceCommand command)
        {
            if (command.FallBackName != null && _serviceProvider.IsRegistered<IFallbackInvoker>(command.FallBackName) && command.Strategy == StrategyType.FallBack)
            {
                var invoker = _serviceProvider.GetInstances<IFallbackInvoker>(command.FallBackName);
                return await invoker.Invoke(parameters, returnType, serviceId, _serviceKey);
            }
            else
            {
                var invoker = _serviceProvider.GetInstances<IClusterInvoker>(command.Strategy.ToString());
                return await invoker.Invoke(parameters, returnType, serviceId, _serviceKey, returnType == UtilityType.ObjectType);
            }
        }

        private async Task<object> CallInvokeBackFallBackRetryInvoke(IDictionary<string, object> parameters, string serviceId, ServiceCommand command, Type returnType, bool decodeJOject)
        {
            if (command.FallBackName != null && _serviceProvider.IsRegistered<IFallbackInvoker>(command.FallBackName) && command.Strategy == StrategyType.FallBack)
            {
                var invoker = _serviceProvider.GetInstances<IFallbackInvoker>(command.FallBackName);
                return await invoker.Invoke(parameters, returnType, serviceId, _serviceKey);
            }
            else
            {
                var invoker = _serviceProvider.GetInstances<IClusterInvoker>(command.Strategy.ToString());
                return await invoker.Invoke(parameters, returnType, serviceId, _serviceKey, decodeJOject);
            }
        }

        private async Task FallBackRetryInvoke(IDictionary<string, object> parameters, string serviceId, ServiceCommand command)
        {
            if (command.FallBackName != null && _serviceProvider.IsRegistered<IFallbackInvoker>(command.FallBackName) && command.Strategy == StrategyType.FallBack)
            {
                var invoker = _serviceProvider.GetInstances<IFallbackInvoker>(command.FallBackName);
                await invoker.Invoke<object>(parameters, serviceId, _serviceKey);
            }
            else
            {
                var invoker = _serviceProvider.GetInstances<IClusterInvoker>(command.Strategy.ToString());
                await invoker.Invoke(parameters, serviceId, _serviceKey, true);
            }
        }

        #endregion Protected Method
    }

    public static class TaskExtension
    {
        public static async Task<object> CastToObject<T>( this Task<T> task)
        {
            return await task.ConfigureAwait(false);
        }

        public static async Task<TResult> Cast<TResult>( this Task<object> task)
        {
            return (TResult)await task.ConfigureAwait(false);
        }

        public async static Task<T> Cast<T>(this Task task)
        {
            if (!task.GetType().IsGenericType) throw new InvalidOperationException();

            await task.ConfigureAwait(false);

            // Harvest the result. Ugly but works
            return (T)((dynamic)task).Result;
        }

        public static Task<object> Convert<T>(this Task<T> task)
        {
            TaskCompletionSource<object> res = new TaskCompletionSource<object>();

            return task.ContinueWith(t =>
            {
                if (t.IsCanceled)
                {
                    res.TrySetCanceled();
                }
                else if (t.IsFaulted)
                {
                    res.TrySetException(t.Exception);
                }
                else
                {
                    res.TrySetResult(t.Result);
                }
                return res.Task;
            }
            , TaskContinuationOptions.ExecuteSynchronously).Unwrap();
        }
    }

    public class BindableDynamicDictionary : System.Dynamic.DynamicObject
    {
        private readonly Dictionary<string, object> _dictionary;

        public BindableDynamicDictionary()
        {
            _dictionary = new Dictionary<string, object>();
        }

        public object this[string key]
        {
            get { return _dictionary[key]; }
            set { _dictionary[key] = value; }
        }

        public override bool TryGetMember(System.Dynamic.GetMemberBinder binder, out object result)
        {
            return _dictionary.TryGetValue(binder.Name, out result);
        }

        public override bool TrySetMember(System.Dynamic.SetMemberBinder binder, object value)
        {
            _dictionary[binder.Name] = value;
            return true;
        }
    }
}