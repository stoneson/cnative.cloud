﻿using CNative.Cloud.Caching.HashAlgorithms;
using CNative.Cloud.CPlatform.Cache;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Cloud.Caching.AddressResolvers
{
    public interface IAddressResolver
    {
        Task<ConsistentHashNode> Resolver(string cacheId, string item);
    }
}
