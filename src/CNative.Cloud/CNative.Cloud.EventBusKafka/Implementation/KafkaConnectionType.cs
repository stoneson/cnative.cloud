﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.EventBusKafka.Implementation
{
   public enum KafkaConnectionType
    {
        Producer,
        Consumer
    }
}
