﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Cloud.EventBusKafka.Configurations
{
   public enum OffsetResetMode
    {
        Earliest,
        Latest,
        None,
    }
}
