﻿using Microsoft.Extensions.Configuration;
using CNative.Cloud.CPlatform.Configurations.Remote;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CNative.Cloud.EventBusKafka.Configurations
{
   public class EventBusConfigurationProvider : FileConfigurationProvider
    {
        public EventBusConfigurationProvider(EventBusConfigurationSource source) : base(source) { }

        public override void Load(Stream stream)
        {
            var parser = new JsonConfigurationParser();
            this.Data = parser.Parse(stream, null);
        }
    }
}
