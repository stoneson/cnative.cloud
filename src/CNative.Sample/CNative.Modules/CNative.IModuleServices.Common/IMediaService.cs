﻿using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.Protocol.WS.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.IModuleServices.Common
{
    [ServiceBundle("Api/{Service}")]
    [BehaviorContract(IgnoreExtensions =true,Protocol = "media")]
    public interface IMediaService : IServiceKey
    { 
        Task Push(IEnumerable<byte> data);
    }

}
