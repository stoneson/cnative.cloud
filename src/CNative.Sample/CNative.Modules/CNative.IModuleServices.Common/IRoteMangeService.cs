﻿using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.IModuleServices.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.IModuleServices.Common
{
    [ServiceBundle("Api/{Service}")]
    public interface IRoteMangeService
    {
        Task<UserModel> GetServiceById(string serviceId);

        Task<bool> SetRote(RoteModel model);
    }
}
