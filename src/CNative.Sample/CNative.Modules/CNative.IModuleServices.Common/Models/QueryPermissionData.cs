﻿using CNative.Cloud.Domain.PagedAndSorted;

namespace CNative.IModuleServices.Common.Models
{
    public class QueryPermissionData : PagedResultRequestDto
    {
        public string UserName
        {
            get;
            set;
        }

        public string Address { get; set; }
    }
}