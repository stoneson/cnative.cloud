﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.IModuleServices.Common.Models
{
    public class DeleteByIdInput
    {
        public long Id { get; set; }
    }
}
