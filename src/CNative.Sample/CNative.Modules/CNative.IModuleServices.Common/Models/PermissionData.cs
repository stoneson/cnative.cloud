﻿using CNative.Cloud.Domain.Entities.Auditing;

namespace CNative.IModuleServices.Common.Models
{
    public class PermissionData : FullAuditedEntity<long>, IOrgAudited
    {
        public string UserName { get; set; }

        public string Address { get; set; }
        
        public long? OrgId { get; set; }
    }
}