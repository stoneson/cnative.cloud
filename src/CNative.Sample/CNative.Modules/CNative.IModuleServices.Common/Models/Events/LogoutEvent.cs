﻿using CNative.Cloud.CPlatform.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.IModuleServices.Common.Models.Events
{
   public class LogoutEvent : IntegrationEvent
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        public string Age { get; set; }
    }
}
 