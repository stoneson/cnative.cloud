﻿using CNative.Cloud.CPlatform.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.IModuleServices.Common.Models.Events
{
    public class UserEvent : IntegrationEvent
    {
        public int UserId { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }
    }
}
