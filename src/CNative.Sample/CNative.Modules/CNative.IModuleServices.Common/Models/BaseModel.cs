﻿//using ProtoBuf;
//using CNative.Cloud.System.Intercept;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.IModuleServices.Common.Models
{
    //[ProtoContract]
    public class BaseModel
    {
        //[ProtoMember(1)]
        public Guid Id => Guid.NewGuid();
    }
}
