﻿using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.CPlatform.Support.Attributes;
using CNative.Cloud.Protocol.Mqtt.Internal.Enums;
using CNative.IModuleServices.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.IModuleServices.Common
{
    /// <summary>
    /// MQTT服务
    /// </summary>
    [ServiceBundle("Device/{Service}")] 
    public interface IControllerService : IServiceKey
    { 
        [Command(ShuntStrategy = AddressSelectorMode.HashAlgorithm)]
        Task Publish(string deviceId, WillMessage message);

        [Command(ShuntStrategy = AddressSelectorMode.HashAlgorithm)]
        Task<bool> IsOnline(string deviceId);
    }
}
