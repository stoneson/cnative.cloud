﻿using System.Threading.Tasks;
using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.KestrelHttpServer;

namespace CNative.IModuleServices.Common
{
    [ServiceBundle("{Service}")]
    public interface IFileService : IServiceKey
    {
        [Service(EnableAuthorization = false)]
        Task<IActionResult> Preview(string fileId);
    }
}