﻿using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.CPlatform.Support.Attributes;
using CNative.Cloud.Protocol.WS;
using CNative.Cloud.Protocol.WS.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.IModuleServices.Common
{
    /// <summary>
    /// WS聊天服务
    /// </summary>
    [ServiceBundle("Api/{Service}")]
    [BehaviorContract(IgnoreExtensions =true)]
    public  interface IChatService: IServiceKey
    {
        [Command( ShuntStrategy=AddressSelectorMode.HashAlgorithm)]
        Task SendMessage(string name,string data);
    }
}
