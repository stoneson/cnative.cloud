﻿using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using System.Threading.Tasks;

namespace CNative.IModuleServices.Common
{
    [ServiceBundle("Api/{Service}")]
    public interface IPermissionService : IServiceKey
    {
        Task<bool> Check(string serviceId);
    }
}
