﻿using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Module;
using CNative.Cloud.ProxyGenerator;
//using CNative.Cloud.System.Intercept;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.IModuleServices.Common
{
    public class IntercepteModule : SystemModule
    {
        public override void Initialize(AppModuleContext context)
        {
            base.Initialize(context);
        }

        /// <summary>
        /// Inject dependent third-party components
        /// </summary>
        /// <param name="builder"></param>
        protected override void RegisterBuilder(ContainerBuilderWrapper builder)
        {
            base.RegisterBuilder(builder);
            //builder.AddClientIntercepted(typeof(CacheProviderInterceptor));
            //builder.AddClientIntercepted(typeof(LogProviderInterceptor));
        }
    }
}

