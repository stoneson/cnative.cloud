﻿using System.Threading.Tasks;
using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.Domain.PagedAndSorted;
using CNative.IModuleServices.Common.Models;

namespace CNative.IModuleServices.Common
{
    [ServiceBundle("api/{DataService}")]
    public interface ITestPermissionDataService : IServiceKey
    {
        [ServiceRoute("search")]
        [HttpPost]
        Task<IPagedResult<PermissionData>> Search(QueryPermissionData query);
    }
}