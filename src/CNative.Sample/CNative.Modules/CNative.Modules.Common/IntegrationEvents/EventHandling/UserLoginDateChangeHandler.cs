﻿using CNative.Cloud.CPlatform.EventBus.Events;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.EventBusRabbitMQ;
using CNative.Cloud.EventBusRabbitMQ.Attributes;
using CNative.IModuleServices.Common;
using CNative.IModuleServices.Common.Models;
using CNative.IModuleServices.Common.Models.Events;
using System;
using System.Threading.Tasks;

namespace CNative.Modules.Common.IntegrationEvents.EventHandling
{
    [QueueConsumer("UserLoginDateChangeHandler",QueueConsumerMode.Normal,QueueConsumerMode.Fail)]
    public  class UserLoginDateChangeHandler : BaseIntegrationEventHandler<UserEvent>
    {
        private readonly IUserService _userService;
        public UserLoginDateChangeHandler()
        {
            _userService = ServiceLocator.GetService<IUserService>("User");
         }
        public override async Task Handle(UserEvent @event)
        {
            Console.WriteLine($"UserLoginDateChangeHandler 消费1。");
            if(await _userService.Update(@event.UserId, new UserModel()
            {
                Age = @event.Age,
                Name = @event.Name,
                UserId = @event.UserId
            }))
            {
                Console.WriteLine($"UserLoginDateChangeHandler  消费1成功。{ @event.UserId} { @event.Name} { @event.Age}");
            }
            else
            {
                Console.WriteLine($"UserLoginDateChangeHandler 消费1失败。");
                throw new Exception();
            }
        }

        public override Task Handled(EventContext context)
        {
            Console.WriteLine($"UserLoginDateChangeHandler 调用{context.Count}次。类型:{context.Type}");
            var model = context.Content as UserEvent;
            return Task.CompletedTask;
        }
    }
}
