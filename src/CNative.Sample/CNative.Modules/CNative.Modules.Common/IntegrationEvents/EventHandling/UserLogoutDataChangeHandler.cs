﻿using CNative.Cloud.CPlatform.EventBus.Events;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.EventBusRabbitMQ.Attributes;
using CNative.IModuleServices.Common;
using CNative.IModuleServices.Common.Models;
using CNative.IModuleServices.Common.Models.Events;
using System;
using System.Threading.Tasks;

namespace CNative.Modules.Common.IntegrationEvents.EventHandling
{
    [QueueConsumer("UserLogoutDateChangeHandler")]
    public class UserLogoutDataChangeHandler : IIntegrationEventHandler<LogoutEvent>
    {
        private readonly IUserService _userService;
        public UserLogoutDataChangeHandler()
        {
            _userService = ServiceLocator.GetService<IUserService>("User");
        }
        public async Task Handle(LogoutEvent @event)
        {
            Console.WriteLine($"UserLogoutDataChangeHandler消费1。");
            if (await _userService.Update(int.Parse(@event.UserId), new UserModel()
            {

            }))
            {
                Console.WriteLine($"UserLogoutDataChangeHandler 消费1成功。");
            }
            else
            {
                Console.WriteLine($"UserLogoutDataChangeHandler 消费1失败。");
                throw new Exception();
            }
        }
    }
}
