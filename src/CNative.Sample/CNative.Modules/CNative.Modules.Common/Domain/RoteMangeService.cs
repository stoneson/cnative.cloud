﻿using CNative.Cloud.ProxyGenerator;
//using CNative.Cloud.System.Ioc;
using CNative.IModuleServices.Common;
using CNative.IModuleServices.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Modules.Common.Domain
{
    public class RoteMangeService : ProxyServiceBase, IRoteMangeService
    {
        public Task<UserModel> GetServiceById(string serviceId)
        {
            return Task.FromResult(new UserModel());
        }

        public Task<bool> SetRote(RoteModel model)
        {
            return Task.FromResult(true);
        }
    }
}
