﻿using System.Threading.Tasks;
using CNative.Cloud.KestrelHttpServer;
using CNative.Cloud.ProxyGenerator;
using CNative.IModuleServices.Common;

namespace CNative.Modules.Common.Domain
{
    public class FileService : ProxyServiceBase, IFileService
    {
        public async Task<IActionResult> Preview(string fileId)
        {
            var captchaBytes = Utils.CreateCaptcha("xxx111");
            return new ImageResult(captchaBytes,"image/png");
        }
    }
}