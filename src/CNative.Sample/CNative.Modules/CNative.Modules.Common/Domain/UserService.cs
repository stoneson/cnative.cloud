﻿
//using CNative.Cloud.Common;
using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.EventBus.Events;
using CNative.Cloud.CPlatform.EventBus.Implementation;
using CNative.Cloud.CPlatform.Exceptions;
using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Session;
using CNative.Cloud.CPlatform.Transport.Implementation;
using CNative.Cloud.KestrelHttpServer;
using CNative.Cloud.KestrelHttpServer.Internal;
using CNative.Cloud.ProxyGenerator;
using CNative.IModuleServices.Common;
using CNative.IModuleServices.Common.Models;
//using CNative.IModuleServices.User;
using CNative.Modules.Common.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Org.BouncyCastle.Security;

namespace CNative.Modules.Common.Domain
{
    /// <summary>
    /// 用户服务
    /// </summary>
    [ModuleName("User")]
    public class UserService : ProxyServiceBase, IUserService
    {
        #region Implementation of IUserService
        private readonly UserRepository _repository;
        private readonly ICNativeSession _CNativeSession;
        public UserService(UserRepository repository)
        {
            this._repository = repository;
            _CNativeSession = NullCNativeSession.Instance;
        }

        public async Task<IDictionary<string, object>> Check(long? userId, string serviceId)
        {
            throw new AuthException("测试异常",StatusCode.UnAuthorized);
        }

        public async Task<string> GetUserName(int id)
        {
           //var text= await this.GetService<IManagerService>().SayHello("stone");
            return await Task.FromResult<string>(id.ToString());
        }

        public Task<bool> Exists(int id)
        {
            var loginUserId = _CNativeSession.UserId;
            return Task.FromResult(true);
        }

       public Task<UserModel> GetUserById(Guid id)
        {
            return Task.FromResult(new UserModel {

            });
        }

        public Task<int> GetUserId(string userName)
        {
            var xid = RpcContext.GetContext().GetAttachment("xid");
            return Task.FromResult(1);
        }

        public Task<DateTime> GetUserLastSignInTime(int id)
        {
            return Task.FromResult(new DateTime(DateTime.Now.Ticks));
        }

        public Task<bool> Get(List<UserModel> users)
        {
            return Task.FromResult(true);
        }

        public Task<UserModel> GetUser(UserModel user)
        {
            return Task.FromResult(new UserModel
            {
                Name = "stone",
                Age = 18
            });
        }
        public Task<string> GetUser(List<int> idList)
        {
            return Task.FromResult("type is List<int>");
        }

        public Task<string> GetUser()
        {
            return Task.FromResult("type is string");
        }

        public Task<bool> Update(int id, UserModel model)
        {
            return Task.FromResult(true);
        }

        public Task<bool> GetDictionary()
        {
            return Task.FromResult<bool>(true);
        }
        public Task<UserModel> GetDictionary2(UserModel user, int id)
        {
            user.UserId = id;
            return Task.FromResult<UserModel>(user);
        }

        public UserModel GetDictionary3(UserModel um, int id)
        {
            um.Age = 333;
            um.UserId = id;
            return um;
        }

        public async Task Try()
        {
            Console.WriteLine("start");
            await Task.Delay(5000);
            Console.WriteLine("end");
        }

        public Task TryThrowException()
        {
            throw new Exception("用户Id非法！");
        }

        public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            Publish(evt);
            await Task.CompletedTask;
        }

        public Task<IDictionary<string,object>> Authentication(AuthenticationRequestData requestData)
        {
            if (requestData.UserName != null && requestData.UserName == "admin" && requestData.Password == "admin")
            {
                IDictionary<string, object> payload = new Dictionary<string,object>();
                payload.Add(ClaimTypes.UserId, 1);
                payload.Add(ClaimTypes.UserName, "admin");
                payload.Add(ClaimTypes.OrgId, 2);
                payload.Add(ClaimTypes.DataPermissionOrgIds, new long[] { 2,4});

                return Task.FromResult(payload);
            }
            throw new AuthException("用户名账号不正确");
        }

        public Task<IdentityUser> Save(IdentityUser requestData)
        {
            return Task.FromResult(requestData);
        }

        public Task<UserModel> GetApiResult()
        {
            return Task.FromResult(new UserModel { Name = "stone" });
        }

        public async Task<bool> UploadFile(HttpFormCollection form)
        {
            var files = form.Files;
            foreach (var file in files)
            {
                using (var stream = new FileStream(Path.Combine(Cloud.CPlatform.Utilities.EnvironmentHelper.BaseDirectory, file.FileName), FileMode.OpenOrCreate))
                {
                   await stream.WriteAsync(file.File, 0, (int)file.Length);
                }
            }
            return true;
        }

        public async Task<Dictionary<string, object>> GetAllThings()
        {
            return await Task.FromResult(new Dictionary<string, object> { { "aaa", 12 } });
        }

        public async Task<IActionResult> DownFile(string fileName,string contentType)
        {
            string uploadPath = Path.Combine(Cloud.CPlatform.Utilities.EnvironmentHelper.BaseDirectory, fileName); 
            if (File.Exists(uploadPath))
            {
                using (var stream = new FileStream(uploadPath, FileMode.Open))
                {
                    var bytes = new Byte[stream.Length];
                    await stream.ReadAsync(bytes, 0, bytes.Length);
                    stream.Dispose();
                    return new FileContentResult(bytes, contentType, fileName);
                }
            }
            else
            {
                throw new FileNotFoundException(fileName);
            }
        }

        public async Task<Sex> SetSex(Sex sex)
        {
            return await Task.FromResult(sex);
        }

        public async Task<DeleteByIdOutput> Delete(DeleteByIdInput input)
        {
            return new DeleteByIdOutput  { Id = Guid.NewGuid().ToString(), Message = "删除成功" };
        }

        public async Task<string> Create(UserModel input)
        {
            return "创建用户成功";
        }
        #endregion Implementation of IUserService
    }
}