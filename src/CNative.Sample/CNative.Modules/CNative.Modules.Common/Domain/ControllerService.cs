﻿using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.Protocol.Mqtt.Internal.Enums;
using CNative.Cloud.Protocol.Mqtt.Internal.Messages;
using CNative.Cloud.Protocol.Mqtt.Internal.Services;
using CNative.Cloud.ProxyGenerator;
using CNative.IModuleServices.Common;
using CNative.IModuleServices.Common.Models;
//using CNative.IModuleServices.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Modules.Common.Domain
{
    public class ControllerService : MqttBehavior, IControllerService
    {
        public override async Task<bool> Authorized(string username, string password)
        {
            bool result = false;
            if (username == "admin" && password == "123456")
                result= true;
            return await Task.FromResult(result);
        }

       public async Task<bool> IsOnline(string deviceId)
        {
            //var text = await GetService<IManagerService>().SayHello("stone");
            return await base.GetDeviceIsOnine(deviceId);
        }

        public async Task Publish(string deviceId, WillMessage message)
        {
            var willMessage = new MqttWillMessage
            {
                WillMessage = message.Message,
                Qos = message.Qos,
                Topic = message.Topic,
                WillRetain = message.WillRetain
            };
            await Publish(deviceId, willMessage);
            await RemotePublish(deviceId, willMessage);
        }
    }
}
