﻿using System.Threading.Tasks;
using CNative.Cloud.CPlatform.Runtime.Session;
using CNative.Cloud.ProxyGenerator;
using CNative.IModuleServices.Common;

namespace CNative.Modules.Common.Domain
{
    public class PermissionService : ProxyServiceBase, IPermissionService
    {
        private readonly ICNativeSession _CNativeSession;

        public PermissionService() {
            _CNativeSession = NullCNativeSession.Instance;
        }

        public async Task<bool> Check(string serviceId)
        {
            var loginUserId = _CNativeSession.UserId;
            return true;
        }
    }
}
