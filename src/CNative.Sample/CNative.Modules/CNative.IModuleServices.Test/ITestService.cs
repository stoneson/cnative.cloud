﻿

using CNative.Cloud.CPlatform.Ioc;
using CNative.Cloud.CPlatform.Runtime.Client.Address.Resolvers.Implementation.Selectors.Implementation;
using CNative.Cloud.CPlatform.Runtime.Server.Implementation.ServiceDiscovery.Attributes;
using CNative.Cloud.CPlatform.Support;
using CNative.Cloud.CPlatform.Support.Attributes;
using System.Threading.Tasks;

namespace CNative.IModuleServices.Test
{

    [ServiceBundle("api/{Service}")]
    public interface ITestService : IServiceKey
    {
       // [Command(Strategy = StrategyType.Injection, ShuntStrategy = AddressSelectorMode.HashAlgorithm, ExecutionTimeoutInMilliseconds = 2500, BreakerRequestVolumeThreshold = 3, Injection = "return \"1\";", RequestCacheEnabled = false)]
        Task<string> SayHello(string name);

        //[Command(Strategy = StrategyType.Injection, ShuntStrategy = AddressSelectorMode.HashAlgorithm, ExecutionTimeoutInMilliseconds = 2500, BreakerRequestVolumeThreshold = 3, Injection = "return \"1\";", RequestCacheEnabled = false)]
        string SayHello2(string name);
    }
}
