﻿using CNative.Modules.WCFTestContract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CNative.Modules.WCFTest
{
    /// <summary>
    /// 测试WCF服务
    /// </summary>
    public class ServiceTest : IServiceTest
    {
        public void DoWork()
        {
           Console.WriteLine($"DoWork: {DateTime.Now}");
        }

        private static readonly string[] Summaries = new[]
          {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public List<WeatherForecast> HelloWorld()
        {
            Console.WriteLine($"HelloWorld: {DateTime.Now}");
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                code = index,
                msg = "成功" + index,
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToList();
            //return "Hello World";
        }

        public int GetInt(int a, int b)
        {
            Console.WriteLine($"GetInt:( {a},  {b})={b + a}");
            return b + a;
        }
        public DateTime GetDate()
        {
            Console.WriteLine($"GetDate:( {DateTime.Now})");
            return DateTime.Now;
        }

        public WeatherForecast getBySummarie(string summarie)
        {
            Console.WriteLine($"getBySummarie:( {summarie})");
            var rng = new Random();
            var ls = Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                code = index,
                msg = "成功" + index,
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
             .ToList();
            var fd = ls.FirstOrDefault(f => f.Summary == summarie);
            if (fd == null) fd = ls.FirstOrDefault();
            return fd;
        }
        public string AddCar(MyCar MyCar)
        {
            Console.WriteLine($"AddCar:( {MyCar})");
            var _car = MyCar;

            return ResponseResultObj.Success(_car, "Success").ToJson();
        }
        public string AddCar2(MyCar car1, MyCar car2,int id)
        {
            Console.WriteLine($"AddCar2:( {(car1==null?"":car1.ToJson())},{(car2 == null ? "" : car2.ToJson())})");
            //var ss = System.ServiceModel.Web.WebOperationContext.Current;
            return ResponseResultObj.Success(new List<MyCar> { car1, car2 }, "AddCar2 Success " + id).ToJson();
        }

        public string AddCars(List<MyCar> cars)
        {
            Console.WriteLine($"AddCars:( {cars})");
            var _car = cars;

            return ResponseResultObj.Success(_car, "Success").ToJson();
        }
        public string AddCar2(List<MyCar> cars, MyCar car2,string name)
        {
            Console.WriteLine($"AddCars2:( {(cars == null ? "" : cars.ToJson())},{(car2 == null ? "" : car2.ToJson())})");
            var _car = cars;
            if (_car == null) _car = new List<MyCar>();
            if (car2 != null) _car.Add(car2);

            return ResponseResultObj.Success(_car, "AddCars2 Success " + name).ToJson();
        }

        public string AddWeather(WeatherForecast wf)
        {
            Console.WriteLine($"AddWeather:( {wf})");
            return ResponseResultObj.Success(wf, "Success").ToJson();
        }
    }
}
