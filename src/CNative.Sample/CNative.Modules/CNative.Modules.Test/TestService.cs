﻿using System.Threading.Tasks;

namespace CNative.Modules.Test.Domain
{
    public class TestService : Cloud.CPlatform.Ioc.ServiceBase, IModuleServices.Test.ITestService
    {
        public Task<string> SayHello(string name)
        {
              return Task.FromResult($"{name} testSay:Hello");
        }

        public string SayHello2(string name)
        {
            return $"{name} testSay:Hello2";
        }
    }
}
