﻿using System.Threading.Tasks;
using CNative.Cloud.ProxyGenerator;
using CNative.IModuleServices.User;

namespace CNative.Modules.Manager.Domain
{
    public class ManagerService : ProxyServiceBase, IManagerService
    {
        public Task<string> SayHello(string name)
        {
              return Task.FromResult($"{name} say:Hello");
        }

        public string SayHello2(string name)
        {
            return $"{name} say:Hello2";
        }
    }
}
