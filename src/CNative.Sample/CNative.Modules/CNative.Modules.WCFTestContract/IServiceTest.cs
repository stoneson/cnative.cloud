﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CNative.Modules.WCFTestContract
{
    /// <summary>
    /// 测试WCF服务
    /// </summary>
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码和配置文件中的接口名“IServiceTest”。
    [ServiceContract]
    public interface IServiceTest
    {
        /// <summary>
        /// 测试无返回值
        /// </summary>
        [OperationContract]
        void DoWork();

        /// <summary>
        /// 测试返回集合
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<WeatherForecast> HelloWorld();
        /// <summary>
        /// 测试值入参
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        [OperationContract]
        int GetInt(int a, int b);
        /// <summary>
        /// 返回日期
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        DateTime GetDate();
        /// <summary>
        /// 返回实体
        /// </summary>
        /// <param name="summarie"></param>
        /// <returns></returns>
        [OperationContract]
        WeatherForecast getBySummarie(string summarie);
        /// <summary>
        /// 入参为实体
        /// </summary>
        /// <param name="MyCar"></param>
        /// <returns></returns>
        [OperationContract]
        string AddCar(MyCar MyCar);
        /// <summary>
        /// 两实体入参
        /// </summary>
        /// <param name="car1"></param>
        /// <param name="car2"></param>
        /// <returns></returns>
        [OperationContract]
        string AddCar2(MyCar car1, MyCar car2,int id);
        /// <summary>
        /// 入参实体集合
        /// </summary>
        /// <param name="cars"></param>
        /// <returns></returns>
        [OperationContract]
        string AddCars(List<MyCar> cars);
        /// <summary>
        /// 入参实体集合加实体
        /// </summary>
        /// <param name="cars"></param>
        /// <param name="car2"></param>
        /// <returns></returns>
        [OperationContract]
        string AddCar2(List<MyCar> cars, MyCar car2,string name);
        /// <summary>
        /// 入参为实体内嵌实体
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        [OperationContract]
        string AddWeather(WeatherForecast wf);
    }
}
