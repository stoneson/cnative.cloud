using CNative.Cloud.CPlatform.Engines.Implementation;
using CNative.Cloud.CPlatform.Utilities;

namespace CNative.Services.GateWay
{
    public class CNativeServiceEngine : VirtualPathProviderServiceEngine
    {
        public CNativeServiceEngine()
        {
            ModuleServiceLocationFormats = new[] {
                EnvironmentHelper.GetEnvironmentVariable("${ModulePath1}|Modules"),
            };
            ComponentServiceLocationFormats = new[] {
                 EnvironmentHelper.GetEnvironmentVariable("${ComponentPath1}|Components"),
            };
        }
    }
}