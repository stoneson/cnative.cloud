using System.Threading.Tasks;
using CNative.Cloud.CPlatform;
//using CNative.Cloud.EventBusKafka;
//using CNative.Cloud.Zookeeper;
//using CNative.Cloud.Zookeeper.Configurations;
using Microsoft.Extensions.Hosting;
using CNative.Cloud.Caching.Configurations;
using CNative.Cloud.CPlatform.Configurations;
//using CNative.Cloud.System;
using CNative.Cloud.EventBusRabbitMQ.Configurations;
using CNative.Cloud.Consul.Configurations;
using Topshelf;
using System.IO;
using System;

namespace CNative.Services.GateWay
{
    public class Program
    {
        static void Main(string[] args)
        {
            string baseDirectory = Cloud.CPlatform.Utilities.EnvironmentHelper.BaseDirectory;

            var builder = new HostBuilder()//await new HostBuilder()//Host.CreateDefaultBuilder(args)
                .RegisterMicroServices()
                .ConfigureAppConfiguration((hostContext, configure) =>
                {
                    configure.AddCPlatformFile("${CNativePath}|" + Path.Combine(baseDirectory, "cnativeSettings.json"), optional: false, reloadOnChange: true);
                    configure.AddCacheFile("${cachePath}|" + Path.Combine(baseDirectory, "configs/cacheSettings.json"), optional: false, reloadOnChange: true);
                    configure.AddEventBusFile("${eventBusPath}|" + Path.Combine(baseDirectory, "configs/eventBusSettings.json"), optional: false, reloadOnChange: true);
                    configure.AddConsulFile("${consulPath}|" + Path.Combine(baseDirectory, "configs/consul.json"), optional: false, reloadOnChange: true);
                    //configure.AddZookeeperFile("${zookeeperPath}|"+Path.Combine(ProcessDirectory, "configs/zookeeper.json"), optional: false, reloadOnChange: true);
                })
                .UseServer()
                .UseClient()
                //.UseDefaultCacheInterceptor()
                //.Build().RunAsync()
                ;

            if (Environment.OSVersion.Platform != PlatformID.Unix
               && Environment.OSVersion.Platform != PlatformID.MacOSX)
            {
                HostFactory.Run(x =>
                {
                    x.Service<IHost>(s =>
                    {
                        s.ConstructUsing(() => builder.Build());
                        s.WhenStarted(service =>
                        {
                            service?.RunAsync();
                        });
                        s.WhenStopped(service =>
                        {
                            service?.StopAsync();
                        });
                    });
                    x.UseNLog();
                    x.RunAsLocalSystem();

                    x.SetDescription("CNative网关服务");
                    x.SetDisplayName("CNative GateWay Service");
                    x.SetServiceName("CNative.GateWay.Service");
                    x.StartAutomaticallyDelayed();
                    x.EnableShutdown();

                    // 设置服务失败后的操作，分别对应第一次、第二次、后续
                    x.EnableServiceRecovery(t =>
                    {
                        t.RestartService(0);
                        t.RestartService(1);
                        t.RestartService(10);
                        t.OnCrashOnly();
                    });
                });
            }
            else
            {
                using (var host = builder.Build())
                {
                    host.Run();
                }
            }
        }
    }
}
