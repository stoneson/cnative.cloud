﻿using CNative.Cloud.CPlatform.Engines.Implementation;
using CNative.Cloud.CPlatform.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CNative.Services.Server
{
   public class CNativeServiceEngine: VirtualPathProviderServiceEngine
    {
        public CNativeServiceEngine()
        {
        
            ModuleServiceLocationFormats = new[] {
                EnvironmentHelper.GetEnvironmentVariable("${ModulePath1}|Modules"),
            };
            ComponentServiceLocationFormats  = new[] {
                 EnvironmentHelper.GetEnvironmentVariable("${ComponentPath1}|Components"),
            };
            //ModuleServiceLocationFormats = new[] {
            //   ""
            //};
        }
    }
}
