﻿using Autofac;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using CNative.Cloud.Caching;
using CNative.Cloud.Caching.Configurations;
using CNative.Cloud.Codec.MessagePack;
using CNative.Cloud.Consul;
using CNative.Cloud.Consul.Configurations;
using CNative.Cloud.CPlatform;
using CNative.Cloud.CPlatform.Configurations;
using CNative.Cloud.CPlatform.DependencyResolution;
using CNative.Cloud.CPlatform.Utilities;
using CNative.Cloud.DotNetty;
using CNative.Cloud.EventBusRabbitMQ;
using CNative.Cloud.EventBusRabbitMQ.Configurations;
using CNative.Cloud.ProxyGenerator;
//using CNative.Cloud.System;
//using CNative.Cloud.System.Intercept;
using CNative.IModuleServices.Common;
using System;
using System.Diagnostics;
//using CNative.Cloud.Zookeeper;
//using CNative.Cloud.Zookeeper.Configurations;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CNative.IModuleServices.Common.Models;
//using CNative.Cloud.Caching.Configurations;

namespace CNative.Services.Client
{
    public class Program
    {
        private static int _endedConnenctionCount = 0;
        private static DateTime begintime;
        static async Task Main(string[] args)
        {
            await new HostBuilder()//Host.CreateDefaultBuilder(args)
                   .RegisterMicroServices()
                   .ConfigureAppConfiguration((hostContext, configure) =>
                   {
                     //configure.AddCacheFile("${cachePath}|cacheSettings.json", optional: false, reloadOnChange: true);
                       configure.AddCPlatformFile("${CNativePath}|cnativeSettings.json", optional: false, reloadOnChange: true);
                       configure.AddEventBusFile("${eventBusPath}|eventBusSettings.json", optional: false, reloadOnChange: true);
                       //configure.AddConsulFile("${consulPath}|./configs/consul.json", optional: false, reloadOnChange: true);
                       //configure.AddZookeeperFile("${zookeeperPath}|./configs/zookeeper.json", optional: false, reloadOnChange: true);

                       //configure.UseProxy();
                   })
                .UseClient()
                //.UseDefaultCacheInterceptor()
                .Build().StartAsync();

            //Startup.TestRabbitMq(ServiceLocator.GetService<IServiceProxyFactory>());
            //Startup.Test(ServiceLocator.GetService<IServiceProxyFactory>());
            Startup.TestForRoutePath(ServiceLocator.GetService<IServiceProxyProvider>(), ServiceLocator.GetService<IServiceProxyFactory>());

            //// test Parallel 
            //var connectionCount = 300000;
            //StartRequest(connectionCount);
            //Console.ReadLine();


        }

        private static void StartRequest(int connectionCount)
        {
            // var service = ServiceLocator.GetService<IServiceProxyFactory>(); 
            var sw = new Stopwatch();
            sw.Start();
            var userProxy = ServiceLocator.GetService<IServiceProxyFactory>().CreateProxy<IUserService>();
            //ServiceResolver.Current.Register("User", userProxy);
            //var service = ServiceLocator.GetService<IServiceProxyFactory>();
            //userProxy = ServiceResolver.Current.GetService<IUserService>();
            sw.Stop();

            ConsoleC.WriteLine($"代理所花{sw.ElapsedMilliseconds}ms",ConsoleColor.DarkGray);
            ThreadPool.SetMinThreads(100, 100);
            Parallel.For(0, connectionCount / 6000, new ParallelOptions() { MaxDegreeOfParallelism = 50 }, async u =>
               {
                   for (var i = 0; i < 6000; i++)
                       await Test(userProxy, i,connectionCount);
               });
        }

        public static async Task Test(IUserService userProxy,int i, int connectionCount)
        {
            var ab = await userProxy.GetDictionary();
            ConsoleC.WriteLine($"GetDictionary：{ab}", ConsoleColor.DarkGray);
            var a = await userProxy.GetDictionary2(new UserModel
            {
                Name = "stone",
                Age = 12,
                UserId = 2,
                Sex = Sex.Woman
            }, i);
            ConsoleC.WriteLine($"GetDictionary2：{a.UserId},{a.Name}", ConsoleColor.DarkCyan);

            IncreaseSuccessConnection(connectionCount);
        }

        private static void IncreaseSuccessConnection(int connectionCount)
        {
            Interlocked.Increment(ref _endedConnenctionCount);
            if (_endedConnenctionCount == 1)
                begintime = DateTime.Now;
            if (_endedConnenctionCount >= connectionCount)
                Console.WriteLine($"结束时间{(DateTime.Now - begintime).TotalMilliseconds}");
        }
    }
}
