﻿
using CNative.MQExtend;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleTestApp
{
    public class Notice:EventData
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Msg { get; set; }
    }
}
