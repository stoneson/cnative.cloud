﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CNative.MQExtend;
namespace ConsoleTestApp
{
    class EventsBusTest
    {
        public void Handle()
        {
            var bus = EventBusFactory.CreateEventBus();
            Console.WriteLine("消息队列类型：" + bus.MQType);
            bus.SubscribeAll(typeof(EventsBusTest).Assembly);
            Notice notice = new Notice()
            {
                Id = 1100,
                EventSource = this,
                Name = "STONE",
                Msg = "后天放假，祝节假日快乐！发送时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF")
            };

            TT tt = new TT()
            {
                Id = 1100,
                EventSource = notice,
                Name = "STONETT",
                Msg = "TT后天放假，祝节假日快乐！发送时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF")
            };
            bus.Publish(notice);
            bus.Publish(tt);


        }
    }
}
