﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("测试消息队列");
                new MQTest().Handle();

                Console.WriteLine("\n测试消息总线");
                new EventsBusTest().Handle();
            }
            catch(Exception ex){ Console.WriteLine(ex + " Error"); }
            Console.ReadLine();
        }

    }
}
