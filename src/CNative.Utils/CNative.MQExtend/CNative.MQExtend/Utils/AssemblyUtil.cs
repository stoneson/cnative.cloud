﻿using System;
using System.IO;
using System.Reflection;

namespace CNative.MQExtend.Utils
{
    class AssemblyUtil
    {
        public static Type GetTypeFromAssembly(string assemblyName, string typeName)
        {
            Type type = null;
            if (assemblyName == null || typeName == null)
            {
                return type;
            }
            try
            {
                string fullPath = Path.Combine(GetCurrentPath(), assemblyName);
                if (File.Exists(fullPath))
                {
                    Assembly asm = Assembly.LoadFrom(fullPath);
                    type = asm.GetType(typeName);
                }

                return type;
            }
            catch
            {
                return type;
            }
        }

        /// <summary>
        /// 获取当前类库执行路径
        /// </summary>
        /// <returns></returns>
        private static string GetCurrentPath()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

    }
}
