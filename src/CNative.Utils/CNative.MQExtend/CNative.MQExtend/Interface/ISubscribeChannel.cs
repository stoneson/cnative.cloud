﻿using CNative.MQExtend.Model.Message;
using System;

namespace CNative.MQExtend.Interface
{
    /// <summary>
    /// 消息订阅者
    /// </summary>
    public interface ISubscribeChannel
    {

        /// <summary>
        /// 消息订阅
        /// </summary>
        /// <param name="queueName">队列名称</param>
        /// <param name="action">匿名方法</param>
        void Subscribe(string queueName, Action<IMessageContent> action);

    }

}
