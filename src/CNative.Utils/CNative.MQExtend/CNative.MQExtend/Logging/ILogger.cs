﻿using System;

namespace CNative.MQExtend.Logging
{
    public interface ILogger
    {

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="ex"></param>
        void Error(Exception ex);

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="error"></param>
        void Error(string error);

        /// <summary>
        /// 警告消息
        /// </summary>
        /// <param name="msg"></param>
        void Warning(string msg);

        /// <summary>
        /// Info
        /// </summary>
        /// <param name="msg"></param>
        void Info(string msg);

    }
}
