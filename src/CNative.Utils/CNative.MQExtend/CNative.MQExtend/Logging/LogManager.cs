﻿using CNative.MQExtend.Config;
using CNative.MQExtend.Utils;
using System;

namespace CNative.MQExtend.Logging
{
    public class LogManager
    {
        private static readonly object obj = new object();
        private static ILogger logger = null;

        public static ILogger Logger
        {
            get
            {
                if (logger == null)
                {
                    lock (obj)
                    {
                        if (logger == null)
                        {
                            try
                            {
                                if(!string.IsNullOrEmpty(Constants.LoggerAssemblyName) && !string.IsNullOrEmpty(Constants.LoggerAssemblyTypeName))
                                {
                                    Type type = AssemblyUtil.GetTypeFromAssembly(Constants.LoggerAssemblyName, Constants.LoggerAssemblyTypeName);
                                    if (type != null)
                                    {
                                        logger = Activator.CreateInstance(type) as ILogger;
                                    }
                                    else
                                    {
                                        logger = new DefaultLogger();
                                    }
                                }
                                else
                                {
                                    logger = new DefaultLogger();
                                }
                            }
                            catch
                            {
                                logger = new DefaultLogger();
                            }
                        }
                    }
                }

                return logger;
            }
        }

        /// <summary>
        /// 日志实例
        /// </summary>
        /// <param name="loggerImpl"></param>
        public static void Initialize(ILogger loggerImpl)
        {
            if (logger == null)
            {
                lock (obj)
                {
                    if (logger == null)
                    {
                        logger = loggerImpl;
                    }
                }
            }
        }

    }
}
