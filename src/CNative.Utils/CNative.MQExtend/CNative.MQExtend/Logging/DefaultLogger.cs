﻿using System;

namespace CNative.MQExtend.Logging
{
    public class DefaultLogger : ILogger
    {
        /// <summary>
        /// 异常日志处理
        /// </summary>
        /// <param name="ex"></param>
        public void Error(Exception ex) { }

        /// <summary>
        /// 异常日志
        /// </summary>
        /// <param name="error"></param>
        public void Error(string error) { }

        /// <summary>
        /// info
        /// </summary>
        /// <param name="msg"></param>
        public void Info(string msg) { }

        /// <summary>
        /// Warning
        /// </summary>
        /// <param name="msg"></param>
        public void Warning(string msg) { }
    }
}
