﻿using System;

namespace CNative.MQExtend.Model.Config.Subscribe
{
    public interface ISubscribeConfig
    {
        bool Check();
    }
}
