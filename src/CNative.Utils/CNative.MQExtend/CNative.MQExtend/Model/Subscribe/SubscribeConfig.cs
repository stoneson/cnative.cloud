﻿using CNative.MQExtend.Model.Exceptions;
using CNative.MQExtend.Model.Message;
using System;

namespace CNative.MQExtend.Model.Config.Subscribe
{
    public class SubscribeConfig : ServesConfig, ISubscribeConfig
    {
        /// <summary>
        /// 队列名称
        /// </summary>
        public string QueueName { get; set; }
        /// <summary>
        /// 设置是否回写确认消息
        /// </summary>
        public bool IsAckBack{ get; set; } = false;
        public SubscribeConfig() : base()
        {
        }
        public SubscribeConfig(string servers, string username, string password) : base(servers, username, password)
        {
        }
        /// <summary>
        /// 校验参数配置
        /// </summary>
        /// <returns></returns>
        public virtual bool Check()
        {
            if (string.IsNullOrEmpty(this.BrokerUri))
            {
                throw new MQException("BrokerUri参数不能为空");
            }
            return true;
        }

        protected override void Init()
        {
            base.Init();
            try
            {
                QueueName = this.GetAppSettings("MQQueueName", "").Trim();
            }
            catch (Exception ex)
            {
                Logging.LogManager.Logger.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
            }
            try
            {
                var temp = this.GetAppSettings("MQIsAckBack", "false").Trim();
                this.IsAckBack = bool.Parse(temp);
            }
            catch (Exception ex) { this.IsAckBack = false; Logging.LogManager.Logger.Error(ex.ToString()); }
        }
    }
}
