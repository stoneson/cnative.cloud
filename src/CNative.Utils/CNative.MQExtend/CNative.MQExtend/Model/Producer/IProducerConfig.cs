﻿using System;

namespace CNative.MQExtend.Model.Config.Producer
{
    public interface IProducerConfig
    {

        /// <summary>
        /// 校验参数配置
        /// </summary>
        /// <returns></returns>
        bool Check();

    }
}
