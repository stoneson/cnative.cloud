﻿using System;

namespace CNative.MQExtend.Model.Message
{
    public class KafkaMessageContent : MessageContent
    {

        public KafkaMessageContent(string msg, string key = "")
        {
            this.Value = msg;
            this.Key = key;
        }

    }
}
