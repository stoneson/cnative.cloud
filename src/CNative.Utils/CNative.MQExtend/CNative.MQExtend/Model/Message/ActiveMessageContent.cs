﻿using System;

namespace CNative.MQExtend.Model.Message
{
    public class ActiveMessageContent : MessageContent
    {
        public ActiveMessageContent(string msg)
        {
            this.Value = msg;
        }
    }
}
