﻿using System;
using CNative.MQExtend.Logging;
#if !NETFRAMEWORK
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
#else
using System.Configuration;
#endif
namespace CNative.MQExtend.Config
{
    public sealed class Constants
    {
        /// <summary>
        /// 日志类库名称
        /// xxx.xxxx.DLL
        /// </summary>
        public static string LoggerAssemblyName = "";
        /// <summary>
        /// 日志类库类名
        /// xxx.xxxx.MQLogging
        /// </summary>
        public static string LoggerAssemblyTypeName = "";

        static Constants()
        {
            try
            {
#if !NETFRAMEWORK
                LoadConfiguration();
#else
                ConfigurationManager.RefreshSection("appSettings");
#endif
                LoggerAssemblyName = GetAppSettings("LoggerAssemblyName", "").Trim();
                LoggerAssemblyTypeName = GetAppSettings("LoggerAssemblyTypeName", "").Trim();
            }
            catch
            {
            }
        }

#if !NETFRAMEWORK
        private static IConfiguration s_ConfigJson = null;
        private static System.Collections.Specialized.NameValueCollection _configuration = null;
        /// <summary>
        /// 配置节点关键字
        /// </summary>
        private static string _configSection = "AppSettings";

        /// <summary>
        /// 解析Json文件
        /// </summary>
        /// <param name="FilePath">文件路径</param>
        /// <returns></returns>
        private static IConfiguration LoadJsonFile(string FilePath)
        {
            //#if !DEBUG
            s_ConfigJson = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile(FilePath, optional: false, reloadOnChange: true)
                .Build();
            return s_ConfigJson;
        }
        /// <summary>
        /// 读取配置文件内容
        /// </summary>
        private static void LoadConfiguration(string configPath = "")
        {
            if (string.IsNullOrEmpty(configPath)) configPath = "appsettings.json";
            configPath = System.IO.Path.Combine(AppContext.BaseDirectory, configPath);

            s_ConfigJson = LoadJsonFile(configPath);
            if (s_ConfigJson == null)
            {
                LogManager.Logger.Info(" s_ConfigJson == null ");
                return;
            }
            //--------------------------------------------------------------
            _configuration = new System.Collections.Specialized.NameValueCollection();
            foreach (var prop in s_ConfigJson.GetChildren())
            {
                if (!string.IsNullOrEmpty(prop.Value))
                    _configuration[prop.Key] = prop.Value;
            }
            //LogManager.Logger.Info("AppSettings=" + _configuration.Count);
            //--------------------------------------------------------------
            var configSteps = s_ConfigJson.GetSection(_configSection);
            foreach (var prop in configSteps.GetChildren())
            {
                if (!string.IsNullOrEmpty(prop.Value))
                    _configuration[prop.Key] = prop.Value;//.ToString();
            }
            //_configurationCollection = _configuration;
            //if(LoggingHelper.IsDebugEnabled)
            LogManager.Logger.Info("AppSettings Count=" + _configuration.Count);
            LogManager.Logger.Info("LoadConfiguration end ");
        }
#endif
        #region GetAppSettings
        /// <summary>
        /// GetAppSettings
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSettings(string key, string def = "")
        {
            return GetAppSettings(key, def, "");
        }
        public static string GetAppSettings(string strKey, string def = "", string configPath = "")
        {
            try
            {
#if !NETFRAMEWORK
                if (!string.IsNullOrEmpty(configPath))
                    LoadConfiguration(configPath);

                if (!strKey.StartsWith("AppSettings:", StringComparison.OrdinalIgnoreCase))
                {
                    strKey = "AppSettings:" + strKey;
                }
                var tem = s_ConfigJson[strKey];
                if (string.IsNullOrEmpty(tem))
                    return def;
                return tem.Trim();
#else
                if (!string.IsNullOrEmpty(configPath))
                {
                    strKey = strKey.Trim().ToLower();
                    var config = ConfigurationManager.OpenExeConfiguration(string.IsNullOrEmpty(configPath) ? System.Reflection.Assembly.GetEntryAssembly().Location : configPath);
                    foreach (string key in config.AppSettings.Settings.AllKeys)
                    {
                        if (key.Trim().ToLower() == strKey)
                        {
                            return config.AppSettings.Settings[strKey].Value.ToString();
                        }
                    }
                }
                var tem = System.Configuration.ConfigurationManager.AppSettings[strKey];
                if (string.IsNullOrEmpty(tem))
                    return def;
                return tem.Trim();
#endif
            }
            catch { }
            return def;
        }
        #endregion
    }
}
