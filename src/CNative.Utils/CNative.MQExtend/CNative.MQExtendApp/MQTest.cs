﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CNative.MQExtend;
using CNative.MQExtend.Interface;
using CNative.MQExtend.Model.Config.Producer;

namespace CNative.MQExtendApp
{
    class MQTest
    {
        /// <summary>
        /// 消息队列生产者通道
        /// </summary>
        protected  IProducerChannel producerChannel { get; set; }
        /// <summary>
        /// 消息队列消费者通道
        /// </summary>
        protected  ISubscribeChannel subscribeChannel { get; set; }
        protected  ProducerConfigs producerConfigs { get; set; }
        public void Handle()
        {
            try
            {
                producerConfigs = new ProducerConfigs();
                producerChannel = ChannelAdapterFactory.GetProducerChannel();
                subscribeChannel = ChannelAdapterFactory.GetSubscribeChannel();

                subscribeChannel.Subscribe(producerConfigs.QueueName, SubscribeAction);

                producerChannel.Producer("测试消息队列"+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF"));
            }
            catch (Exception ex) { Console.WriteLine(ex + " Error"); }

        }

        #region SubscribeAction
        /// <summary>
        /// 收到排样信息
        /// </summary>
        /// <param name="messag"></param>
        protected  void SubscribeAction(CNative.MQExtend.Model.Message.IMessageContent messag)
        {
            try
            {
                Console.WriteLine("接收时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF") + "【" + messag?.Value+"】");
                //if (messag != null && !string.IsNullOrWhiteSpace(messag.Value))
                //{

                //    //var msg = messag.Value.JsonToObject<LISParser.Entity.InspectOrder>();
                //    //if (msg != null)
                //    //{
                //    lock (producerConfigs)
                //        {
                //           var msg = messag.Value;
                //            //mListSubscribe.Add(msg);

                //            //ii_times = 0;
                //            //ib_send = true;
                //            //is_status = 0;
                //            //ii_serial = 0;

                //            //var ls_serial = getSerial();
                //            //var ls_output = CheckSumOne(ls_serial + is_asking);

                //            //MessageChannel.Send(formatString(ls_output));
                //        }
                //    //}
                //    //else
                //    //{
                //    //    Console.WriteLine("SubscribeAction 反序列化失败=" + messag.Value);
                //    //}
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex + "SubscribeAction Error");
            }
        }
        #endregion
    }
}
