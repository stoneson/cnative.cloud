﻿using System;
using System.Collections.Generic;
using System.Text;
using CNative.MQExtend;

namespace CNative.MQExtendApp
{
    public class TTSend : IEventHandler<TT>
    {
        public void Handler(TT entity)
        {
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF")}:【你好{entity.Name},{entity.Msg}】");
        }
    }
    public class TTend : IEventHandler<TT>
    {
        public void Handler(TT entity)
        {
            Console.WriteLine($"EventSource,{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF")}:【{entity.EventSource}】");
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF")}:【消息发送完毕！{entity?.Name},{entity?.Msg}】");
        }
    }
}
