﻿using System;
using System.Collections.Generic;
using System.Text;
using CNative.MQExtend;

namespace CNative.MQExtendApp
{
    public class MailSend : IEventHandler<Notice>
    {
        public void Handler(Notice entity)
        {
            Console.WriteLine($"执行时间：{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF")}:【你好{entity.Name},{entity.Msg}】");
        }
    }
    public class Mailend : IEventHandler<Notice>
    {
        public void Handler(Notice entity)
        {
            Console.WriteLine($"执行时间：{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss FFFFF")}:【消息发送完毕！{entity?.Name},{entity?.Msg}】");
        }
    }
}
