using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnitTest461;
using CNative;
using CNative.Dapper.Utils;
//using CNative.DbUtils;
using System.Linq;
using CNative.Utilities;
using Dapper;

namespace NUnitTestCore
{
    public class NUnitTestCoreDB
    {
        private ISqlBuilder DRY2 = null;
        private ISqlBuilder sqlBuilder = null;
        //private CNative.DbUtils.IRepository drMysql = null;
        [SetUp]
        public void Setup()
        {
            DRY2 = new SqlBuilder("BaseDb");
            sqlBuilder = new SqlBuilder("BaseDb");
            //drMysql = new CNative.DbUtils.DbRepository("mysql");
        }
        [Test]
        public void Testlog()
        {
            var id = System.Guid.NewGuid().GetHashCode();
            CNative.Utilities.NlogHelper.Error("Error:" + id);
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };
            CNative.Utilities.NlogHelper.Debug("Debug:" + ent.ToJson());
            CNative.Utilities.NlogHelper.Info("Info:" + DateTime.Now);
            CNative.Utilities.NlogHelper.Fatal("Fatal:" + ent.ToJson());
            CNative.Utilities.NlogHelper.Warn("Warn:" + ent.ToJson());
        }

        [Test]
        public  void Testlog4()
        {
            var id = System.Guid.NewGuid().GetHashCode();
            CNative.Utilities.Log4NetHelper.Error("Error:" + id);
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };
            CNative.Utilities.Log4NetHelper.Debug("Debug:" + ent.ToJson());
            CNative.Utilities.Log4NetHelper.Info("Info:" + DateTime.Now);
            CNative.Utilities.Log4NetHelper.Fatal("Fatal:" + ent.ToJson());
            CNative.Utilities.Log4NetHelper.Warn("Warn:" + ent.ToJson());
        }
        [Test]
        public static void TestPssslBuilder()
        {
            //try
            //{
            //    var conn = new System.Data.SqlClient.SqlConnection("Data Source=.;Initial Catalog=test;Persist Security Info=True;User ID=sa;Password=123456");
            //    conn.Open();
            //    const string query = "select * from DM_KSXXB";
            //    var ss = SqlMapper.Query<Entity_DMKSXXB>(conn,query, null);
            //}
            //catch (TypeLoadException ex)
            //{
            //    var _ = ex;
            //}
            //catch (Exception ex)
            //{
            //    var _ = ex;
            //}
        }
        [Test]
        public void TestGetIQueryable()
        {
            var ss3 = String.Format("{0:0.##}", 123.4567);      // "123.46"
            var ss23 = String.Format("{0:0.##}", 123.4);         // "123.4"
            var ss233 = String.Format("{0:0.##}", 123.0);         // "123"

            var ent = new Entity_persons()
            { id = 100000001, adress = "Classify", name = "name", remark = "1,2,3" };
            var dt = DateTime.Now;
            var ss = "ss";
            var dd = new List<int>() { 2, 3, 45, 6 };
            var sql3 = DRY2.DoSelect.Where<Entity_persons>(g => g.name.In("1"));

            var sqlent = DRY2.CreateSqlEntity();
            var whe = ExpressionHelper.BuildWhere<Entity_persons>(sqlent,// t =>!("111" == t.name || t.id < 200+1)
                g => g.id.Between(10, 20) && g.id.In(dd) && (ss == "" || g.name == ss || g.id == 2) && g.adress == "dddd" && g.createTime == DateTime.Now && g.id < 200 + 1
                , true);

            var sql = DRY2.DoSelect.Where<Entity_persons>(g => g.id == 2 + 1 && (ss == "" || g.name == ss || g.id == 2) && g.adress == "dddd" && g.adress == ent.adress && g.id < 200 + 1);
            var sssds = DRY2.DoSelect.Query<Entity_persons>();
            var ssss = DRY2.DoSelect.Query<Entity_persons>();
            var sss = DRY2.DoSelect.Where<Entity_persons>(g => g.id < 20)
             .OrderBy<Entity_persons>(aa => aa.id)
             .OrderByDescending<Entity_persons>(aa => aa.name);

            var entpersons = new Entity_persons() { id = -3, name = "xjhsss", updateTime = DateTime.Now };
            DRY2.DoDelete.Delete(entpersons);
            DRY2.DoInsert.Insert(entpersons);
            DRY2.DoUpdate.Update(entpersons);
            DRY2.DoDelete.Delete(entpersons);
            DRY2.DoUpdate.Set<Entity_persons>(s => new Entity_persons { adress = "fadsfas" })
                .Where<Entity_persons>(f => f.id == 2);

            //var entpersons = new Entity_persons() { id = 3, name = "xjhsss" };
            //drMysql.Insert(entpersons);
            //drMysql.Update(entpersons);
            //drMysql.Delete(entpersons);
            //drMysql.UpdateWhere<Entity_persons>((es) =>
            //{
            //    es.name = "ffffss";
            //}, f => f.id == 2);
            //var tb = drMysql.QueryDataTable(new CNative.DbUtils.SqlEntity(drMysql.DBType) { Sql = "select * from `persons` " });
            //var myss = drMysql.GetList<Entity_persons>(null, null, null, 20);


            //var sssf = drsqLite.GetList<Entity_persons>(null, null, a => a.OrderBy(o => o.Id), 20);
            //if (sssf?.Count > 0)
            //{
            //    sssf[1].test = 333.32555m;
            //    drsqLite.Save(sssf[1]);
            //    drsqLite.Delete(sssf[0]);
            //    drsqLite.Execute(drsqLite.GetMergeSql(sssf[0]));
            //}
            var ssd = DateTime.MinValue;


        }

        [Test]
        public void TestInsertFrom()
        {
            var id = System.Guid.NewGuid().GetHashCode();
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };
            DRY2.DoDelete.Delete(ent);
            var ss = DRY2.DoInsert.Insert(ent);
            var guid = Guid.NewGuid();
            int[] arr = new int[] { 5, 8, 6 };
            string[] arr2 = new string[] { "1", "2", "3" };
            var sssff = new List<int>(arr);

            //LambdaToSqlHelper.GetInsertField<Entity_persons>(f => f.id == 2, f => f.name == "xjh");
            //return;
            //DRY2.InsertFrom<Entity_persons>("[dbo].[test]", w => 1 == 1, new Dictionary<string, object>()
            //{
            //    { "Remark", "testsssxjh" },{ "GuidId", Guid.NewGuid() }
            //});

            var sql = DRY2.DoSelect.Where<Entity_persons>(g =>
                1 == 3 || g.createTime == DateTime.Now || g.id == 4 && g.id.In(sssff) &&
                g.name.In(arr2) && (g.adress.Like("Clas%sify") || g.name.Equals("nam(e16'91780562")));

            // DRY2.InsertFrom<Entity_配置信息表>("[FGCPOE].[dbo].[配置信息表2]", w => w.Id.In(3,5,6), f=> 1 == 1 && f.Remark== "testsssxjh333"&& 1==2 && f.GuidId== Guid.NewGuid());
        }

      
        [Test]
        public void TestMongoDb()
        {
           // var dbh = DbFactory.CreateMongoDb("yapi");
           //var dd = dbh.Find<object>(null);
        }
        [Test]
        public void TestFastReflection()
        {
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };

            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getPublic"));
            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getInternal"));
            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getprotected"));
            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getPrivate", new object[] { null, "1" }));

            var ent2 = "Entity_persons2".FastInstance("xjh");
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getPublic"));
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getInternal"));
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getprotected"));
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getPrivate", new object[] { null, "1" }));
        }


        //单表操作测试
        [Test]
        public  void TestSelectSqlInBuilder1()
        {
            //var lst3 = sqlBuilder.doSelect<Entity_persons>()//取单行+排序
            //    .Fields(s => new { s.id, s.name, s.adress })//添加查询多个字段
            //    .Where(w => w.id==12 || w.name=="测试15")
            //    .OrderBy(d => d.id) //按列排序
            //    .Query(); //返回结果

            var sqle = sqlBuilder.CreateSqlEntity();
            sqle.Sql = @"SELECT *  FROM [test].dbo.[PERSONS]  WHERE  ([id] =@id OR [name] =@name)  ORDER BY [id] ASC ;
                        SELECT *  FROM [test].dbo.[PERSONS]  WHERE  ([id] =@id );
                        SELECT *  FROM [test].dbo.[PERSONS]  WHERE  ([name] =@name )";
            sqle.AddParameters(new { id = 12, name = "测试15" });
            var lst32 = sqlBuilder.Query<Entity_persons>(sqle);

            var ds2 = sqlBuilder.QueryDataSet(sqle);
            var dt2 = sqlBuilder.QueryDataTable(sqle);
        }

        [Test]
        public void TestSelectSqlBuilder1()
        {
            var lst = sqlBuilder.doSelect<Entity_persons>()//查询集合+排序+TOP
                 .Fields(s => new { s.id, s.name, s.adress })//添加查询多个字段
                 .Top(8)
                 // .Where(w => w.id.Between(10, 20))
                 .OrderByDescending(d => d.createTime)//按列倒向排序
                 .Query(); //返回结果

            var lst2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                 .Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                                                 // .Where(w => w.id.Between(10, 20))
                .OrderByDescending(d => d.updateTime) //按列倒向排序
                 .GetSingle<string>(); //返回结果


            var lst3 = sqlBuilder.doSelect<Entity_persons>()//取单行+排序
                .Fields(s => new { s.id, s.name, s.adress })//添加查询多个字段
                .Where(w => w.id.Between(11, 20))
                .OrderBy(d => d.id) //按列排序
                .QuerySingle(); //返回结果

            var lst4 = sqlBuilder.doSelect<Entity_persons>().Count(w => w.id.Between(11, 20)); //返回结果

        }

        [Test]
        public void TestInsertSqlBuilder1()
        {
            var jg = new Entity_DMJGXXB() { OrgId = 1002, jgdm = "56783", jgmc = "方舱" };

            var ret = sqlBuilder.doDelete<Entity_DMJGXXB>()//实体删除
                 .Delete(jg)
                 .Exec;
            ret = sqlBuilder.doInsert<Entity_DMJGXXB>()//插入实体
                .Insert(jg)
                .Execute();

            ret = sqlBuilder.doDelete<Entity_DMJGXXB>()//表达式删除
                 .Where(s => s.OrgId == 1003)
                 .Execute();
            ret = sqlBuilder.doInsert<Entity_DMJGXXB>()//表达式插入
                .Insert(new Entity_DMJGXXB()
                {
                    OrgId = 1003,
                    jgdm = "532236783",
                    jgmc = "方舱3",
                    CreateId = 0,
                    UpdateId = 0,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                })
                .Exec;

            //表复制
            var ret1 = sqlBuilder.doDelete<Entity_DMCZYJGDYB2>()
                    .Where(s => s.ksid == 1100453)
                    .Execute();

            var ret22 = sqlBuilder.doInsert<Entity_DMCZYJGDYB2>()//表复制
                .InsertSelect<Entity_DMCZYJGDYB>(s => new Entity_DMCZYJGDYB2()
                {
                    OrgId = s.OrgId,
                    czyid = s.czyid,
                    czyjgdyid = s.czyjgdyid,
                    czyxm = s.czyxm,
                    jgmc = "fasdfafdsaf",
                    ksid = s.ksid,
                    ksmc = s.ksmc
                }, s => s.ksid == 1100453)
                .Exec;
        }
        [Test]
        public void TestUpdateSqlBuilder1()
        {
            var jg = new Entity_DMJGXXB()
            {
                OrgId = 1003,
                jgdm = "5322336783",
                jgmc = "方舱32",
                CreateId = 0,
                UpdateId = 0,
                jgjpm = "7oipsafdasfd",
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            var ret = sqlBuilder.doUpdate<Entity_DMJGXXB>()
               .Set(s => new Entity_DMJGXXB()
               {
                   jgdm = "5322336783",
                   jgmc = "方舱232aaa",
                   CreateId = 0,
                   UpdateId = 0,
                   jgjpm = "ffff",
                   UpdateTime = DateTime.Now
               })
               .Where(s => s.OrgId == jg.OrgId)
               .Exec;

            jg.jgjpm = "345454";
            ret = sqlBuilder.DoUpdate
               .Update(jg, "jgjpm");

            var ret1 = sqlBuilder.doDelete<Entity_DMJGXXB>()
                  .Where(s => s.OrgId == 1005)
                  .Execute();

            //更新或插入
            var ret23 = sqlBuilder.DoUpdate
                .UpdateOrInsert(jg, w => w.OrgId == jg.OrgId);

            ret = sqlBuilder.doUpdate<Entity_DMJGXXB>()
                .UpdateOrInsert(s => new Entity_DMJGXXB()
                {
                    OrgId = 1006,
                    jgdm = "5322336783",
                    jgmc = "方舱232",
                    CreateId = 0,
                    UpdateId = 0,
                    jgjpm = "wsafdasfd",
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                }, w => w.OrgId == 1006);

        }

        //多表操作测试
        [Test]
        public void TestSelectFieldExprSqlBuilder1()
        {
            //查寻时加集合函数
            var ret1 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
                .Fields(f => new { czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where(s => s.ksid == 1100453)
                .GetSingle<object>();

            var ret2 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
               .Fields(f => new
               { f.czyid, f.czyxm, jgmc = f.jgmc.SQL_UCASE(), ksmc = f.jgmc.SQL_SUBSTR(2, 3) })
               //.Where(s => s.ksid == 1100453)
               .Where(s => s.ksid == 1100453 && s.jgmc.SQL_UCASE() == "FASDFAFDSAF")
               .Query();

            var ret3 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
                .Fields(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where(s => s.OrgId == 1001)
                .GroupBy(g => new { g.ksid, g.ksmc })
                .OrderBy(g => g.ksid)
                .Query();

            var ret4 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
                .Fields(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_COUNT(), czyid = f.czyjgdyid.SQL_MAX() })
                .Where(s => s.OrgId == 1001)
                .GroupBy(g => new { g.ksid, g.ksmc })
                .Having(h => h.czyjgdyid.SQL_COUNT() > 3 && h.czyjgdyid.SQL_MAX() > 0)
                .OrderBy(g => g.ksid)
                .Query();
        }

        [Test]
        public void TestSelectSqlBuilder()
        {
            var lst = sqlBuilder.DoSelect
                .From<Entity_persons>("a")
                 //.Fields<Entity_persons>()//添加查询多个字段
                 .Fields<Entity_persons>(s => new { s.id, s.name, adress = s.adress.SQL_NVL("无") })//添加查询多个字段
                 .Fields<Entity_persons>(s => s.adress, "a")// 添加查询单个字段带别名
                 .Top(8)
                 .Where<Entity_persons>(w => w.id.Between(10, 20))
                 .OrderByDescending<Entity_persons>(d => d.createTime)//按列倒向排序
                 .Query<Entity_persons>(); //返回结果

            var lst2 = sqlBuilder.DoSelect
                 .From<Entity_persons>()
                 .Fields("name AS ad")// 添加查询单个字段带别名
                 .Where<Entity_persons>(w => w.id.Between(10, 20))
                 .OrderByDescending<Entity_persons>(d => d.updateTime) //按列倒向排序
                 .GetSingle<string>(); //返回结果


            var lst3 = sqlBuilder.DoSelect
                .From<Entity_persons>()
                .Fields<Entity_persons>(s => new { s.id, s.name, s.adress })//添加查询多个字段
                .Where<Entity_persons>(w => w.id.Between(11, 20))
                .OrderBy<Entity_persons>(d => d.id) //按列排序
                .QuerySingle<Entity_persons>(); //返回结果

            var lst4 = sqlBuilder.DoSelect.Count<Entity_persons>(w => w.id.Between(11, 20)); //返回结果

        }


        [Test]
        public void TestSelectSqlBuilderJoin()
        {
            var lst = sqlBuilder.DoSelect
                 .From<Entity_DMKSXXB>("k")
                 .Top(8)
                 .Fields<Entity_DMKSXXB>(s => new { s.ksid, s.ksmc, s.ksdm, s.OrgId })//添加查询多个字段
                 .Fields("k.sjksid")// 添加查询单个字段带别名
                 .Fields<Entity_DMJGXXB>(s => new { s.jgjc, s.jgdm }, "j")
                 .Fields("j.jgmc")
                 .InnerJoin<Entity_DMKSXXB, Entity_DMJGXXB>((k, j) => k.OrgId == j.OrgId, "j")
                 .Where<Entity_DMKSXXB>(w => w.ksid.Between(1100440, 1100450))
                 .OrderByDescending<Entity_DMKSXXB>(d => d.ksid)//按列倒向排序
                 .Query<Entity_DMKSXXB>(); //返回结果

        }

        [Test]
        public void TestQueryPagingList()
        {
            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_DMKSXXB>()
                .Fields(s => new { s.ksid, s.ksmc, s.ksdm, s.OrgId })//添加查询多个字段
                .Fields("sjksid")// 添加查询单个字段带别名
                .Where(w => w.ksid.Between(1100440, 1100450))
                .OrderByDescending(d => d.ksid)//按列倒向排序
                .QueryPagingList(2, 10, ref totalNumber); //返回结果

            var pae = new PagingInfo() { PageNumber = 1, PageSize = 10 };
            var lst2 = sqlBuilder.DoSelect
                 .From<Entity_DMKSXXB>("k")
                 .Fields<Entity_DMKSXXB>(s => new { s.ksid, s.ksmc, s.ksdm, s.OrgId })//添加查询多个字段
                 .Fields("k.sjksid")// 添加查询单个字段带别名
                 .Fields<Entity_DMJGXXB>(s => new { s.jgjc, s.jgdm }, "j")
                 .Fields("j.jgmc")
                 .InnerJoin<Entity_DMKSXXB, Entity_DMJGXXB>((k, j) => k.OrgId == j.OrgId, "j")
                 .Where<Entity_DMKSXXB>(w => w.ksid.Between(1100440, 1100450))
                 .OrderByDescending<Entity_DMKSXXB>(d => d.ksid)//按列倒向排序
                 .QueryPagingList<Entity_DMKSXXB>(pae); //返回结果

            var ret3 = sqlBuilder.DoSelect
               .Fields<Entity_DMCZYJGDYB>(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_MAX() })
               .Where<Entity_DMCZYJGDYB>(s => s.OrgId == 1001)
               .GroupBy<Entity_DMCZYJGDYB>(g => new { g.ksid, g.ksmc })
               .OrderBy<Entity_DMCZYJGDYB>(g => g.ksid)
              .QueryPagingList<Entity_DMCZYJGDYB>(pae); //返回结果
        }

        [Test]
        public void TestInsertSqlBuilder()
        {
            var jg = new Entity_DMJGXXB() { OrgId = 1002, jgdm = "56783", jgmc = "方舱" };

            var ret = sqlBuilder.DoDelete
                 .Delete(jg)
                 .Exec;
            ret = sqlBuilder.DoInsert
                .Insert(jg)
                .Execute();

            ret = sqlBuilder.DoDelete
                 .Where<Entity_DMJGXXB>(s => s.OrgId == 1003)
                 .Execute();
            ret = sqlBuilder.DoInsert
                .Insert(new Entity_DMJGXXB()
                {
                    OrgId = 1003,
                    jgdm = "532236783",
                    jgmc = "方舱3",
                    CreateId = 0,
                    UpdateId = 0,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                })
                .Exec;


        }

        [Test]
        public void TestUpdateSqlBuilder()
        {
            var jg = new Entity_DMJGXXB()
            {
                OrgId = 1003,
                jgdm = "5322336783",
                jgmc = "方舱32",
                CreateId = 0,
                UpdateId = 0,
                jgjpm = "7oipsafdasfd",
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            var ret = sqlBuilder.DoUpdate
               .Set<Entity_DMJGXXB>(s => new Entity_DMJGXXB()
               {
                   jgdm = "5322336783",
                   jgmc = "方舱232aaa",
                   CreateId = 0,
                   UpdateId = 0,
                   jgjpm = "ffff",
                   UpdateTime = DateTime.Now
               })
               .Where<Entity_DMJGXXB>(s => s.OrgId == jg.OrgId)
               .Exec;
            jg.jgjpm = "345454";
            ret = sqlBuilder.DoUpdate
               .Update(jg, "jgjpm");

        }
        [Test]
        public void TestUpdateOrInsertSqlBuilder()
        {
            var ret1 = sqlBuilder.DoDelete
                     .Where<Entity_DMJGXXB>(s => s.OrgId == 1005)
                     .Execute();

            var jg = new Entity_DMJGXXB()
            {
                OrgId = 1005,
                jgdm = "5322336783",
                jgmc = "方舱54",
                CreateId = 0,
                UpdateId = 0,
                jgjpm = "fffss",
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            //jg.jgjpm = "345454";
            var ret = sqlBuilder.DoUpdate
                .UpdateOrInsert(jg, w => w.OrgId == jg.OrgId);

            ret = sqlBuilder.DoUpdate
                .UpdateOrInsert<Entity_DMJGXXB>(s => new Entity_DMJGXXB()
                {
                    OrgId = 1006,
                    jgdm = "5322336783",
                    jgmc = "方舱232",
                    CreateId = 0,
                    UpdateId = 0,
                    jgjpm = "wsafdasfd",
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                }, w => w.OrgId == 1006);
        }

        [Test]
        public void TestInsertSelectSqlBuilder()
        {
            var dt = sqlBuilder.DoSelect.GetDateNow();

            var ret1 = sqlBuilder.DoDelete
                     .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453)
                     .Execute();

            var ret = sqlBuilder.DoInsert
                .InsertSelect<Entity_DMCZYJGDYB2, Entity_DMCZYJGDYB>(s => new Entity_DMCZYJGDYB2()
                {
                    OrgId = s.OrgId,
                    czyid = s.czyid,
                    czyjgdyid = s.czyjgdyid,
                    czyxm = s.czyxm,
                    jgmc = "fasdfafdsaf",
                    ksid = s.ksid,
                    ksmc = s.ksmc
                }, s => s.ksid == 1100453)
                .Exec;
        }

        [Test]
        public void TestSelectFieldExprSqlBuilder()
        {
            var ret1 = sqlBuilder.DoSelect
                .Fields<Entity_DMCZYJGDYB2>(f => new { czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453)
                .GetSingle<object>();

            var ret2 = sqlBuilder.DoSelect
               .Fields<Entity_DMCZYJGDYB2>(f => new
               { f.czyid, f.czyxm, jgmc = f.jgmc.SQL_UCASE(), ksmc = f.jgmc.SQL_SUBSTR(2, 3) })
               .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453)
               .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453 && s.jgmc.SQL_UCASE() == "FASDFAFDSAF")
               .Query<Entity_DMCZYJGDYB2>();

            var ret3 = sqlBuilder.DoSelect
                .Fields<Entity_DMCZYJGDYB>(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where<Entity_DMCZYJGDYB>(s => s.OrgId == 1001)
                .GroupBy<Entity_DMCZYJGDYB>(g => new { g.ksid, g.ksmc })
                .OrderBy<Entity_DMCZYJGDYB>(g => g.ksid)
                .Query<Entity_DMCZYJGDYB>();

            var ret4 = sqlBuilder.DoSelect
                .From<Entity_DMCZYJGDYB>()
                .Fields<Entity_DMCZYJGDYB>(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_COUNT(), czyid = f.czyjgdyid.SQL_MAX() })
                .Where<Entity_DMCZYJGDYB>(s => s.OrgId == 1001)
                .GroupBy<Entity_DMCZYJGDYB>(g => new { g.ksid, g.ksmc })
                .Having<Entity_DMCZYJGDYB>(h => h.czyjgdyid.SQL_COUNT() > 3 && h.czyjgdyid.SQL_MAX() > 0)
                .OrderBy<Entity_DMCZYJGDYB>(g => g.ksid)
                .Query<Entity_DMCZYJGDYB>();
        }

        [Test]
        public void TestSqliteSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("Sqlite");
            var dt = sqlBuilder.DoSelect.GetDateNow();

            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_sysmenu>()
                 //.Fields(null)//添加查询多个字段
                 //.Fields("sjksid")// 添加查询单个字段带别名
                 //.Where(w => w.ksid.Between(1100440, 1100450))
                 .OrderBy(d => d.Id)//按列倒向排序
                 .QueryPagingList(2, 10, ref totalNumber); //返回结果
        }

        [Test]
        public void TestMySqlSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("MySql");
            var dt = sqlBuilder.DoSelect.GetDateNow();

            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_sysmenu>()
                 //.Fields(null)//添加查询多个字段
                 //.Fields("sjksid")// 添加查询单个字段带别名
                 //.Where(w => w.ksid.Between(1100440, 1100450))
                 .OrderBy(d => d.Id)//按列倒向排序
                 .QueryPagingList(3, 10, ref totalNumber); //返回结果

            var lst23 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                // .Where(w => w.id.Between(10, 20))
               .OrderBy(d => d.id) //按列倒向排序
               .QueryPagingList(3, 10, ref totalNumber); //返回结果
        }

        [Test]
        public void TestPostgreSqlSqlInBuilder1()
        {
            var sqlBuilder = new SqlBuilder("PostgreSql");
            var lst3 = sqlBuilder.doSelect<Entity_persons>()//取单行+排序
                .Fields(s => new { s.id, s.name, s.adress })//添加查询多个字段
                .Where(w => w.id.In( 12 ,6)|| w.name == "测试15")
                .OrderBy(d => d.id) //按列排序
                .Query(); //返回结果

            var sqle = sqlBuilder.CreateSqlEntity();
            sqle.Sql = @"SELECT *  FROM PERSONS  WHERE  (id =@id OR name =@name)  ORDER BY id ASC ;
                        SELECT *  FROM PERSONS  WHERE  (id =@id );
                        SELECT *  FROM PERSONS  WHERE  (name =@name )";
            sqle.AddParameters(new { id = 12, name = "测试15" });
            var lst32 = sqlBuilder.Query<Entity_persons>(sqle);

            var ds2 = sqlBuilder.QueryDataSet(sqle);
            var dt2 = sqlBuilder.QueryDataTable(sqle);
        }


        [Test]
        public void TestPostgreSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("PostgreSql");
            var dt = sqlBuilder.DoSelect.GetDateNow();

            var ss = sqlBuilder.doDelete<Entity_persons>()
                 .Where(w => w.id == -1)
                .Exec;

            var sf = sqlBuilder.doInsert<Entity_persons>()
                .Insert(f => new Entity_persons() { id = -1, name = "test", adress = "fs", age = 33, createTime = DateTime.Now, updateTime = DateTime.Now })
                .Exec;

            var sfs = sqlBuilder.doUpdate<Entity_persons>()
                .Set(f => new Entity_persons { remark = "remark" })
                .Where(w=>w.id==-1)
                .Exec;

            var lst2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                 .Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                //.Where(w => w.id.Between(10, 20))
                 .OrderByDescending(d => d.id) //按列倒向排序
                 .GetSingle<string>(); //返回结果

            long totalNumber = 0;
            var lst23 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                //.Where(w => w.id.Between(10, 20))
                .OrderBy(d => d.id) //按列倒向排序
                .QueryPagingList(3, 10, ref totalNumber); //返回结果

            var dt2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                                                           //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                                                           // .Where(w => w.id.Between(10, 20))
              .OrderByDescending(d => d.updateTime) //按列倒向排序
               .QueryDataTable(); //返回结果

            var ds2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                                                           //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                                                           // .Where(w => w.id.Between(10, 20))
              .OrderByDescending(d => d.updateTime) //按列倒向排序
               .QueryDataSet(); //返回结果
        }

        [Test]
        public void TestOracleSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("Oracle");
            var dt = sqlBuilder.DoSelect.GetDateNow();

            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_DRUGSTOCK>()
                 //.Fields(null)//添加查询多个字段
                 //.Fields("sjksid")// 添加查询单个字段带别名
                 .Where(w => w.STORAGE == "110003" || w.QUANTITY==400)
                 //.OrderBy(d => d.Id)//按列倒向排序
                 .QueryPagingList(1, 5, ref totalNumber); //返回结果

            var sqle = sqlBuilder.CreateSqlEntity();
            sqle.Sql = @"SELECT *  FROM DRUG_STOCK  WHERE  (STORAGE =:STORAGE OR QUANTITY=:QUANTITY )   ORDER BY QUANTITY ASC";
            sqle.AddParameters(new  { STORAGE = "110003", QUANTITY = 400m });
            //sqle.AddParameter("STORAGE", "110003", System.Data.DbType.String);
            //sqle.AddParameter("QUANTITY", 400, System.Data.DbType.Int64);

            var lst32 = sqlBuilder.Query<Entity_DRUGSTOCK>(sqle);
            var ds2 = sqlBuilder.QueryDataSet(sqle);
            var dt2 = sqlBuilder.QueryDataTable(sqle);

            //var lst23 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
            //                                                 //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
            //                                                 // .Where(w => w.id.Between(10, 20))
            //   .OrderBy(d => d.id) //按列倒向排序
            //   .QueryPagingList(3, 10, ref totalNumber); //返回结果
        }

        [Test]
        public void TestMsAccessSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("MsAccess2");
            var ping = sqlBuilder.Ping();

            var dt = sqlBuilder.DoSelect.GetDateNow();

            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_Orders>()
                 //.Fields(null)//添加查询多个字段
                 //.Where(w => w.ksid.Between(1100440, 1100450))
                 .OrderBy(d => d.OrderID)//按列倒向排序
                                         //.Query();
                 .QueryPagingList(3, 10, ref totalNumber); //返回结果

            var order_Details = new Entity_Order_Details() { OrderID = 10268, Discount = 32.1f, ProductID = 72, Quantity = 100, UnitPrice = 266.33M };
            //var ret = sqlBuilder.doDelete<Entity_Order_Details>()//实体删除
            //     .Delete(order_Details)
            //     .Exec;
            var ret1 = sqlBuilder.doUpdate<Entity_Order_Details>()//插入实体
               .Update(order_Details)
               ;
            //ret = sqlBuilder.doInsert<Entity_Order_Details>()//插入实体
            //    .Insert(order_Details)
            //    .Execute();

            var lst3 = sqlBuilder.doSelect<Entity_Order_Details>()
                 //.Fields(null)//添加查询多个字段
                 .Where(w => w.OrderID == 10268)                                         //.Query();
                 .Query(); //返回结果
        }

    }
}