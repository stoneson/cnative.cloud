﻿using CNative.Dapper.Utils;
//using CNative.DbUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest461
{
    [TableMap(Schema = "RSA", TableName = "test")]
    public class Entity_test
    {
        /// <summary>
        /// 
        /// </summary>
        [DbFieldMap(CloumnName = "ID", Type = "double", IsNullable = false, IsPrimaryKey = true)]
        public double id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DbFieldMap(CloumnName = "NAME")] 
        public string name { get; set; }

    }
    [TableMap(Schema = "test", TableName = "persons")]
    public class Entity_persons
    {
        #region 构造函数

        //public Entity_persons()
        //{ }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        [DbFieldMap(CloumnName = "id", Type = "int", IsNullable = false, IsPrimaryKey = true)]
        public int id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }

        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

        public int age { get; set; }

        public string adress { get; set; }
        public string remark { get; set; }


        public string getPublic()
        {
            return "getPublic";
        }
        internal string getInternal()
        {
            return "getInternal";
        }
        protected string getProtected()
        {
            return "getProtected";
        }
        private string getPrivate()
        {
            return "getPrivate";
        }

        private string getPrivate(string str = "", string str1 = "")
        {
            return "getPrivatestrstr1";
        }
        private string getPrivate(string str = "", int? id = 0)
        {
            return "getPrivatestrid";
        }
    }

    public class Entity_persons2 : Entity_persons
    {
        #region 构造函数

        public Entity_persons2(string _name)
        { name = _name; }
        #endregion


        public new string getPublic()
        {
            return name + " getPublic";
        }
        internal new string getInternal()
        {
            return name + " getInternal";
        }
        protected new string getProtected()
        {
            return name + " getProtected";
        }
        private string getPrivate()
        {
            return name + " getPrivate";
        }

        private string getPrivate(string str = "", string str1 = "")
        {
            return name + " getPrivatestrstr1";
        }
        private string getPrivate(string str = "", int? id = 0)
        {
            return name + " getPrivatestrid";
        }
    }

    [TableMap(Schema = "", TableName = "DM_JGXXB")]
    public class Entity_DMJGXXB
    {
        #region 构造函数

        public Entity_DMJGXXB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// OrgId
        /// </summary>
        [DbFieldMap(CloumnName = "OrgId", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int OrgId { get; set; }
        /// <summary>
        /// jgdm
        /// </summary>
        public string jgdm { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        /// <summary>
        /// jgjc
        /// </summary>
        public string jgjc { get; set; }
        /// <summary>
        /// sjjgid
        /// </summary>
        public int sjjgid { get; set; }
        /// <summary>
        /// jgjpm
        /// </summary>
        public string jgjpm { get; set; }
        /// <summary>
        /// yhzh
        /// </summary>
        public string yhzh { get; set; }
        /// <summary>
        /// yhmc
        /// </summary>
        public string yhmc { get; set; }
        public string test { get; set; }
        /// <summary>
        /// zt
        /// </summary>
        public int zt { get; set; }
        /// <summary>
        /// bz
        /// </summary>
        public string bz { get; set; }
        /// <summary>
        /// CreateId
        /// </summary>
        public int CreateId { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// UpdateId
        /// </summary>
        public int UpdateId { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_KSXXB")]
    public class Entity_DMKSXXB
    {
        #region 构造函数

        public Entity_DMKSXXB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// ksid
        /// </summary>
        [DbFieldMap(CloumnName = "ksid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int ksid { get; set; }
        /// <summary>
        /// sjksid
        /// </summary>
        public int sjksid { get; set; }
        /// <summary>
        /// ksjc
        /// </summary>
        public string ksjc { get; set; }
        /// <summary>
        /// ksmc
        /// </summary>
        public string ksmc { get; set; }
        /// <summary>
        /// ksdm
        /// </summary>
        public string ksdm { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// CreateID
        /// </summary>
        public int CreateID { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// UpdateID
        /// </summary>
        public int UpdateID { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        #endregion

        /// <summary>
        /// jgdm
        /// </summary>
        public string jgdm { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        /// <summary>
        /// jgjc
        /// </summary>
        public string jgjc { get; set; }
    }
    [TableMap(Schema = "test", TableName = "DM_CZYXXB")]
    public class Entity_DMCZYXXB
    {
        #region 构造函数

        public Entity_DMCZYXXB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// czyid
        /// </summary>
        [DbFieldMap(CloumnName = "czyid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int czyid { get; set; }
        /// <summary>
        /// czygh
        /// </summary>
        public string czygh { get; set; }
        /// <summary>
        /// czyxm
        /// </summary>
        public string czyxm { get; set; }
        /// <summary>
        /// xb
        /// </summary>
        public int xb { get; set; }
        /// <summary>
        /// CreateId
        /// </summary>
        public int CreateId { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// UpdateId
        /// </summary>
        public int UpdateId { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// srm
        /// </summary>
        public string srm { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_CZYJGDYB")]
    public class Entity_DMCZYJGDYB
    {
        #region 构造函数

        public Entity_DMCZYJGDYB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// czyjgdyid
        /// </summary>
        [DbFieldMap(CloumnName = "czyjgdyid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int czyjgdyid { get; set; }
        /// <summary>
        /// czyid
        /// </summary>
        public int czyid { get; set; }
        /// <summary>
        /// czyxm
        /// </summary>
        public string czyxm { get; set; }
        /// <summary>
        /// ksid
        /// </summary>
        public int ksid { get; set; }
        /// <summary>
        /// ksmc
        /// </summary>
        public string ksmc { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_CZYJGDYB2")]
    public class Entity_DMCZYJGDYB2
    {
        #region 构造函数

        public Entity_DMCZYJGDYB2()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// czyjgdyid
        /// </summary>
        [DbFieldMap(CloumnName = "czyjgdyid", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int czyjgdyid { get; set; }
        /// <summary>
        /// czyid
        /// </summary>
        public int czyid { get; set; }
        /// <summary>
        /// czyxm
        /// </summary>
        public string czyxm { get; set; }
        /// <summary>
        /// ksid
        /// </summary>
        public int ksid { get; set; }
        /// <summary>
        /// ksmc
        /// </summary>
        public string ksmc { get; set; }
        /// <summary>
        /// OrgId
        /// </summary>
        public int OrgId { get; set; }
        /// <summary>
        /// jgmc
        /// </summary>
        public string jgmc { get; set; }
        #endregion
    }
    [TableMap(Schema = "test", TableName = "DM_GJDMB")]
    public class Entity_DMGJDMB
    {
        #region 构造函数

        public Entity_DMGJDMB()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// id
        /// </summary>
        [DbFieldMap(CloumnName = "id", Type = "int", MaxLength = 4, IsNullable = false, IsPrimaryKey = true)]
        public int id { get; set; }
        /// <summary>
        /// gjmc
        /// </summary>
        public string gjmc { get; set; }
        /// <summary>
        /// gjjc
        /// </summary>
        public string gjjc { get; set; }
        /// <summary>
        /// gjjpm
        /// </summary>
        public string gjjpm { get; set; }
        /// <summary>
        /// gjwbm
        /// </summary>
        public string gjwbm { get; set; }
        /// <summary>
        /// xsxh
        /// </summary>
        public int xsxh { get; set; }
        /// <summary>
        /// bz
        /// </summary>
        public string bz { get; set; }
        #endregion
    }

    [TableMap(Schema = "", TableName = "sys_menu")]
    public class Entity_sysmenu
    {
        #region 构造函数
        public Entity_sysmenu()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// Id
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Pid
        /// </summary>
        public Int64 Pid { get; set; }
        /// <summary>
        /// Pids
        /// </summary>
        public string Pids { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// Icon
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// Router
        /// </summary>
        public string Router { get; set; }
        /// <summary>
        /// Component
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// Permission
        /// </summary>
        public string Permission { get; set; }
        /// <summary>
        /// Application
        /// </summary>
        public string Application { get; set; }
        /// <summary>
        /// OpenType
        /// </summary>
        public int OpenType { get; set; }
        /// <summary>
        /// Visible
        /// </summary>
        public string Visible { get; set; }
        /// <summary>
        /// Link
        /// </summary>
        public string Link { get; set; }
        /// <summary>
        /// Redirect
        /// </summary>
        public string Redirect { get; set; }
        /// <summary>
        /// Weight
        /// </summary>
        public int Weight { get; set; }
        /// <summary>
        /// Sort
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// CreatedTime
        /// </summary>
        public string CreatedTime { get; set; }
        /// <summary>
        /// UpdatedTime
        /// </summary>
        public string UpdatedTime { get; set; }
        /// <summary>
        /// CreatedUserId
        /// </summary>
        public int? CreatedUserId { get; set; }
        /// <summary>
        /// CreatedUserName
        /// </summary>
        public string CreatedUserName { get; set; }
        /// <summary>
        /// UpdatedUserId
        /// </summary>
        public int? UpdatedUserId { get; set; }
        /// <summary>
        /// UpdatedUserName
        /// </summary>
        public string UpdatedUserName { get; set; }
        /// <summary>
        /// IsDeleted
        /// </summary>
        public int IsDeleted { get; set; }
        #endregion
    }

    [TableMap(Schema = "PHARMACY", TableName = "DRUG_STOCK")]
    public class Entity_DRUGSTOCK
    {
        #region 构造函数

        public Entity_DRUGSTOCK()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// 库存管理单位：库房代码，见库存单位字典
        /// </summary>
        [DbFieldMap(CloumnName = "STORAGE", Type = "VARCHAR2", MaxLength = 8, IsNullable = false, IsPrimaryKey = true, Description = "库存管理单位：库房代码，见库存单位字典")]
        public string STORAGE { get; set; }
        /// <summary>
        /// 药品代码：由药品字典定义的代码
        /// </summary>
        [DbFieldMap(CloumnName = "DRUG_CODE", Type = "VARCHAR2", MaxLength = 10, IsNullable = false, IsPrimaryKey = true, Description = "药品代码：由药品字典定义的代码")]
        public string DRUGCODE { get; set; }
        /// <summary>
        /// 规格：由药品字典定义的规格
        /// </summary>
        [DbFieldMap(CloumnName = "DRUG_SPEC", Type = "VARCHAR2", MaxLength = 20, IsNullable = false, IsPrimaryKey = true, Description = "规格：由药品字典定义的规格")]
        public string DRUGSPEC { get; set; }
        /// <summary>
        /// 单位：对应剂型及规格，使用规范名称，见4.32计量单位字典
        /// </summary>
        public string UNITS { get; set; }
        /// <summary>
        /// 批号：使用“XX/XX/XXXXXX”
        /// </summary>
        [DbFieldMap(CloumnName = "BATCH_NO", Type = "VARCHAR2", MaxLength = 16, IsNullable = false, IsPrimaryKey = true, Description = "批号：使用“XX/XX/XXXXXX”")]
        public string BATCHNO { get; set; }
        /// <summary>
        /// 有效期：药品的有效截止日期
        /// </summary>
        [DbFieldMap(CloumnName = "EXPIRE_DATE", Type = "DATE", MaxLength = 7, Description = "有效期：药品的有效截止日期")]
        public DateTime? EXPIREDATE { get; set; }
        /// <summary>
        /// 厂家标识：反映生产厂家，见药品生产厂家字典
        /// </summary>
        [DbFieldMap(CloumnName = "FIRM_ID", Type = "VARCHAR2", MaxLength = 10, IsNullable = false, IsPrimaryKey = true, Description = "厂家标识：反映生产厂家，见药品生产厂家字典")]
        public string FIRMID { get; set; }
        /// <summary>
        /// 进货价：购买价，以包装单位记单价
        /// </summary>
        [DbFieldMap(CloumnName = "PURCHASE_PRICE", Type = "NUMBER", MaxLength = 22, Description = "进货价：购买价，以包装单位记单价")]
        public decimal PURCHASEPRICE { get; set; }
        /// <summary>
        /// 折扣：该药品购入时的折扣率。百分数，只记录数值部分
        /// </summary>
        public decimal DISCOUNT { get; set; }
        /// <summary>
        /// 包装规格：反映药品含量及包装信息，如0.25g*30
        /// </summary>
        [DbFieldMap(CloumnName = "PACKAGE_SPEC", Type = "VARCHAR2", MaxLength = 20, IsNullable = false, IsPrimaryKey = true, Description = "包装规格：反映药品含量及包装信息，如0.25g*30")]
        public string PACKAGESPEC { get; set; }
        /// <summary>
        /// 数量：以包装规格及包装单位所计的现库存数量，每次出库，该数量核减
        /// </summary>
        public decimal QUANTITY { get; set; }
        /// <summary>
        /// 包装单位：对应包装规格的计量单位，可使用任一级管理上方便的包装
        /// </summary>
        [DbFieldMap(CloumnName = "PACKAGE_UNITS", Type = "VARCHAR2", MaxLength = 8, Description = "包装单位：对应包装规格的计量单位，可使用任一级管理上方便的包装")]
        public string PACKAGEUNITS { get; set; }
        /// <summary>
        /// 存放包装1：上述一个包装单位中包含的小包装数量，为空或1表示为无此级包装
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_1", Type = "NUMBER", MaxLength = 22, Description = "存放包装1：上述一个包装单位中包含的小包装数量，为空或1表示为无此级包装")]
        public decimal SUBPACKAGE1 { get; set; }
        /// <summary>
        /// 存放包装1单位：对应内含包装1的单位
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_UNITS_1", Type = "VARCHAR2", MaxLength = 8, Description = "存放包装1单位：对应内含包装1的单位")]
        public string SUBPACKAGEUNITS1 { get; set; }
        /// <summary>
        /// 存放包装1规格：对应内含包装1的规格
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_SPEC_1", Type = "VARCHAR2", MaxLength = 20, Description = "存放包装1规格：对应内含包装1的规格")]
        public string SUBPACKAGESPEC1 { get; set; }
        /// <summary>
        /// 存放包装2：内含包装1中包含的小包装数量，为空或1表示为无此级包装
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_2", Type = "NUMBER", MaxLength = 22, Description = "存放包装2：内含包装1中包含的小包装数量，为空或1表示为无此级包装")]
        public decimal SUBPACKAGE2 { get; set; }
        /// <summary>
        /// 存放包装2单位：对应内含包装2的单位
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_UNITS_2", Type = "VARCHAR2", MaxLength = 8, Description = "存放包装2单位：对应内含包装2的单位")]
        public string SUBPACKAGEUNITS2 { get; set; }
        /// <summary>
        /// 存放包装2规格：对应内含包装2的规格
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_PACKAGE_SPEC_2", Type = "VARCHAR2", MaxLength = 20, Description = "存放包装2规格：对应内含包装2的规格")]
        public string SUBPACKAGESPEC2 { get; set; }
        /// <summary>
        /// 存放库房：一个库存管理单位内的存放库房
        /// </summary>
        [DbFieldMap(CloumnName = "SUB_STORAGE", Type = "VARCHAR2", MaxLength = 8, Description = "存放库房：一个库存管理单位内的存放库房")]
        public string SUBSTORAGE { get; set; }
        /// <summary>
        /// 货位：描述存放该批药品的位置，自由描述
        /// </summary>
        public string LOCATION { get; set; }
        /// <summary>
        /// 入库单号：该药品对应的入库单号，当多次入库的药品合并记录时，该项为空
        /// </summary>
        [DbFieldMap(CloumnName = "DOCUMENT_NO", Type = "VARCHAR2", MaxLength = 10, Description = "入库单号：该药品对应的入库单号，当多次入库的药品合并记录时，该项为空")]
        public string DOCUMENTNO { get; set; }
        /// <summary>
        /// 供应标志：反映该药品当前是否可供使用，0-不可供 1-可供
        /// </summary>
        [DbFieldMap(CloumnName = "SUPPLY_INDICATOR", Type = "NUMBER", MaxLength = 22, Description = "供应标志：反映该药品当前是否可供使用，0-不可供 1-可供")]
        public decimal SUPPLYINDICATOR { get; set; }
        /// <summary>
        /// 说明书
        /// </summary>
        public string INSTRUCTION { get; set; }
        #endregion


    }

    [TableMap(Schema = "", TableName = "Orders")]
    public class Entity_Orders
    {
        #region 构造函数

        public Entity_Orders()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// Same entry as in Customers table.
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// Same entry as in Employees table.
        /// </summary>
        public int EmployeeID { get; set; }
        /// <summary>
        /// Freight
        /// </summary>
        public decimal Freight { get; set; }
        /// <summary>
        /// OrderDate
        /// </summary>
        public DateTime? OrderDate { get; set; }
        /// <summary>
        /// Unique order number.
        /// </summary>
        [DbFieldMap(CloumnName = "OrderID", Type = "Integer", IsNullable = false, IsPrimaryKey = true, Description = "Unique order number.")]
        public int OrderID { get; set; }
        /// <summary>
        /// RequiredDate
        /// </summary>
        public DateTime? RequiredDate { get; set; }
        /// <summary>
        /// Street address only -- no post-office box allowed.
        /// </summary>
        public string ShipAddress { get; set; }
        /// <summary>
        /// ShipCity
        /// </summary>
        public string ShipCity { get; set; }
        /// <summary>
        /// ShipCountry
        /// </summary>
        public string ShipCountry { get; set; }
        /// <summary>
        /// Name of person or company to receive the shipment.
        /// </summary>
        public string ShipName { get; set; }
        /// <summary>
        /// ShippedDate
        /// </summary>
        public DateTime? ShippedDate { get; set; }
        /// <summary>
        /// ShipPostalCode
        /// </summary>
        public string ShipPostalCode { get; set; }
        /// <summary>
        /// State or province.
        /// </summary>
        public string ShipRegion { get; set; }
        /// <summary>
        /// Same as Shipper ID in Shippers table.
        /// </summary>
        public int ShipVia { get; set; }
        #endregion


        #region 属性
        /// <summary>
        /// Discount
        /// </summary>
        public Single Discount { get; set; }
        /// <summary>
        /// Same as Product ID in Products table.
        /// </summary>
        public int ProductID { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// UnitPrice
        /// </summary>
        public decimal UnitPrice { get; set; }
        #endregion
    }


    [TableMap(Schema = "", TableName = "Order Details")]
    public class Entity_Order_Details
    {
        #region 构造函数

        public Entity_Order_Details()
        { }

        #endregion

        #region 属性
        /// <summary>
        /// OrderID
        /// </summary>
        [DbFieldMap(CloumnName = "OrderID", Type = "Integer", IsNullable = false, IsPrimaryKey = true)]
        public long OrderID { get; set; }
        /// <summary>
        /// ProductID
        /// </summary>
        [DbFieldMap(CloumnName = "ProductID", Type = "Integer", IsNullable = false, IsPrimaryKey = true)]
        public long ProductID { get; set; }
        /// <summary>
        /// UnitPrice
        /// </summary>
        public decimal UnitPrice { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// Discount
        /// </summary>
        public float Discount { get; set; }

        #endregion

    }

    [TableMap(Schema = "", TableName = "UriToLis")]
    public class AVEMessageModel
    {
        #region 构造函数

        public AVEMessageModel()
        { }

        #endregion
        #region 属性
        /// <summary>
        /// 样液号
        /// </summary>
        [DbFieldMap(CloumnName = "SickSampleID", Type = "nvarchar", MaxLength = 100, IsNullable = false, IsPrimaryKey = true, Description = "样液号")]
        public string SickSampleID { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime SickDate { get; set; }
        /// <summary>
        /// 样本号
        /// </summary>
        public string SickExamineID { get; set; }
        /// <summary>
        /// 尿液序号
        /// </summary>
        public string UriSeqID { get; set; }
        /// <summary>
        /// 干化序号
        /// </summary>
        public string ChemSeqID { get; set; }
        /// <summary>
        /// 理学序号
        /// </summary>
        public string PhySeqID { get; set; }
        /// <summary>
        /// 条码编号
        /// </summary>
        public string BarCode { get; set; }
        /// <summary>
        /// 扩展类型
        /// </summary>
        public string ExtendType { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// UpdateFlag
        /// </summary>
        public int UpdateFlag { get; set; }
        /// <summary>
        /// 住院号
        /// </summary>
        public string InpatientNum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string SickName { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public string Age { get; set; }
        /// <summary>
        /// 年龄单位
        /// </summary>
        public int AgeUnit { get; set; }
        /// <summary>
        /// 科室名
        /// </summary>
        public string SectionNum { get; set; }
        /// <summary>
        /// 病床号
        /// </summary>
        public string SickbedNum { get; set; }
        /// <summary>
        /// 病历号
        /// </summary>
        public string MedicalNum { get; set; }
        /// <summary>
        /// 检验目的
        /// </summary>
        public string CheckDist { get; set; }
        /// <summary>
        /// 临床诊断
        /// </summary>
        public string Result { get; set; }
        /// <summary>
        /// 样本状态
        /// </summary>
        public string SwatchState { get; set; }
        /// <summary>
        /// 送检者
        /// </summary>
        public string Sender { get; set; }
        /// <summary>
        /// 检测者
        /// </summary>
        public string Verifier { get; set; }
        /// <summary>
        /// 审核者
        /// </summary>
        public string Assessor { get; set; }
        /// <summary>
        /// 采样时间
        /// </summary>
        public DateTime? SampleDate { get; set; }
        /// <summary>
        /// 送检时间
        /// </summary>
        public DateTime? CheckDate { get; set; }
        /// <summary>
        /// 报告时间
        /// </summary>
        public DateTime? ReportDate { get; set; }
        /// <summary>
        /// LastAuditTime
        /// </summary>
        public DateTime? LastAuditTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
        public string ColorSet { get; set; }
        /// <summary>
        /// 浊度
        /// </summary>
        public string MuddySet { get; set; }
        /// <summary>
        /// GravitySet
        /// </summary>
        public string GravitySet { get; set; }
        /// <summary>
        /// 电导率
        /// </summary>
        public string Conduct { get; set; }
        /// <summary>
        /// 已离心
        /// </summary>
        public bool IsCondense { get; set; }
        /// <summary>
        /// 已稀释
        /// </summary>
        public bool IsDilute { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// 样液号
        /// </summary>
        public string UriSampleID { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime? UriDate { get; set; }
        /// <summary>
        /// 样本号
        /// </summary>
        public string UriExamineID { get; set; }
        /// <summary>
        /// 镜检类型标志
        /// </summary>
        public string UriExamType { get; set; }
        /// <summary>
        /// 试管架号
        /// </summary>
        public int UriTubeRackNo { get; set; }
        /// <summary>
        /// 试管号
        /// </summary>
        public int UriTubeNo { get; set; }
        /// <summary>
        /// UriBarCode
        /// </summary>
        public string UriBarCode { get; set; }
        /// <summary>
        /// 工作类型
        /// </summary>
        public int UriExperType { get; set; }
        /// <summary>
        /// 图片打印路径
        /// </summary>
        public string UriPrintPath { get; set; }
        /// <summary>
        /// 打印图片名
        /// </summary>
        public string PrintImage { get; set; }
        /// <summary>
        /// 红细胞
        /// </summary>
        public int Rbc { get; set; }
        /// <summary>
        /// 红细胞参照值
        /// </summary>
        public string RbcRef { get; set; }
        /// <summary>
        /// RbcPF
        /// </summary>
        public string RbcPF { get; set; }
        /// <summary>
        /// 白细胞
        /// </summary>
        public int Wbc { get; set; }
        /// <summary>
        /// 白细胞参照值
        /// </summary>
        public string WbcRef { get; set; }
        /// <summary>
        /// WbcPF
        /// </summary>
        public string WbcPF { get; set; }
        /// <summary>
        /// 结晶
        /// </summary>
        public int Crystal { get; set; }
        /// <summary>
        /// 结晶参照值
        /// </summary>
        public string CrystalRef { get; set; }
        /// <summary>
        /// CrystalPF
        /// </summary>
        public string CrystalPF { get; set; }
        /// <summary>
        /// 真菌
        /// </summary>
        public int Epiphyte { get; set; }
        /// <summary>
        /// 真菌参照值
        /// </summary>
        public string EpiphyteRef { get; set; }
        /// <summary>
        /// EpiphytePF
        /// </summary>
        public string EpiphytePF { get; set; }
        /// <summary>
        /// 管型
        /// </summary>
        public int Duct { get; set; }
        /// <summary>
        /// 管型参照值
        /// </summary>
        public string DuctRef { get; set; }
        /// <summary>
        /// DuctPF
        /// </summary>
        public string DuctPF { get; set; }
        /// <summary>
        /// 上皮细胞
        /// </summary>
        public int Epithelia { get; set; }
        /// <summary>
        /// 上皮细胞参照值
        /// </summary>
        public string EpitheliaRef { get; set; }
        /// <summary>
        /// EpitheliaPF
        /// </summary>
        public string EpitheliaPF { get; set; }
        /// <summary>
        /// 正常红细胞
        /// </summary>
        public int WhackRbc { get; set; }
        /// <summary>
        /// 正常红细胞参照值
        /// </summary>
        public string WhackRbcRef { get; set; }
        /// <summary>
        /// WhackRbcPF
        /// </summary>
        public string WhackRbcPF { get; set; }
        /// <summary>
        /// 异常红细胞
        /// </summary>
        public int SingularRbc { get; set; }
        /// <summary>
        /// 异常红细胞参照值
        /// </summary>
        public string SingularRbcRef { get; set; }
        /// <summary>
        /// SingularRbcPF
        /// </summary>
        public string SingularRbcPF { get; set; }
        /// <summary>
        /// 滴虫
        /// </summary>
        public int Infusorial { get; set; }
        /// <summary>
        /// 滴虫参照值
        /// </summary>
        public string InfusorialRef { get; set; }
        /// <summary>
        /// InfusorialPF
        /// </summary>
        public string InfusorialPF { get; set; }
        /// <summary>
        /// 粘液丝
        /// </summary>
        public int Mucous { get; set; }
        /// <summary>
        /// 粘液丝参照值
        /// </summary>
        public string MucousRef { get; set; }
        /// <summary>
        /// MucousPF
        /// </summary>
        public string MucousPF { get; set; }
        /// <summary>
        /// 脓球
        /// </summary>
        public int Pyoid { get; set; }
        /// <summary>
        /// 脓球参照值
        /// </summary>
        public string PyoidRef { get; set; }
        /// <summary>
        /// PyoidPF
        /// </summary>
        public string PyoidPF { get; set; }
        /// <summary>
        /// 尿酸盐结晶
        /// </summary>
        public int NCrystal { get; set; }
        /// <summary>
        /// 尿酸盐结晶参照值
        /// </summary>
        public string NCrystalRef { get; set; }
        /// <summary>
        /// NCrystalPF
        /// </summary>
        public string NCrystalPF { get; set; }
        /// <summary>
        /// 磷酸盐结晶
        /// </summary>
        public int LCrystal { get; set; }
        /// <summary>
        /// 磷酸盐结晶参照值
        /// </summary>
        public string LCrystalRef { get; set; }
        /// <summary>
        /// LCrystalPF
        /// </summary>
        public string LCrystalPF { get; set; }
        /// <summary>
        /// 磺胺类结晶
        /// </summary>
        public int HCrystal { get; set; }
        /// <summary>
        /// 磺胺类结晶参照值
        /// </summary>
        public string HCrystalRef { get; set; }
        /// <summary>
        /// HCrystalPF
        /// </summary>
        public string HCrystalPF { get; set; }
        /// <summary>
        /// 草酸盐结晶
        /// </summary>
        public int CCrystal { get; set; }
        /// <summary>
        /// 草酸盐结晶参照值
        /// </summary>
        public string CCrystalRef { get; set; }
        /// <summary>
        /// CCrystalPF
        /// </summary>
        public string CCrystalPF { get; set; }
        /// <summary>
        /// 无机盐结晶
        /// </summary>
        public int WCrystal { get; set; }
        /// <summary>
        /// 无机盐结晶参照值
        /// </summary>
        public string WCrystalRef { get; set; }
        /// <summary>
        /// WCrystalPF
        /// </summary>
        public string WCrystalPF { get; set; }
        /// <summary>
        /// 红细胞管型
        /// </summary>
        public int RbcDuct { get; set; }
        /// <summary>
        /// 红细胞管型参照值
        /// </summary>
        public string RbcDuctRef { get; set; }
        /// <summary>
        /// RbcDuctPF
        /// </summary>
        public string RbcDuctPF { get; set; }
        /// <summary>
        /// 白细胞管型
        /// </summary>
        public int WbcDuct { get; set; }
        /// <summary>
        /// 白细胞管型参照值
        /// </summary>
        public string WbcDuctRef { get; set; }
        /// <summary>
        /// WbcDuctPF
        /// </summary>
        public string WbcDuctPF { get; set; }
        /// <summary>
        /// 颗粒管型
        /// </summary>
        public int KDuct { get; set; }
        /// <summary>
        /// 颗粒管型参照值
        /// </summary>
        public string KDuctRef { get; set; }
        /// <summary>
        /// KDuctPF
        /// </summary>
        public string KDuctPF { get; set; }
        /// <summary>
        /// 透明管型
        /// </summary>
        public int TDuct { get; set; }
        /// <summary>
        /// 透明管型参照值
        /// </summary>
        public string TDuctRef { get; set; }
        /// <summary>
        /// TDuctPF
        /// </summary>
        public string TDuctPF { get; set; }
        /// <summary>
        /// 腊样管型
        /// </summary>
        public int LDuct { get; set; }
        /// <summary>
        /// 腊样管型参照值
        /// </summary>
        public string LDuctRef { get; set; }
        /// <summary>
        /// LDuctPF
        /// </summary>
        public string LDuctPF { get; set; }
        /// <summary>
        /// 鳞状上皮细胞
        /// </summary>
        public int LEpithelia { get; set; }
        /// <summary>
        /// 鳞状上皮细胞参照值
        /// </summary>
        public string LEpitheliaRef { get; set; }
        /// <summary>
        /// LEpitheliaPF
        /// </summary>
        public string LEpitheliaPF { get; set; }
        /// <summary>
        /// 小圆上皮细胞
        /// </summary>
        public int XEpithelia { get; set; }
        /// <summary>
        /// 小圆上皮细胞参照值
        /// </summary>
        public string XEpitheliaRef { get; set; }
        /// <summary>
        /// XEpitheliaPF
        /// </summary>
        public string XEpitheliaPF { get; set; }
        /// <summary>
        /// 基底上皮细胞
        /// </summary>
        public int JEpithelia { get; set; }
        /// <summary>
        /// 基底上皮细胞参照值
        /// </summary>
        public string JEpitheliaRef { get; set; }
        /// <summary>
        /// JEpitheliaPF
        /// </summary>
        public string JEpitheliaPF { get; set; }
        /// <summary>
        /// 大红细胞
        /// </summary>
        public int BigRbc { get; set; }
        /// <summary>
        /// 大红细胞参照值
        /// </summary>
        public string BigRbcRef { get; set; }
        /// <summary>
        /// BigRbcPF
        /// </summary>
        public string BigRbcPF { get; set; }
        /// <summary>
        /// 小红细胞
        /// </summary>
        public int SmallRbc { get; set; }
        /// <summary>
        /// 小红细胞参照值
        /// </summary>
        public string SmallRbcRef { get; set; }
        /// <summary>
        /// SmallRbcPF
        /// </summary>
        public string SmallRbcPF { get; set; }
        /// <summary>
        /// 棘型红细胞
        /// </summary>
        public int JRbc { get; set; }
        /// <summary>
        /// 棘型红细胞参照值
        /// </summary>
        public string JRbcRef { get; set; }
        /// <summary>
        /// JRbcPF
        /// </summary>
        public string JRbcPF { get; set; }
        /// <summary>
        /// 皱缩红细胞
        /// </summary>
        public int ZRbc { get; set; }
        /// <summary>
        /// 皱缩红细胞参照值
        /// </summary>
        public string ZRbcRef { get; set; }
        /// <summary>
        /// ZRbcPF
        /// </summary>
        public string ZRbcPF { get; set; }
        /// <summary>
        /// 其他红细胞
        /// </summary>
        public int QRbc { get; set; }
        /// <summary>
        /// 其他红细胞参照值
        /// </summary>
        public string QRbcRef { get; set; }
        /// <summary>
        /// QRbcPF
        /// </summary>
        public string QRbcPF { get; set; }
        /// <summary>
        /// 中性细胞
        /// </summary>
        public int Neutrophil { get; set; }
        /// <summary>
        /// 中性细胞参照值
        /// </summary>
        public string NeutrophilRef { get; set; }
        /// <summary>
        /// NeutrophilPF
        /// </summary>
        public string NeutrophilPF { get; set; }
        /// <summary>
        /// 淋巴细胞
        /// </summary>
        public int Lymphocyte { get; set; }
        /// <summary>
        /// 淋巴细胞参照值
        /// </summary>
        public string LymphocyteRef { get; set; }
        /// <summary>
        /// LymphocytePF
        /// </summary>
        public string LymphocytePF { get; set; }
        /// <summary>
        /// 嗜酸性细胞
        /// </summary>
        public int Acidophil { get; set; }
        /// <summary>
        /// 嗜酸性细胞参照值
        /// </summary>
        public string AcidophilRef { get; set; }
        /// <summary>
        /// AcidophilPF
        /// </summary>
        public string AcidophilPF { get; set; }
        /// <summary>
        /// 嗜碱性细胞
        /// </summary>
        public int Basophil { get; set; }
        /// <summary>
        /// 嗜碱性细胞参照值
        /// </summary>
        public string BasophilRef { get; set; }
        /// <summary>
        /// BasophilPF
        /// </summary>
        public string BasophilPF { get; set; }
        /// <summary>
        /// 单核细胞
        /// </summary>
        public int Monocyte { get; set; }
        /// <summary>
        /// 单核细胞参照值
        /// </summary>
        public string MonocyteRef { get; set; }
        /// <summary>
        /// MonocytePF
        /// </summary>
        public string MonocytePF { get; set; }
        /// <summary>
        /// 细菌
        /// </summary>
        public int Bacterial { get; set; }
        /// <summary>
        /// 细菌参考值
        /// </summary>
        public string BacterialRef { get; set; }
        /// <summary>
        /// BacterialPF
        /// </summary>
        public string BacterialPF { get; set; }
        /// <summary>
        /// 球菌
        /// </summary>
        public int Cocci { get; set; }
        /// <summary>
        /// 球菌参考值
        /// </summary>
        public string CocciRef { get; set; }
        /// <summary>
        /// CocciPF
        /// </summary>
        public string CocciPF { get; set; }
        /// <summary>
        /// 杆菌
        /// </summary>
        public int Bacilli { get; set; }
        /// <summary>
        /// 杆菌参考值
        /// </summary>
        public string BacilliRef { get; set; }
        /// <summary>
        /// BacilliPF
        /// </summary>
        public string BacilliPF { get; set; }
        /// <summary>
        /// 霉菌
        /// </summary>
        public int Mildew { get; set; }
        /// <summary>
        /// 霉菌参考值
        /// </summary>
        public string MildewRef { get; set; }
        /// <summary>
        /// MildewPF
        /// </summary>
        public string MildewPF { get; set; }
        /// <summary>
        /// 酵母菌
        /// </summary>
        public int Yeast { get; set; }
        /// <summary>
        /// 酵母菌参考值
        /// </summary>
        public string YeastRef { get; set; }
        /// <summary>
        /// YeastPF
        /// </summary>
        public string YeastPF { get; set; }
        /// <summary>
        /// 寄生虫
        /// </summary>
        public int Parasites { get; set; }
        /// <summary>
        /// 寄生虫参考值
        /// </summary>
        public string ParasitesRef { get; set; }
        /// <summary>
        /// ParasitesPF
        /// </summary>
        public string ParasitesPF { get; set; }
        /// <summary>
        /// 精子
        /// </summary>
        public int Sperm { get; set; }
        /// <summary>
        /// 精子参考值
        /// </summary>
        public string SpermRef { get; set; }
        /// <summary>
        /// SpermPF
        /// </summary>
        public string SpermPF { get; set; }
        /// <summary>
        /// 可疑物
        /// </summary>
        public int Question { get; set; }
        /// <summary>
        /// 可疑物参照值
        /// </summary>
        public string QuestionRef { get; set; }
        /// <summary>
        /// QuestionPF
        /// </summary>
        public string QuestionPF { get; set; }
        /// <summary>
        /// 样液号
        /// </summary>
        public string ChemSampleID { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime? ChemDate { get; set; }
        /// <summary>
        /// 样本号
        /// </summary>
        public string ChemExamineID { get; set; }
        /// <summary>
        /// 协议中的日期
        /// </summary>
        public string RecDate { get; set; }
        /// <summary>
        /// 协议中的序号
        /// </summary>
        public string RecSeqID { get; set; }
        /// <summary>
        /// 镜检类型标志
        /// </summary>
        public string ChemExamType { get; set; }
        /// <summary>
        /// 试管架号
        /// </summary>
        public int ChemTubeRackNo { get; set; }
        /// <summary>
        /// 试管号
        /// </summary>
        public int ChemTubeNo { get; set; }
        /// <summary>
        /// ChemBarCode
        /// </summary>
        public string ChemBarCode { get; set; }
        /// <summary>
        /// 工作类型
        /// </summary>
        public int ChemExperType { get; set; }
        /// <summary>
        /// 葡萄糖
        /// </summary>
        public string GLU { get; set; }
        /// <summary>
        /// 葡萄糖参照值
        /// </summary>
        public string GLURef { get; set; }
        /// <summary>
        /// 葡萄糖参照值
        /// </summary>
        public string GLUUnit { get; set; }
        /// <summary>
        /// 尿潜血
        /// </summary>
        public string BLD { get; set; }
        /// <summary>
        /// 尿潜血参照值
        /// </summary>
        public string BLDRef { get; set; }
        /// <summary>
        /// 尿潜血单位
        /// </summary>
        public string BLDUnit { get; set; }
        /// <summary>
        /// 白细胞
        /// </summary>
        public string LEU { get; set; }
        /// <summary>
        /// 白细胞参照值
        /// </summary>
        public string LEURef { get; set; }
        /// <summary>
        /// 白细胞单位
        /// </summary>
        public string LEUUnit { get; set; }
        /// <summary>
        /// 蛋白质
        /// </summary>
        public string PRO { get; set; }
        /// <summary>
        /// 蛋白质参照值
        /// </summary>
        public string PRORef { get; set; }
        /// <summary>
        /// 蛋白质单位
        /// </summary>
        public string PROUnit { get; set; }
        /// <summary>
        /// 亚硝酸盐
        /// </summary>
        public string NIT { get; set; }
        /// <summary>
        /// 亚硝酸盐参照值
        /// </summary>
        public string NITRef { get; set; }
        /// <summary>
        /// 亚硝酸盐单位
        /// </summary>
        public string NITUnit { get; set; }
        /// <summary>
        /// 尿胆原
        /// </summary>
        public string URO { get; set; }
        /// <summary>
        /// 尿胆原参照值
        /// </summary>
        public string URORef { get; set; }
        /// <summary>
        /// 尿胆原单位
        /// </summary>
        public string UROUnit { get; set; }
        /// <summary>
        /// 胆红素
        /// </summary>
        public string BIL { get; set; }
        /// <summary>
        /// 胆红素参照值
        /// </summary>
        public string BILRef { get; set; }
        /// <summary>
        /// 胆红素单位
        /// </summary>
        public string BILUnit { get; set; }
        /// <summary>
        /// 酮体
        /// </summary>
        public string KET { get; set; }
        /// <summary>
        /// 酮体参照值
        /// </summary>
        public string KETRef { get; set; }
        /// <summary>
        /// 酮体单位
        /// </summary>
        public string KETUnit { get; set; }
        /// <summary>
        /// PH值
        /// </summary>
        public string PH { get; set; }
        /// <summary>
        /// PH值参照值
        /// </summary>
        public string PHRef { get; set; }
        /// <summary>
        /// PH值单位
        /// </summary>
        public string PHUnit { get; set; }
        /// <summary>
        /// 比重
        /// </summary>
        public string SG { get; set; }
        /// <summary>
        /// 比重参照值
        /// </summary>
        public string SGRef { get; set; }
        /// <summary>
        /// 比重单位
        /// </summary>
        public string SGUnit { get; set; }
        /// <summary>
        /// 维生素C
        /// </summary>
        public string VC { get; set; }
        /// <summary>
        /// 维生素C参照值
        /// </summary>
        public string VCRef { get; set; }
        /// <summary>
        /// 维生素C单位
        /// </summary>
        public string VCUnit { get; set; }
        /// <summary>
        /// MALB
        /// </summary>
        public string ALB { get; set; }
        /// <summary>
        /// MALB参照值
        /// </summary>
        public string ALBRef { get; set; }
        /// <summary>
        /// MALB单位
        /// </summary>
        public string ALBUnit { get; set; }
        /// <summary>
        /// 肌酐
        /// </summary>
        public string Cr { get; set; }
        /// <summary>
        /// 肌酐参照值
        /// </summary>
        public string CrRef { get; set; }
        /// <summary>
        /// 肌酐单位
        /// </summary>
        public string CrUnit { get; set; }
        /// <summary>
        /// Item1
        /// </summary>
        public string Item1 { get; set; }
        /// <summary>
        /// Item2
        /// </summary>
        public string Item2 { get; set; }
        /// <summary>
        /// Item3
        /// </summary>
        public string Item3 { get; set; }
        /// <summary>
        /// Item4
        /// </summary>
        public string Item4 { get; set; }
        /// <summary>
        /// Item5
        /// </summary>
        public string Item5 { get; set; }
        /// <summary>
        /// Item6
        /// </summary>
        public string Item6 { get; set; }
        /// <summary>
        /// Item7
        /// </summary>
        public string Item7 { get; set; }
        /// <summary>
        /// Item8
        /// </summary>
        public string Item8 { get; set; }
        /// <summary>
        /// Item9
        /// </summary>
        public string Item9 { get; set; }
        /// <summary>
        /// Item10
        /// </summary>
        public string Item10 { get; set; }
        /// <summary>
        /// Item11
        /// </summary>
        public string Item11 { get; set; }
        /// <summary>
        /// Item12
        /// </summary>
        public string Item12 { get; set; }
        /// <summary>
        /// Item13
        /// </summary>
        public string Item13 { get; set; }
        /// <summary>
        /// Item14
        /// </summary>
        public string Item14 { get; set; }
        /// <summary>
        /// Item15
        /// </summary>
        public string Item15 { get; set; }
        /// <summary>
        /// Item16
        /// </summary>
        public string Item16 { get; set; }
        /// <summary>
        /// Item17
        /// </summary>
        public string Item17 { get; set; }
        /// <summary>
        /// Item18
        /// </summary>
        public string Item18 { get; set; }
        /// <summary>
        /// Item19
        /// </summary>
        public string Item19 { get; set; }
        /// <summary>
        /// Item20
        /// </summary>
        public string Item20 { get; set; }
        /// <summary>
        /// Item21
        /// </summary>
        public string Item21 { get; set; }
        /// <summary>
        /// Item22
        /// </summary>
        public string Item22 { get; set; }
        /// <summary>
        /// Item23
        /// </summary>
        public string Item23 { get; set; }
        /// <summary>
        /// Item24
        /// </summary>
        public string Item24 { get; set; }
        /// <summary>
        /// Item25
        /// </summary>
        public string Item25 { get; set; }
        /// <summary>
        /// Item26
        /// </summary>
        public string Item26 { get; set; }
        /// <summary>
        /// Item27
        /// </summary>
        public string Item27 { get; set; }
        /// <summary>
        /// Item28
        /// </summary>
        public string Item28 { get; set; }
        /// <summary>
        /// Item29
        /// </summary>
        public string Item29 { get; set; }
        /// <summary>
        /// Item30
        /// </summary>
        public string Item30 { get; set; }
        /// <summary>
        /// Item31
        /// </summary>
        public string Item31 { get; set; }
        /// <summary>
        /// Item32
        /// </summary>
        public string Item32 { get; set; }
        /// <summary>
        /// Item33
        /// </summary>
        public string Item33 { get; set; }
        /// <summary>
        /// Item34
        /// </summary>
        public string Item34 { get; set; }
        /// <summary>
        /// Item35
        /// </summary>
        public string Item35 { get; set; }
        /// <summary>
        /// Item36
        /// </summary>
        public string Item36 { get; set; }
        /// <summary>
        /// Item37
        /// </summary>
        public string Item37 { get; set; }
        /// <summary>
        /// Item38
        /// </summary>
        public string Item38 { get; set; }
        /// <summary>
        /// Item39
        /// </summary>
        public string Item39 { get; set; }
        /// <summary>
        /// Item40
        /// </summary>
        public string Item40 { get; set; }
        /// <summary>
        /// Item41
        /// </summary>
        public string Item41 { get; set; }
        /// <summary>
        /// Item42
        /// </summary>
        public string Item42 { get; set; }
        /// <summary>
        /// Item43
        /// </summary>
        public string Item43 { get; set; }
        /// <summary>
        /// Item44
        /// </summary>
        public string Item44 { get; set; }
        /// <summary>
        /// Item45
        /// </summary>
        public string Item45 { get; set; }
        /// <summary>
        /// Item46
        /// </summary>
        public string Item46 { get; set; }
        /// <summary>
        /// Item47
        /// </summary>
        public string Item47 { get; set; }
        /// <summary>
        /// Item48
        /// </summary>
        public string Item48 { get; set; }
        /// <summary>
        /// Item49
        /// </summary>
        public string Item49 { get; set; }
        /// <summary>
        /// Item50
        /// </summary>
        public string Item50 { get; set; }
        /// <summary>
        /// RbcPFRef
        /// </summary>
        public string RbcPFRef { get; set; }
        /// <summary>
        /// WbcPFRef
        /// </summary>
        public string WbcPFRef { get; set; }
        /// <summary>
        /// CrystalPFRef
        /// </summary>
        public string CrystalPFRef { get; set; }
        /// <summary>
        /// DuctPFRef
        /// </summary>
        public string DuctPFRef { get; set; }
        /// <summary>
        /// EpitheliaPFRef
        /// </summary>
        public string EpitheliaPFRef { get; set; }
        /// <summary>
        /// WhackRbcPFRef
        /// </summary>
        public string WhackRbcPFRef { get; set; }
        /// <summary>
        /// SingularRbcPFRef
        /// </summary>
        public string SingularRbcPFRef { get; set; }
        /// <summary>
        /// InfusorialPFRef
        /// </summary>
        public string InfusorialPFRef { get; set; }
        /// <summary>
        /// MucousPFRef
        /// </summary>
        public string MucousPFRef { get; set; }
        /// <summary>
        /// PyoidPFRef
        /// </summary>
        public string PyoidPFRef { get; set; }
        /// <summary>
        /// NCrystalPFRef
        /// </summary>
        public string NCrystalPFRef { get; set; }
        /// <summary>
        /// LCrystalPFRef
        /// </summary>
        public string LCrystalPFRef { get; set; }
        /// <summary>
        /// HCrystalPFRef
        /// </summary>
        public string HCrystalPFRef { get; set; }
        /// <summary>
        /// CCrystalPFRef
        /// </summary>
        public string CCrystalPFRef { get; set; }
        /// <summary>
        /// WCrystalPFRef
        /// </summary>
        public string WCrystalPFRef { get; set; }
        /// <summary>
        /// RbcDuctPFRef
        /// </summary>
        public string RbcDuctPFRef { get; set; }
        /// <summary>
        /// WbcDuctPFRef
        /// </summary>
        public string WbcDuctPFRef { get; set; }
        /// <summary>
        /// KDuctPFRef
        /// </summary>
        public string KDuctPFRef { get; set; }
        /// <summary>
        /// TDuctPFRef
        /// </summary>
        public string TDuctPFRef { get; set; }
        /// <summary>
        /// LDuctPFRef
        /// </summary>
        public string LDuctPFRef { get; set; }
        /// <summary>
        /// LEpitheliaPFRef
        /// </summary>
        public string LEpitheliaPFRef { get; set; }
        /// <summary>
        /// XEpitheliaPFRef
        /// </summary>
        public string XEpitheliaPFRef { get; set; }
        /// <summary>
        /// JEpitheliaPFRef
        /// </summary>
        public string JEpitheliaPFRef { get; set; }
        /// <summary>
        /// BigRbcPFRef
        /// </summary>
        public string BigRbcPFRef { get; set; }
        /// <summary>
        /// SmallRbcPFRef
        /// </summary>
        public string SmallRbcPFRef { get; set; }
        /// <summary>
        /// JRbcPFRef
        /// </summary>
        public string JRbcPFRef { get; set; }
        /// <summary>
        /// ZRbcPFRef
        /// </summary>
        public string ZRbcPFRef { get; set; }
        /// <summary>
        /// QRbcPFRef
        /// </summary>
        public string QRbcPFRef { get; set; }
        /// <summary>
        /// NeutrophilPFRef
        /// </summary>
        public string NeutrophilPFRef { get; set; }
        /// <summary>
        /// LymphocytePFRef
        /// </summary>
        public string LymphocytePFRef { get; set; }
        /// <summary>
        /// AcidophilPFRef
        /// </summary>
        public string AcidophilPFRef { get; set; }
        /// <summary>
        /// BasophilPFRef
        /// </summary>
        public string BasophilPFRef { get; set; }
        /// <summary>
        /// MonocytePFRef
        /// </summary>
        public string MonocytePFRef { get; set; }
        /// <summary>
        /// EpiphytePFRef
        /// </summary>
        public string EpiphytePFRef { get; set; }
        /// <summary>
        /// BacterialPFRef
        /// </summary>
        public string BacterialPFRef { get; set; }
        /// <summary>
        /// CocciPFRef
        /// </summary>
        public string CocciPFRef { get; set; }
        /// <summary>
        /// BacilliPFRef
        /// </summary>
        public string BacilliPFRef { get; set; }
        /// <summary>
        /// MildewPFRef
        /// </summary>
        public string MildewPFRef { get; set; }
        /// <summary>
        /// YeastPFRef
        /// </summary>
        public string YeastPFRef { get; set; }
        /// <summary>
        /// ParasitesPFRef
        /// </summary>
        public string ParasitesPFRef { get; set; }
        /// <summary>
        /// SpermPFRef
        /// </summary>
        public string SpermPFRef { get; set; }
        /// <summary>
        /// QuestionPFRef
        /// </summary>
        public string QuestionPFRef { get; set; }
        /// <summary>
        /// Item1Ref
        /// </summary>
        public string Item1Ref { get; set; }
        /// <summary>
        /// Item2Ref
        /// </summary>
        public string Item2Ref { get; set; }
        /// <summary>
        /// Item3Ref
        /// </summary>
        public string Item3Ref { get; set; }
        /// <summary>
        /// Item4Ref
        /// </summary>
        public string Item4Ref { get; set; }
        /// <summary>
        /// Item5Ref
        /// </summary>
        public string Item5Ref { get; set; }
        /// <summary>
        /// Item6Ref
        /// </summary>
        public string Item6Ref { get; set; }
        /// <summary>
        /// Item7Ref
        /// </summary>
        public string Item7Ref { get; set; }
        /// <summary>
        /// Item8Ref
        /// </summary>
        public string Item8Ref { get; set; }
        /// <summary>
        /// Item9Ref
        /// </summary>
        public string Item9Ref { get; set; }
        /// <summary>
        /// Item10Ref
        /// </summary>
        public string Item10Ref { get; set; }
        /// <summary>
        /// Item11Ref
        /// </summary>
        public string Item11Ref { get; set; }
        /// <summary>
        /// Item12Ref
        /// </summary>
        public string Item12Ref { get; set; }
        /// <summary>
        /// Item13Ref
        /// </summary>
        public string Item13Ref { get; set; }
        /// <summary>
        /// Item14Ref
        /// </summary>
        public string Item14Ref { get; set; }
        /// <summary>
        /// Item15Ref
        /// </summary>
        public string Item15Ref { get; set; }
        /// <summary>
        /// Item16Ref
        /// </summary>
        public string Item16Ref { get; set; }
        /// <summary>
        /// Item17Ref
        /// </summary>
        public string Item17Ref { get; set; }
        /// <summary>
        /// Item18Ref
        /// </summary>
        public string Item18Ref { get; set; }
        /// <summary>
        /// Item19Ref
        /// </summary>
        public string Item19Ref { get; set; }
        /// <summary>
        /// Item20Ref
        /// </summary>
        public string Item20Ref { get; set; }
        /// <summary>
        /// Item21Ref
        /// </summary>
        public string Item21Ref { get; set; }
        /// <summary>
        /// Item22Ref
        /// </summary>
        public string Item22Ref { get; set; }
        /// <summary>
        /// Item23Ref
        /// </summary>
        public string Item23Ref { get; set; }
        /// <summary>
        /// Item24Ref
        /// </summary>
        public string Item24Ref { get; set; }
        /// <summary>
        /// Item25Ref
        /// </summary>
        public string Item25Ref { get; set; }
        /// <summary>
        /// Item26Ref
        /// </summary>
        public string Item26Ref { get; set; }
        /// <summary>
        /// Item27Ref
        /// </summary>
        public string Item27Ref { get; set; }
        /// <summary>
        /// Item28Ref
        /// </summary>
        public string Item28Ref { get; set; }
        /// <summary>
        /// Item29Ref
        /// </summary>
        public string Item29Ref { get; set; }
        /// <summary>
        /// Item30Ref
        /// </summary>
        public string Item30Ref { get; set; }
        /// <summary>
        /// Item31Ref
        /// </summary>
        public string Item31Ref { get; set; }
        /// <summary>
        /// Item32Ref
        /// </summary>
        public string Item32Ref { get; set; }
        /// <summary>
        /// Item33Ref
        /// </summary>
        public string Item33Ref { get; set; }
        /// <summary>
        /// Item34Ref
        /// </summary>
        public string Item34Ref { get; set; }
        /// <summary>
        /// Item35Ref
        /// </summary>
        public string Item35Ref { get; set; }
        /// <summary>
        /// Item36Ref
        /// </summary>
        public string Item36Ref { get; set; }
        /// <summary>
        /// Item37Ref
        /// </summary>
        public string Item37Ref { get; set; }
        /// <summary>
        /// Item38Ref
        /// </summary>
        public string Item38Ref { get; set; }
        /// <summary>
        /// Item39Ref
        /// </summary>
        public string Item39Ref { get; set; }
        /// <summary>
        /// Item40Ref
        /// </summary>
        public string Item40Ref { get; set; }
        /// <summary>
        /// Item41Ref
        /// </summary>
        public string Item41Ref { get; set; }
        /// <summary>
        /// Item42Ref
        /// </summary>
        public string Item42Ref { get; set; }
        /// <summary>
        /// Item43Ref
        /// </summary>
        public string Item43Ref { get; set; }
        /// <summary>
        /// Item44Ref
        /// </summary>
        public string Item44Ref { get; set; }
        /// <summary>
        /// Item45Ref
        /// </summary>
        public string Item45Ref { get; set; }
        /// <summary>
        /// Item46Ref
        /// </summary>
        public string Item46Ref { get; set; }
        /// <summary>
        /// Item47Ref
        /// </summary>
        public string Item47Ref { get; set; }
        /// <summary>
        /// Item48Ref
        /// </summary>
        public string Item48Ref { get; set; }
        /// <summary>
        /// Item49Ref
        /// </summary>
        public string Item49Ref { get; set; }
        /// <summary>
        /// Item50Ref
        /// </summary>
        public string Item50Ref { get; set; }
        /// <summary>
        /// Item1PFRef
        /// </summary>
        public string Item1PFRef { get; set; }
        /// <summary>
        /// Item2PFRef
        /// </summary>
        public string Item2PFRef { get; set; }
        /// <summary>
        /// Item3PFRef
        /// </summary>
        public string Item3PFRef { get; set; }
        /// <summary>
        /// Item4PFRef
        /// </summary>
        public string Item4PFRef { get; set; }
        /// <summary>
        /// Item5PFRef
        /// </summary>
        public string Item5PFRef { get; set; }
        /// <summary>
        /// Item6PFRef
        /// </summary>
        public string Item6PFRef { get; set; }
        /// <summary>
        /// Item7PFRef
        /// </summary>
        public string Item7PFRef { get; set; }
        /// <summary>
        /// Item8PFRef
        /// </summary>
        public string Item8PFRef { get; set; }
        /// <summary>
        /// Item9PFRef
        /// </summary>
        public string Item9PFRef { get; set; }
        /// <summary>
        /// Item10PFRef
        /// </summary>
        public string Item10PFRef { get; set; }
        /// <summary>
        /// Item11PFRef
        /// </summary>
        public string Item11PFRef { get; set; }
        /// <summary>
        /// Item12PFRef
        /// </summary>
        public string Item12PFRef { get; set; }
        /// <summary>
        /// Item13PFRef
        /// </summary>
        public string Item13PFRef { get; set; }
        /// <summary>
        /// Item14PFRef
        /// </summary>
        public string Item14PFRef { get; set; }
        /// <summary>
        /// Item15PFRef
        /// </summary>
        public string Item15PFRef { get; set; }
        /// <summary>
        /// Item16PFRef
        /// </summary>
        public string Item16PFRef { get; set; }
        /// <summary>
        /// Item17PFRef
        /// </summary>
        public string Item17PFRef { get; set; }
        /// <summary>
        /// Item18PFRef
        /// </summary>
        public string Item18PFRef { get; set; }
        /// <summary>
        /// Item19PFRef
        /// </summary>
        public string Item19PFRef { get; set; }
        /// <summary>
        /// Item20PFRef
        /// </summary>
        public string Item20PFRef { get; set; }
        /// <summary>
        /// Item21PFRef
        /// </summary>
        public string Item21PFRef { get; set; }
        /// <summary>
        /// Item22PFRef
        /// </summary>
        public string Item22PFRef { get; set; }
        /// <summary>
        /// Item23PFRef
        /// </summary>
        public string Item23PFRef { get; set; }
        /// <summary>
        /// Item24PFRef
        /// </summary>
        public string Item24PFRef { get; set; }
        /// <summary>
        /// Item25PFRef
        /// </summary>
        public string Item25PFRef { get; set; }
        /// <summary>
        /// Item26PFRef
        /// </summary>
        public string Item26PFRef { get; set; }
        /// <summary>
        /// Item27PFRef
        /// </summary>
        public string Item27PFRef { get; set; }
        /// <summary>
        /// Item28PFRef
        /// </summary>
        public string Item28PFRef { get; set; }
        /// <summary>
        /// Item29PFRef
        /// </summary>
        public string Item29PFRef { get; set; }
        /// <summary>
        /// Item30PFRef
        /// </summary>
        public string Item30PFRef { get; set; }
        /// <summary>
        /// Item31PFRef
        /// </summary>
        public string Item31PFRef { get; set; }
        /// <summary>
        /// Item32PFRef
        /// </summary>
        public string Item32PFRef { get; set; }
        /// <summary>
        /// Item33PFRef
        /// </summary>
        public string Item33PFRef { get; set; }
        /// <summary>
        /// Item34PFRef
        /// </summary>
        public string Item34PFRef { get; set; }
        /// <summary>
        /// Item35PFRef
        /// </summary>
        public string Item35PFRef { get; set; }
        /// <summary>
        /// Item36PFRef
        /// </summary>
        public string Item36PFRef { get; set; }
        /// <summary>
        /// Item37PFRef
        /// </summary>
        public string Item37PFRef { get; set; }
        /// <summary>
        /// Item38PFRef
        /// </summary>
        public string Item38PFRef { get; set; }
        /// <summary>
        /// Item39PFRef
        /// </summary>
        public string Item39PFRef { get; set; }
        /// <summary>
        /// Item40PFRef
        /// </summary>
        public string Item40PFRef { get; set; }
        /// <summary>
        /// Item41PFRef
        /// </summary>
        public string Item41PFRef { get; set; }
        /// <summary>
        /// Item42PFRef
        /// </summary>
        public string Item42PFRef { get; set; }
        /// <summary>
        /// Item43PFRef
        /// </summary>
        public string Item43PFRef { get; set; }
        /// <summary>
        /// Item44PFRef
        /// </summary>
        public string Item44PFRef { get; set; }
        /// <summary>
        /// Item45PFRef
        /// </summary>
        public string Item45PFRef { get; set; }
        /// <summary>
        /// Item46PFRef
        /// </summary>
        public string Item46PFRef { get; set; }
        /// <summary>
        /// Item47PFRef
        /// </summary>
        public string Item47PFRef { get; set; }
        /// <summary>
        /// Item48PFRef
        /// </summary>
        public string Item48PFRef { get; set; }
        /// <summary>
        /// Item49PFRef
        /// </summary>
        public string Item49PFRef { get; set; }
        /// <summary>
        /// Item50PFRef
        /// </summary>
        public string Item50PFRef { get; set; }
        /// <summary>
        /// Item1PF
        /// </summary>
        public string Item1PF { get; set; }
        /// <summary>
        /// Item2PF
        /// </summary>
        public string Item2PF { get; set; }
        /// <summary>
        /// Item3PF
        /// </summary>
        public string Item3PF { get; set; }
        /// <summary>
        /// Item4PF
        /// </summary>
        public string Item4PF { get; set; }
        /// <summary>
        /// Item5PF
        /// </summary>
        public string Item5PF { get; set; }
        /// <summary>
        /// Item6PF
        /// </summary>
        public string Item6PF { get; set; }
        /// <summary>
        /// Item7PF
        /// </summary>
        public string Item7PF { get; set; }
        /// <summary>
        /// Item8PF
        /// </summary>
        public string Item8PF { get; set; }
        /// <summary>
        /// Item9PF
        /// </summary>
        public string Item9PF { get; set; }
        /// <summary>
        /// Item10PF
        /// </summary>
        public string Item10PF { get; set; }
        /// <summary>
        /// Item11PF
        /// </summary>
        public string Item11PF { get; set; }
        /// <summary>
        /// Item12PF
        /// </summary>
        public string Item12PF { get; set; }
        /// <summary>
        /// Item13PF
        /// </summary>
        public string Item13PF { get; set; }
        /// <summary>
        /// Item14PF
        /// </summary>
        public string Item14PF { get; set; }
        /// <summary>
        /// Item15PF
        /// </summary>
        public string Item15PF { get; set; }
        /// <summary>
        /// Item16PF
        /// </summary>
        public string Item16PF { get; set; }
        /// <summary>
        /// Item17PF
        /// </summary>
        public string Item17PF { get; set; }
        /// <summary>
        /// Item18PF
        /// </summary>
        public string Item18PF { get; set; }
        /// <summary>
        /// Item19PF
        /// </summary>
        public string Item19PF { get; set; }
        /// <summary>
        /// Item20PF
        /// </summary>
        public string Item20PF { get; set; }
        /// <summary>
        /// Item21PF
        /// </summary>
        public string Item21PF { get; set; }
        /// <summary>
        /// Item22PF
        /// </summary>
        public string Item22PF { get; set; }
        /// <summary>
        /// Item23PF
        /// </summary>
        public string Item23PF { get; set; }
        /// <summary>
        /// Item24PF
        /// </summary>
        public string Item24PF { get; set; }
        /// <summary>
        /// Item25PF
        /// </summary>
        public string Item25PF { get; set; }
        /// <summary>
        /// Item26PF
        /// </summary>
        public string Item26PF { get; set; }
        /// <summary>
        /// Item27PF
        /// </summary>
        public string Item27PF { get; set; }
        /// <summary>
        /// Item28PF
        /// </summary>
        public string Item28PF { get; set; }
        /// <summary>
        /// Item29PF
        /// </summary>
        public string Item29PF { get; set; }
        /// <summary>
        /// Item30PF
        /// </summary>
        public string Item30PF { get; set; }
        /// <summary>
        /// Item31PF
        /// </summary>
        public string Item31PF { get; set; }
        /// <summary>
        /// Item32PF
        /// </summary>
        public string Item32PF { get; set; }
        /// <summary>
        /// Item33PF
        /// </summary>
        public string Item33PF { get; set; }
        /// <summary>
        /// Item34PF
        /// </summary>
        public string Item34PF { get; set; }
        /// <summary>
        /// Item35PF
        /// </summary>
        public string Item35PF { get; set; }
        /// <summary>
        /// Item36PF
        /// </summary>
        public string Item36PF { get; set; }
        /// <summary>
        /// Item37PF
        /// </summary>
        public string Item37PF { get; set; }
        /// <summary>
        /// Item38PF
        /// </summary>
        public string Item38PF { get; set; }
        /// <summary>
        /// Item39PF
        /// </summary>
        public string Item39PF { get; set; }
        /// <summary>
        /// Item40PF
        /// </summary>
        public string Item40PF { get; set; }
        /// <summary>
        /// Item41PF
        /// </summary>
        public string Item41PF { get; set; }
        /// <summary>
        /// Item42PF
        /// </summary>
        public string Item42PF { get; set; }
        /// <summary>
        /// Item43PF
        /// </summary>
        public string Item43PF { get; set; }
        /// <summary>
        /// Item44PF
        /// </summary>
        public string Item44PF { get; set; }
        /// <summary>
        /// Item45PF
        /// </summary>
        public string Item45PF { get; set; }
        /// <summary>
        /// Item46PF
        /// </summary>
        public string Item46PF { get; set; }
        /// <summary>
        /// Item47PF
        /// </summary>
        public string Item47PF { get; set; }
        /// <summary>
        /// Item48PF
        /// </summary>
        public string Item48PF { get; set; }
        /// <summary>
        /// Item49PF
        /// </summary>
        public string Item49PF { get; set; }
        /// <summary>
        /// Item50PF
        /// </summary>
        public string Item50PF { get; set; }
        #endregion

    }
}
