﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CNative;
using CNative.Dapper.Utils;
//using CNative.DbUtils;
using CNative.Utilities;
using Dapper;
using UnitTest461;

namespace UnitTest40
{
    public class UnitTest40DbUtils
    {
        private static ISqlBuilder DRY2 = null;
        private static SqlBuilder sqlBuilder = null;

        // private ISqlBuilder<TClass> sqlBuilder1 = null;
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //    //DRY2 = new  SqlBuilder("BaseDb");
        //    sqlBuilder = new SqlBuilder("BaseDb");

        //    //sqlBuilderp = new SqlBuilder("PostgreSql");

        //    //drMysql = new CNative.DbUtils.DbRepository("MySql");
        //    //drsqLite = new CNative.DbUtils.DbRepository("Sqlite");

        //}
        //[TestMethod]
        public static void TestSsqInBuilder1()
        {
            string conStr = "Data Source=.;Initial Catalog=test;Persist Security Info=True;User ID=sa;Password=123456";
            string sqlOrPro = "sp_getPERSONS";
            /*调用一个存储过程并执行多次*/
            using (var con = new System.Data.SqlClient.SqlConnection(conStr))
            {
                con.Open();
                var result = con.Execute(sqlOrPro, new[] {
                   new { id = 12, name = "测试15" },
                   new { id = 13, name = "测试14" }
                }, commandType: System.Data.CommandType.StoredProcedure);
                con.Close();
                con.Dispose();
            }
        }
        public static void TestPrint()
        {
            string conStr = @" _      ____  _      ____  _____ ____  ____ 
/ \  /|/  _ \/ \  /|/  _ \/  __//  __\/ ___\
| |  ||| / \|| |\ ||| | \||  \  |  \/||    \
| |/\||| \_/|| | \||| |_/||  /_ |    /\___ |
\_/  \|\____/\_/  \|\____/\____\\_/\_\\____/
                                            ";

            var logo = @"                          _               
                         | |              
 __      _____  _ __   __| | ___ _ __ ___ 
 \ \ /\ / / _ \| '_ \ / _` |/ _ \ '__/ __|
  \ V  V / (_) | | | | (_| |  __/ |  \__ \
   \_/\_/ \___/|_| |_|\__,_|\___|_|  |___/
                                          
                                          ";
            var logo3 = @"                               __                         
                              /\ \                        
 __  __  __    ___     ___    \_\ \     __   _ __   ____  
/\ \/\ \/\ \  / __`\ /' _ `\  /'_` \  /'__`\/\`'__\/',__\ 
\ \ \_/ \_/ \/\ \L\ \/\ \/\ \/\ \L\ \/\  __/\ \ \//\__, `\
 \ \___x___/'\ \____/\ \_\ \_\ \___,_\ \____\\ \_\\/\____/
  \/__//__/   \/___/  \/_/\/_/\/__,_ /\/____/ \/_/ \/___/ 
                                                          
                                                          ";
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(conStr);
            Console.ResetColor();
            Console.WriteLine(logo);
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(logo3);
            Console.ResetColor();
        }
        public  static void Main()
        {
            try
            {
                TestPrint();

                // ShowModules();
                // ShowXObjectPool();

                //DRY2 = new  SqlBuilder("BaseDb");
                //sqlBuilder = new SqlBuilder("sql2000");

                //TestSsqInBuilder1();
                //TestFastReflection();
                //TestSelectSqlBuilder1();
                //TestInsertSqlBuilder1();

                //Testlog();
                //Testlog4();
                // TestSqliteSqlBuilder();
                //TestMySqlSqlBuilder();
                TestOracleUS7SqlBuilder();
                TestOracleSqlBuilder();
                //TestPostgreSqlBuilder();
                // TestMsAccessSqlBuilder();
                //TestPssslBuilder();

                //TestSelectSqlInBuilder1();
                //TestSql2000lBuilder();

                //Oracle.ManagedDataAccess.Client.OracleBulkCopy oracleBulkCopy;

                //TestOracleUS7SqlBuilder();
                //
                //Method1();
                //Method2();
                //testDbApiProxy();
                //Method1();
                //TestOracleUS7SqlBuilder2();
            }
            catch (Exception ex)
            {
                Console.WriteLine("连接失败!" + ex.Message);
            }
            Console.Read();
        }
        private static void testDbApiProxy()
        {
            //System.Environment.SetEnvironmentVariable("nls_lang", "American_america.us7ascii");
            var s = new System.Diagnostics.Stopwatch();
            s.Start();
            var serviceProxy = new CNative.DBWebApiClient.DbApiProxyWS();

            var _eSql = new CNative.DBWebApiClient.DBSqlEntity();
            _eSql.CommandType = 1;
            _eSql.DBName = "Oraclec";

            _eSql.Sql = @"delete test where id=2";
            var returnexec = serviceProxy.Execute(_eSql);
            Console.WriteLine(returnexec.ToJson());
            var srcString = "oracle us7ascii 中文, US7ASCII字符集中汉字显示问题22";
            var temp = //Encoding.GetEncoding("ISO-8859-1").GetBytes(srcString);
            (srcString);// new String(System.Text.UTF8Encoding.Default.GetBytes("oracle us7ascii 中文,US7ASCII字符集中汉字显示问题22"));
            _eSql.Sql = @" insert into test (id,name) values(2,'"+ temp + "')";
            returnexec = serviceProxy.Execute(_eSql);
            Console.WriteLine(returnexec.ToJson());
           
            _eSql.Sql = @" select id,name from test order by id";
            var returnStr = serviceProxy.QueryDataSet(_eSql);//.QuerySingle<Entity_test>(_eSql);//.Query<Entity_test>(_eSql);
            Console.WriteLine(returnStr.ToJson());
            
            Console.WriteLine(s.Elapsed);
        }
        private static void Method2()
        {
            //连接字符串方式1:
            //string connString = "Provider=OraOLEDB.Oracle.1;Data Source=(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = IP地址)(PORT = 1521)))(CONNECT_DATA = (SID = orcl))) ;User Id=test;Password=123456";
            //连接字符串方式2:
            string connString = "Data Source=192.168.3.130/orcl.localdomain;Persist Security Info=True;User ID=rsa;Password=123456";
            //"Provider=OraOLEDB.Oracle.1;Data Source=192.168.3.130/orcl.localdomain;User Id=rsa;Password=123456";
            using (var conn = new System.Data.OracleClient.OracleConnection(connString))
            {
                try
                {
                    conn.Open();
                    Console.WriteLine("连接成功!");
                    string sql = "select * from test";
                    var com = new System.Data.OracleClient.OracleCommand("delete test where id=2", conn);
                    var rets2 = com.ExecuteNonQuery();
                    Console.WriteLine("rets2:{0}", rets2);

                    com = new System.Data.OracleClient.OracleCommand("insert into test (id,name) values(2,N'oracle us7ascii 中文,US7ASCII字符集中汉字显示问题')", conn);
                    var rets3 = com.ExecuteNonQuery();
                    Console.WriteLine("rets3:{0}", rets3);

                    com = new System.Data.OracleClient.OracleCommand(sql, conn);
                    var read = com.ExecuteReader();

                    while (read.Read())
                    {
                        String name = read["name"].ToString();
                        Console.WriteLine("name:{0}", name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("连接失败!" + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        
        public static string ToAscii(string dirty)
        {
            string ans = string.Concat(dirty.Select(c => string.Format("\\u{0:x4}", (int)c)).ToArray());
            return ans;
            ASCIIEncoding asciiEncoding = new ASCIIEncoding();
            byte[] bytes = asciiEncoding.GetBytes(dirty);
            string clean = asciiEncoding.GetString(bytes);
            return clean;
        }
        public static string ToChString(string str)
        {
            System.Text.Encoding gbk_encoder = System.Text.Encoding.GetEncoding("UTF8");
            byte[] bs = gbk_encoder.GetBytes(str);
            char[] cs = new char[bs.Length];
            for (int i = 0; i < bs.Length; i++)
                cs[i] = Convert.ToChar(bs[i]);
            return new String(cs);

        }
        private static string StringToUS7ASCII(string srcText)
        {
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.GetEncoding("GBK");//Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(srcText);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);

            string msg = iso.GetString(isoBytes);
            return msg;
        }
        private static string ConvertISO88591ToEncoding(string srcString)
        {
            Encoding dstEncode = Encoding.Default;
            String sResult;
            Encoding ISO88591Encoding = Encoding.GetEncoding("ISO-8859-1");
            Encoding GB2312Encoding = Encoding.GetEncoding("GB2312"); //这个地方很特殊，必须利用GB2312编码
            byte[] srcBytes = ISO88591Encoding.GetBytes(srcString);
            //将原本存储ISO-8859-1的字节数组当成GB2312转换成目标编码(关键步骤)
            byte[] dstBytes = Encoding.Convert(GB2312Encoding, dstEncode, srcBytes);
            char[] dstChars = new char[dstEncode.GetCharCount(dstBytes, 0, dstBytes.Length)];
            dstEncode.GetChars(dstBytes, 0, dstBytes.Length, dstChars, 0);//利用char数组存储字符
            sResult = new string(dstChars);
            return sResult;
        }
        // <summary>
        /// 将原始字串转换为格式为&#....;&#....
        /// </summary>
        /// <param name="srcText"></param>
        /// <returns></returns>
        private static string StringToISO_8859_1(string srcText)
        {
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(srcText);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
            string msg = iso.GetString(isoBytes);
            return msg;
            //string dst = "";
            //char[] src = srcText.ToCharArray();
            //for (int i = 0; i < src.Length; i++)
            //{
            //    string str = @"&#" + (int)src[i] + ";";
            //    dst += str;
            //}
            //return dst;
        }

        /// <summary>
        /// 将字串&#....;&#....;格式字串转换为原始字符串
        /// </summary>
        /// <param name="srcText"></param>
        /// <returns></returns>
        private static string ISO_8859_1ToString(string srcText)
        {
            string dst = "";
            string[] src = srcText.Split(';');

            for (int i = 0; i < src.Length; i++)
            {
                if (src[i].Length > 0)
                {
                    string str = ((char)int.Parse(src[i].Substring(2))).ToString();
                    dst += str;
                }
            }
            return dst;
        }
        private static string ConvertISO88591ToEncoding1(string srcString, Encoding dstEncode)
        {
            String sResult;

            Encoding ISO88591Encoding = Encoding.GetEncoding("ISO-8859-1");
            Encoding GB2312Encoding = Encoding.GetEncoding("GB2312"); //这个地方很特殊，必须利用GB2312编码
            byte[] srcBytes = ISO88591Encoding.GetBytes(srcString);

            //将原本存储ISO-8859-1的字节数组当成GB2312转换成目标编码(关键步骤)
            byte[] dstBytes = Encoding.Convert(GB2312Encoding, dstEncode, srcBytes);

            char[] dstChars = new char[dstEncode.GetCharCount(dstBytes, 0, dstBytes.Length)];

            dstEncode.GetChars(dstBytes, 0, dstBytes.Length, dstChars, 0);//利用char数组存储字符
            sResult = new string(dstChars);
            return sResult;

        }
        private static string StringToASCII(string value)
        {
            byte[] bytes = Encoding.Default.GetBytes(value);
            string textAscii = string.Empty;
            for (int i = 0; i < bytes.Length; i++)
            {
                textAscii += bytes[i].ToString() + ",";
            }
            return textAscii.TrimEnd(',');
        }
        private static void Method1()
        {
            //System.Environment.SetEnvironmentVariable("ORA_NCHAR_LITERAL_REPLACE", "TRUE");
            //连接字符串方式1:
            //string connString = "Provider=OraOLEDB.Oracle.1;Data Source=(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = IP地址)(PORT = 1521)))(CONNECT_DATA = (SID = orcl))) ;User Id=test;Password=123456";
            //连接字符串方式2:
            string connString = //"Provider=OraOLEDB.Oracle;Data Source=(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.3.130)(PORT = 1521)))(CONNECT_DATA = (SID = orcl))) ;User Id=rsa;Password=123456";
                "Provider=MSDAORA.1;Data Source=(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.3.130)(PORT = 1521)))(CONNECT_DATA = (SID = orcl))) ;User Id=rsa;Password=123456";
                //"Provider=OraOLEDB.Oracle;Data Source=192.168.3.130/orcl.localdomain;User Id=rsa;Password=123456";
            using (var conn = new System.Data.OleDb.OleDbConnection(connString))
            {
                var str = ("");
                try
                {
                    conn.Open();
                    Console.WriteLine("连接成功!");
                    string sql = "select * from test order by id";
                    var com = new System.Data.OleDb.OleDbCommand("delete test where id=2", conn);
                    var rets2 = com.ExecuteNonQuery();
                    Console.WriteLine("rets2:{0}", rets2);
                    
                    com = new System.Data.OleDb.OleDbCommand("insert into test (id,name) values(2,'oracle us7ascii 中文,US7ASCII字符集中汉字显示问题3')", conn);
                    //var parameter = new System.Data.OleDb.OleDbParameter("str", System.Data.OleDb.OleDbType.VarChar);
                    //parameter.Value = "oracle us7ascii 中文,US7ASCII字符集中汉字显示问题3";
                    //com.Parameters.Add(parameter);
                    var rets3 = com.ExecuteNonQuery();
                    Console.WriteLine("rets3:{0}", rets3);

                    com = new System.Data.OleDb.OleDbCommand(sql, conn);
                    var read = com.ExecuteReader();

                    while (read.Read())
                    {
                        String name = read["name"].ToString();
                        Console.WriteLine("name:{0}", name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("连接失败!" + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }
        public static void TestOracleUS7SqlBuilder2()
        {
            var s = new System.Diagnostics.Stopwatch();
            s.Start();
            string connString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.3.130)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=orcl.localdomain)));Persist Security Info=True;User ID=rsa;Password=123456";
            //string connString = "Provider=OraOLEDB.Oracle.1;Data Source=192.168.3.130/orcl.localdomain;User Id=rsa;Password=123456";
            using (var conn = new System.Data.OracleClient.OracleConnection(connString))
            {
                try
                {
                    conn.Open();
                    Console.WriteLine("连接成功!");
                    string sql = "select * from test order by id";
                    var com = new System.Data.OracleClient.OracleCommand(sql, conn);
                    var read = com.ExecuteReader();

                    while (read.Read())
                    {
                        String name = read["name"].ToString();
                        Console.WriteLine("name:{0}", name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("连接失败!" + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            Console.WriteLine(s.Elapsed);
        }
        public static void TestOracleUS7SqlBuilder()
        {
            var s = new System.Diagnostics.Stopwatch();
            s.Start();

            var sqlBuilder = new SqlBuilder("OracleOleDbM");

            var dt = sqlBuilder.DoSelect.GetDateNow();
            Console.WriteLine("Date Now:" + dt.ToString("yyyy-MM-dd HH:mm:ss ffff"));
            var n = 10;
            var sql = "select * from test where id=:wp_id order by id";
            while (n > 0)
            {
                try
                {
                    var ent = new Entity_test() { id = 2, name = "oracle MSDAORA us7ascii 中文,US7ASCII字符集中汉字显示问题2" };
                    Console.WriteLine("Delete:{0}", sqlBuilder.DoDelete.Delete(ent).Execute());
                    Console.WriteLine("Insert:{0}", sqlBuilder.DoInsert.Insert(ent).Execute());

                    var esql = sqlBuilder.CreateSqlEntity(sql);
                    esql.AddParameter("wp_id", 2);
                    esql.Parameter = new Dictionary<string, object> { { "wp_id", 2 } };
                    var lst = sqlBuilder//.Query<Entity_test>(esql);
                    .doSelect<Entity_test>().Where(f => f.id == 2).Query();
                    foreach (var item in lst)
                    {
                        Console.WriteLine(item.name);
                    }
                }
                catch (TypeLoadException ex)
                {
                    var _ = ex;
                    Console.WriteLine(ex.ToString());
                }
                n--;
            }
            Console.WriteLine(s.Elapsed);
        }
        public static void TestSelectSqlInBuilder1()
        {
            //var lst3 = sqlBuilder.doSelect<Entity_persons>()//取单行+排序
            //    .Fields(s => new { s.id, s.name, s.adress })//添加查询多个字段
            //    .Where(w => w.id==12 || w.name=="测试15")
            //    .OrderBy(d => d.id) //按列排序
            //    .Query(); //返回结果

            var sqle = sqlBuilder.CreateSqlEntity();
            sqle.Sql = @"SELECT *  FROM [test].dbo.[PERSONS]  WHERE  ([id] =@id OR [name] =@name)  ORDER BY [id] ASC ;
                        SELECT *  FROM [test].dbo.[PERSONS]  WHERE  ([id] =@id );
                        SELECT *  FROM [test].dbo.[PERSONS]  WHERE  ([name] =@name )";
            sqle.AddParameters(new { id = 12, name = "测试15" });
            var lst32 = sqlBuilder.Query<Entity_persons>(sqle);

            var ds2 = sqlBuilder.QueryDataSet(sqle);
            var dt2 = sqlBuilder.QueryDataTable(sqle);
        }
        public static void TestSql2000lBuilder()
        {
            try
            {
                var dbh = new DbHelper("sql2000");

                var sqle = dbh.CreateSqlEntity();
                sqle.Sql = "select top 101 * from UriToLis";

                var ds = dbh.QueryDataSet(sqle);
                Console.WriteLine("TestSql2000lBuilder:lst3=" + ds?.Tables.Count);

                var ss = dbh.Query<AVEMessageModel>(sqle);

                var lst32 = sqlBuilder.Query<AVEMessageModel>(sqle);
                Console.WriteLine("TestSql2000lBuilder:ss=" + ss?.Count());

                long totalNumber = 0;
                var lst = sqlBuilder.doSelect<AVEMessageModel>()
                 //.Fields(null)//添加查询多个字段
                 //.Where(w => w.ksid.Between(1100440, 1100450))
                 .Where(w => w.SickDate == DateTime.Parse("2019-06-11 00:00:00.000") && w.UpdateFlag == 1)
                 .OrderBy(d => d.SickSampleID)//按列倒向排序
                                              //.Query();
                 .QueryPagingList(1, 10, ref totalNumber); //返回结果
                Console.WriteLine("TestSql2000lBuilder:totalNumber=" + totalNumber);
            }
            catch (TypeLoadException ex)
            {
                var _ = ex;
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                var _ = ex;
                Console.WriteLine(ex.ToString());
            }
        }
        public static void TestPssslBuilder()
        {
            try
            {
                var conn = new System.Data.SqlClient.SqlConnection("Data Source=.;Initial Catalog=test;Persist Security Info=True;User ID=sa;Password=123456");
                conn.Open();
                const string query = "select * from DM_KSXXB";
                var ss = conn.Query<Entity_DMKSXXB>(query, null);
            }
            catch (TypeLoadException ex)
            {
                var _ = ex;
            }
            catch (Exception ex)
            {
                var _ = ex;
            }
        }
        public static void ShowModules()
        {
            var c1 = new User();
            //  Show the current module.
            Module m = c1.GetType().Module;
            Console.WriteLine("The current module is {0}.", m.Name);

            //  List all modules in the assembly.
            Assembly curAssembly = typeof(UnitTest40DbUtils).Assembly;
            Console.WriteLine("The current executing assembly is {0}.", curAssembly);

            Module[] mods = curAssembly.GetModules();
            foreach (Module md in mods)
            {
                Console.WriteLine("This assembly contains the {0} module", md.Name);
            }
            Console.ReadLine();
        }
        class User
        {
            public int Age { get; set; }
            public string Name { get; set; }
        } 
        public static void ShowXObjectPool()
        {
            var _activedTransportCount = 0;
            //初始化一个对象池  最大活动对象数量 50个，最小闲置对象数量2个，最大闲置数量20个。
            var UserPool = new XObjectPool<User>(50, 2, 20, () =>
            {
                int age = Interlocked.Increment(ref _activedTransportCount);
                Console.WriteLine($"activedTransportCount:{_activedTransportCount}");
                return new User()
                {
                    Age = age,
                    Name = $"Name{age}"
                };
            });
            //200个并行调用
            Parallel.For(0, 2000, x =>
            {
                using (var user = UserPool.Borrow())
                {
                    Console.WriteLine($"Age:{user.Value.Age},Name:{user.Value.Name},X:{x}");//,Msg:{user.Value.Msg}
                }
            });

            Console.ReadLine();
        }
        public void TestGetIQueryable()
        {
            var ss3 = String.Format("{0:0.##}", 123.4567);      // "123.46"
            var ss23 = String.Format("{0:0.##}", 123.4);         // "123.4"
            var ss233 = String.Format("{0:0.##}", 123.0);         // "123"

            var ent = new Entity_persons()
            { id = 100000001, adress = "Classify", name = "name", remark = "1,2,3" };
            var dt = DateTime.Now;
            var ss = "ss";
            var dd = new List<int>() { 2, 3, 45, 6 };
            var sql3 = DRY2.DoSelect.Where<Entity_persons>(g => g.name.In("1"));

            var sqlent = DRY2.CreateSqlEntity();
            var whe = ExpressionHelper.BuildWhere<Entity_persons>(sqlent,// t =>!("111" == t.name || t.id < 200+1)
                g => g.id.Between(10, 20) && g.id.In(dd) && (ss == "" || g.name == ss || g.id == 2) && g.adress == "dddd" && g.createTime == DateTime.Now && g.id < 200 + 1
                , true);

            var sql = DRY2.DoSelect.Where<Entity_persons>(g => g.id == 2 + 1 && (ss == "" || g.name == ss || g.id == 2) && g.adress == "dddd" && g.adress == ent.adress && g.id < 200 + 1);
            var sssds = DRY2.DoSelect.Query<Entity_persons>();
            var ssss = DRY2.DoSelect.Query<Entity_persons>();
            var sss = DRY2.DoSelect.Where<Entity_persons>(g => g.id < 20)
             .OrderBy<Entity_persons>(aa => aa.id)
             .OrderByDescending<Entity_persons>(aa => aa.name); 

            var entpersons = new Entity_persons() { id = -3, name = "xjhsss", updateTime = DateTime.Now };
            DRY2.DoDelete.Delete(entpersons);
            DRY2.DoInsert.Insert(entpersons);
            DRY2.DoUpdate.Update(entpersons);
            DRY2.DoDelete.Delete(entpersons);
            DRY2.DoUpdate.Set<Entity_persons>(f => new Entity_persons { adress = "fadsfas" })
                .Where<Entity_persons>(f => f.id == 2);

            //var entpersons = new Entity_persons() { id = 3, name = "xjhsss" };
            //drMysql.Insert(entpersons);
            //drMysql.Update(entpersons);
            //drMysql.Delete(entpersons);
            //drMysql.UpdateWhere<Entity_persons>((es) =>
            //{
            //    es.name = "ffffss";
            //}, f => f.id == 2);
            //var tb = drMysql.QueryDataTable(new CNative.DbUtils.SqlEntity(drMysql.DBType) { Sql = "select * from `persons` " });
            //var myss = drMysql.GetList<Entity_persons>(null, null, null, 20);


            //var sssf = drsqLite.GetList<Entity_persons>(null, null, a => a.OrderBy(o => o.Id), 20);
            //if (sssf?.Count > 0)
            //{
            //    sssf[1].test = 333.32555m;
            //    drsqLite.Save(sssf[1]);
            //    drsqLite.Delete(sssf[0]);
            //    drsqLite.Execute(drsqLite.GetMergeSql(sssf[0]));
            //}
            var ssd = DateTime.MinValue;


        }

       
        public void TestInsertFrom()
        {
            var id = System.Guid.NewGuid().GetHashCode();
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };
            DRY2.DoDelete.Delete(ent);
            var ss = DRY2.DoInsert.Insert(ent);
            var guid = Guid.NewGuid();
            int[] arr = new int[] { 5, 8, 6 };
            string[] arr2 = new string[] { "1", "2", "3" };
            var sssff = new List<int>(arr);

            //LambdaToSqlHelper.GetInsertField<Entity_persons>(f => f.id == 2, f => f.name == "xjh");
            //return;
            //DRY2.InsertFrom<Entity_persons>("[dbo].[test]", w => 1 == 1, new Dictionary<string, object>()
            //{
            //    { "Remark", "testsssxjh" },{ "GuidId", Guid.NewGuid() }
            //});

            var sql = DRY2.DoSelect.Where<Entity_persons>(g =>
                1 == 3 || g.createTime == DateTime.Now || g.id == 4 && g.id.In(sssff) &&
                g.name.In(arr2) && (g.adress.Like("Clas%sify") || g.name.Equals("nam(e16'91780562")));

            // DRY2.InsertFrom<Entity_配置信息表>("[FGCPOE].[dbo].[配置信息表2]", w => w.Id.In(3,5,6), f=> 1 == 1 && f.Remark== "testsssxjh333"&& 1==2 && f.GuidId== Guid.NewGuid());
        }

       
        public static void Testlog()
        {
            var id = System.Guid.NewGuid().GetHashCode();
            CNative.Utilities.NlogHelper.Error("Error:" + id);
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };
            CNative.Utilities.NlogHelper.Debug("Debug:" + ent.ToJson());
            CNative.Utilities.NlogHelper.Info("Info:" + DateTime.Now);
            CNative.Utilities.NlogHelper.Fatal("Fatal:" + ent.ToJson());
            CNative.Utilities.NlogHelper.Warn("Warn:" + ent.ToJson());
        }
        public static void Testlog4()
        {
            var id = System.Guid.NewGuid().GetHashCode();
            CNative.Utilities.Log4NetHelper.Error("Error:" + id);
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };
            CNative.Utilities.Log4NetHelper.Debug("Debug:" + ent.ToJson());
            CNative.Utilities.Log4NetHelper.Info("Info:" + DateTime.Now);
            CNative.Utilities.Log4NetHelper.Fatal("Fatal:" + ent.ToJson());
            CNative.Utilities.Log4NetHelper.Warn("Warn:" + ent.ToJson());
        }
        public void TestConvertType()
        {
            var obj = FuncTable2Entity.ConvertType("322", typeof(int));
            var obj3 = FuncTable2Entity.ConvertType("322.55", typeof(int));
            var obj4 = FuncTable2Entity.ConvertType("322.55", typeof(decimal));
            var obj43 = FuncTable2Entity.ConvertType("322.55", typeof(double));
            var obj34 = FuncTable2Entity.ConvertType("16", typeof(System.Reflection.BindingFlags));
        }

       
        public static void TestFastReflection()
        {
            var ent = new Entity_persons()
            { id = 1000000021, adress = "Classify", name = "name", remark = "1,2,3" };
            //System.IO.Directory.CreateDirectory(@"test\test2");
            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getPublic"));
            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getInternal"));
            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getprotected"));
            NlogHelper.Info("FastInvoke：" + ent.FastInvoke("getPrivate", new object[] { null, "1" }));

            var ent2 = "Entity_persons2".FastInstance("xjh");
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getPublic"));
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getInternal"));
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getprotected"));
            NlogHelper.Info("FastInvoke by type：" + ent2.FastInvoke("getPrivate", new object[] { null, "1" }));
        }

        //单表操作测试
       
        public static void TestSelectSqlBuilder1()
        {
            var lst = sqlBuilder.doSelect<Entity_persons>()//查询集合+排序+TOP
                 .Fields(s => new { s.id, s.name, s.adress })//添加查询多个字段
                 .Top(8)
                 // .Where(w => w.id.Between(10, 20))
                 .OrderByDescending(d => d.createTime)//按列倒向排序
                 .Query(); //返回结果

            var lst2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                 .Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                 // .Where(w => w.id.Between(10, 20))
                .OrderByDescending(d => d.updateTime) //按列倒向排序
                 .GetSingle<string>(); //返回结果


            var lst3 = sqlBuilder.doSelect<Entity_persons>()//取单行+排序
                .Fields(s => new { s.id, s.name, s.adress })//添加查询多个字段
                .Where(w => w.id.Between(11, 20))
                .OrderBy(d => d.id) //按列排序
                .QuerySingle(); //返回结果

            var lst4 = sqlBuilder.doSelect<Entity_persons>().Count(w => w.id.Between(11, 20)); //返回结果

        }

       
        public static void TestInsertSqlBuilder1()
        {
            var jg = new Entity_DMJGXXB() { OrgId = 1002, jgdm = "56783", jgmc = "方舱" };

            var ret = sqlBuilder.doDelete<Entity_DMJGXXB>()//实体删除
                 .Delete(jg)
                 .Exec;
            ret = sqlBuilder.doInsert<Entity_DMJGXXB>()//插入实体
                .Insert(jg)
                .Execute();

            ret = sqlBuilder.doDelete<Entity_DMJGXXB>()//表达式删除
                 .Where(s => s.OrgId == 1003)
                 .Execute();
            ret = sqlBuilder.doInsert<Entity_DMJGXXB>()//表达式插入
                .Insert(new Entity_DMJGXXB()
                {
                    OrgId = 1003,
                    jgdm = "532236783",
                    jgmc = "方舱3",
                    CreateId = 0,
                    UpdateId = 0,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                })
                .Exec;

            //表复制
            var ret1 = sqlBuilder.doDelete<Entity_DMCZYJGDYB2>()
                    .Where(s => s.ksid == 1100453)
                    .Execute();

            var ret22 = sqlBuilder.doInsert<Entity_DMCZYJGDYB2>()//表复制
                .InsertSelect<Entity_DMCZYJGDYB>(s => new Entity_DMCZYJGDYB2()
                {
                    OrgId = s.OrgId,
                    czyid = s.czyid,
                    czyjgdyid = s.czyjgdyid,
                    czyxm = s.czyxm,
                    jgmc = "fasdfafdsaf",
                    ksid = s.ksid,
                    ksmc = s.ksmc
                }, s => s.ksid == 1100453)
                .Exec;
        }
       
        public static void TestUpdateSqlBuilder1()
        {
            var jg = new Entity_DMJGXXB()
            {
                OrgId = 1003,
                jgdm = "5322336783",
                jgmc = "方舱32",
                CreateId = 0,
                UpdateId = 0,
                jgjpm = "7oipsafdasfd",
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            var ret = sqlBuilder.doUpdate<Entity_DMJGXXB>()
               .Set(s => new Entity_DMJGXXB()
               {
                   jgdm = "5322336783",
                   jgmc = "方舱232aaa",
                   CreateId = 0,
                   UpdateId = 0,
                   jgjpm = "ffff",
                   UpdateTime = DateTime.Now
               })
               .Where(s => s.OrgId == jg.OrgId)
               .Exec;

            jg.jgjpm = "345454";
            ret = sqlBuilder.DoUpdate
               .Update(jg, "jgjpm");

            var ret1 = sqlBuilder.doDelete<Entity_DMJGXXB>()
                  .Where(s => s.OrgId == 1005)
                  .Execute();

            //更新或插入
            var ret23 = sqlBuilder.DoUpdate
                .UpdateOrInsert(jg, w => w.OrgId == jg.OrgId);

            ret = sqlBuilder.doUpdate<Entity_DMJGXXB>()
                .UpdateOrInsert(s => new Entity_DMJGXXB()
                {
                    OrgId = 1006,
                    jgdm = "5322336783",
                    jgmc = "方舱232",
                    CreateId = 0,
                    UpdateId = 0,
                    jgjpm = "wsafdasfd",
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                }, w => w.OrgId == 1006);

        }

        //多表操作测试
       
        public void TestSelectFieldExprSqlBuilder1()
        {
            //查寻时加集合函数
            var ret1 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
                .Fields(f => new { czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where(s => s.ksid == 1100453)
                .GetSingle<object>();

            var ret2 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
               .Fields(f => new
               { f.czyid, f.czyxm, jgmc = f.jgmc.SQL_UCASE(), ksmc = f.jgmc.SQL_SUBSTR(2, 3) })
               //.Where(s => s.ksid == 1100453)
               .Where(s => s.ksid == 1100453 && s.jgmc.SQL_UCASE() == "FASDFAFDSAF")
               .Query();

            var ret3 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
                .Fields(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where(s => s.OrgId == 1001)
                .GroupBy(g => new { g.ksid, g.ksmc })
                .OrderBy(g => g.ksid)
                .Query();

            var ret4 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
                .Fields(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_COUNT(), czyid = f.czyjgdyid.SQL_MAX() })
                .Where(s => s.OrgId == 1001)
                .GroupBy(g => new { g.ksid, g.ksmc })
                .Having(h => h.czyjgdyid.SQL_COUNT() > 3 && h.czyjgdyid.SQL_MAX() > 0)
                .OrderBy(g => g.ksid)
                .Query();
        }

       
        public void TestSelectSqlBuilder()
        {
            var lst = sqlBuilder.DoSelect
                .From<Entity_persons>("a")
                 //.Fields<Entity_persons>()//添加查询多个字段
                 .Fields<Entity_persons>(s => new { s.id, s.name, adress=s.adress.SQL_NVL("无") })//添加查询多个字段
                 .Fields<Entity_persons>(s => s.adress, "a")// 添加查询单个字段带别名
                 .Top(8)
                 .Where<Entity_persons>(w => w.id.Between(10, 20))
                 .OrderByDescending<Entity_persons>(d => d.createTime)//按列倒向排序
                 .Query<Entity_persons>(); //返回结果

            var lst2 = sqlBuilder.DoSelect
                 .From<Entity_persons>()
                 .Fields("name AS ad")// 添加查询单个字段带别名
                 .Where<Entity_persons>(w => w.id.Between(10, 20))
                 .OrderByDescending<Entity_persons>(d => d.updateTime) //按列倒向排序
                 .GetSingle<string>(); //返回结果


            var lst3 = sqlBuilder.DoSelect
                .From<Entity_persons>()
                .Fields<Entity_persons>(s => new { s.id, s.name, s.adress })//添加查询多个字段
                .Where<Entity_persons>(w => w.id.Between(11, 20))
                .OrderBy<Entity_persons>(d => d.id) //按列排序
                .QuerySingle<Entity_persons>(); //返回结果

            var lst4 = sqlBuilder.DoSelect.Count<Entity_persons>(w => w.id.Between(11, 20)); //返回结果

        }


       
        public void TestSelectSqlBuilderJoin()
        {
            var lst = sqlBuilder.DoSelect
                 .From<Entity_DMKSXXB>("k")
                 .Top(8)
                 .Fields<Entity_DMKSXXB>(s => new { s.ksid, s.ksmc, s.ksdm, s.OrgId })//添加查询多个字段
                 .Fields("k.sjksid")// 添加查询单个字段带别名
                 .Fields<Entity_DMJGXXB>(s => new { s.jgjc, s.jgdm },"j")
                 .Fields("j.jgmc")
                 .InnerJoin<Entity_DMKSXXB, Entity_DMJGXXB>((k, j) => k.OrgId == j.OrgId, "j")
                 .Where<Entity_DMKSXXB>(w => w.ksid.Between(1100440, 1100450))
                 .OrderByDescending<Entity_DMKSXXB>(d => d.ksid)//按列倒向排序
                 .Query<Entity_DMKSXXB>(); //返回结果

        }

       
        public void TestQueryPagingList()
        {
            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_DMKSXXB>()
                .Fields(s => new { s.ksid, s.ksmc, s.ksdm, s.OrgId })//添加查询多个字段
                .Fields("sjksid")// 添加查询单个字段带别名
                .Where(w => w.ksid.Between(1100440, 1100450))
                .OrderByDescending(d => d.ksid)//按列倒向排序
                .QueryPagingList(2,10,ref totalNumber); //返回结果

            var pae = new PagingInfo() { PageNumber=1,PageSize=10};
            var lst2 = sqlBuilder.DoSelect
                 .From<Entity_DMKSXXB>("k")
                 .Fields<Entity_DMKSXXB>(s => new { s.ksid, s.ksmc, s.ksdm, s.OrgId })//添加查询多个字段
                 .Fields("k.sjksid")// 添加查询单个字段带别名
                 .Fields<Entity_DMJGXXB>(s => new { s.jgjc, s.jgdm }, "j")
                 .Fields("j.jgmc")
                 .InnerJoin<Entity_DMKSXXB, Entity_DMJGXXB>((k, j) => k.OrgId == j.OrgId, "j")
                 .Where<Entity_DMKSXXB>(w => w.ksid.Between(1100440, 1100450))
                 .OrderByDescending<Entity_DMKSXXB>(d => d.ksid)//按列倒向排序
                 .QueryPagingList<Entity_DMKSXXB>(pae); //返回结果

            var ret3 = sqlBuilder.DoSelect
               .Fields<Entity_DMCZYJGDYB>(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_MAX() })
               .Where<Entity_DMCZYJGDYB>(s => s.OrgId == 1001)
               .GroupBy<Entity_DMCZYJGDYB>(g => new { g.ksid, g.ksmc })
               .OrderBy<Entity_DMCZYJGDYB>(g => g.ksid)
              .QueryPagingList<Entity_DMCZYJGDYB>(pae); //返回结果
        }

       
        public void TestInsertSqlBuilder()
        {
            var jg = new Entity_DMJGXXB() { OrgId = 1002, jgdm = "56783", jgmc = "方舱" };

            var ret = sqlBuilder.DoDelete
                 .Delete(jg)
                 .Exec;
            ret = sqlBuilder.DoInsert
                .Insert(jg)
                .Execute();

            ret = sqlBuilder.DoDelete
                 .Where<Entity_DMJGXXB>(s => s.OrgId == 1003)
                 .Execute();
            ret = sqlBuilder.DoInsert
                .Insert(new Entity_DMJGXXB()
                {
                    OrgId = 1003,
                    jgdm = "532236783",
                    jgmc = "方舱3",
                    CreateId = 0,
                    UpdateId = 0,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                })
                .Exec;


        }

       
        public void TestUpdateSqlBuilder()
        {
            var jg = new Entity_DMJGXXB()
            {
                OrgId = 1003,
                jgdm = "5322336783",
                jgmc = "方舱32",
                CreateId = 0,
                UpdateId = 0,
                jgjpm = "7oipsafdasfd",
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            var ret = sqlBuilder.DoUpdate
               .Set<Entity_DMJGXXB>(s => new Entity_DMJGXXB()
               {
                   jgdm = "5322336783",
                   jgmc = "方舱232aaa",
                   CreateId = 0,
                   UpdateId = 0,
                   jgjpm = "ffff",
                   UpdateTime = DateTime.Now
               })
               .Where<Entity_DMJGXXB>(s => s.OrgId == jg.OrgId)
               .Exec;
            jg.jgjpm = "345454";
            ret = sqlBuilder.DoUpdate
               .Update(jg, "jgjpm");

        }
       
        public void TestUpdateOrInsertSqlBuilder()
        {
          var  ret1 = sqlBuilder.DoDelete
                   .Where<Entity_DMJGXXB>(s => s.OrgId == 1005)
                   .Execute();

            var jg = new Entity_DMJGXXB()
            {
                OrgId = 1005,
                jgdm = "5322336783",
                jgmc = "方舱54",
                CreateId = 0,
                UpdateId = 0,
                jgjpm = "fffss",
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            //jg.jgjpm = "345454";
            var ret = sqlBuilder.DoUpdate
                .UpdateOrInsert(jg, w => w.OrgId == jg.OrgId);

            ret = sqlBuilder.DoUpdate
                .UpdateOrInsert<Entity_DMJGXXB>(s => new Entity_DMJGXXB()
                {
                    OrgId = 1006,
                    jgdm = "5322336783",
                    jgmc = "方舱232",
                    CreateId = 0,
                    UpdateId = 0,
                    jgjpm = "wsafdasfd",
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                }, w => w.OrgId == 1006);
        }

       
        public void TestInsertSelectSqlBuilder()
        {
            var dt = sqlBuilder.DoSelect.GetDateNow();

            var ret1 = sqlBuilder.DoDelete
                     .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453)
                     .Execute();

            var ret = sqlBuilder.DoInsert
                .InsertSelect<Entity_DMCZYJGDYB2, Entity_DMCZYJGDYB>(s => new Entity_DMCZYJGDYB2()
                {
                    OrgId = s.OrgId,
                    czyid = s.czyid,
                    czyjgdyid = s.czyjgdyid,
                    czyxm = s.czyxm,
                    jgmc = "fasdfafdsaf",
                    ksid = s.ksid,
                    ksmc = s.ksmc
                }, s => s.ksid == 1100453)
                .Exec;
        }

       
        public void TestSelectFieldExprSqlBuilder()
        {
            var ret1 = sqlBuilder.DoSelect
                .Fields<Entity_DMCZYJGDYB2>(f => new { czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453)
                .GetSingle<object>();

            var ret2 = sqlBuilder.DoSelect
               .Fields<Entity_DMCZYJGDYB2>(f => new
               { f.czyid, f.czyxm, jgmc = f.jgmc.SQL_UCASE(), ksmc = f.jgmc.SQL_SUBSTR(2, 3) })
               .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453)
               .Where<Entity_DMCZYJGDYB2>(s => s.ksid == 1100453 && s.jgmc.SQL_UCASE() == "FASDFAFDSAF")
               .Query<Entity_DMCZYJGDYB2>();

            var ret3 = sqlBuilder.DoSelect
                .Fields<Entity_DMCZYJGDYB>(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_MAX() })
                .Where<Entity_DMCZYJGDYB>(s => s.OrgId == 1001)
                .GroupBy<Entity_DMCZYJGDYB>(g => new { g.ksid, g.ksmc })
                .OrderBy<Entity_DMCZYJGDYB>(g => g.ksid)
                .Query<Entity_DMCZYJGDYB>();

            var ret4 = sqlBuilder.DoSelect
                .From<Entity_DMCZYJGDYB>()
                .Fields<Entity_DMCZYJGDYB>(f => new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_COUNT(), czyid= f.czyjgdyid.SQL_MAX() })
                .Where<Entity_DMCZYJGDYB>(s => s.OrgId == 1001)
                .GroupBy<Entity_DMCZYJGDYB>(g => new { g.ksid, g.ksmc })
                .Having<Entity_DMCZYJGDYB>(h => h.czyjgdyid.SQL_COUNT()>3 && h.czyjgdyid.SQL_MAX()>0)
                .OrderBy<Entity_DMCZYJGDYB>(g => g.ksid)
                .Query<Entity_DMCZYJGDYB>();
        }

       
        public static void TestSqliteSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("Sqlite");
            var dt = sqlBuilder.DoSelect.GetDateNow();

            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_sysmenu>()
                 //.Fields(null)//添加查询多个字段
                 //.Fields("sjksid")// 添加查询单个字段带别名
                 //.Where(w => w.ksid.Between(1100440, 1100450))
                 .OrderBy(d => d.Id)//按列倒向排序
                 .QueryPagingList(3, 10, ref totalNumber); //返回结果

            var createSql = sqlBuilder.CreateSqlEntity();
            createSql.Sql = "create table LocalMessage" +
                                  "(" +
                                  "Id nvarchar(50) not null," +
                                  "FileName nvarchar(200)," +
                                  "ReceivedTime datetime," +
                                  "ProcessTime datetime," +
                                  "MsgSize int," +
                                  "Status int," +
                                  " PRIMARY KEY(Id))";
            sqlBuilder.Execute(createSql);
        }

       
        public static void TestMySqlSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("MySql");
            var dt = sqlBuilder.DoSelect.GetDateNow();

            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_sysmenu>()
                 //.Fields(null)//添加查询多个字段
                 //.Fields("sjksid")// 添加查询单个字段带别名
                 //.Where(w => w.ksid.Between(1100440, 1100450))
                 .OrderBy(d => d.Id)//按列倒向排序
                 .QueryPagingList(3, 10, ref totalNumber); //返回结果
        }

       
        public static void TestPostgreSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("PostgreSql");
            var dt = sqlBuilder.DoSelect.GetDateNow();

            var lst2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                 .Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                                                 // .Where(w => w.id.Between(10, 20))
                .OrderByDescending(d => d.updateTime) //按列倒向排序
                 .GetSingle<string>(); //返回结果

            var dt2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                                                // .Where(w => w.id.Between(10, 20))
               .OrderByDescending(d => d.updateTime) //按列倒向排序
                .QueryDataTable(); //返回结果

            var ds2 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
                                                           //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
                                                           // .Where(w => w.id.Between(10, 20))
              .OrderByDescending(d => d.updateTime) //按列倒向排序
               .QueryDataSet(); //返回结果

            long totalNumber = 0;
            //var lst = sqlBuilder.doSelect<Entity_sysmenu>()
            //     //.Fields(null)//添加查询多个字段
            //     //.Fields("sjksid")// 添加查询单个字段带别名
            //     //.Where(w => w.ksid.Between(1100440, 1100450))
            //     .OrderBy(d => d.Id)//按列倒向排序
            //     .QueryPagingList(3, 10, ref totalNumber); //返回结果
        }


        public static void TestOracleSqlBuilder()
        {
            //var sqlBuilder = new SqlBuilder("Oracle");
            //var dt = sqlBuilder.DoSelect.GetDateNow();
            ////var sql2 = sqlBuilder.SqlDbProvider.gete

            //var sql = sqlBuilder.CreateSqlEntity();
            //sql.CommandType = System.Data.CommandType.StoredProcedure;

            //var pr = new OracleDynamicParametersC();
            //pr.Add("V_CUR_OUT1",null, Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor, System.Data.ParameterDirection.Output);

            //sql.Parameter=(pr);
            //sql.Sql = "testmdatas1";
            //var ds1 = sqlBuilder.QueryDataSet(sql);

            //pr.Add("V_CUR_OUT2", null, Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor, System.Data.ParameterDirection.Output);
            //pr.Add("V_CUR_OUT3", null, Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor, System.Data.ParameterDirection.Output);

            //sql.Parameter = pr;
            ////sql.AddParameters(pr);
            //sql.Sql = "testmdatas";
            ////sql.Sql = "declare ret1 sys_refcursor;ret2 sys_refcursor;ret3 sys_refcursor; begin testmdatas(ret1,ret2,ret3);end;";
            //var ds = sqlBuilder.QueryDataSet(sql);

            //var sqlBuilder = new SqlBuilder("OracleDevart");
            //var dt = sqlBuilder.DoSelect.GetDateNow();

            //long totalNumber = 0;
            //var lst = sqlBuilder.doSelect<Entity_DRUGSTOCK>()
            //     //.Fields(null)//添加查询多个字段
            //     //.Fields("sjksid")// 添加查询单个字段带别名
            //     .Where(w => w.STORAGE== "110003")
            //     //.OrderBy(d => d.Id)//按列倒向排序
            //     .QueryPagingList(1, 5, ref totalNumber); //返回结果

            ////var lst23 = sqlBuilder.doSelect<Entity_persons>()//取单列+倒序
            ////                                                 //.Fields(s => s.name.SQL_UCASE())// 添加查询单个字段带别名
            ////                                                 // .Where(w => w.id.Between(10, 20))
            ////   .OrderBy(d => d.id) //按列倒向排序
            ////   .QueryPagingList(3, 10, ref totalNumber); //返回结果
            ///
            sqlBuilder = new SqlBuilder("OracleClient");
            var dt2 = sqlBuilder.DoSelect.GetDateNow();
            Console.WriteLine("GetDateNow=" + dt2.ToJson());

            var sql = sqlBuilder.CreateSqlEntity();
            sql.CommandType = System.Data.CommandType.StoredProcedure;
            var prs = new OracleDynamicParameters();
            prs.Add("v_cur_out1",System.Data.OracleClient.OracleType.Cursor, System.Data.ParameterDirection.Output);
            sql.Parameter = prs;
            sql.Sql = "testmdatas1";
            var ds31 = sqlBuilder.QueryDataSet(sql);
            Console.WriteLine("Tables1=" + ds31.Tables.Count);
            Console.WriteLine(ds31.ToJson());

            var sql2 = sqlBuilder.CreateSqlEntity();
            sql2.CommandType = System.Data.CommandType.StoredProcedure;
            var prs2 = new OracleDynamicParameters();
            prs2.Add("v_cur_out1", System.Data.OracleClient.OracleType.Cursor, System.Data.ParameterDirection.Output);
            prs2.Add("v_cur_out2", System.Data.OracleClient.OracleType.Cursor, System.Data.ParameterDirection.Output);
            prs2.Add("v_cur_out3", System.Data.OracleClient.OracleType.Cursor, System.Data.ParameterDirection.Output);
            sql2.Parameter = prs2;
            sql2.Sql = "testmdatas";

            var ds3212 = sqlBuilder.QueryMultiple<object,object,object>(sql2);
            Console.WriteLine("QueryMultiple.Item1=" + ds3212.Item1.ToJson());
            Console.WriteLine("QueryMultiple.Item2=" + ds3212.Item2.ToJson());
            Console.WriteLine("QueryMultiple.Item3=" + ds3212.Item3.ToJson());

            var ds321 = sqlBuilder.QueryDataSet(sql2);
            Console.WriteLine("Tables=" + ds321.Tables.Count);
            Console.WriteLine(ds321.ToJson());
        }

        public static void TestMsAccessSqlBuilder()
        {
            var sqlBuilder = new SqlBuilder("MsAccess2");
            var ping = sqlBuilder.Ping();

            var dt = sqlBuilder.DoSelect.GetDateNow();

            long totalNumber = 0;
            var lst = sqlBuilder.doSelect<Entity_Orders>()
                 //.Fields(null)//添加查询多个字段
                 //.Where(w => w.ksid.Between(1100440, 1100450))
                 .OrderBy(d => d.OrderID)//按列倒向排序
                 //.Query();
                 .QueryPagingList(3, 10, ref totalNumber); //返回结果

            var order_Details = new Entity_Order_Details() { OrderID = 10268, Discount = 4.5f, ProductID = 72, Quantity = 5, UnitPrice =28.3M };
            //var ret = sqlBuilder.doDelete<Entity_Order_Details>()//实体删除
            //     .Delete(order_Details)
            //     .Exec;

            var ret1 = sqlBuilder.doUpdate<Entity_Order_Details>()//插入实体
               .Update(order_Details, w => w.OrderID == 10268 && w.ProductID == 72, "Quantity")
               ;

            //ret = sqlBuilder.doInsert<Entity_Order_Details>()//插入实体
            //    .Insert(order_Details)
            //    .Execute();

            var lst3 = sqlBuilder.doSelect<Entity_Order_Details>()
                 //.Fields(null)//添加查询多个字段
                 .Where(w => w.OrderID== 10268 && w.ProductID==72)                                         //.Query();
                 .Query(); //返回结果
        }
    }
}
