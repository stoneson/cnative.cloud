﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CNative.Dapper.Utils
{
    public class AopProvider
    {
        private AopProvider() { }
        public AopProvider(DbHelper context)
        {
            this.Context = context;
            this.IsEnableLogEvent = true;
            this.AopEvents = new AopEvents();
        }
        public AopProvider(string dbName) : this(new DbHelper(dbName))
        {
        }
        protected DbHelper Context { get; set; }
        public virtual bool IsEnableLogEvent { get; set; }
        public AopEvents AopEvents { get; set; }
        public Action<DiffLogModel> OnDiffLogEvent { set { this.AopEvents.OnDiffLogEvent = value; } }
        public Action<CNativeException> OnError { set { this.AopEvents.OnError = value; } }
        public Action<SqlEntity> OnLogExecuting { set { this.AopEvents.OnLogExecuting = value; } }
        public Action<SqlEntity> OnLogExecuted { set { this.AopEvents.OnLogExecuted = value; } }
    }

    public class AopEvents
    {
        public Action<DiffLogModel> OnDiffLogEvent { get; set; }
        public Action<CNativeException> OnError { get; set; }
        public Action<SqlEntity> OnLogExecuting { get; set; }
        public Action<SqlEntity> OnLogExecuted { get; set; }
    }
}
