﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using CNative.Utilities;
using System.Data.OleDb;

namespace CNative.Dapper.Utils
{
    internal class MsAccessProvider : BaseProvider
    {
        public MsAccessProvider(IDbHelper _db) : base(_db)
        {
        }

        #region 关键字
        /// <summary>
        /// 数据库提供程序名字
        /// </summary>
        public override string ProviderName
        {
            get
            {
                return "System.Data.OleDb";
            }
        }
        public override string ProviderNameFactory
        {
            get
            {
                return "System.Data.OleDb.OleDbFactory";
            }
        }
        /// <summary>
        /// 数据库类型
        /// </summary>
        public override DatabaseType DBType { get { return DatabaseType.MsAccess; } }
        /// <summary>
        /// 参数关键字 @ or :
        /// </summary>
        public override string ParamKeyword
        {
            get
            {
                return "@";
            }
        }
        /// <summary>
        /// 关键字前缀 [
        /// </summary>
        public override string SuffixLeft
        {
            get
            {
                return "[";
            }
        }
        /// <summary>
        /// 关键字后缀 ]
        /// </summary>
        public override string SuffixRigh
        {
            get
            {
                return "]";
            }
        }
        #endregion

        #region DbProviderFactory
       
        public override string GetSchemaTableName(Type tableType)
        {
            var tb = Funs.GetDbTableInfo(dbHelper, tableType);
            if (tb == null)
            {
                return "";
            }
            return tb.Schema.IsNullOrEmpty() ? SuffixLeft + tb.TableName + SuffixRigh : SuffixLeft + tb.Schema + SuffixRigh + ".dbo." + SuffixLeft + tb.TableName + SuffixRigh;
        }
        #endregion

        #region GetDbParameter
        ///// <summary>
        ///// 获取DbParameter
        ///// </summary>
        ///// <param name="dbType">数据库类型</param>
        ///// <returns></returns>
        //public override IDataParameter GetDbParameter(string propName, object val, string paramSuffix = "exp_",
        //    DbTableInfo tb = null, ParameterDirection direction = ParameterDirection.Input, DbType valDBType = DbType.String)
        //{
        //    IDataParameter para = null;
        //    //--------------------------------------------------------------------------------------------------------------
        //    var frets = FillerParameter(propName, val, paramSuffix);
        //    if (frets.Item1)
        //    {
        //        return para;
        //    }
        //    var parameterName = frets.Item2;
        //    val = frets.Item3;
        //    //--------------------------------------------------------------------------------------------------------------
        //    #region OleDb
        //    if (direction == ParameterDirection.Output)
        //    {
        //        para = new System.Data.OleDb.OleDbParameter()
        //        {
        //            ParameterName = parameterName,
        //            Value = val,
        //            DbType = valDBType,
        //            Direction = direction
        //        };
        //        return para;
        //    }
        //    para = new System.Data.OleDb.OleDbParameter(parameterName, val) {  Direction= direction };
        //    if (para is System.Data.OleDb.OleDbParameter par)
        //    {
        //        if (tb != null && tb.TableInfo != null)
        //        {
        //            #region TableInfo
        //            var dtinfo = tb.TableInfo.Find(f =>
        //                f.Name.Equals(propName.Trim(), StringComparison.OrdinalIgnoreCase));
        //            if (dtinfo != null)
        //            {
        //                try
        //                {
        //                    (para as System.Data.OleDb.OleDbParameter).OleDbType = SqlTypeString2SqlType(dtinfo.Type);// dtinfo.Type.ToEnum<MySqlDbType>();
        //                    if (dtinfo.MaxLength > 0 && dtinfo.MaxLength < int.MaxValue)
        //                        (para as System.Data.OleDb.OleDbParameter).Size = (int)dtinfo.MaxLength;
        //                }
        //                catch { }
        //            }

        //            #endregion
        //        }
        //    }
        //    #endregion
        //    return para;
        //}

        //private OleDbType SqlTypeString2SqlType(string sqlTypeString)
        //{
        //    OleDbType dbType = OleDbType.Variant; //默认为Object
        //    sqlTypeString = sqlTypeString.Trim().ToLower();
        //    var parameterType = GetParameterTypeName(sqlTypeString);
        //    dbType = parameterType.ToEnum<OleDbType>();
        //    return dbType;
        //}
        #endregion
        #region MappingTypes
        /// <summary>
        /// 类型映射
        /// sqlTypeName，CsharpType，ParameterType
        /// </summary>
        public override List<Tuple<string, string, string>> MappingTypes
        {
            get
            {
                return new List<Tuple<string, string, string>>()
                    {
                        Tuple.Create("bigint", "int", "BigInt"),
                        Tuple.Create("binary", "byte[]", "Binary"),
                        Tuple.Create("boolean", "bool", "Boolean"),
                        Tuple.Create("WChar", "string", "WChar"),
                        Tuple.Create("char", "string", "Char"),
                        Tuple.Create("BSTR", "string", "Char"),
                        Tuple.Create("DBTime", "TimeSpan", "DBTime"),
                        Tuple.Create("DBDate", "DateTime", "DBDate"),
                        Tuple.Create("DBTimeStamp", "DateTime", "DBTimeStamp"),
                        Tuple.Create("Filetime", "DateTime", "Filetime"),
                        Tuple.Create("datetime", "DateTime", "DateTime"),
                        Tuple.Create("date", "DateTime", "Date"),
                        Tuple.Create("time", "DateTime", "Time"),
                        Tuple.Create("datetimeoffset", "DateTime", "DateTimeOffset"),
                        Tuple.Create("Currency", "decimal", "Currency"),
                        Tuple.Create("decimal", "decimal", "Decimal"),
                        Tuple.Create("Single", "float", "Single"),
                        Tuple.Create("Double", "Double", "Double"),
                        Tuple.Create("image", "byte[]", "Image"),
                        Tuple.Create("int", "int", "Int"),
                        Tuple.Create("money", "decimal", "Money"),
                        Tuple.Create("nchar", "string", "NChar"),
                        Tuple.Create("VarWChar", "string", "VarWChar"),
                        Tuple.Create("LongVarChar", "string", "LongVarChar"),
                        Tuple.Create("nchar", "string", "NChar"),
                        Tuple.Create("nchar", "string", "NChar"),
                        Tuple.Create("ntext", "string", "NText"),
                        Tuple.Create("VarNumeric", "decimal", "VarNumeric"),
                        Tuple.Create("nvarchar", "string", "NVarChar"),
                        Tuple.Create("real", "decimal", "Real"),
                        Tuple.Create("smalldatetime", "DateTime", "SmallDateTime"),
                        Tuple.Create("smallint", "int", "SmallInt"),
                        Tuple.Create("Integer", "int", "Integer"),
                        Tuple.Create("smallmoney", "decimal", "SmallMoney"),
                        Tuple.Create("sql_variant", "string", "Variant"),
                        Tuple.Create("text", "string", "Text"),
                        Tuple.Create("timestamp", "string", "Timestamp"),
                        Tuple.Create("tinyint", "int", "TinyInt"),
                        Tuple.Create("uniqueidentifier", "Guid", "UniqueIdentifier"),
                        Tuple.Create("varbinary", "byte[]", "VarBinary"),
                        Tuple.Create("varchar", "string", "VarChar"),
                        Tuple.Create("xml", "string", "Xml"),
                        Tuple.Create("PropVariant", "string", "PropVariant"),
                        Tuple.Create("VarBinary", "byte[]", "VarBinary"),
                        Tuple.Create("LongVarBinary", "byte[]", "LongVarBinary"),
                        Tuple.Create("sysname", "string", "Variant")
                };
            }
        }

        /// <summary>
        /// 获取库类型
        /// </summary>
        /// <param name="csharpTypeName"></param>
        /// <returns></returns>
        public override string GetDbTypeName(string csharpTypeName)
        {
            if (csharpTypeName == Constants.ByteArrayType.Name)
                return "varbinary";
            csharpTypeName = CheckCsharpTypeName(csharpTypeName);
            var mappings = this.MappingTypes?.Where(it => it.Item2.Equals(csharpTypeName, StringComparison.CurrentCultureIgnoreCase)).ToList();
            if (mappings != null && mappings.Count > 0)
                return mappings.First().Item1;
            else
                return "varchar";
        }
        /// <summary>
        /// 获取sql Parameter Type
        /// </summary>
        /// <param name="dbTypeName"></param>
        /// <returns></returns>
        public override string GetParameterTypeName(string dbTypeName)
        {
            var mappings = this.MappingTypes?.Where(it => it.Item1.Equals(dbTypeName, StringComparison.CurrentCultureIgnoreCase));
            return !mappings.IsNullOrEmpty_() ? mappings.First().Item3 : "VarChar";
        }
        #endregion

        #region Sql Fun Templet
        /// <summary>
        /// SQL NVL() 从两个表达式返回一个非 null 值	函数
        /// </summary>
        /// <param name="check_expression">将被检查是否为 NULL的表达式。可以是任何类型的。</param>
        /// <param name="replacement_value">在 check_expression 为 NULL时将返回的表达式。replacement_value 必须与 check_expresssion 具有相同的类型。</param>
        /// <returns>如果 check_expression 不为 NULL，那么返回该表达式的值；否则返回 replacement_value</returns>
        public override string SQL_NVL(string check_expression, string replacement_value)
        {
            return $"ISNULL({check_expression},{replacement_value})";
        }
        /// <summary>
        /// 获取数据库时间函数
        /// </summary>
        public override string SqlDateNow
        {
            get
            {
                return " now() ";
            }
        }
        /// <summary>
        /// 查寻当前时间字符串
        /// </summary>
        public override string FullSqlDateNow
        {
            get
            {
                return "SELECT now()";
            }
        }

        /// <summary>
        /// SQL UCASE() 函数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="column_name">列名</param>
        /// <returns>函数把字段的值转换为大写</returns>
        public override string SQL_UCASE(string column_name)
        {
            return $"UPPER({column_name})";
        }

        /// <summary>
        /// SQL LCASE() 函数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="column_name">列名</param>
        /// <returns>函数把字段的值转换为小写</returns>
        public override string SQL_LCASE(string column_name)
        {
            return $"LOWER({column_name})";
        }

        /// <summary>
        /// SQL LEFT(s,n) 函数
        /// </summary>
        /// <param name="str">列名</param>
        /// <returns>返回字符串 str 的前 length 个字符</returns>
        public override string SQL_LEFT(string str, int length)
        {
            return $"LEFT({str},{length})";
        }
        /// <summary>
        /// SQL RIGHT(s,n) 函数
        /// </summary>
        /// <param name="str">列名</param>
        /// <returns>返回字符串 str 的前 length 个字符</returns>
        public override string SQL_RIGHT(string str, int length)
        {
            return $"RIGHT({str},{length})";
        }
        /// <summary>
        /// SQL SUBSTR(s, start, length) 函数
        /// </summary>
        /// <param name="str">列名</param>
        /// <returns>从字符串 str 的 start 位置截取长度为 length 的子字符串</returns>
        public override string SQL_SUBSTR(string str, int start, int length)
        {
            return $"SUBSTRING({str},{1 + start},{length})";
        }
        /// <summary>
        /// SQL TRIM(s) 函数
        /// </summary>
        /// <param name="str">列名</param>
        /// <returns>去掉字符串 str 开始和结尾处的空格</returns>
        public override string SQL_TRIM(string str)
        {
            return $"TRIM({str})";
        }
        /// <summary>
        /// SQL CAST() 转换数据类型	函数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="column_name">列名</param>
        /// <param name="type">数据类型</param>
        /// <returns>转换数据类型</returns>
        public override string SQL_CAST(string column_name, string type)
        {
            return $"CAST({column_name} AS {type})";
        }

        public override string SQL_ToInt32(string column_name)
        {
            return SQL_CAST(column_name, "INT");
        }

        public override string SQL_ToInt64(string column_name)
        {
            return SQL_CAST(column_name, "BIGINT");
        }
        public override string SQL_ToDouble(string column_name)
        {
            return SQL_CAST(column_name, "Double");
        }
        /// <summary>转换高精度的十进制数（一般用于货币）</summary>
        /// <param name="column_name"></param>
        /// <param name="d">小数位数</param>
        /// <returns></returns>
        public override string SQL_ToDecimal(string column_name, int d)
        {
            if (d < 0 || d > 10) d = 4;
            return SQL_CAST(column_name, "DECIMAL(18, " + d + ")");
        }

        public override string SQL_ToBool(string column_name)
        {
            return SQL_CAST(column_name, "BIT");
        }

        public override string SQL_ToVarchar(string column_name)
        {
            return SQL_CAST(column_name, "VARCHAR(MAX)");
        }

        public override string SQL_ToGuid(string column_name)
        {
            return SQL_CAST(column_name, "UNIQUEIDENTIFIER");
        }

        public override string SQL_ToDate(string column_name)
        {
            return SQL_CAST(column_name, "DATETIME");
        }

        public override string SQL_ToDateShort(string column_name)
        {
            return SQL_CAST(column_name, "DATE");
        }

        public override string SQL_ToTime(string column_name)
        {
            return SQL_CAST(column_name, "TIME");
        }
        #endregion

        #region SqlTemplet
        /// <summary>
        /// 一般查询语句模板
        /// </summary>
        /// <param name="tbname"></param>
        /// <param name="fieldsstr"></param>
        /// <param name="orderbyStr"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public override string GetSelectSQL(String tbname, string fieldsstr, string orderbyStr, int top = 0)
        {
            var sqlMeta = @"SELECT " + (top > 0 ? " TOP " + top + " " : "") + fieldsstr + " FROM " + tbname + " WHERE {0} " + orderbyStr;
            return sqlMeta;
        }
        /// <summary>
        /// 分页查询语句模板
        /// </summary>
        /// <param name="tbName">表名</param>
        /// <param name="fieldsstr"></param>
        /// <param name="whereStr"></param>
        /// <param name="orderbyStr"></param>
        /// <param name="page">当前页面</param>
        /// <param name="nums">每页记录数</param>
        /// <returns></returns>
        public override string GetSelectPageSQL(string tbName, string fieldsstr, string whereStr, string orderbyStr, int page = 1, int nums = 25)
        {
            if (page < 1) page = 1;
            if (nums < 1) nums = 10;
            var m = (page - 1) * nums;// + 1;
            var n = page * nums;
            //if (m <= 0) m = n;

            var primaryKey = orderbyStr.Replace_("ORDER BY", "", true).Replace_("ASC", "", true).Replace_("desc", "", true).Trim();

            //field, start + pageSize, tableName, sortName, sortOrder, start, primaryKey, sqlWhere
            var sql = $"select a.* from ( select top {n} " + fieldsstr + $" from {tbName} where {whereStr} {orderbyStr}) a left join ( select top {(m <= 0 ? n : m)} " + fieldsstr
                + $" from {tbName} where {whereStr} and {m} > 0 {orderbyStr}) b on a.{primaryKey}=b.{primaryKey} where iif(b.{primaryKey},'0','1')='1'";

            return sql;
            //SELECT top 页大小* FROM table where ID > max(Select Top(页码 - 1) * 页大小 ID from table order by id)

            //var sqlMeta = "select * from("
            //                     + "select " + fieldsstr + $" ,ROW_NUMBER() over({orderbyStr}) as RN  from {tbName} where {whereStr }) as t "
            //                  + $" where t.RN between {m} and {n}";
            //return sqlMeta;
        }
        /// <summary>
        /// MERGE 合并语句模板
        /// </summary>
        /// <param name="_dbType"></param>
        /// <param name="tbname"></param>
        /// <returns></returns>
        public override string GetMergeSql(String tbname)
        {
            var sqlMeta = @"MERGE " + tbname + " AS [target]  USING(SELECT 1 AS ____Id_____) AS source ON {0}  WHEN MATCHED THEN {1}  WHEN NOT MATCHED THEN {2};";

            return sqlMeta;
        }

        /// <summary>
        /// 更新语句模板
        /// "UPDATE " + (isMerge ? "" : fromStr) + " SET {0} " + (isMerge ? "" : " WHERE {1} ")
        /// </summary>
        /// <param name="_dbType"></param>
        /// <param name="fromStr"></param>
        /// <param name="isMerge">是参与MERGE</param>
        /// <returns></returns>
        public override string GetUpdateSql(String fromStr, bool isMerge = false)
        {
            var sqlMeta = "UPDATE " + (isMerge ? "" : fromStr) + " SET {0} " + (isMerge ? "" : " WHERE {1} ");

            return sqlMeta;
        }
        /// <summary>
        /// 插入语句模板
        /// "INSERT " + (isMerge ? "" : " INTO " + fromStr) + " ({0}) VALUES ({1})"
        /// </summary>
        /// <param name="_dbType"></param>
        /// <param name="fromStr"></param>
        /// <param name="isMerge">是参与MERGE</param>
        /// <returns></returns>
        public override string GetInsertSql(String fromStr, bool isMerge = false)
        {
            var sqlMeta = "INSERT " + (isMerge ? "" : " INTO " + fromStr) + " ({0}) VALUES ({1})";

            return sqlMeta;
        }
        /// <summary>
        /// 统计数量语句模板
        /// $"SELECT Count(0) FROM {fromStr} WHERE {0}";
        /// </summary>
        /// <param name="_dbType"></param>
        /// <param name="fromStr"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public override string GetCountSql(String fromStr, string fieldName = "")
        {
            var sqlMeta = "SELECT Count(" + (fieldName.IsNullOrEmpty() ? "0" : fieldName) + ") FROM " + fromStr + " WHERE {0}";

            return sqlMeta;
        }
        /// <summary>
        /// GetInsertSelectSql
        /// INSERT INTO " + insertTable + " ({0}) " + " SELECT {1} FROM " + selectTable + "  WHERE {2} 
        /// </summary>
        /// <param name="insertTable"></param>
        /// <param name="selectTable"></param>
        /// <param name="_dbType"></param>
        /// <returns></returns>
        public override string GetInsertSelectSql(String insertTable, String selectTable)
        {
            var sqlMeta = "INSERT INTO " + insertTable + " ({0}) " +
                            " SELECT {1} FROM " + selectTable + "  WHERE {2} ";

            return sqlMeta;
        }
        #endregion

        #region WithNextSequence
        /// <summary>
        /// 下个序列脚本
        /// </summary>
        /// <param name="sequenceName"></param>
        /// <returns></returns>
        public override string WithNextSequence(string sequenceName = null)
        {
            if (string.IsNullOrEmpty(sequenceName))
            {
                sequenceName = "default_seq";
            }
            return $"nextval('{sequenceName}')";
        }
        #endregion

        #region DML CodeFirst
        /// <summary>
        /// 获取所有数据库
        /// </summary>
        public override string GetDataBaseSql
        {
            get
            {
                return "database";
            }
        }
        /// <summary>
        /// 获取数据库中的所有表
        /// </summary>
        public override string GetTableInfoListSql
        {
            get
            {
                return @"{0}select name,'' Description from MSYSOBJECTS where [Flags]=0  and [type] = 1 order by name";
            }
        }
        /// <summary>
        /// 获取数据库中的所有视图
        /// </summary>
        public override string GetViewInfoListSql
        {
            get
            {
                return @"{0}select name,'' Description from MSYSOBJECTS where [Flags]=0  and [type] = 0 order by name";
            }
        }
        /// <summary>
        /// 得到一个表的所有列信息
        /// </summary>
        public override string GetColumnInfosByTableNameSql
        {
            get
            {
                return @"";
            }
        }
        #endregion
    }
}
