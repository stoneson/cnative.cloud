﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using CNative.Utilities;

namespace CNative.Dapper.Utils
{
    internal class OracleOleDbProvider : OracleProvider
    {
        public OracleOleDbProvider(IDbHelper _db) : base(_db)
        {
        }
        #region 关键字
        /// <summary>
        /// 数据库提供程序名字
        /// </summary>
        public override string ProviderName
        {
            get
            {
                return "System.Data.OleDb";//依赖于oracle官方驱动，需要另外安装oracle客户端
                //return "Oracle.DataAccess.Client";//Oracle数据库，官方非托管驱动，限制比较多
                //return "Devart.Data.Oracle";//Oracle官方托管驱动,10g以下版本不支持，无任何依赖
            }
        }
        public override string ProviderNameFactory
        {
            get
            {
                return "System.Data.OleDb.OleDbFactory";
            }
        }
        /// <summary>
        /// 数据库类型
        /// </summary>
        public override DatabaseType DBType { get { return DatabaseType.Oracle; } }
        #endregion

        #region MappingTypes
        /// <summary>
        /// 类型映射
        /// sqlTypeName，CsharpType，ParameterType
        /// </summary>
        public override List<Tuple<string, string, string>> MappingTypes
        {
            get
            {
                return new List<Tuple<string, string, string>>()
                    {
                        Tuple.Create("BFILE", "byte[]", "BFile"),
                        Tuple.Create("BLOB", "byte[]", "Blob"),
                        Tuple.Create("CHAR", "string", "Char"),
                        Tuple.Create("CLOB", "string", "Clob"),
                        Tuple.Create("DATE", "DateTime", "DateTime"),
                        Tuple.Create("TIMESTAMP", "DateTime", "DateTime"),
                        Tuple.Create("TIMESTAMP WITH LOCAL TIME ZONE", "DateTime", "DateTime"),
                        Tuple.Create("TIMESTAMP WITH TIME ZONE", "DateTime", "DateTime"),
                        Tuple.Create("LONG RAW", "byte[]", "LongRaw"),
                        Tuple.Create("LONG", "string", "LongVarChar"),
                        Tuple.Create("NCHAR", "string", "NChar"),
                        Tuple.Create("NCLOB", "string", "NClob"),
                        Tuple.Create("NUMBER", "decimal", "Number"),
                        Tuple.Create("INTEGER", "decimal", "Number"),
                        Tuple.Create("UNSIGNED INTEGER", "decimal", "Number"),
                        Tuple.Create("NVARCHAR2", "string", "NVarChar"),
                        Tuple.Create("RAW", "byte[]", "Raw"),
                        Tuple.Create("RAWID", "string", "RowId"),
                        Tuple.Create("VARCHAR2", "string", "VarChar"),
                        Tuple.Create("FLOAT", "decimal", "Float")
                    };
            }
        }

        #endregion
    }
}
