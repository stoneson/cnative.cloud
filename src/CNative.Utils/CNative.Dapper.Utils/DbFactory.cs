﻿using System;
using System.Configuration;

namespace CNative.Dapper.Utils
{
    public class DbFactory
    {
        public static ISqlBuilder CreateSqlBuilder(string dbName)
        {
            return new SqlBuilder(dbName);
        }

        public static IDbHelper CreateDb(string dbName)
        {
            return new DbHelper(dbName);
        }
    }

}
