﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Dapper.Utils
{
    #region IInsertSqlBuilder
    public partial interface IInsertSqlBuilder : IExecuteSql
    {
        /// <summary>
        /// 插入实体表达式
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        IInsertSqlBuilder Insert<TClass>(Expression<Func<TClass,TClass>> values) where TClass : class, new();
        /// <summary>
        /// 插入实体
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        IInsertSqlBuilder Insert<TClass>(TClass entity) where TClass : class, new();
        /// <summary>
        /// 插入实体集合
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Insert<TClass>(List<TClass> entities) where TClass : class, new();
        /// <summary>  
        /// 批量插入功能  
        /// </summary>  
        bool BulkCopyData(System.Data.DataTable table, string destinationTableName = null, int? bulkCopyTimeout = null);
        /// <summary>  
        /// 批量插入功能  
        /// </summary>  
        bool BulkCopyData<TClass>(List<TClass> entityList, int? bulkCopyTimeout = null) where TClass : class, new();
        /// <summary>
        /// 从一个表复制数据，然后把数据插入到一个已存在的表中
        /// INSERT INTO SELECT 语句
        /// <typeparam name="TINTO">INSERT INTO目标表</typeparam>
        /// <typeparam name="TSELECT">SELECT数据源表</typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        IInsertSqlBuilder InsertSelect<TINTO, TSELECT>(Expression<Func<TSELECT,TINTO>> values, Expression<Func<TSELECT, bool>> where) where TINTO : class, new() where TSELECT : class, new();
        //---------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        bool InsertOrUpdate<TClass>(TClass entity, Expression<Func<TClass, bool>> where, params string[] columns) where TClass : class, new();

        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="columns">需要更新的字段</param>
        bool InsertOrUpdate<TClass>(List<TClass> entities,  params string[] columns) where TClass : class, new();

        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        bool InsertOrUpdate<TClass>(List<TClass> entities, IEnumerable<string> IgnoreColumns = null) where TClass : class, new();

        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        bool InsertOrUpdate<TClass>(TClass entity, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns) where TClass : class, new();

        /// <summary>
        /// 保存数据（自动新增或更新动作）
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="fields">设置多个更新字段</param>
        /// <param name="where">条件表达式</param>
        /// <returns></returns>
        bool InsertOrUpdate<TClass>(Expression<Func<TClass,TClass>> fields, Expression<Func<TClass, bool>> where) where TClass : class, new();

        //---------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists<TClass>(TClass entity) where TClass : class, new();
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists<TClass>(Expression<Func<TClass, bool>> where) where TClass : class, new();
    }
    #endregion
    #region IInsertSqlBuilder<TClass>
    /// <summary>
    /// 单表操作
    /// </summary>
    /// <typeparam name="TClass"></typeparam>
    public partial interface IInsertSqlBuilder<TClass> : IExecuteSql where TClass : class, new()
    {
        /// <summary>
        /// 插入实体表达式
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        IInsertSqlBuilder<TClass> Insert(Expression<Func<TClass,TClass>> values);
        /// <summary>
        /// 插入实体
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        IInsertSqlBuilder<TClass> Insert(TClass entity);
        /// <summary>
        /// 插入实体集合
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Insert(List<TClass> entities);
        /// <summary>  
        /// 批量插入功能  
        /// </summary>  
        bool BulkCopyData(System.Data.DataTable table, string destinationTableName = null, int? bulkCopyTimeout = null);
        /// <summary>  
        /// 批量插入功能  
        /// </summary>  
        bool BulkCopyData(List<TClass> entityList, int? bulkCopyTimeout = null);
        /// <summary>
        /// 从一个表复制数据，然后把数据插入到一个已存在的表中
        /// INSERT INTO SELECT 语句
        /// <typeparam name="TClass">INSERT INTO目标表</typeparam>
        /// <typeparam name="TSELECT">SELECT数据源表</typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        IInsertSqlBuilder<TClass> InsertSelect<TSELECT>(Expression<Func<TSELECT, TClass>> values, Expression<Func<TSELECT, bool>> where) where TSELECT : class, new();
        //---------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        bool InsertOrUpdate(TClass entity, Expression<Func<TClass, bool>> where, params string[] columns);

        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="columns">需要更新的字段</param>
        bool InsertOrUpdate(List<TClass> entities, params string[] columns);

        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        bool InsertOrUpdate(List<TClass> entities, IEnumerable<string> IgnoreColumns = null) ;

        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        bool InsertOrUpdate(TClass entity, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns);

        /// <summary>
        /// 保存数据（自动新增或更新动作）
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="fields">设置多个更新字段</param>
        /// <param name="where">条件表达式</param>
        /// <returns></returns>
        bool InsertOrUpdate(Expression<Func<TClass,TClass>> fields, Expression<Func<TClass, bool>> where);

        //---------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists(TClass entity);
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists(Expression<Func<TClass, bool>> where);
    }
    #endregion
}
