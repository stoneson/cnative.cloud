﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Dapper.Utils
{
    public partial interface ISqlBuilder : IDbHelper 
    {
        /// <summary>
        /// 开始添加(可多表操作)
        /// </summary>
        /// <returns></returns>
        IInsertSqlBuilder DoInsert { get; }
        /// <summary>
        /// 开始查找(可多表操作)
        /// </summary>
        /// <returns></returns>
        ISelectSqlBuilder DoSelect { get; }
        /// <summary>
        /// 开始更新(可多表操作)
        /// </summary>
        /// <returns></returns>
        IUpdateSqlBuilder DoUpdate { get; }
        /// <summary>
        /// 开始删除(可多表操作)
        /// </summary>
        /// <returns></returns>
        IDeleteSqlBuilder DoDelete { get; }

        IDbFirst DbFirst { get; }
        ICodeFirst CodeFirst { get; }
        //------------------------------------------------------------------------------------
        /// <summary>
        /// 开始添加(单表操作)
        /// </summary>
        /// <returns></returns>
        IInsertSqlBuilder<TClass> doInsert<TClass>() where TClass : class, new();
        /// <summary>
        /// 开始查找(单表操作)
        /// </summary>
        /// <returns></returns>
        ISelectSqlBuilder<TClass> doSelect<TClass>() where TClass : class, new();
        /// <summary>
        /// 开始更新(单表操作)
        /// </summary>
        /// <returns></returns>
        IUpdateSqlBuilder<TClass> doUpdate<TClass>() where TClass : class, new();
        /// <summary>
        /// 开始删除(单表操作)
        /// </summary>
        /// <returns></returns>
        IDeleteSqlBuilder<TClass> doDelete<TClass>() where TClass : class, new();

    }
}
