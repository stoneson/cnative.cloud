﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Dapper.Utils
{
    public partial interface IQuerySql : IDisposable
    {
        /// <summary>
        /// 返回sql脚本实体
        /// </summary>
        /// <returns></returns>
        SqlEntity GetSql(bool isCount = false);
        //-----------------------------------------------------------------------------------
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        DataSet QueryDataSet();

        /// <summary>
        /// 查询返回DataTable
        /// </summary>
        /// <returns></returns>
        DataTable QueryDataTable();

        /// <summary>
        /// 查询返回单个字段值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetSingle<T>();

        /// <summary>
        /// 返回数据库当前时间
        /// </summary>
        /// <returns></returns>
        DateTime GetDateNow();
        //---------------------------------------------------------------------------------
#if !NET40
        /// <summary>
        /// 异步返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        Task<DataSet> QueryDataSetAsync();
        /// <summary>
        /// 异步返回DataTable
        /// </summary>
        /// <returns></returns>
        Task<DataTable> QueryDataTableAsync();
        
        /// <summary>
        /// 异步返回单个字段值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<T> GetSingleAsync<T>();
#endif
    }
}
