﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CNative.Dapper.Utils
{
    public partial interface ICodeFirst
    {
        DbHelper Context { get; set; }
        ICodeFirst SettingClassTemplate(Func<string, string> func);
        ICodeFirst SettingClassDescriptionTemplate(Func<string, string> func);
        ICodeFirst SettingPropertyTemplate(Func<string, string> func);
        ICodeFirst SettingPropertyDescriptionTemplate(Func<string, string> func);
        ICodeFirst SettingConstructorTemplate(Func<string, string> func);
        ICodeFirst SettingNamespaceTemplate(Func<string, string> func);
        ICodeFirst IsCreateAttribute(bool isCreateAttribute = true);
        ICodeFirst IsCreateDefaultValue(bool isCreateDefaultValue = true);
        ICodeFirst IsGuid2tringValue(bool isGuid2tring = true);
        ICodeFirst IsSetEntitySuffix(string entitySuffix = "");
        ICodeFirst Where(params string[] objectNames);
        ICodeFirst Where(Func<string, bool> func);
        ICodeFirst WhereColumns(Func<string, bool> func);
        ICodeFirst Where(DbObjectType dbObjectType);
        void CreateClassFile(string directoryPath, string creator, string nameSpace = "Models");
        Dictionary<string, string> ToClassStringList(string creator = "xx", string nameSpace = "Models");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="creator"></param>
        /// <param name="nameSpace"></param>
        /// <returns>className,classText</returns>
        Tuple<string, string> ToClassStringList(string tableName, string creator = "xx", string nameSpace = "Models");
        void CreateClassFile(string directoryPath, string tableName, string creator, string nameSpace = "Models");
        void Init(string dbName);
    }
}
