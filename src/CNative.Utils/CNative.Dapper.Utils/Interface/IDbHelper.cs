﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CNative.Dapper.Utils
{
    public interface IDbHelper : IDisposable
    {
        /// <summary>
        /// 当前数据库连接字符串key名称
        /// </summary>
        string DBName { get; }
        /// <summary>
        /// 当前数据库连接字符串
        /// </summary>
        string ConnectString { get; set; }
        /// <summary>
        /// 是否启用主从分离模式
        /// </summary>
        bool IsUseMasterSlaveSeparation { get; set; }
        /// <summary>
        /// 主数据库连接字符串
        /// </summary>
        string MasterConnectString { get; set; }
        /// <summary>
        /// 从数据库连接字符串集合
        /// </summary>
        List<string> SlaveConnectStrings { set; get; }
        int? CommandTimeout { get; set; }
        /// <summary>
        /// 数据库类型
        /// </summary>
        DatabaseType DBType { get; }
        /// <summary>
        /// 数据库提供者
        /// </summary>
        BaseProvider SqlDbProvider { get; }
        AopProvider Aop { get; }
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        bool Execute(SqlEntity sql);
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        //bool Execute(string sql,List<IDataParameter> parameters);
        /// <summary>
        /// 批量执行脚本
        /// </summary>
        /// <param name="sqlList"></param>
        /// <returns></returns>
        bool Execute(List<SqlEntity> sqlList);
        /// <summary>  
        /// 批量插入功能  
        /// </summary>  
        bool BulkCopyData(DataTable table, string destinationTableName = null, int? bulkCopyTimeout = null);
        /// <summary>  
        /// 批量插入功能  
        /// </summary>  
        bool BulkCopyData<T>(List<T> entityList, string dbName = "", string destinationTableName = null, int? bulkCopyTimeout = null) where T : class, new();
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        DataSet QueryDataSet(SqlEntity sql);
        /// <summary>
        /// 查寻返回DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        DataTable QueryDataTable(SqlEntity sql);
        /// <summary>
        /// 查寻返回实体集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        List<T> Query<T>(SqlEntity sql) where T : class;

        /// <summary>
        /// 执行多个查询并返回多个结果
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        IEnumerable<dynamic> QueryMultiple(SqlEntity sql);
        /// <summary>
        /// 执行多个查询并返回多个结果
        /// </summary>
        /// <typeparam name="Entity1"></typeparam>
        /// <typeparam name="Entity2"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        Tuple< List<Entity1>, List<Entity2>> QueryMultiple<Entity1,Entity2>(SqlEntity sql);
        /// <summary>
        /// 执行多个查询并返回多个结果
        /// </summary>
        /// <typeparam name="Entity1"></typeparam>
        /// <typeparam name="Entity2"></typeparam>
        /// <typeparam name="Entity3"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        Tuple<List<Entity1>, List<Entity2>, List<Entity3>> QueryMultiple<Entity1, Entity2, Entity3>(SqlEntity sql);
        /// <summary>
        /// 执行多个查询并返回多个结果
        /// </summary>
        /// <typeparam name="Entity1"></typeparam>
        /// <typeparam name="Entity2"></typeparam>
        /// <typeparam name="Entity3"></typeparam>
        /// <typeparam name="Entity4"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        Tuple<List<Entity1>, List<Entity2>, List<Entity3>, List<Entity4>> QueryMultiple<Entity1, Entity2, Entity3, Entity4>(SqlEntity sql);
        /// <summary>
        /// 执行多个查询并返回多个结果
        /// </summary>
        /// <typeparam name="Entity1"></typeparam>
        /// <typeparam name="Entity2"></typeparam>
        /// <typeparam name="Entity3"></typeparam>
        /// <typeparam name="Entity4"></typeparam>
        /// <typeparam name="Entity5"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        Tuple<List<Entity1>, List<Entity2>, List<Entity3>, List<Entity4>, List<Entity5>> QueryMultiple<Entity1, Entity2, Entity3, Entity4, Entity5>(SqlEntity sql);
        /// <summary>
        /// 执行多个查询并返回多个结果
        /// </summary>
        /// <typeparam name="Entity1"></typeparam>
        /// <typeparam name="Entity2"></typeparam>
        /// <typeparam name="Entity3"></typeparam>
        /// <typeparam name="Entity4"></typeparam>
        /// <typeparam name="Entity5"></typeparam>
        /// <typeparam name="Entity6"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        Tuple<List<Entity1>, List<Entity2>, List<Entity3>, List<Entity4>, List<Entity5>, List<Entity6>> QueryMultiple<Entity1, Entity2, Entity3, Entity4, Entity5, Entity6>(SqlEntity sql);
        /// <summary>
        /// 查寻返回单行实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        T QuerySingle<T>(SqlEntity sql) where T : class;
        /// <summary>
        /// 查寻返回单个字段值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        T GetSingle<T>(SqlEntity sql);
        /// <summary>
        /// Execute parameterized SQL and return an System.Data.IDataReader
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        IDataReader GetDataReader(SqlEntity sql);
        /// <summary>
        /// 开始单库事物
        /// </summary>
        /// </summary>
        /// <param name="isol"></param>
        /// <returns></returns>
        IDbTransaction BeginTransaction(System.Data.IsolationLevel isol = IsolationLevel.ReadCommitted);
        /// <summary>
        /// 结束事物提交
        /// </summary>
        void Commit();
        /// <summary>
        /// 创建脚本对象
        /// </summary>
        /// <returns></returns>
        SqlEntity CreateSqlEntity(string sql);
        /// <summary>
        /// 创建脚本对象
        /// </summary>
        /// <returns></returns>
        SqlEntity CreateSqlEntity();
        /// <summary>
        /// 判断当前连接字符串是否有效
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="isThrow"></param>
        /// <returns></returns>
        bool Ping(string connectionString = "", bool isThrow = false);
    }
}
