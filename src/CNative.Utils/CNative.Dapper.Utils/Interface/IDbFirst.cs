﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CNative.Dapper.Utils
{
    public partial interface IDbFirst
    {
        DbHelper Context { get; set; }
        IDbFirst BackupTable(int maxBackupDataRows = int.MaxValue);
        IDbFirst SetStringDefaultLength(int length);
        void InitTables(string entitiesNamespace);
        void InitTables(string[] entitiesNamespaces);
        void InitTables(params Type[] entityTypes);
        void InitTables(Type entityType);
        void InitTables<T>();
    }
}
