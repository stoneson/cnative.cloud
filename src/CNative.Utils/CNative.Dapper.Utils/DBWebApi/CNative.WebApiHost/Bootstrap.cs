﻿
using CNative.WebApi.Common;
using CNative.WebApi.Utils;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
namespace CNative.WebApiHost
{
    public class Bootstrap : WebApi.ServiceProcess.BootstrapBase
    {
        #region Bootstrap
        public Bootstrap(string serviceName, string description, IConfigBase _configBase)
          : base(serviceName, description, _configBase)
        {
        }
        #endregion

        #region Start/Stop
        public override void Start()
        {
            try
            {
                base.Start();
                StartWebAPI();
            }
            catch (Exception ex) { LogUtil.Error(ex.ToString()); }
        }
        private void StartWebAPI()
        {
            var port = ServiceHelper.GetAppSettingsVal("WebApiServicePort", "8500").NullToInt();
            var options = new StartOptions();
            //options.Urls.Add("http://0.0.0.0:" + port);
            options.Urls.Add("http://127.0.0.1:" + port);
            options.Urls.Add("http://localhost:" + port);
            options.Urls.Add("http://+:" + port);
            WebApp.Start<Startup>(options);

            LogUtil.Debug("Web API 服务 启动成功, http://localhost:" + port + "/swagger");

            //-----------------------------------------------------------------------------------
            var wsService = ServiceHelper.GetAppSettingsVal("WSServices");
            if (!string.IsNullOrEmpty(wsService))
            {
                var wsServices = wsService.Split(';');
                foreach (var wss in wsServices)
                {
                    if (!string.IsNullOrEmpty(wss))
                    {
                        var wsss = wss.Split(',');
                        if (wsss.Length > 1)
                            ServiceHelper.SoapReceiversAdd(wsss[0], wsss[1]);
                    }
                }
            }
        }
        /// <summary>
        /// 停止此服务。
        /// </summary>
        public override void Stop()
        {
            try
            {
                base.Stop();
            }
            catch (Exception ex) { LogUtil.Error(ex.ToString()); }
        }
        #endregion

        #region timerTick
        /// <summary>
        /// 定时调用处理
        /// </summary>
        protected override void timerCall()
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                LogUtil.Error(ex.Message);
            }
            finally { this.endCall(); }
        }
        #endregion

    }
}
