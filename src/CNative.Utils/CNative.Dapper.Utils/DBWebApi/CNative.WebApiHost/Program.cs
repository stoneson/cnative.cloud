﻿using CNative.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CNative.WebApiHost
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main(string[] args)
        {
            MyProgram.Init();
            if (args != null && args.Length > 0)
            {
                LogUtil.Debug("Main args=" + string.Join("  ", args));
                MyProgram.InstallOrUninstallServerce(args, new Bootstrap(MyProgram.ServiceName, MyProgram.Description
                    , new MyConfig()));
            }
            else
            {
                MyProgram.MyMain(new Bootstrap(MyProgram.ServiceName, MyProgram.Description
                    , new MyConfig())
                    , () =>
                    {

                    });
            }
        }
    }
}
