﻿using CNative.WebApi.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;


namespace CNative.WebApiHost
{
    [RunInstaller(true)]
    public partial class iServiceInstaller : System.Configuration.Install.Installer
    {
        protected ServiceInstaller serviceInstaller;
        protected ServiceProcessInstaller processInstaller;

        public iServiceInstaller()
        {
            InitializeComponent();

            init();
            processInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            serviceInstaller.ServiceName = MyProgram.ServiceName;
            serviceInstaller.DisplayName = MyProgram.DisplayName;
            serviceInstaller.Description = MyProgram.Description;

            if (!string.IsNullOrEmpty(MyProgram.DependedOn) && MyProgram.DependedOn.Length > 0)
            {
                var servicesDependedOn = MyProgram.DependedOn.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (servicesDependedOn.Length > 0)
                    serviceInstaller.ServicesDependedOn = servicesDependedOn;
            }

            Installers.Add(serviceInstaller);
            Installers.Add(processInstaller);
        }
        protected virtual void init()
        {
            try
            {
                using (var setting = new SettingHelper())
                {
                    //系统用于标志此服务名称(唯一性)
                    MyProgram.ServiceName = setting.ServiceName;
                    //向用户标志服务的显示名称(可以重复)
                    MyProgram.DisplayName = setting.DisplayName;
                    //服务的说明(描述)
                    MyProgram.Description = setting.Description;
                    MyProgram.DependedOn = setting.DependedOn;
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
            }
        }
        public override void Commit(IDictionary savedState)
        {
            try
            {
                base.Commit(savedState);

                LogUtil.Debug("开始启动服务，" + serviceInstaller.ServiceName);
                SelfInstaller.StartService(serviceInstaller.ServiceName);

            }
            catch (Exception ex)
            {
                LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
            }
        }

        public override void Uninstall(IDictionary savedState)
        {
            try
            {
                try
                {
                    LogUtil.Debug("开始停止服务，" + serviceInstaller.ServiceName);
                    SelfInstaller.StopService(serviceInstaller.ServiceName);
                }
                catch (Exception ex)
                {
                    LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
                }
                base.Uninstall(savedState);
            }
            catch (Exception ex)
            {
                LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
            }
        }
    }
}
