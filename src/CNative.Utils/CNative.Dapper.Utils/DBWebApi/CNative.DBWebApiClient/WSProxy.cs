﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

using System.Web.Services;
using System.Web.Services.Protocols;

namespace CNative.DBWebApiClient
{
    [System.Web.Services.WebServiceBindingAttribute(Name = "ServiceSoap", Namespace = "http://tempuri.org/")]
    public partial class DBWebService : Microsoft.Web.Services3.WebServicesClientProtocol
    {
        /// <remarks/>
        public DBWebService(string ip, int port)
        {
            if (string.IsNullOrEmpty(ip)) ip = "localhost";
            if (port < 1 || port > 65535) port = 8600;

            this.Url = "soap.tcp://" + ip + ":" + port + "/DBWebService";
        }

        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/HelloWorld", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string Health()
        {
            object[] results = this.Invoke("Health", new object[0]);
            return ((string)(results[0]));
        }
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Execute", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public WSResponseMessage Execute(DBSqlEntity _eSql)
        {
            object[] results = this.Invoke("Execute", new object[] { _eSql });
            return ((WSResponseMessage)(results[0]));
        }
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/ExecuteList", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public WSResponseMessage ExecuteList(List<DBSqlEntity> _StrSqlList)
        {
            object[] results = this.Invoke("ExecuteList", new object[] { _StrSqlList });
            return ((WSResponseMessage)(results[0]));
        }
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/QueryDataSet", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public WSResponseMessage QueryDataSet(DBSqlEntity _eSql)
        {
            object[] results = this.Invoke("QueryDataSet", new object[] { _eSql });
            return ((WSResponseMessage)(results[0]));
        }
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Query", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public WSResponseMessage Query(DBSqlEntity _eSql)
        {
            object[] results = this.Invoke("Query", new object[] { _eSql });
            return ((WSResponseMessage)(results[0]));
        }
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/QuerySingle", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public WSResponseMessage QuerySingle(DBSqlEntity _eSql)
        {
            object[] results = this.Invoke("QuerySingle", new object[] { _eSql });
            return ((WSResponseMessage)(results[0]));
        }
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/ExecuteScalar", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public WSResponseMessage ExecuteScalar(DBSqlEntity _eSql)
        {
            object[] results = this.Invoke("ExecuteScalar", new object[] { _eSql });
            return ((WSResponseMessage)(results[0]));
        }
    }
}
