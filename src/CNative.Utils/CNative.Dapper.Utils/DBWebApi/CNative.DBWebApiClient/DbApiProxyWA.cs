﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;

namespace CNative.DBWebApiClient
{
    /// <summary>
    /// Web Api 调用代理
    /// </summary>
    public class DbApiProxyWA : IDbApiProxy
    {
       protected static MyWebClient serviceProxy = null;
        protected static string baseUrl = "";
        static DbApiProxyWA()
        {
            baseUrl = PubFuns.GetAppSettingsVal("WebApiBaseUrl", "http://localhost:8500/api/db/");
            baseUrl = baseUrl.TrimEnd('/')+"/";
            serviceProxy = new MyWebClient();
        }
        //------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="_eSql"></param>
        /// <returns></returns>
        public JsonResult<bool> Execute(DBSqlEntity _eSql)
        {
            return ApiHelper.ToJsonResult<bool>(() => { return serviceProxy.POST(baseUrl+ "Execute", _eSql.ToJson()); });
        }
        /// <summary>
        /// 批量执行脚本
        /// </summary>
        /// <param name="_StrSqlList"></param>
        /// <returns></returns>
        public JsonResult<bool> ExecuteList(List<DBSqlEntity> _StrSqlList)
        {
            return ApiHelper.ToJsonResult<bool>(() => { return serviceProxy.POST(baseUrl + "ExecuteList", _StrSqlList.ToJson()); });
        }
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<DataSet> QueryDataSet(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<DataSet>(() => { return serviceProxy.POST(baseUrl + "QueryDataSet", sql.ToJson()); });
        }
        /// <summary>
        /// 查寻返回实体集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<List<T>> Query<T>(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<List<T>>(() => { return serviceProxy.POST(baseUrl + "Query", sql.ToJson()); });
        }
        /// <summary>
        /// 查寻返回单行实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<T> QuerySingle<T>(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<T>(() => { return serviceProxy.POST(baseUrl + "QuerySingle", sql.ToJson()); });
        }
        /// <summary>
        /// 查寻返回单个字段值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<T> ExecuteScalar<T>(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<T>(() => { return serviceProxy.POST(baseUrl + "ExecuteScalar", sql.ToJson()); });
        }
    }
}
