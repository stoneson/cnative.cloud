﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;

namespace CNative.DBWebApiClient
{
    /// <summary>
    /// Web Service 调用代理
    /// </summary>
    public class DbApiProxyWS : IDbApiProxy
    {
       protected static DBWebService serviceProxy = null;
        static DbApiProxyWS()
        {
            var ip = PubFuns.GetAppSettingsVal("WSServiceIP", "localhost");
            var port = Convert.ToInt32(PubFuns.GetAppSettingsVal("WSServicePort", "8600"));
            serviceProxy = new DBWebService(ip, port);
            ConfigureProxy(serviceProxy);
        }
        protected static void ConfigureProxy(SoapHttpClientProtocol protocol)
        {
            string remoteHost = PubFuns.GetAppSettingsVal("remoteHost");
            if (remoteHost != null && !string.IsNullOrEmpty(remoteHost))
            {
                Uri remoteHostUri = new Uri(remoteHost);
                Uri protocolUrl = new Uri(protocol.Url);
                Uri newUri = new Uri(remoteHostUri, protocolUrl.AbsolutePath);

                if (protocol is Microsoft.Web.Services3.WebServicesClientProtocol)
                {
                    ((Microsoft.Web.Services3.WebServicesClientProtocol)protocol).Url = newUri.AbsoluteUri;
                }
                else
                {
                    protocol.Url = newUri.AbsoluteUri;
                }
            }

            protocol.SoapVersion = SoapProtocolVersion.Default;
        }
        //------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="_eSql"></param>
        /// <returns></returns>
        public JsonResult<bool> Execute(DBSqlEntity _eSql)
        {
            return ApiHelper.ToJsonResult<bool>(() => { return serviceProxy.Execute(_eSql).Content; });
        }
        /// <summary>
        /// 批量执行脚本
        /// </summary>
        /// <param name="_StrSqlList"></param>
        /// <returns></returns>
        public JsonResult<bool> ExecuteList(List<DBSqlEntity> _StrSqlList)
        {
            return ApiHelper.ToJsonResult<bool>(() => { return serviceProxy.ExecuteList(_StrSqlList).Content; });
        }
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<DataSet> QueryDataSet(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<DataSet>(() => { return serviceProxy.QueryDataSet(sql).Content; });
        }
        /// <summary>
        /// 查寻返回实体集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<List<T>> Query<T>(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<List<T>>(() => { return serviceProxy.Query(sql).Content; });
        }
        /// <summary>
        /// 查寻返回单行实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<T> QuerySingle<T>(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<T>(() => { return serviceProxy.QuerySingle(sql).Content; });
        }
        /// <summary>
        /// 查寻返回单个字段值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public JsonResult<T> ExecuteScalar<T>(DBSqlEntity sql)
        {
            return ApiHelper.ToJsonResult<T>(() => { return serviceProxy.ExecuteScalar(sql).Content; });
        }
    }
}
