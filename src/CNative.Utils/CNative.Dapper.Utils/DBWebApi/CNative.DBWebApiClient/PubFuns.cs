﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CNative.DBWebApiClient
{
    public static class PubFuns
    {
        public static string ExecutablePath = "";
        static PubFuns()
        {
            ExecutablePath = Assembly.GetEntryAssembly().Location;
        }
        #region GetAppSettingsVal
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static string GetAppSettingsVal(string key, string defaultVal = "", string configPath = "")
        {
            try
            {
                return GetAppSettings(key, defaultVal, configPath);
            }
            catch { return defaultVal; }
        }
        public static string GetAppConfig(string strKey, string def = "", string configPath = "")
        {
            var config = ConfigurationManager.OpenExeConfiguration(string.IsNullOrEmpty(configPath) ? ExecutablePath : configPath);
            foreach (string key in config.AppSettings.Settings.AllKeys)
            {
                if (key == strKey)
                {
                    return config.AppSettings.Settings[strKey].Value.ToString();
                }
            }
            return def;
        }
        public static string GetAppSettings(string strKey, string def = "", string configPath = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(configPath))
                {
                    var config = ConfigurationManager.OpenExeConfiguration(string.IsNullOrEmpty(configPath) ? ExecutablePath : configPath);
                    foreach (string key in config.AppSettings.Settings.AllKeys)
                    {
                        if (key == strKey)
                        {
                            return config.AppSettings.Settings[strKey].Value.ToString();
                        }
                    }
                }
                var tem = System.Configuration.ConfigurationManager.AppSettings[strKey];
                if (string.IsNullOrEmpty(tem))
                    return def;
                return tem.Trim();
            }
            catch { return def; }
        }
        #endregion
    }
}
