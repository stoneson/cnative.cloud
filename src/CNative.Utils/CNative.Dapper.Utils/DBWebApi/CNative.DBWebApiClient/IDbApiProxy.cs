﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CNative.DBWebApiClient
{
    public interface IDbApiProxy
    {
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="_eSql"></param>
        /// <returns></returns>
        JsonResult<bool> Execute(DBSqlEntity _eSql);
        /// <summary>
        /// 批量执行脚本
        /// </summary>
        /// <param name="_StrSqlList"></param>
        /// <returns></returns>
        JsonResult<bool> ExecuteList(List<DBSqlEntity> _StrSqlList);
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        JsonResult<DataSet> QueryDataSet(DBSqlEntity sql);
        /// <summary>
        /// 查寻返回实体集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        JsonResult<List<T>> Query<T>(DBSqlEntity sql);
        /// <summary>
        /// 查寻返回单行实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        JsonResult<T> QuerySingle<T>(DBSqlEntity sql);
        /// <summary>
        /// 查寻返回单个字段值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        JsonResult<T> ExecuteScalar<T>(DBSqlEntity sql);
    }
}
