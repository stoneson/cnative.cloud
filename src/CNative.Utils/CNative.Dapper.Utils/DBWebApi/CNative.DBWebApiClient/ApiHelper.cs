﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace CNative.DBWebApiClient
{
    public static class ApiHelper
    {
        public static string ToJson(this object obj)
        {
            string result = JsonConvert.SerializeObject(obj);
            return result;
        }
        public static JsonResult<T> ToJsonResult<T>(this string str)
        {
            var result = JsonConvert.DeserializeObject<JsonResult<T>>(str);
            return result;
        }
        public static JsonResult<T> ToJsonResult<T>(Func<string> func)
        {
            try
            {
                var jsonResult = func.Invoke();
                return jsonResult.ToJsonResult<T>();
            }
            catch (Exception ex)
            {
                var jsonResult = new JsonResult<T>(default(T));
                jsonResult.errorMsg = ex.ToString();
                jsonResult.resultCode = ResultCode.其他错误;
                return jsonResult;
            }
        }
    }
}