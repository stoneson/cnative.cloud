﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Fugle
{

    #region 跨数据库执行事务
    public class SqlEvent
    {
        public SqlEvent(int _Index, string _Sql, List<IDataParameter> _Paras, int _Count)
        {
            _index = _Index;
            _sql = _Sql;
            _paras = _Paras;
            _count = _Count;
        }

        private int _index = 0;
        public int Index { get { return _index; } }

        private string _sql = string.Empty;
        public string Sql { get { return _sql; } }

        private List<IDataParameter> _paras = null;
        public List<IDataParameter> Paras { get { return _paras; } }

        private int _count = 0;
        public int Count { get { return _count; } }

        public bool NeedRollback { get; set; }
    }

    public class SqlEntity
    {
        /// <summary>
        /// 数据库工厂中定义的数据库连接名称
        /// </summary>
        public string DbName = string.Empty;
        /// <summary>
        /// 存储过程名或一般语句(select 语句除外)
        /// </summary>
        public string sql = string.Empty;
        /// <summary>
        /// 存储过程参数
        /// </summary>
        public List<IDataParameter> Paras = null;
        /// <summary>
        /// insert update delete 所影响的行数
        /// </summary>
        public int count = 0;
        /// <summary>
        /// 存储过程返回的结果集
        /// </summary>
        public DataSet ds = null;

        public Database db;
        public DbConnection conn;
        public DbTransaction tran;
    }
    //public class EntityLogNew : EntityLog
    //{
    //    /// <summary>
    //    /// 日志开启时间
    //    /// </summary>
    //    public DateTime LogOnTime { get; set; }
    //}
    #endregion

    /// <summary>
    /// 链接数据库类型
    /// </summary>
    public enum EnumDBType
    {
        /// <summary>
        /// SQL server数据库=0
        /// </summary>
        SQLDb = 0,
        /// <summary>
        /// Oracle数据库=1
        /// </summary>
        OraDb = 1,
        /// <summary>
        /// Sybase数据库=2
        /// </summary>
        SybDb = 2,
        /// <summary>
        /// MySql数据库=3
        /// </summary>
        MSDb = 3
    }
    public enum OracleProcParameterDirection
    {
        In,
        Out
    }

    public class OracleProcParameter
    {
        public string ArguName { get; set; }

        public int Position { get; set; }

        public OracleProcParameterDirection ParameterDirection { get; set; }
    }



    public class ParameterSplit
    {
        public int StartIndex { get; set; }

        public int StopIndex { get; set; }

        public string ParameterName { get; set; }

        public string NewParameterName { get; set; }
    }
    public class Win32API
    {
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(int hWnd, int Msg, int wParam, ref COPYDATASTRUCT lParam);

        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);

        public const int WM_COPYDATA = 0x004A;

        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        public static IntPtr FgFindWindow(string _sProName)
        {
            Process[] pros = Process.GetProcessesByName(_sProName.Trim());
            if (pros.Length != 0)
                return pros[0].MainWindowHandle;
            return IntPtr.Zero;
        }
    }
    public class DataCache
    {
        public DataCache() { }
        public DataCache(int _Parameter) { }
        public string Parameter { get; set; }
    }
    public class FgParameterCache
    {
        private static object lockOjbect = new object();
        private static Dictionary<string, List<OracleProcParameter>> _dicCache = new Dictionary<string, List<OracleProcParameter>>();
        public static List<OracleProcParameter> GetParameters(string db, string procName)
        {
            lock (lockOjbect)
            {
                var key = db + "." + procName;
                if (_dicCache.ContainsKey(key))
                {
                    return _dicCache[key];
                }
                return null;
            }
        }

        public static void AddProcParameterCache(string db, string procName, List<OracleProcParameter> parameters)
        {
            lock (lockOjbect)
            {
                var key = db + "." + procName;
                if (!_dicCache.ContainsKey(key))
                {
                    _dicCache.Add(key, parameters);
                }
            }
        }
    }
    /// <summary>
    /// 计算方法执行时间
    /// </summary>
    public class FgFuncQueryTime
    {
        Stopwatch mstw = new Stopwatch();
        /// <summary>
        /// 默认构造
        /// </summary>
        public FgFuncQueryTime()
        {
            mstw.Start();
        }
        /// <summary>
        /// 计算方法执行时间
        /// </summary>
        /// <returns></returns>
        public double Calculate()
        {
            try
            {
                mstw.Stop();
                return mstw.Elapsed.TotalSeconds;
            }
            catch { return 0; }
        }
    }
}
