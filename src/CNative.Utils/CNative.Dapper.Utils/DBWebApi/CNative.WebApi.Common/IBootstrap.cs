﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CNative.WebApi.Common
{
    public interface IBootstrap
    {
        string ServiceName
        {
            get;
            set;
        }
        string Description
        {
            get;
            set;
        }
        IConfigBase ConfigBase
        {
            get;
            set;
        }
        void Start();
        /// <summary>
        /// 停止此服务。
        /// </summary>
        void Stop();
    }
}
