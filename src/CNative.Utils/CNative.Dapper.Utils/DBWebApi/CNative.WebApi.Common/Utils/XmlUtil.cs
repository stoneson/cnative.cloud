﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace CNative.WebApi.Utils
{
    /// <summary>
    /// XML工具类
    /// </summary>
    public class XmlUtil
    {
        /// <summary>
        /// 从XML读取注释
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetActionDesc()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            string xmlPath = string.Format("{0}/{1}.XML", System.AppDomain.CurrentDomain.BaseDirectory, typeof(XmlUtil).Assembly.GetName().Name);
            if (File.Exists(xmlPath))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlPath);

                XmlNode summaryNode; string type; string desc; int pos; string key;
                foreach (XmlNode node in xmlDoc.SelectNodes("//member"))
                {
                    type = type = node.Attributes["name"].Value;
                    if (type.StartsWith("M:PrisonWebApi.Controllers"))
                    {
                        pos = type.IndexOf("(");
                        if (pos == -1) pos = type.Length;
                        key = type.Substring(2, pos - 2);
                        summaryNode = node.SelectSingleNode("summary");
                        desc = summaryNode.InnerText.Trim();
                        result.Add(key, desc);
                    }
                }
            }

            return result;
        }
    }
}