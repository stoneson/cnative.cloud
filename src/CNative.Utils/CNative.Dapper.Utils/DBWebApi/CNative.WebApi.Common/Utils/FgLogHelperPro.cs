﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net.Config;
using log4net;

namespace CNative
{
    /// <summary>
    /// 日志处理类，依赖log4net
    /// </summary>
    public class FgLogHelperPro
    {
        private readonly ILog log = LogManager.GetLogger("Log");

        /// <summary>
        /// log4net接口
        /// </summary>
        public ILog Write
        {
            get { return log; }
        }

        /// <summary>
        /// log4net配置文件
        /// </summary>
        public FgLogHelperPro(string _FileName = "Log")
        {
            XmlDocument _Doc = new XmlDocument();
            _Doc.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?><log4net><logger name=""Log""><level value=""ALL"" /><appender-ref ref=""Base"" /></logger><appender name=""Base"" type=""log4net.Appender.RollingFileAppender,log4net""><file type=""log4net.Util.PatternString"" value=""Log/%date{yyyy-MM-dd}_" + _FileName + @".log"" /><lockingModel type=""log4net.Appender.FileAppender+MinimalLock"" /><appendToFile value=""true"" /><rollingStyle value=""Date"" /><MaxSizeRollBackups value=""10"" /><maximumFileSize value=""10MB"" /><layout type=""log4net.Layout.PatternLayout""><conversionPattern value=""%date [%t]%-5p %c - %m%n"" /></layout>z</appender></log4net>");
            XmlConfigurator.Configure(_Doc.DocumentElement);
        }
    }
}
