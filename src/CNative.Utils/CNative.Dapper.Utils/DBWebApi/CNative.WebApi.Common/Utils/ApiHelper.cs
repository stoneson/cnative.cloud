﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace CNative.WebApi.Utils
{
    public class WSResponseMessage
    {
        [DataMember]
        public string Content { get; set; }
        [DataMember] 
        public int StatusCode { get; set; }
    }
    public  static class ApiHelper
    {
        public static HttpResponseMessage ToJson(this object obj)
        {
            string str = JsonConvert.SerializeObject(obj);
            HttpResponseMessage result = new HttpResponseMessage { Content = new StringContent(str, Encoding.UTF8, "application/json") };
            return result;
        }
        public static WSResponseMessage ToJsonWS(this object obj)
        {
            string str = JsonConvert.SerializeObject(obj);
            var result = new WSResponseMessage { Content = str };
            return result;
        }
    }
}