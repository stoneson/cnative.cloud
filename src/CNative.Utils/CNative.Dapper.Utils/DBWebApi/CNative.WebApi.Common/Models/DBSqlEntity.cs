﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CNative.WebApi.Common.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DBSqlEntity
    {
        public DBSqlEntity()
        {

        }
        public DBSqlEntity(string sql, object parameter = null)
        {
            Sql = sql;
            Parameter = parameter;
        }
        /// <summary>
        /// 当前数据库连接字符串key名称
        /// </summary>
        [DataMember]
        public string DBName { get; set; }
        /// <summary>
        /// 指定如何解释命令字符串
        /// SQL文本命令（默认），存储过程的名称 ，    表的名称
        /// Text = 1,           StoredProcedure = 4,TableDirect = 512 
        /// </summary>
        [DataMember]
        public int CommandType { get; set; } = 1;
        /// <summary>
        /// SQL脚本
        /// </summary>
        [DataMember]
        public string Sql { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int? CommandTimeout { get; set; }
        /// <summary>
        /// 支持Dapper原生参数
        /// 实体，集合List都行
        /// </summary>
        [DataMember]
        public object Parameter { get; set; }
    }
}
