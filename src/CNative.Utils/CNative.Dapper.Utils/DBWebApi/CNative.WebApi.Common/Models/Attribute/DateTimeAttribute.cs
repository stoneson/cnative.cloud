﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CNative.WebApi.Models
{
    /// <summary>
    /// 日期时间字段
    /// </summary>
    public class DateTimeAttribute : Attribute
    {
        /// <summary>
        /// 日期时间格式
        /// </summary>
        public string Format { get; set; }
    }
}