﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;

namespace CNative.WebApi.Models
{
    /// <summary>
    /// Json返回
    /// </summary>
    public class JsonResult
    {
        /// <summary>
        /// 接口是否成功
        /// </summary>
        [Required]
        public virtual bool success { get; set; }

        /// <summary>
        /// 结果编码
        /// </summary>
        [Required]
        public virtual ResultCode resultCode { get; set; }

        /// <summary>
        /// 接口错误信息
        /// </summary>
        public virtual string errorMsg { get; set; }

        /// <summary>
        /// 记录总数(可空类型)
        /// </summary>
        //public virtual int? total { get; set; }

        /// <summary>
        /// 默认构造函数
        /// </summary>
        public JsonResult() { }

        /// <summary>
        /// 接口失败返回数据
        /// </summary>
        public JsonResult(string errorMsg, ResultCode resultCode)
        {
            this.success = false;
            this.resultCode = resultCode;
            this.errorMsg = errorMsg;
        }

    }

    /// <summary>
    /// Json返回
    /// </summary>
    public class JsonResult<T> : JsonResult
    {
        /* 子类重写属性解决JSON序列化属性顺序问题 */

        /// <summary>
        /// 接口是否成功
        /// </summary>
        [Required]
        public override bool success { get; set; }

        /// <summary>
        /// 结果编码
        /// </summary>
        [Required]
        public override ResultCode resultCode { get; set; }

        /// <summary>
        /// 接口错误信息
        /// </summary>
        public override string errorMsg { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public T data { get; set; }

        /// <summary>
        /// 接口成功返回数据
        /// </summary>
        public JsonResult(T data)
        {
            this.success = true;
            this.resultCode = ResultCode.OK;
            this.data = data;
        }

    }

    /// <summary>
    /// Json返回
    /// </summary>
    public class JsonListResult<T> : JsonResult
    {
        /* 子类重写属性解决JSON序列化属性顺序问题 */

        /// <summary>
        /// 接口是否成功
        /// </summary>
        [Required]
        public override bool success { get; set; }

        /// <summary>
        /// 结果编码
        /// </summary>
        [Required]
        public override ResultCode resultCode { get; set; }

        /// <summary>
        /// 接口错误信息
        /// </summary>
        public override string errorMsg { get; set; }

        /// <summary>
        /// 记录总数(可空类型)
        /// </summary>
        public virtual int? total { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public List<T> info { get; set; }

        /// <summary>
        /// 接口成功返回数据
        /// </summary>
        public JsonListResult(List<T> list, int total)
        {
            this.success = true;
            this.resultCode = ResultCode.OK;
            this.info = list;
            this.total = total;
        }

        /// <summary>
        /// 接口成功返回数据
        /// </summary>
        public JsonListResult(List<T> list, PagerModel pager)
        {
            this.success = true;
            this.resultCode = ResultCode.OK;
            this.info = list;
            this.total = pager.totalRows;
        }

    }

    /// <summary>
    /// 结果编码
    /// </summary>
    public enum ResultCode
    {
        OK = 200,

        token不匹配或已过期 = 1001,
        请求头中不存在token = 1002,
        用户不存在 = 1101,
        密码不正确 = 1102,
        参数不正确 = 1201,
        操作失败 = 1301,
        资源不存在 = 1302,
        其他错误 = 1401,

        服务器内部错误 = 1501
    }

    /// <summary>
    /// 通用返回数据
    /// </summary>
    public class CommonSubmitResult
    {
        /// <summary>
        /// 提示信息
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 记录ID
        /// </summary>
        public string id { get; set; }
    }

    /// <summary>
    /// 通用返回数据
    /// </summary>
    public class CommonMsgResult
    {
        /// <summary>
        /// 提示信息
        /// </summary>
        public string msg { get; set; }
    }
}
