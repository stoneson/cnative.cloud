﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CNative.WebApi.Common
{
    public interface IConfigBase
    {
        Action ConfigFileChanged { get; set; }
        bool WatchConfig();
        void Load();

        /// <summary>
        /// 时间类型（0=秒;1=分钟;2=小时）
        /// </summary>
        int TimeType { get; set; }
        /// <summary>
        /// 执行时间间隔（秒）
        /// </summary>
        int IntervalTime { get; set; }
        /// <summary>
        /// 是否自动更新
        /// </summary>
        bool IsAutoUpdate { get; set; }
        /// <summary> 
        /// 系统用于标志此服务的名称 
        /// </summary> 
        string ServiceName { get; set; }
        /// <summary> 
        /// 向用户标志服务的友好名称 
        /// </summary> 
        string DisplayName { get; set; }
        /// <summary> 
        /// 服务的说明 
        /// </summary> 
        string Description { get; set; }
        /// <summary>
        /// 服务依赖，多个以“;”分隔
        /// </summary>
        string DependedOn { get; set; }
        string GetAppSettings(string key, string def = "");
    }
}
