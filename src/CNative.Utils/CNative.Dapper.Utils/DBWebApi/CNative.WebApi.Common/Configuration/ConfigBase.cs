using System;
using System.Xml;
using System.IO;
using System.Reflection;
using CNative.WebApi.Utils;

namespace CNative.WebApi.Common
{
	/// <summary>
	/// ConfigBase 的摘要说明。
	/// </summary>
    public class ConfigBase : IConfigBase, IDisposable
    {
        protected static System.IO.FileInfo configFileInfo = null;
        protected static string _configfile = "";
        public Action ConfigFileChanged { get; set; }
        public static string ConfigFile
        {
            set
            {
                try
                {
                    _configfile = value;
                    configFileInfo = new System.IO.FileInfo(_configfile);
                }
                catch (Exception ex)
                {
                    LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
                }
            }
        }
        public static string BaseDirectory
        {
            get
            {
                string location = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.LastIndexOf('\\'));
                return location;
            }
        }
        /// <summary>
        /// 初始化对象System.Windows.Forms.Application.StartupPath
        /// </summary>
        static ConfigBase()
        {
            ConfigFile = BaseDirectory + @"\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe.config";
        }

        public ConfigBase()
        {
            Init();
        }

        /// <summary>
        /// CheckLogger
        /// </summary>
        /// <returns></returns>
        public virtual bool WatchConfig()
        {
            try
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(_configfile);

                if (Math.Abs((fileInfo.LastWriteTime - configFileInfo.LastWriteTime).TotalSeconds) > 0)
                {
                    configFileInfo = fileInfo;
                    this.Init();
                    if (ConfigFileChanged != null)
                        ConfigFileChanged();
                    LogUtil.Info("Config file #" + _configfile + " changed.");
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
            }
            return false;
        }

        /// <summary>
        /// GetAppSettings
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual string GetAppSettings(string key, string defaultVal = "")
        {
            return ConfigHelper.GetAppSettings(key, defaultVal, "");
        }
        protected virtual void Init()
        {
            Init(configFileInfo.FullName);
        }
        protected virtual void Init(string xmlFileName)
        {
            try
            {
                System.Configuration.ConfigurationManager.RefreshSection("appSettings");
                string temp = "";
                try
                {
                    temp = this.GetAppSettings("TimeType","1").Trim();
                    this.TimeType = int.Parse(temp);
                }
                catch (Exception ex) { TimeType = 1; LogUtil.Error(ex.ToString()); }

                try
                {
                    temp = this.GetAppSettings("IntervalTime", "100").Trim();
                    this.IntervalTime = int.Parse(temp);

                    if (this.TimeType == 2)//小时
                        this.IntervalTime = IntervalTime * 60 * 60;
                    else if (this.TimeType == 1)//分钟
                        this.IntervalTime = IntervalTime * 60;
                }
                catch (Exception ex) { IntervalTime = 60 * 60; LogUtil.Error(ex.ToString()); }
                try
                {
                    temp = this.GetAppSettings("IsAutoUpdate", "false").Trim();
                    this.IsAutoUpdate = bool.Parse(temp);
                }
                catch (Exception ex) { IsAutoUpdate = false; LogUtil.Error(ex.ToString()); }
                //--------------------------------------------------------------------------------------------------
                try
                {
                    ServiceName = this.GetAppSettings("ServiceName", System.Diagnostics.Process.GetCurrentProcess().ProcessName).Trim();
                }
                catch (Exception ex) { ServiceName = System.Diagnostics.Process.GetCurrentProcess().ProcessName; LogUtil.Error(ex.ToString()); }
                try
                {
                    DisplayName = this.GetAppSettings("DisplayName", ServiceName).Trim();
                }
                catch (Exception ex) { DisplayName = ServiceName; LogUtil.Error(ex.ToString()); }
                try
                {
                    Description = this.GetAppSettings("Description", ServiceName).Trim();
                }
                catch (Exception ex) { Description = ServiceName; LogUtil.Error(ex.ToString()); }
                try
                {
                    DependedOn = this.GetAppSettings("DependedOn").Trim();
                }
                catch (Exception ex) { DependedOn = ""; LogUtil.Error(ex.ToString()); }
                //--------------------------------------------------------------------------------------------------
                this.Load();
            }
            catch (Exception ex)
            {
                LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
            }
        }
        /// <summary>
        /// 读取系统配置
        /// </summary>
        public virtual void Load()
        {

        }
        /// <summary>
        ///  打开XmlDocument
        /// </summary>
        /// <param name="xmlFileName">XmlDocument xmlFileName</param>
        /// <returns>XmlDocument</returns>
        XmlDocument XmlOpenFile(string xmlFileName)
        {
            XmlDocument xmlDoc = new XmlDocument();

            if (!System.IO.File.Exists(xmlFileName))
            {
                return null;
            }
            xmlDoc.Load(xmlFileName);

            return xmlDoc;
        }
        bool IsXmlElement(XmlNode XmlNode)
        {
            try { XmlElement MainMenuXe = (XmlElement)XmlNode; return true; }
            catch //(Exception ex)
            {
                //LogUtil.Error(ex.TargetSite + "\n" + ex.StackTrace + "\n" + ex.Message);
                return false;
            }
        }

        #region 属性
        /// <summary>
        /// 时间类型（0=秒;1=分钟;2=小时）
        /// </summary>
        public int TimeType { get; set; }
        /// <summary>
        /// 执行时间间隔（秒）
        /// </summary>
        public int IntervalTime { get; set; }
        /// <summary>
        /// 是否自动更新
        /// </summary>
        public bool IsAutoUpdate { get; set; }
        /// <summary> 
        /// 系统用于标志此服务的名称 
        /// </summary> 
        public string ServiceName { get; set; }
        /// <summary> 
        /// 向用户标志服务的友好名称 
        /// </summary> 
        public string DisplayName { get; set; }
        /// <summary> 
        /// 服务的说明 
        /// </summary> 
        public string Description { get; set; }
        /// <summary>
        /// 服务依赖，多个以“;”分隔
        /// </summary>
        public string DependedOn { get; set; }
        #endregion

        #region IDisposable 成员
        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //managed dispose 
                    IntervalTime = 0;
                    ServiceName = null;
                    DisplayName = null;
                    Description = null;
                }
                //unmanaged dispose 
            }
            disposed = true;
        }
        ~ConfigBase()
        {
            Dispose(false);
        }
        #endregion 

    }
}
