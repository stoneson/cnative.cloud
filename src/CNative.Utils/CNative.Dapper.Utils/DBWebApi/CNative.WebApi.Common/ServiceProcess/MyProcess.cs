﻿using CNative.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CNative.WebApi.ServiceProcess
{
    public class MyProcess
    {
        public MyProcess(string processFullName)
        {
            ProcessFullName = processFullName;
            ProcessName = ServiceHelper.GetFileName(processFullName);
        }
        public string ProcessName { get; set; }
        public string ProcessFullName { get; set; }

        public int 进程ID { get; set; }
        public string 进程映像名 { get; set; }
        public string 启动线程数 { get; set; }
        public string CPU占用时间 { get; set; }
        public string 线程优先级 { get; set; }
        public string 启动时间 { get; set; }

        public string 专用内存 { get; set; }
        public string 峰值虚拟内存 { get; set; }
        public string 峰值分页内存 { get; set; }
        public string 分页系统内存 { get; set; }
        public string 分页内存 { get; set; }
        public string 未分页系统内存 { get; set; }
        public string 物理内存 { get; set; }
        public string 虚拟内存 { get; set; }

        public void Refresh(string processFullName = "")
        {
            //显示选择的系统进程详细信息
            try
            {
                if (!string.IsNullOrEmpty(processFullName))
                    ProcessFullName = processFullName;
                var currentModuleName = ServiceHelper.GetFileName(ProcessFullName);
                Process[] processes = Process.GetProcessesByName(currentModuleName);
                var myprocess = processes[0];
                //遍历有相同进程名称正在运行的进程 
                foreach (Process process in processes)
                {
                    try
                    {
                        if (process.MainModule.FileName.Trim().ToLower() == ProcessFullName.Trim().ToLower())
                        {
                            myprocess = process;
                            break;
                        }
                    }
                    catch (Exception ex) { LogUtil.Error(ex.ToString()); }
                }

                进程映像名 = myprocess.ProcessName;
                进程ID = myprocess.Id;
                启动线程数 = myprocess.Threads.Count.ToString();
                CPU占用时间 = myprocess.TotalProcessorTime.ToString();
                线程优先级 = myprocess.PriorityClass.ToString();
                启动时间 = myprocess.StartTime.ToLongTimeString();
                if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                   Environment.OSVersion.Version.Major >= 6)
                {
                    专用内存 = ServiceHelper.GetFileSize(myprocess.PrivateMemorySize64);
                    峰值虚拟内存 = ServiceHelper.GetFileSize(myprocess.PeakVirtualMemorySize64);
                    峰值分页内存 = ServiceHelper.GetFileSize(myprocess.PeakPagedMemorySize64);
                    分页系统内存 = ServiceHelper.GetFileSize(myprocess.PagedSystemMemorySize64);
                    分页内存 = ServiceHelper.GetFileSize(myprocess.PagedMemorySize64);
                    未分页系统内存 = ServiceHelper.GetFileSize(myprocess.NonpagedSystemMemorySize64);
                    物理内存 = ServiceHelper.GetFileSize(myprocess.WorkingSet64);
                    虚拟内存 = ServiceHelper.GetFileSize(myprocess.VirtualMemorySize64);
                }
                else
                {
                    专用内存 = ServiceHelper.GetFileSize(myprocess.PrivateMemorySize);
                    峰值虚拟内存 = ServiceHelper.GetFileSize(myprocess.PeakVirtualMemorySize);
                    峰值分页内存 = ServiceHelper.GetFileSize(myprocess.PeakPagedMemorySize);
                    分页系统内存 = ServiceHelper.GetFileSize(myprocess.PagedSystemMemorySize);
                    分页内存 = ServiceHelper.GetFileSize(myprocess.PagedMemorySize);
                    未分页系统内存 = ServiceHelper.GetFileSize(myprocess.NonpagedSystemMemorySize);
                    物理内存 = ServiceHelper.GetFileSize(myprocess.WorkingSet);
                    虚拟内存 = ServiceHelper.GetFileSize(myprocess.VirtualMemorySize);
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error(ex.ToString());
            }
        }
    }
}
