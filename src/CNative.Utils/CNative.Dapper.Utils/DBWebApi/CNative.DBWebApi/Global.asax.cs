﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace CNative.DBWebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
        void Application_Error(object sender, EventArgs ev)
        {
            //捕获整个解决方案下的所有异常  
            try
            {
                //在出现未处理的错误时运行的代码         
                Exception objError = Server.GetLastError();
                if (objError != null)
                {
                    var objError1 = objError.GetBaseException();
                    if (objError1 != null) objError = objError1;
                    else
                    {
                        Server.ClearError();
                        return;
                    }
                }
                if (objError == null)
                {
                    Server.ClearError();
                    Response.Write("<br>程序出现异常状态，请联系管理员");
                    return;
                }

                var sbError = new System.Text.StringBuilder();
                sbError.Append("程序出现异常状态，请联系管理员");
                sbError.Append("<br>时间: " + System.DateTime.Now.ToString());
                sbError.Append("<br>页面: " + HttpContext.Current.Request.Url);
                sbError.Append("<br>信息: " + objError.Message);
                sbError.Append("<br>来源: " + objError.Source);
                sbError.Append("<br>堆栈: " + objError.StackTrace);
                sbError.Append("<br>类名：" + objError.TargetSite.DeclaringType.FullName);
                sbError.Append("<br>方法：" + objError.TargetSite.Name);
                sbError.Append("<br>Url OriginalString：" + Request.Url.OriginalString);
                sbError.Append("<br>URL PathAndQuery：" + Request.Url.PathAndQuery);
                sbError.Append("<br>URL.ToString()：" + Request.Url.ToString());
                sbError.Append("<br>URL AbsoluteUri：" + Request.Url.AbsoluteUri);
                if (objError.InnerException != null)
                    sbError.Append("<br>内部信息: " + objError.InnerException.ToString());
                //清除当前异常 使之不返回到请求页面
                Server.ClearError();
                lock (this)
                {
                    new FgLogHelperPro("错误日志").Write.Error(sbError.ToString().Replace("<br>", "\r\n"));
                }
                Response.Write(sbError.ToString());


                //// 在出现未处理的错误时运行的代码
                //Exception e = Server.GetLastError().GetBaseException();
                //string strE = "内部错误:" + e.InnerException.ToString() + "\r\n堆栈：" + e.StackTrace + "\r " + "Message:" + e.Message + "\r 来源:" + e.Source;
                //Logger.WriteLogFile(strE);
                //Server.ClearError();
            }
            catch { }
        }
    }
}
