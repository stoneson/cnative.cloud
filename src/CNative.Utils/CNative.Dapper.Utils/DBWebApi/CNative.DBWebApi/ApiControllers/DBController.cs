﻿
using CNative.WebApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CNative.WebApi.Utils;
using System.Data;
using CNative.DBWebApi.DBBaseRule;
using CNative.Dapper.Utils;
using CNative.WebApi.Common.Models;

namespace CNative.DBWebApi.Controllers
{

    /// <summary>
    /// 访问数据库接口
    /// </summary>
    [RoutePrefix("api/db")]
    public class DBController : ApiController
    {
        private static DBSqlRule dbSqlRule { get; }
        /// <summary>
        /// 
        /// </summary>
        static DBController()
        {
            dbSqlRule = new DBSqlRule();
        }
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Execute")]
        public HttpResponseMessage Execute(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.Execute(_eSql));
        }
        /// <summary>
        /// 批量执行脚本
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("ExecuteList")]
        public HttpResponseMessage ExecuteList(List<DBSqlEntity> _StrSqlList)
        {
            return DoSql(() => dbSqlRule.ExecuteList(_StrSqlList));
        }
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("QueryDataSet")]
        public HttpResponseMessage QueryDataSet(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.QueryDataSet(_eSql));
        }
        ///// <summary>
        ///// 查寻返回DataTable
        ///// </summary>
        ///// <returns></returns>
        //[HttpPost]
        //[Route("QueryDataTable")]
        //public HttpResponseMessage QueryDataTable([FromBody]DBSqlEntity _eSql)
        //{
        //    return DoSql(() => dbSqlRule.QueryDataTable(_eSql));
        //}
        /// <summary>
        /// 查寻返回实体集合
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Query")]
        public HttpResponseMessage Query(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.Query<object>(_eSql));
        }
        /// <summary>
        /// 查寻返回单行实体
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("QuerySingle")]
        public HttpResponseMessage QuerySingle(DBSqlEntity _eSql) 
        {
            return DoSql(() => dbSqlRule.QuerySingle<object>(_eSql));
        }
        /// <summary>
        /// 查寻返回单个字段值
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("ExecuteScalar")]
        public HttpResponseMessage ExecuteScalar(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.GetSingle<object>(_eSql));
        }
        private HttpResponseMessage DoSql<T>(Func<T> func)
        {
            try
            {
                var rets = func.Invoke();
                var jsonResult = new JsonResult<T>(rets);

                return ApiHelper.ToJson(jsonResult);
            }
            catch (Exception ex)
            {
                var jsonResult = new JsonResult(ex.ToString(), ResultCode.服务器内部错误);
                return ApiHelper.ToJson(jsonResult);
            }
        }

        /// <summary>
        /// 心跳监测
        /// </summary>
        [HttpGet]
        [Route("Health")]
        public HttpResponseMessage Health()
        {
            return ApiHelper.ToJson(new JsonResult<string>("OK"));
        }

    }
}
