﻿using CNative.WebApi.Common.Models;
using CNative.WebApi.Models;
using CNative.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Services;

namespace CNative.DBWebApi
{
    /// <summary>
    /// DBWebService 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class DBWebService : System.Web.Services.WebService
    {
        private static DBBaseRule.DBSqlRule dbSqlRule { get; }
        /// <summary>
        /// 
        /// </summary>
        static DBWebService()
        {
            dbSqlRule = new DBBaseRule.DBSqlRule();
        }
        private static WSResponseMessage DoSql<T>(Func<T> func)
        {
            try
            {
                var rets = func.Invoke();
                var jsonResult = new JsonResult<T>(rets);

                return ApiHelper.ToJsonWS(jsonResult);
            }
            catch (Exception ex)
            {
                var jsonResult = new JsonResult(ex.ToString(), ResultCode.服务器内部错误);
                return ApiHelper.ToJsonWS(jsonResult);
            }
        }
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "执行脚本", MessageName = "Execute")]
        public WSResponseMessage Execute(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.Execute(_eSql));
        }
        /// <summary>
        /// 批量执行脚本
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "批量执行脚本", MessageName = "ExecuteList")]
        public WSResponseMessage ExecuteList(List<DBSqlEntity> _StrSqlList)
        {
            return DoSql(() => dbSqlRule.ExecuteList(_StrSqlList));
        }
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "查寻返回DataSet", MessageName = "QueryDataSet")]
        public WSResponseMessage QueryDataSet(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.QueryDataSet(_eSql));
        }
        ///// <summary>
        ///// 查寻返回DataTable
        ///// </summary>
        ///// <returns></returns>
        //[WebMethod(Description = "查寻返回DataTable", MessageName = "QueryDataTable")]
        //public WSResponseMessage QueryDataTable(DBSqlEntity _eSql)
        //{
        //    return DoSql(() => dbSqlRule.QueryDataTable(_eSql));
        //}
        /// <summary>
        /// 查寻返回实体集合
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "查寻返回实体集合", MessageName = "Query")]
        public WSResponseMessage Query(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.Query<object>(_eSql));
        }
        /// <summary>
        /// 查寻返回单行实体
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "查寻返回单行实体", MessageName = "QuerySingle")]
        public WSResponseMessage QuerySingle(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.QuerySingle<object>(_eSql));
        }
        /// <summary>
        /// 查寻返回单个字段值
        /// </summary>
        /// <returns></returns>
        [WebMethod(Description = "查寻返回单个字段值", MessageName = "ExecuteScalar")]
        public WSResponseMessage ExecuteScalar(DBSqlEntity _eSql)
        {
            return DoSql(() => dbSqlRule.GetSingle<object>(_eSql));
        }
        /// <summary>
        /// 心跳监测
        /// </summary>
        [WebMethod(Description = "心跳监测", MessageName = "Health")]
        public WSResponseMessage Health()
        {
            return ApiHelper.ToJsonWS(new JsonResult<string>("OK"));
        }
    }
}
