# CNative.Dapper.Utils

#### 介绍
CNative.Dapper.DbUtils is the ORM in .NetCore, .NetFramework. It supports Mysql,  SqlServer, Oracle,SqlLite,Npgsql,MsAccess

基于 [Dapper ](https://github.com/DapperLib/Dapper)构建的微型 ORM 类库，提供一个包含增、删、改、查，分页等常用方法的数据访问层基类，支持用 Lambda 表达式书写查询和更新条件，且实体类有T4模版自动生成.省去手写实体类的麻烦。

#### 软件架构
软件架构说明

[![CNative.Dapper.Utils MyGet](https://img.shields.io/myget/CNative.Dapper.Utils/vpre/CNative.Dapper.Utils.svg)](https://www.nuget.org/packages/CNative.Dapper.Utils) 

#### Features

- 零配置，开箱即用。
- 数据库表、实体类型自动映射，主键自动映射。
- 灵活易用的增、删、改、查、分页查询等常用重载方法，单表操作无需编写任何 SQL 语句。
- 查询和更新条件支持 Lambda 表达式组合，自动生成安全参数化的 SQL 语句。
- 提供 SQL 语句、存储过程执行方法，返回结果集自动模型映射，比 DataSet 效率高。
- 支持部分字段更新，无变化字段不更新。
- 数据库表字段变化时重新生成实体类即可，数据访问层无需重新生成。
- 完善的单元测试。
- 支持多表联合查询
- 支持分页查询
- 支持主从库模式
- 支持环境： net40;net461;netstandard2.1;netcoreapp3.1;net5.0
- 目前支持Mysql,  SqlServer, SqlServer2000,Oracle,SqlLite,Npgsql，MsAccess 
- 动态库分别为 MySql.Data.dll，Oracle.ManagedDataAccess.dll，Devart.Data.Oracle.dll，System.Data.SqlClient.dll，System.Data.SQLite.dll，System.Data.OleDb.dll，Npgsql.dll
- 动态库无需引用，运行时会自动加载，和执行文件同一个目录就行
- 有丰富的测试用例
- 添加脚本跟踪功能


#### 安装教程

MyGet Pre-release feed: https://www.nuget.org/packages/CNative.Dapper.Utils/

| Package | NuGet Stable | NuGet Pre-release | Downloads | MyGet |
| ------- | ------------ | ----------------- | --------- | ----- |
| [CNative.Dapper.Utils.Utils](https://www.nuget.org/packages/CNative.Dapper.Utils/) | [![CNative.Dapper.Utils](https://img.shields.io/nuget/v/CNative.Dapper.Utils.svg)](https://www.nuget.org/packages/CNative.Dapper.Utils/) | [![CNative.Dapper.Utils](https://img.shields.io/nuget/vpre/CNative.Dapper.Utils.svg)](https://www.nuget.org/packages/DCNative.apper/) | [![CNative.Dapper.Utils](https://img.shields.io/nuget/dt/CNative.Dapper.Utils.svg)](https://www.nuget.org/packages/CNative.Dapper.Utils/) | [![CNative.Dapper.Utils MyGet](https://img.shields.io/myget/CNative.Dapper.Utils/vpre/CNative.Dapper.Utils.svg)](https://www.nuget.org/packages/CNative.Dapper.Utils) |

#### 使用说明

appsettings.json

```
{
"AppSettings": {
"Title": "Test",
"Version": "1.2.1",
"AccessToken": "xxxxxx@abc.com"
},
"connectionStrings": [
{
"name": "BaseDb",
"CommandTimeout": 45,//秒
"connectionString": "Data Source=.;Initial Catalog=test;Persist Security Info=True;User ID=sa;Password=xxxxxx",
"providerName": "System.Data.SqlClient"
},
{
"name": "Oracle",
"connectionString": "Data Source=xxxxxx;Persist Security Info=True;User ID=hispro;Password=xxxxxx",
"providerName": "System.Data.OracleClient"
},
{
"name": "MySql",
"connectionString": "server=xxxxxx;uid=root;pwd=xxxxxx;database=test",
"providerName": "System.Data.MySql"
},
{
"name": "Sqlite",
"connectionString": "DataSource=xxxxxx.db;Version=3;Pooling=False;Max Pool Size=100;",
"providerName": "System.Data.Sqlite"
},
{
"name": "PostgreSql",
"connectionString": "User id=postgres;Password=xxxxxx;Host=xxxxxx;Port=55433;Database=test;Pooling=true;",
"providerName": "System.Data.Npgsql"
}{
"name": "MsAccess",
"connectionString": "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Public\Documents\DevExpress 2010.1 Demos\Components\ASPxEditors\CS\ASPxEditorsDemos\App_Data\nwind.mdb;Persist Security Info=False ",
"providerName": "System.Data.Access"
},
{
"name": "MsAccess2",
"IsUseMasterSlaveSeparation": true,//是否启用主从分离模式
"SlaveConnectStrings": [ "MsAccess", "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Public\Documents\DevExpress 2010.1 Demos\Components\ASPxEditors\CS\ASPxEditorsDemos\App_Data\nwind.mdb;Persist Security Info=False " ],//从数据库连接字符串集合,
"connectionString": "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Public\Documents\DevExpress 2010.1 Demos\Components\ASPxEditors\CS\ASPxEditorsDemos\App_Data\nwind.mdb;Persist Security Info=False",
"providerName": "CNative.Dapper.Utils.MsAccessProvider"
},{
    "name": "OracleOleDbM2",
    "connectionString": "Provider=MSDAORA;User Id=xxx;Password=xxxx;Persist Security Info=True;Data Source=(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = xxxxxxxx)(PORT = 1521)))(CONNECT_DATA = (SID = orcl)))",
    "providerName": "System.Data.OracleOleDb"
}, 
{
    "name": "sql2000",
    "connectionString": "Data Source=xxxxxxxx;Initial Catalog=ave2;User ID=sa;Password=xxxxxxx",
    "providerName": "System.Data.SqlClient2000"
}
]
}
```


//测试用例

```
//脚本跟踪功能
 SqlMapperTrace.SetMapperTrace((trec)=> 
{
    Console.WriteLine(trec);
}, (trec) => 
{
    Console.WriteLine(trec);
});

[TestClass]
public class UnitTest461DbUtils
{
private ISqlBuilder DRY2 = null;
private ISqlBuilder sqlBuilder = null;

//在运行每个测试之前，使用 TestInitialize 来运行代码
[TestInitialize()]
public void MyTestInitialize()
{
DRY2 = new SqlBuilder("BaseDb");
sqlBuilder = new SqlBuilder("BaseDb");

}
[TestMethod]

//单表操作测试
[TestMethod]
public void TestSelectSqlBuilder1()
{
var lst = sqlBuilder.doSelect<Entity_persons>()//查询集合+排序+TOP
.Fields(s ⇒ new { s.id, s.name, s.adress })//添加查询多个字段
.Top(8)
// .Where(w ⇒ w.id.Between(10, 20))
.OrderByDescending(d ⇒ d.createTime)//按列倒向排序
.Query(); //返回结果
var lst3 = sqlBuilder.doSelect<Entity_persons>()//取单行+排序
.Fields(s ⇒ new { s.id, s.name, s.adress })//添加查询多个字段
.Where(w ⇒ w.id.Between(11, 20))
.OrderBy(d ⇒ d.id) //按列排序
.QuerySingle(); //返回结果

var lst4 = sqlBuilder.doSelect<Entity_persons>().Count(w ⇒ w.id.Between(11, 20)); //返回结果
}

[TestMethod]
public void TestInsertSqlBuilder1()
{
var jg = new Entity_DMJGXXB() { OrgId = 1002, jgdm = "56783", jgmc = "方舱" };

var ret = sqlBuilder.doDelete<Entity_DMJGXXB>()//实体删除
.Delete(jg)
.Exec;
ret = sqlBuilder.doInsert<Entity_DMJGXXB>()//插入实体
.Insert(jg)
.Execute();

ret = sqlBuilder.doDelete<Entity_DMJGXXB>()//表达式删除
.Where(s ⇒ s.OrgId == 1003)
.Execute();
ret = sqlBuilder.doInsert<Entity_DMJGXXB>()//表达式插入
.Insert(new Entity_DMJGXXB()
{
OrgId = 1003,
jgdm = "532236783",
jgmc = "方舱3",
CreateId = 0,
UpdateId = 0,
CreateTime = DateTime.Now,
UpdateTime = DateTime.Now
})
.Exec;

//表复制
var ret1 = sqlBuilder.doDelete<Entity_DMCZYJGDYB2>()
.Where(s ⇒ s.ksid == 1100453)
.Execute();

var ret22 = sqlBuilder.doInsert<Entity_DMCZYJGDYB2>()//表复制
.InsertSelect<Entity_DMCZYJGDYB>(s ⇒ new Entity_DMCZYJGDYB2()
{
OrgId = s.OrgId,
czyid = s.czyid,
czyjgdyid = s.czyjgdyid,
czyxm = s.czyxm,
jgmc = "fasdfafdsaf",
ksid = s.ksid,
ksmc = s.ksmc
}, s ⇒ s.ksid == 1100453)
.Exec;
}
[TestMethod]
public void TestUpdateSqlBuilder1()
{
var ret = sqlBuilder.doUpdate<Entity_DMJGXXB>()
.Set(s ⇒ new Entity_DMJGXXB()
{
jgdm = "5322336783",
jgmc = "fff232aaa",
CreateId = 0,
UpdateId = 0,
jgjpm = "ffff",
UpdateTime = DateTime.Now
})
.Where(s ⇒ s.OrgId == jg.OrgId)
.Exec;

jg.jgjpm = "345454";
ret = sqlBuilder.DoUpdate
.Update(jg, "jgjpm");

var ret1 = sqlBuilder.doDelete<Entity_DMJGXXB>()
.Where(s ⇒ s.OrgId == 1005)
.Execute();

//批量导入数据
ret = sqlBuilder.doInsert<Entity_DMJGXXB>()
    .BulkCopyData(new List<Entity_DMJGXXB>(){new Entity_DMJGXXB()
    {
        OrgId = 1004,
        jgdm = "532236784",
        jgmc = "方舱4",
        CreateId = 0,
        UpdateId = 0,
        CreateTime = DateTime.Now,
        UpdateTime = DateTime.Now
    },new Entity_DMJGXXB()
    {
        OrgId = 1005,
        jgdm = "532236785",
        jgmc = "方舱5",
        CreateId = 0,
        UpdateId = 0,
        CreateTime = DateTime.Now,
        UpdateTime = DateTime.Now
    },new Entity_DMJGXXB()
    {
        OrgId = 1006,
        jgdm = "532236786",
        jgmc = "方舱6",
        CreateId = 0,
        UpdateId = 0,
        CreateTime = DateTime.Now,
        UpdateTime = DateTime.Now
    } });

//更新或插入
var ret23 = sqlBuilder.DoUpdate
.UpdateOrInsert(jg, w ⇒ w.OrgId == jg.OrgId);

ret = sqlBuilder.doUpdate<Entity_DMJGXXB>()
.UpdateOrInsert(s ⇒ new Entity_DMJGXXB()
{
OrgId = 1006,
jgdm = "5322336783",
jgmc = "方舱232",
CreateId = 0,
UpdateId = 0,
jgjpm = "wsafdasfd",
CreateTime = DateTime.Now,
UpdateTime = DateTime.Now
}, w ⇒ w.OrgId == 1006);
}

//多表操作测试
[TestMethod]
public void TestSelectFieldExprSqlBuilder1()
{
//查寻时加集合函数
var ret1 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
.Fields(f ⇒ new { czyjgdyid = f.czyjgdyid.SQL_MAX() })
.Where(s ⇒ s.ksid == 1100453)
.GetSingle<object>();

//查寻返回单行实体
 var ret2 = sqlBuilder.doSelect<Entity_DMJGXXB>()
               .Where(s => s.OrgId == 1008)
               .QuerySingle();

var ret2 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
.Fields(f ⇒ new
{ f.czyid, f.czyxm, jgmc = f.jgmc.SQL_UCASE(), ksmc = f.jgmc.SQL_SUBSTR(2, 3) })
//.Where(s ⇒ s.ksid == 1100453)
.Where(s ⇒ s.ksid == 1100453 && s.jgmc.SQL_UCASE() == "FASDFAFDSAF")
.Query();

var ret3 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
.Fields(f ⇒ new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_MAX() })
.Where(s ⇒ s.OrgId == 1001)
.GroupBy(g ⇒ new { g.ksid, g.ksmc })
.OrderBy(g ⇒ g.ksid)
.Query();

var ret4 = sqlBuilder.doSelect<Entity_DMCZYJGDYB2>()
.Fields(f ⇒ new { f.ksid, f.ksmc, czyjgdyid = f.czyjgdyid.SQL_COUNT(), czyid = f.czyjgdyid.SQL_MAX() })
.Where(s ⇒ s.OrgId == 1001)
.GroupBy(g ⇒ new { g.ksid, g.ksmc })
.Having(h ⇒ h.czyjgdyid.SQL_COUNT() > 3 && h.czyjgdyid.SQL_MAX() > 0)
.OrderBy(g ⇒ g.ksid)
.Query();
}
```


#### 特技

1. 如果其中的代码有不妥的地方，欢迎各位大佬进行指正！ 非常感谢！！！