﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace CNative.Dapper.Utils
{
    #region DeleteSqlBuilder
    internal class DeleteSqlBuilder : BaseSqlBuilder, IDeleteSqlBuilder
    {
        public DeleteSqlBuilder(IDbHelper _db) : base(_db)
        {
        }
        /// <summary>
        /// 添加条件表达式
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public IDeleteSqlBuilder Where<TClass>(Expression<Func<TClass, bool>> where, bool isAnd = true) where TClass : class, new()
        {
            BuildWhere(sqlent, where, isAnd);
            return this;
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        public IDeleteSqlBuilder Delete<TClass>(TClass entity) where TClass : class, new()
        {
            return Delete(new List<TClass> { entity });
        }

        /// <summary>
        /// 删除多条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        public IDeleteSqlBuilder Delete<TClass>(List<TClass> entities) where TClass : class, new()
        {
            if (!entities.IsNullOrEmpty_())
            {
                entities.ForEach(entity =>
                {
                    if (entity.IsNotNullOrEmpty())
                    {
                        var sql = BuildDelete(entity);
                        if (sql != null)
                            sqlList.Add(sql);
                    }
                });
            }
            return this;
        }
        /// <summary>
        /// 通过条件表达式删除
        /// </summary> 
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        public bool Delete<TClass>(Expression<Func<TClass, bool>> where) where TClass : class, new()
        {
            BuildWhere(sqlent, where, false);
            return Exec;
        }

        #region Exists
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Exists<TClass>(TClass entity) where TClass : class, new()
        {
            return base.Exists(entity);
        }
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Exists<TClass>(Expression<Func<TClass, bool>> where) where TClass : class, new()
        {
            return base.Exists(where);
        }
        #endregion
        //-------------------------------------------------------------------------------------------------------------------------------
        #region override
        /// <summary>
        /// 开始
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <returns></returns>
        protected override Tuple<DbTableInfo, string, string> From<TClass>(string tableAlias = "")
        {
            var tableInfo = GetTableInfo<TClass>(tableAlias);
            fromStr = $"{tableInfo.Item3}";
            return tableInfo;
        }
       
        protected override bool Build(bool isCount = false)
        {
            if (fromStr.IsNullOrEmpty())
                return false;
            base.Build();
            //-----------------------------------------------------------------------------------------------------------------
            var wheresStr = "0=1";
            if (WhereList.Count > 0)
            {
                wheresStr = string.Join(" ", WhereList).Trim()
                    .ReplaceIgnoreCase("WHERE", "").TrimPrefixName("AND").TrimPrefixName("OR");

                sqlent.Sql = string.Format(@"DELETE FROM {0} WHERE {1}", fromStr, wheresStr);

                sqlList.Add(sqlent);
                return true;
            }
            //-----------------------------------------------------------------------------------------------------------------

            return false;
        }
        /// <summary>
        /// 返回sql脚本实体
        /// </summary>
        /// <returns></returns>
        public override List<SqlEntity> GetSqls() { Build(); return sqlList; }
        #endregion
        #region BuildDelete
        protected virtual SqlEntity BuildDelete<TClass>(TClass _obj) where TClass : class, new()
        {
            var tableInfo = GetTableInfo<TClass>();
            var tb = tableInfo.Item1;
            if (tb == null) return null;

            var sqle = CreateSqlEntity();
            var whereStr = BuildEntityWhere(sqle,_obj, null, tb);
            var schemaTableName = tableInfo.Item3;
            if (whereStr.IsNotNullOrEmpty())
            {
                sqle.Sql = string.Format(@"DELETE FROM " + schemaTableName + " WHERE {0}", whereStr);
                return sqle;
            }
            return null;
        }
        #endregion

    }
    #endregion
    #region DeleteSqlBuilder<TClass>
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TClass">实体类型</typeparam>
    internal class DeleteSqlBuilder<TClass> : DeleteSqlBuilder, IDeleteSqlBuilder<TClass> where TClass : class, new()
    {
        public DeleteSqlBuilder(IDbHelper _db) : base(_db)
        {
            From();
        }
        /// <summary>
        /// 添加条件表达式
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public IDeleteSqlBuilder<TClass> Where(Expression<Func<TClass, bool>> where, bool isAnd = true)
        {
            base.Where(where, isAnd);
            return this;
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        public IDeleteSqlBuilder<TClass> Delete(TClass entity)
        {
            base.Delete(entity);
            return this;
        }

        /// <summary>
        /// 删除多条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        public IDeleteSqlBuilder<TClass> Delete(List<TClass> entities)
        {
            base.Delete(entities);
            return this;
        }
        /// <summary>
        /// 通过条件表达式删除
        /// </summary> 
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        public bool Delete(Expression<Func<TClass, bool>> where)
        {
            return base.Delete(where);
        }
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Exists(TClass entity)
        {
            return base.Exists(entity);
        }
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Exists(Expression<Func<TClass, bool>> where)
        {
            return base.Exists(where);
        }

        /// <summary>
        /// 开始
        /// </summary>
        /// <returns></returns>
        protected virtual Tuple<DbTableInfo, string, string> From()
        {
            tupleTableInfo = GetTableInfo<TClass>();
            fromStr = $"{tupleTableInfo.Item3}";
            return tupleTableInfo;
        }
    }
    #endregion
}
