﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CNative.Dapper.Utils
{
    public class Check
    {
        public static void ThrowNotSupportedException(string message)
        {
            message = message.IsNullOrEmpty() ? new NotSupportedException().Message : message;
            throw new CNativeException("CNativeException.NotSupportedException：" + message);
        }

        public static void ArgumentNullException(object checkObj, string message)
        {
            if (checkObj == null)
                throw new CNativeException("CNativeException.ArgumentNullException：" + message);
        }

        public static void ArgumentNullException(object[] checkObj, string message)
        {
            if (checkObj == null || checkObj.Length == 0)
                throw new CNativeException("CNativeException.ArgumentNullException：" + message);
        }

        public static void Exception(bool isException, string message, params string[] args)
        {
            if (isException)
                throw new CNativeException(string.Format(message, args));
        }
    }
    internal static partial class ErrorMessage
    {
        internal static string ObjNotExist
        {
            get
            {
                return GetThrowMessage("{0} does not exist.",
                                       "{0}不存在。");
            }
        }
        internal static string EntityMappingError
        {
            get
            {
                return GetThrowMessage("Entity mapping error.{0}",
                                       "实体与表映射出错。{0}");
            }
        }

        public static string NotSupportedDictionary
        {
            get
            {
                return GetThrowMessage("This type of Dictionary is not supported for the time being. You can try Dictionary<string, string>, or contact the author!!",
                                       "暂时不支持该类型的Dictionary 你可以试试 Dictionary<string ,string>或者联系作者！！");
            }
        }

        public static string NotSupportedArray
        {
            get
            {
                return GetThrowMessage("This type of Array is not supported for the time being. You can try object[] or contact the author!!",
                                       "暂时不支持该类型的Array 你可以试试 object[] 或者联系作者！！");
            }
        }

        internal static string GetThrowMessage(string enMessage, string cnMessage, params string[] args)
        {
            List<string> formatArgs = new List<string>() { enMessage, cnMessage };
            formatArgs.AddRange(args);
            return string.Format(@"English Message : {0}
Chinese Message : {1}", formatArgs.ToArray());
        }

        internal static string ConnnectionOpen
        {
            get
            {
                return ErrorMessage.GetThrowMessage("Connection open error . {0}", " 连接数据库过程中发生错误，检查服务器是否正常连接字符串是否正确，实在找不到原因请先Google错误信息：{0}.");
            }
        }
    }
    public class CNativeException : Exception
    {
        public string Sql { get; set; }
        public object Parametres { get; set; }
        public new Exception InnerException;
        public new string StackTrace;
        public new MethodBase TargetSite;
        public new string Source;

        public CNativeException(string message)
            : base(message) { }

        public CNativeException(BaseProvider context, string message, string sql)
            : base(message)
        {
            this.Sql = sql;
        }

        public CNativeException(BaseProvider context, string message, string sql, object pars)
            : base(message)
        {
            this.Sql = sql;
            this.Parametres = pars;
        }

        public CNativeException(BaseProvider context, Exception ex, string sql, object pars)
            : base(ex.Message)
        {
            this.Sql = sql;
            this.Parametres = pars;
            this.InnerException = ex.InnerException;
            this.StackTrace = ex.StackTrace;
            this.TargetSite = ex.TargetSite;
            this.Source = ex.Source;
        }

        public CNativeException(BaseProvider context, string message, object pars)
            : base(message)
        {
            this.Parametres = pars;
        }
    }
    public class VersionExceptions : CNativeException
    {
        public VersionExceptions(string message)
            : base(message) { }
    }
}
