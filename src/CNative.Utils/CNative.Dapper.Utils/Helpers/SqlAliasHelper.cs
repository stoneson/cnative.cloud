﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace CNative.Dapper.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public static class SqlAliasHelper
    {
        private static readonly ConcurrentDictionary<Type, string> PreDefinedAliases = new ConcurrentDictionary<Type, string>();
        private static readonly List<string> BlacklistedAliases = new List<string>();
        private static readonly ConcurrentDictionary<Type, string> Alias = new ConcurrentDictionary<Type, string>();
        private static bool AvoidDuplicateAliasValues;

        static SqlAliasHelper()
        {
            AddBlacklistedAlias("as", "on", "in", "or", "and", "exists");
            AddBlacklistedAlias("from", "where", "union");
            AddBlacklistedAlias("select", "insert", "update", "delete");
            AvoidDuplicateAliasValues = false;
        }

        public static void SetAvoidDuplicateAliasValues(bool value = true)
        {
            AvoidDuplicateAliasValues = value;
        }

        public static void ClearAliases()
        {
            Alias.Clear();
        }
        public static void ClearPreDefinedAliases()
        {
            PreDefinedAliases.Clear();
        }

        public static void ClearBlacklistedAliases()
        {
            BlacklistedAliases.Clear();
        }
        /// <summary>
        /// 添加黑名单
        /// </summary>
        /// <param name="keys"></param>
        public static void AddBlacklistedAlias(params string[] keys)
        {
            foreach (var key in keys)
            {
                if (BlacklistedAliases.Contains(key, StringComparer.OrdinalIgnoreCase))
                {
                    continue;
                }

                BlacklistedAliases.Add(key);
            }
        }
        /// <summary>
        /// 预定义表别名
        /// </summary>
        /// <param name="key"></param>
        /// <param name="alias"></param>
        public static void PreDefinedAlias(Type key, string alias)
        {
            if (PreDefinedAliases.ContainsKey(key))
            {
                PreDefinedAliases[key] = alias;
                return;
            }

            PreDefinedAliases.TryAdd(key, alias);
        }
        /// <summary>
        /// 获取表别名
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="tableAlias"></param>
        /// <returns></returns>
        public static string GetTableAlias<TClass>(string tableAlias = null)
        {
            var type = typeof(TClass);
            return GetTableAlias(type, tableAlias);
        }
        /// <summary>
        /// 获取表别名
        /// </summary>
        /// <param name="type"></param>
        /// <param name="tableAlias"></param>
        /// <returns></returns>
        public static string GetTableAlias(Type type, string tableAlias = null)
        {
            //var tableAlias = SqlAliasHelper.GetAliasForType(type);
            if (!Alias.TryGetValue(type, out var alias) && tableAlias.IsNotNullOrEmpty())
            {
                alias = CreateAliasForType(type, null, tableAlias);
                Alias.TryAdd(type, alias);
            }

            alias = alias?.Replace(".", "");
            alias = alias?.Length > 0 ? alias + "." : "";

            return alias;
        }

        private static string GetAliasForType<T>(string tableAlias = null, string tableName = null)
        {
            var type = typeof(T);
            return GetAliasForType(type, tableAlias, tableName);
        }
        private static string GetAliasForType(Type type, string tableAlias = null, string tableName = null)
        {
            if (type.IsNullOrEmpty()) return "";
            if (tableAlias.IsNotNullOrEmpty() &&
                !BlacklistedAliases.Contains(tableAlias, StringComparer.OrdinalIgnoreCase))
            {
                if (AvoidDuplicateAliasValues)
                {
                    return tableAlias;
                }
                if (Alias.TryGetValue(type, out var oldTableAlias))
                {
                    if (!tableAlias.EqualsIgnoreCase(oldTableAlias))
                    {
                        Alias.TryUpdate(type, tableAlias, oldTableAlias);
                    }
                }
                else
                {
                    Alias.TryAdd(type, tableAlias);
                }

                return tableAlias;
            }

            if (!Alias.TryGetValue(type, out var alias))
            {
                alias = CreateAliasForType(type, tableName, tableAlias);
                Alias.TryAdd(type, alias);
            }

            return alias;
        }

        private static string CreateAliasForType(Type type, string typeName, string tableAlias = null)
        {
            typeName = typeName.WithDefault(type.Name);
            if (PreDefinedAliases.TryGetValue(type, out var alias))
            {
                if (!BlacklistedAliases.Contains(alias, StringComparer.OrdinalIgnoreCase))
                {
                    return alias;
                }

                PreDefinedAliases.TryRemove(type, out _);
            }

            var typeNameLength = typeName.Length;
            string[] existingAliasValues = null;

            var aliasFromUpperCase = tableAlias.WithDefault(new string(typeName.Where(char.IsUpper).ToArray()).ToLower());
            if (aliasFromUpperCase.IsNullOrEmpty())
            {
                return string.Empty;
            }

            for (var i = 0; i < 5; i++)
            {
                alias = aliasFromUpperCase;

                if (i != 0)
                {
                    var pos = typeNameLength - i;
                    if (pos > -1)
                    {
                        alias += typeName.Substring(pos, 1);
                    }
                }

                existingAliasValues = existingAliasValues ?? BlacklistedAliases.Union(PreDefinedAliases.Values).ToArray();

                if (!existingAliasValues.Contains(alias, StringComparer.OrdinalIgnoreCase))
                {
                    return alias;
                }
            }

            existingAliasValues = existingAliasValues ?? BlacklistedAliases.Union(PreDefinedAliases.Values).ToArray();

            for (var i = 1; i < 20; i++)
            {
                alias = $"{aliasFromUpperCase}{i}";

                if (!existingAliasValues.Contains(alias, StringComparer.OrdinalIgnoreCase))
                {
                    return alias;
                }
            }

            return string.Empty;

        }
    }
}
