﻿/********************************************************************************
** 类名称： CacheHelper
** 描述： 缓存帮助接口
** 作者： xjh
** 创建时间：2019-5-3
** 最后修改人：
** 最后修改时间：
** 版权所有 (C) :CNative
*********************************************************************************/
using System;
using System.Collections.Generic;

namespace CNative.Dapper.Utils
{
    #region interface ICacheHelper
    /// <summary>
    /// 缓存帮助接口
    /// </summary>
    public interface ICacheHelper : IDisposable
    {
        void SetSysCustomKey(string customKey);

        bool Set<T>(string key, T value, TimeSpan? expireTime = null);

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存项的唯一标识符</param>
        /// <returns></returns>
        T Get<T>(string key);

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">缓存项的唯一标识符</param>
        /// <param name="cachePopulate">>获取缓存值的操作(当缓存不存在相应的数据时，执行此方法获取数据)</param>
        /// <param name="expiresTime">缓存时长(分钟), enum ExpiresTime</param>
        /// <param name="checkPopulate">判断是否保存到缓存</param>
        /// <returns></returns>
        T Get<T>(string key, Func<T> cachePopulate, TimeSpan? expiresTime, Func<T, bool> checkPopulate = null);

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">缓存项的唯一标识符</param>
        /// <returns></returns>
        bool Remove(string key);
        /// <summary>
        /// 删除多个key
        /// </summary>
        /// <param name="keys">rediskey</param>
        /// <returns>成功删除的个数</returns>
        long Remove(List<string> keys);
        /// <summary>
        /// 获取dataKeys所有Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        List<T> HashGet<T>(string key, List<string> dataKeys);
        /// <summary>
        /// 删除所有缓存
        /// </summary>
        bool RemoveAll();


        bool HashSet<T>(string key, string fieldKey, T fieldValue);
        T HashGet<T>(string key, string fieldKey);
        Dictionary<string, T> HashGetAll<T>(string key);
        /// <summary>
        /// 移除hash中的某值
        /// </summary>
        bool RemoveHash(string key, string fieldKey);

        /// <summary>
        /// 确定缓存中是否存在某个缓存项
        /// </summary>
        /// <param name="key">缓存项的唯一标识符</param>
        /// <returns></returns>
        bool Exists(string key);
        /// <summary>
        /// 判断某个数据是否已经被缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="fieldKey"></param>
        /// <returns></returns>
        bool HashExists(string key, string fieldKey);

        // <summary>
        /// 从redis获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        bool TryGet<T>(string key, out T val);
        /// <summary>
        /// 从redis获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="dataKey"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        bool TryHashGet<T>(string key, string dataKey, out T val);
        /// <summary>
        /// 从redis获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="dataKey"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        bool TryHashGet<T>(string key, List<string> dataKey, out List<T> val);
        /// <summary>
        /// 获取hashkey所有Redis key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        bool TryHashGetAll<T>(string key, out Dictionary<string, T> val);
        /// <summary>
        /// 获取hashkey所有Redis key
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        bool TryHashGetAll<T>(string key, out List<T> val);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="listdataKey"></param>
        /// <returns></returns>
        bool HashDeleteExecute(string key, List<string> listdataKey);
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listdataKey"></param>
        /// <returns></returns>
        bool HashDeleteExecute(Dictionary<string, List<string>> listdataKey);
        /// <summary>
        /// 批量更新
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="DellistdataKey">批量删除</param>
        /// <param name="dicHashEntrys">批量保存</param>
        /// <returns></returns>
        bool HashUpdateExecute<T>(Dictionary<string, List<string>> DellistdataKey
           , Dictionary<string, Dictionary<string, T>> dicHashEntrys);
        /// <summary>
        /// 批量保存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="dicHashEntrys"></param>
        /// <returns></returns>
        bool HashSetExecute<T>(string key, Dictionary<string, T> dicHashEntrys);
        /// <summary>
        /// 批量保存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dicHashEntrys"></param>
        /// <returns></returns>
        bool HashSetExecute<T>(Dictionary<string, Dictionary<string, T>> dicHashEntrys);

        //bool LockTake(string key, string value, TimeSpan? expiry);
        //bool LockRelease(string key, string value);
        //IDisposable AcquireLock(string key, TimeSpan timeOut, bool useLock = true);

    }
    #endregion

    #region 缓存时长
    /// <summary>
    /// 缓存时长
    /// </summary>
    public enum ExpiresTime
    {
        /// <summary>
        /// 不限
        /// </summary>
        None = 0,
        /// <summary>
        /// 1分钟
        /// </summary>
        Minutes_1 = 1,
        /// <summary>
        /// 15分钟
        /// </summary>
        Minutes_15 = 15,
        /// <summary>
        /// 30分钟
        /// </summary>
        Minutes_30 = 30,

        /// <summary>
        /// 1小时
        /// </summary>
        Hours_1 = 60,

        /// <summary>
        /// 2小时
        /// </summary>
        Hours_2 = 120,

        /// <summary>
        /// 4小时
        /// </summary>
        Hours_4 = 240,
        /// <summary>
        /// 8小时
        /// </summary>
        Hours_8 = 480,
        /// <summary>
        /// 12小时
        /// </summary>
        Hours_12 = 720,
    }
    #endregion
}
