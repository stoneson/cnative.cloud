﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNative.Dapper.Utils
{
    /// <summary>
    /// 指定类将映射到的数据库表
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TableMapAttribute : Attribute
    {
        /// <summary>
        /// 指定类将映射到的数据库表
        /// </summary>
        public TableMapAttribute()
        {
        }
        public TableMapAttribute(string tableName)
        {
            this.TableName = tableName;
        }
        public TableMapAttribute(string tableName, string tableDescription)
        {
            this.TableName = tableName;
            this.TableDescription = tableDescription;
        }

        public TableMapAttribute(string tableName, string tableDescription, bool isDisabledDelete)
        {
            this.TableName = tableName;
            this.TableDescription = tableDescription;
            this.IsDisabledDelete = isDisabledDelete;
        }
        /// <summary>
        /// 表的类名称
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 类映射到的表的架构,对应数据库名
        /// </summary>
        public string Schema { get; set; }
        public string TableDescription { get; set; }
        public bool IsDisabledDelete { get; set; }
        public bool IsDisabledUpdateAll { get; set; }
    }
}
