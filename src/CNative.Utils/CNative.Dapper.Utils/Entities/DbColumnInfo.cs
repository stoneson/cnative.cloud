﻿using System.Reflection;

namespace CNative.Dapper.Utils
{
    /// <summary>
    /// 数据库表信息
    /// </summary>
    public class DbColumnInfo
    {
        public DbColumnInfo() {}
        public DbColumnInfo(PropertyInfo _PropertyInfo) { PropertyInfo = _PropertyInfo; }
        public PropertyInfo PropertyInfo { get;  set; }
        /// <summary>
        /// 字段Id
        /// </summary>
        public int ColumnId { get; set; }

        /// <summary>
        /// 字段名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 字段类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 字段长度
        /// </summary>
        public long MaxLength { get; set; }

        /// <summary>
        /// 是否为主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// 是否为自增类型
        /// </summary>
        public bool IsIdentity { get; set; } = false;

        /// <summary>
        /// 是否为空
        /// </summary>
        public bool IsNullable { get; set; } = true;

        /// <summary>
        /// 是否表中存在此字段
        /// </summary>
        public bool IsInTable { get; set; }
        /// <summary>
        /// 字段描述说明
        /// </summary>
        public string Description
        {
            get
            {
                return _description.IsNullOrEmpty() ? Name : _description;
            }
            set
            {
                _description = value;
            }
        }
        /// <summary>
        /// 字段类型
        /// </summary>
        public string DbFullType { get; set; }
        public int DecimalDigits { get; set; }
        public int Scale { get; set; }
        private string _description { get; set; }
        public bool IsIgnore { get; set; }
        public string DefaultValue { get; set; }
        public string[] IndexGroupNameList { get; set; }
        public string[] UIndexGroupNameList { get; set; }
        public override string ToString()
        {
            return this.Name 
                + (_description.IsNullOrEmpty() ? "" : " (" + _description + ") ") 
                + " (" + this.Type
                + (MaxLength > 0 ? "(" + MaxLength + ")" : "")
                + (IsPrimaryKey ? ", PK" : "")
                + (IsNullable ? ", null" : ",not null")
                + " )"
               ;
        }
    }
}
