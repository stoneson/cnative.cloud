﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using CNative.Utilities;

namespace CNative.Dapper.Utils
{
    /// <summary>
    /// CNative DbParameter
    /// </summary>
    public class CNDbParameter //: DbParameter
    {
        //public string Name { get; set; }
        //public string ColumnName { get; set; }
        //public object Value { get; set; }
        public DbType? DbType { get; set; }
        //public ParameterDirection? ParameterDirection { get; set; }
        //public int? Size { get; set; }
        public byte? Precision { get; set; }
        public byte? Scale { get; set; }

        public bool IsRefCursor { get; set; }
        public CNDbParameter()
        {
        }
        public CNDbParameter(string name, object value)
        {
            this.Value = value;
            this.ParameterName = name;
            //if (value != null)
            //{
            //    SettingDataType(value.GetType());
            //}
        }
        public CNDbParameter(string name, object value, Type type)
        {
            this.Value = value;
            this.ParameterName = name;
            SettingDataType(type);
        }
        public CNDbParameter(string name, object value, Type type, ParameterDirection direction)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            SettingDataType(type);
        }
        public CNDbParameter(string name, object value, Type type, ParameterDirection direction, int size)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            this.Size = size;
            SettingDataType(type);
        }


        public CNDbParameter(string name, object value, System.Data.DbType type)
        {
            this.Value = value;
            this.ParameterName = name;
            this.DbType = type;
        }
        public CNDbParameter(string name, DataTable value, string SqlServerTypeName)
        {
            this.Value = value;
            this.ParameterName = name;
            this.TypeName = SqlServerTypeName;
        }
        public CNDbParameter(string name, object value, System.Data.DbType type, ParameterDirection direction)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            this.DbType = type;
        }
        public CNDbParameter(string name, object value, System.Data.DbType type, ParameterDirection direction, int size)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            this.Size = size;
            this.DbType = type;
        }

        private void SettingDataType(Type type)
        {
            this.DbType = Funs.LookupDbType(type);
        }
        
        public CNDbParameter(string name, object value, bool isOutput)
        {
            this.Value = value;
            this.ParameterName = name;
            if (isOutput)
            {
                this.Direction = ParameterDirection.Output;
            }
        }
        //public  System.Data.DbType DbType
        //{
        //    get; set;
        //}

        public  ParameterDirection Direction
        {
            get; set;
        }

        //public  bool IsNullable
        //{
        //    get; set;
        //}

        public  string ParameterName
        {
            get; set;
        }

        public int? _Size;

        public  int? Size
        {
            get
            {
                if (_Size == 0 && Value != null)
                {
                    var isByteArray = Value.GetType() == Constants.ByteArrayType;
                    if (isByteArray)
                        _Size = -1;
                    else
                    {
                        var length = Value.ToString().Length;
                        _Size = length < 4000 ? 4000 : -1;

                    }
                }
                if (_Size == 0)
                    _Size = 4000;
                return _Size;
            }
            set
            {
                _Size = value;
            }
        }

        //public override string SourceColumn
        //{
        //    get; set;
        //}

        //public override bool SourceColumnNullMapping
        //{
        //    get; set;
        //}
        //public string UdtTypeName
        //{
        //    get;
        //    set;
        //}
        object _Value;
        public  object Value
        {
            get { return _Value; } 
            set
            {
                _Value = value;
                if (value != null)
                {
                    SettingDataType(value.GetType());
                }
            }
        }

        //public Dictionary<string, object> TempDate
        //{
        //    get; set;
        //}

        ///// <summary>
        ///// 如果类库是.NET 4.5请删除该属性
        ///// If the SqlSugar library is.NET 4.5, delete the property
        ///// </summary>
        //public override DataRowVersion SourceVersion
        //{
        //    get; set;
        //}

        //public override void ResetDbType()
        //{
        //    this.DbType = System.Data.DbType.String;
        //}


        public string TypeName { get; set; }
        public bool IsJson { get; set; }
        public bool IsArray { get; set; }
    }

    //public class OracleDynamicParameters : Dapper.SqlMapper.IDynamicParameters
    //{
    //    private readonly Dapper.DynamicParameters dynamicParameters = new Dapper.DynamicParameters();

    //    private readonly List<OracleParameter> oracleParameters = new List<OracleParameter>();

    //    public void Add(string name, object value = null, DbType? dbType = null, ParameterDirection? direction = null, int? size = null)
    //    {
    //        dynamicParameters.Add(name, value, dbType, direction, size);
    //    }

    //    public void Add(string name, OracleDbType oracleDbType, ParameterDirection direction)
    //    {
    //        var oracleParameter = new OracleParameter(name, oracleDbType, direction);
    //        oracleParameters.Add(oracleParameter);
    //    }

    //    public void AddParameters(IDbCommand command, SqlMapper.Identity identity)
    //    {
    //        ((SqlMapper.IDynamicParameters)dynamicParameters).AddParameters(command, identity);

    //        var oracleCommand = command as OracleCommand;

    //        if (oracleCommand != null)
    //        {
    //            oracleCommand.Parameters.AddRange(oracleParameters.ToArray());
    //        }
    //    }
    //}
}
