﻿using CNative.Utilities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CNative.Dapper.Utils
{
    public class SqlEntity
    {
        public SqlEntity(IDbHelper dbHelper)
        {
            //Parameters = new Dictionary<string,object>();
            CommandType = CommandType.Text;

            DbHelper = dbHelper;
        }
        public SqlEntity(IDbHelper dbHelper, string sql)
        {
            Sql = sql;
            //Parameters = new Dictionary<string, object>();
            CommandType = CommandType.Text;

            DbHelper = dbHelper;
        }
        /// <summary>
        /// 数据库操作类型
        /// </summary>
        public IDbHelper DbHelper { get; }
        /// <summary>
        /// SQL脚本
        /// </summary>
        public string Sql { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CommandType CommandType { get; set; }
        public int? CommandTimeout { get; set; }
        /// <summary>
        /// 支持Dapper原生参数
        /// Parameters优先，Parameters ?? Parameter
        /// </summary>
        public object Parameter { get; set; }
        /// <summary>
        /// 入参集合
        /// </summary>
        public DynamicParameters Parameters { get; set; }

        /// <summary>
        /// 添加参数
        /// </summary>
        /// <param name="propName">参数名称</param>
        /// <param name="val">参数值</param>
        /// <param name="sqlent"></param>
        /// <param name="parameterName"></param>
        /// <param name="tb"></param>
        /// <param name="paramSuffix">参数前缀</param>
        public virtual bool AddDbParameter(string propName, object val, out string parameterName, string paramSuffix = "wp_", DbTableInfo tb = null)
        {
            parameterName = "";
            if (this.Parameters == null) this.Parameters = new DynamicParameters();
            if (paramSuffix.IsNotNullOrEmpty())
                paramSuffix = paramSuffix.NullToStr() + this.Parameters.ParameterNames.Count() + "_";

            var para = DbHelper.SqlDbProvider.GetDbParameter(propName, val, paramSuffix, tb);
            if (para != null)
            {
                parameterName = para.ParameterName;
                this.Parameters.Add(para.ParameterName, para.Value, para.DbType, para.Direction, para.Size, para.Precision, para.Scale);
                return true;
            }
            return false;
        }
        /// <summary>
        /// 添加参数
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterValue"></param>
        /// <param name="direction"></param>
        /// <param name="valDBType"></param>
        public void AddParameter(string parameterName, object parameterValue, DbType? valDBType = null, ParameterDirection direction = ParameterDirection.Input)
        {
            var para = DbHelper.SqlDbProvider.GetDbParameter(parameterName, parameterValue, "", null, direction, valDBType);
            if (para != null)
            {
                if (Parameters == null) Parameters = new DynamicParameters();
                this.Parameters.Add(para.ParameterName, para.Value, para.DbType, para.Direction, para.Size, para.Precision, para.Scale);
            }
        }
        /// <summary>
        /// 批量添加参数
        /// </summary>
        /// <param name="parameters"></param>
        public void AddParameters(params object[] parameters)
        {
            if (parameters?.Length > 0)
            {
                if (Parameters == null) Parameters = new DynamicParameters();
                parameters.ForEach(param =>
                {
                    if (param != null)
                        this.Parameters.AddDynamicParams(param);
                });
            }
        }
        ///// <summary>
        ///// 批量添加参数
        ///// </summary>
        ///// <param name="parameters"></param>
        //public void AddParameters(params DynamicParameters[] parameters)
        //{
        //    if (parameters?.Length > 0)
        //    {
        //        if (Parameters == null) Parameters = new DynamicParameters();
        //        parameters.ForEach(param =>
        //        {
        //            if (param != null)
        //                this.Parameters.AddDynamicParams(param);
        //        });
        //    }
        //}
        /// <summary>
        /// 添加参数
        /// </summary>
        /// <param name="parameters"></param>
        public void AddParameters(params CNDbParameter[] dbParameters)
        {
            if (dbParameters?.Length > 0)
            {
                if (Parameters == null) Parameters = new DynamicParameters();
                dbParameters.ForEach(parameter =>
                {
                    var para = DbHelper.SqlDbProvider.GetDbParameter(parameter.ParameterName, parameter.Value, "", null, parameter.Direction, parameter.DbType);
                    if (para != null)
                    {
                        //this.Parameters[param.ParameterName] = param;
                        this.Parameters.Add(para.ParameterName, para.Value, para.DbType, para.Direction, para.Size, para.Precision, para.Scale);
                    }
                });
            }
        }

        /// <summary>
        /// Compare 2 Identity objects
        /// </summary>
        /// <param name="other">The other <see cref="Identity"/> object to compare.</param>
        /// <returns>Whether the two are equal</returns>
        public bool Equals(SqlEntity other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (other is null) return false;

            return CommandType == other.CommandType
                && Sql == other.Sql
                && Parameter == other.Parameter
                && Parameters == other.Parameters
                && DbHelper == other.DbHelper
                && ((DbHelper == null || DbHelper.ConnectString.IsNullOrEmpty() )
                || (other.DbHelper == null || other.DbHelper.ConnectString.IsNullOrEmpty()) 
                || DbHelper.ConnectString== other.DbHelper.ConnectString);
        }
        /// <summary>
        /// Returns true if this has the same parent  
        /// <see cref="Name"/> and <see cref="Value"/> as <paramref name="obj"/>.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare.</param>
        public override bool Equals(object obj) => Equals(obj as SqlEntity);

        /// <summary>
        /// Returns the XOR of certain properties.
        /// </summary>
        public override int GetHashCode()
        {
            int hashCode = 26;
            unchecked
            {
                hashCode = (hashCode * 12) + CommandType.GetHashCode();
                hashCode = (hashCode * 12) + (Sql?.GetHashCode() ?? 0);
                hashCode = (hashCode * 12) + (Parameter?.GetHashCode() ?? 0);
                hashCode = (hashCode * 12) + (DbHelper?.GetHashCode() ?? 0);
                hashCode = (hashCode * 12) + (DbHelper == null || DbHelper.ConnectString.IsNullOrEmpty() ? 0 : DbHelper.ConnectString.GetHashCode());
                hashCode = (hashCode * 12) + (Parameters?.GetHashCode() ?? 0);
            }
            return hashCode;
        }
    }

    public static class SqlEntityExp
    {
        public static bool Execute(this SqlEntity sql) => sql.DbHelper.Execute(sql);
        public static bool Execute(this List<SqlEntity> sqlList) => sqlList?.Count > 0 ? sqlList[0].DbHelper.Execute(sqlList) : false;
        public static DataSet QueryDataSet(this SqlEntity sql) => sql.DbHelper.QueryDataSet(sql);
        public static DataTable QueryDataTable(this SqlEntity sql) => sql.DbHelper.QueryDataTable(sql);
        public static List<T> Query<T>(this SqlEntity sql) where T : class => sql.DbHelper.Query<T>(sql);

        public static T GetSingle<T>(this SqlEntity sql) => sql.DbHelper.GetSingle<T>(sql);
        public static T QuerySingle<T>(this SqlEntity sql) where T : class => sql.DbHelper.QuerySingle<T>(sql);

        //--------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 是否只查询SQL脚
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        internal static bool IsSelectSQL(this SqlEntity sql)
        {
            if (sql.Sql.IsNullOrEmpty() || sql.CommandType != CommandType.Text) return false;
            var sqlLower = sql.Sql.ToLower();
            var result = System.Text.RegularExpressions.Regex.IsMatch(sqlLower, "[ ]*select[ ]") && !System.Text.RegularExpressions.Regex.IsMatch(sqlLower, "[ ]*insert[ ]|[ ]*update[ ]|[ ]*delete[ ]");
            return result;
        }

        public static string ToJoinSqlInVals<T>(this T[] array)
        {
            if (array == null || array.Length == 0)
            {
                return ToSqlValue(string.Empty);
            }
            else
            {
                return string.Join(",", array.Where(c => c != null).Select(it => it.ToSqlValue()));
            }
        }

        public static object ToSqlValue(this object value)
        {
            if (value != null && Constants.NumericalTypes.Contains(value.GetType()))
                return value;

            var str = value + "";
            return str.ToSqlValue();
        }

        public static string ToSqlValue(this string value)
        {
            return string.Format("'{0}'", value.ToSqlFilter());
        }

        /// <summary>
        ///Sql Filter
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSqlFilter(this string value)
        {
            if (!value.IsNullOrEmpty())
            {
                value = value.Replace("'", "''");
            }
            return value;
        }

    }

}
