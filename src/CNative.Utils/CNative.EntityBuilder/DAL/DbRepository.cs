﻿using CNative.DbUtils;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Devart.Data.Oracle;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Data.Common;

namespace CNative.EntityBuilder.DAL
{
    /// <summary>
    /// 描述：数据库仓储基类类
    /// 作者：xjh
    /// </summary>
    public class DbRepository : DataRepository.DbRepository
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dbName">构造参数，可以为数据库连接字符串</param>
        public DbRepository(string dbName) : base(dbName)
        {
        }
        protected override IDbHelper CreateDb(string dbName)
        {
            var configSettings = ConfigurationManager.ConnectionStrings[dbName];
            if (configSettings == null)
                throw new Exception("未找到名称为[" + dbName + "]的连接字符串");
            SetdbType(dbName);
            return new CNative.EntityBuilder.DAL.DbHelper(dbName);
        }
        /// <summary>
        /// 开始单库事物
        /// 注意:若要使用跨库事务,请使用DistributedTransaction
        /// </summary>
        public override void BeginTransaction()
        {
            if(_db is CNative.EntityBuilder.DAL.DbHelper db)
            {
                db.BeginTransaction();
            }
        }

        /// <summary>
        /// 结束事物提交
        /// </summary>
        public override bool EndTransaction()
        {
            if (_db is CNative.EntityBuilder.DAL.DbHelper db)
            {
                db.Commit();
            }
            return true;
        }
        public override bool Execute(SqlEntity sql)
        {
            return Db.Execute(sql);
        }
        public override bool Execute(List<SqlEntity> sqlList)
        {
            return Db.Execute(sqlList);
        }

        /// <summary>
        /// 获取DbParameter
        /// </summary>
        /// <param name="dbType">数据库类型</param>
        /// <returns></returns>
        protected override IDataParameter GetDbParameter(string propName, object val, string paramSuffix = "wp_", DataRepository.DbTableInfo tb = null)
        {
            val = val ?? DBNull.Value;
            IDataParameter para = base.GetDbParameter(propName, val, paramSuffix, tb);
            if (para == null && _dbType == DataRepository.DatabaseType.Oracle)
            {
                para = new OracleParameter()
                {
                    ParameterName = paramKeyword + paramSuffix + propName,
                    OracleValue = val,
                };
                if (tb != null && tb.TableInfo != null)
                {
                    var dtinfo = tb.TableInfo.Find(f => f.Name.Equals(propName.Trim(), StringComparison.OrdinalIgnoreCase));
                    if (dtinfo != null)
                    {
                        try
                        {
                            (para as OracleParameter).OracleDbType = dtinfo.Type.Trim('2').ToEnum<OracleDbType>();
                            if (dtinfo.MaxLength > 0)
                                (para as OracleParameter).Size = dtinfo.MaxLength;
                        }
                        catch { }
                    }
                }
            }
            //此处可能扩展其他类型的库
            return para;
        }
    }
}
