﻿using CNative.DbUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CNative.EntityBuilder.CreateText
{
    public class CreateModel
    {
        private DbMaintenanceProvider _getMetaDataBase;
        // private GetMetaDataString _getMetaDataString;
        private DatabaseType _dbType;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CreateModel(string dbName)
        {
            _getMetaDataBase = new DbMaintenanceProvider(dbName);
            _dbType = _getMetaDataBase.DBType;
        }
        public CreateModel(DbMaintenanceProvider dbMetadata)
        {
            _getMetaDataBase = dbMetadata;
            _dbType = _getMetaDataBase.DBType;
        }
        //-------------------------------------------------------------------------------------------------------------------------------------------------
        public string Ganarate(string nameSpace, string dbname, string tableName, string tableNameDes, string byname, bool isTableMap, bool isDbFieldMap, bool isGuid2tring=false)
        {
            if (byname.Trim() == "")
            {
                byname = tableName;
            }
            string reValue = CreateHead(nameSpace, dbname, tableName, tableNameDes, byname, isTableMap);
            reValue += CreateBuildFunction(nameSpace, tableName, byname);
            reValue += CreateBody(dbname, tableName, isTableMap, isDbFieldMap, isGuid2tring);
            reValue += CreateBottom();

            return reValue;
        }

        protected string CreateHead(string nameSpace, string dbname, string tableName, string tableNameDes, string byname, bool isTableMap)
        {
            var bz = @"/********************************************************************************
** 类名称： " + byname + @"
* * 描述：" + tableNameDes + "的实体类 " + byname + @"
* * 作者： " + Config.Creator + @"
** 创建时间： " + DateTime.Now.ToString("yyyy-MM-dd") + @"
** 最后修改人：
** 最后修改时间：
** 版权所有 (C) :CNative
*********************************************************************************/";
            StringBuilder str = new StringBuilder();
            str.Append(bz+ "\n\n");
            if (Config.DefaultUsing != null && Config.DefaultUsing.Count > 0)
            {
                Config.DefaultUsing.ForEach(f => str.Append("using " + f + ";\n"));
            }
            else
            {
                str.Append("using System;\n");
            }
            //str.Append("\n");
            str.Append("namespace " + nameSpace + "\n");
            str.Append("{\n");
            str.Append("    /// <summary>\n");
            str.Append("    /// 表" + tableNameDes + "的实体类 " + byname + "\n");
            str.Append("    /// " + DateTime.Now.ToString("yyyy-MM-dd") + " Create by " + Config.Creator + "\n");
            str.Append("    /// </summary>\n");
            if (isTableMap)
                str.AppendFormat("    [TableMap(Schema = \"{0}\",TableName = \"{1}\")]\n", dbname, tableName);
            str.Append("    public class " + byname + (Config.BaseModel.IsNullOrEmpty() ? "" : ":" + Config.BaseModel) + " \n");
            str.Append("    {\n");
            return str.ToString();
        }

        #region 创建构造函数

        protected string CreateBuildFunction(string nameSpace, string tableName, string byname)
        {
            StringBuilder str = new StringBuilder();
            str.Append("        #region 构造函数\n\n");
            str.Append("        public " + byname + "()\n");
            str.Append("        {}\n\n");
            str.Append("        #endregion\n\n");
            return str.ToString();
        }

        #endregion

        protected string CreateBody(string dbname, string tableName, bool isTableMap, bool isDbFieldMap, bool isGuid2tring = false)
        {
            StringBuilder str = new StringBuilder();
            str.Append("        #region 属性\n");
            List<DbColumnInfo> dbColumnCollection = this._getMetaDataBase.GetAllColumns(tableName, dbname);
            if (dbColumnCollection != null)
            {
                if (dbColumnCollection.Count > 0)
                {
                    #region 创建属性

                    foreach (var column in dbColumnCollection)
                    {
                        var type = Config.GetTypeByDataTypeName(column.Type);
                        if (isGuid2tring && type == "Guid")
                            type = "string";
                        //若不是string类型,则设置该字段为可空类型
                        if (type == "DateTime" && column.IsNullable)//type != "int" && type != "string" && type != "byte[]"
                        {
                            type += "?";
                        }

                        var columnName = isDbFieldMap ? Config.GetEntityNameByTableName(column.Name) : column.Name;
                        str.Append("        /// <summary>\n");
                        //得到该列的备注信息
                        var discription = column.Description;
                        str.Append("        /// " + discription + "\n");
                        str.Append("        /// </summary>\n");
                        if (isDbFieldMap || (isTableMap && column.IsPrimaryKey))
                        {
                            str.AppendFormat("        [DbFieldMap(CloumnName = \"{0}\"", column.Name);
                            str.AppendFormat(", Type = \"{0}\"", column.Type);
                            if (column.MaxLength > 0)
                            {
                                str.AppendFormat(", MaxLength = {0}", column.MaxLength.ToString());
                            }
                            if (!column.IsNullable)
                            {
                                str.AppendFormat(", IsNullable = false");
                            }
                            if (column.IsPrimaryKey)
                            {
                                str.Append(", IsPrimaryKey = true");
                            }
                            if (column.Description != column.Name)
                            {
                                str.AppendFormat(", Description = \"{0}\"", column.Description);
                            }
                            str.Append(" )]\n");
                        }
                        str.Append("        public " + type + " " + columnName + " { get; set; }\n");
                        //str.Append("        \n");
                    }
                    #endregion
                }
            }

            return str.ToString();
        }

        protected string CreateBottom()
        {
            StringBuilder str = new StringBuilder();
            str.Append("        #endregion\n");
            str.Append("    }\n");
            str.Append("}\n");
            return str.ToString();
        }
       

       
    }
}
