﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CNative.EntityBuilder
{
    public partial class ucCodeView : UserControl
    {
        public ucCodeView()
        {
            InitializeComponent();
            this.Load += UcCodeView_Load;
        }

        private void UcCodeView_Load(object sender, EventArgs e)
        {
            Config.SetKeywordsColor(txtModelCode);
        }

        public string CodeText
        {
            get { return this.txtModelCode.Text; }
            set
            {
                txtModelCode.Text = value;
            }
        }
    }
}
