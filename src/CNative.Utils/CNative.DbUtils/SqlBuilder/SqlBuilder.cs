﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Linq;

namespace CNative.DbUtils
{
    public class SqlBuilder : DbHelper, ISqlBuilder
    {
        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dbName">构造参数，可以为数据库连接字符串</param>
        public SqlBuilder(string dbName) : base(dbName)
        {
            IsDisposed = false;
        }

        /// <summary>
        /// 标记DbContext是否已经释放
        /// </summary>
        protected bool IsDisposed { get; set; }
        /// <summary>
        /// 释放数据,初始化状态
        /// </summary>
        public void Dispose()
        {
            IsDisposed = true;
        }
        #endregion

        private static ISqlBuilder _IsqlBuilder = null;
        /// <summary>
        /// 一个实例
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static ISqlBuilder Instance(string dbName)
        {
            if (_IsqlBuilder == null)
                _IsqlBuilder = new SqlBuilder(dbName);
            return _IsqlBuilder;
        }

        /// <summary>
        /// 开始添加(可多表操作)
        /// </summary>
        /// <returns></returns>
        public IInsertSqlBuilder DoInsert
        {
            get
            {
                return new InsertSqlBuilder(this);
            }
        }
        /// <summary>
        /// 开始查找(可多表操作)
        /// </summary>
        /// <returns></returns>
        public ISelectSqlBuilder DoSelect
        {
            get
            {
                return new SelectSqlBuilder(this);
            }
        }
        /// <summary>
        /// 开始更新(可多表操作)
        /// </summary>
        /// <returns></returns>
        public IUpdateSqlBuilder DoUpdate
        {
            get
            {
                return new UpdateSqlBuilder(this);
            }
        }
        /// <summary>
        /// 开始删除(可多表操作)
        /// </summary>
        /// <returns></returns>
        public IDeleteSqlBuilder DoDelete
        {
            get
            {
                return new DeleteSqlBuilder(this);
            }
        }
        //------------------------------------------------------------------------------------
        /// <summary>
        /// 开始添加(单表操作)
        /// </summary>
        /// <returns></returns>
        public IInsertSqlBuilder<TClass> doInsert<TClass>() where TClass : class, new()
        {
            return new InsertSqlBuilder<TClass>(this);
        }
        /// <summary>
        /// 开始查找(单表操作)
        /// </summary>
        /// <returns></returns>
        public ISelectSqlBuilder<TClass> doSelect<TClass>() where TClass : class, new()
        {
            return new SelectSqlBuilder<TClass>(this);
        }
        /// <summary>
        /// 开始更新(单表操作)
        /// </summary>
        /// <returns></returns>
        public IUpdateSqlBuilder<TClass> doUpdate<TClass>() where TClass : class, new()
        {
            return new UpdateSqlBuilder<TClass>(this);
        }
        /// <summary>
        /// 开始删除(单表操作)
        /// </summary>
        /// <returns></returns>
        public IDeleteSqlBuilder<TClass> doDelete<TClass>() where TClass : class, new()
        {
            return new DeleteSqlBuilder<TClass>(this);
        }

        #region 扩展DbHelper
        //public virtual bool Execute(string sql)
        //{
        //    return Execute(new SqlEntity(DBType) { Sql = sql });
        //}
        //public virtual bool Execute(List<string> sql)
        //{
        //    return Execute(sql.Select(s => new SqlEntity(DBType) { Sql = s }).ToList());
        //}

        //public virtual List<T> Query<T>(string sql) where T : class
        //{
        //    return Query<T>(new SqlEntity(DBType) { Sql = sql });
        //}

        //public virtual DataSet QueryDataSet(string sql)
        //{
        //    return QueryDataSet(new SqlEntity(DBType) { Sql = sql });
        //}

        //public virtual DataTable QueryDataTable(string sql)
        //{
        //    return QueryDataTable(new SqlEntity(DBType) { Sql = sql });
        //}
        //public virtual T GetSingle<T>(string sql) where T : new()
        //{
        //    return GetSingle<T>(new SqlEntity(DBType) { Sql = sql });
        //}

        //public virtual T GetSingleRow<T>(string sql) where T : class, new()
        //{
        //    return GetSingleRow<T>(new SqlEntity(DBType) { Sql = sql });
        //}
        #endregion
    }
}
