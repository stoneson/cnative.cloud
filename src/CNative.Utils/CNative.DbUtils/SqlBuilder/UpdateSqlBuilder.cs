﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using CNative.Utilities;

namespace CNative.DbUtils
{
    #region UpdateSqlBuilder
    internal class UpdateSqlBuilder : BaseSqlBuilder, IUpdateSqlBuilder
    {
        protected Dictionary<string, Tuple<string, object, Type>> dicfields = new Dictionary<string, Tuple<string, object, Type>>();

        public UpdateSqlBuilder(IDbHelper _db) : base(_db)
        {
        }

        #region Update
        /// <summary>
        /// 更新一条数据,自动对比新老实体对象属性差异来决定更新字段
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="newEntity">新实体</param>
        /// <param name="oldEntity">老实体</param>
        /// <returns></returns>
        public bool Update<TClass>(TClass newEntity, TClass oldEntity) where TClass : class, new()
        {
            List<string> setfields = newEntity.GetPropertyDiffList(oldEntity);
            if ((setfields == null || setfields.Count == 0) && newEntity.GetHashCode() == oldEntity.GetHashCode()) return false;
            return Update(newEntity, setfields.ToArray());
        }

        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update<TClass>(TClass entity, params string[] columns) where TClass : class, new()
        {
            return Update(new List<TClass> { entity }, columns);
        }

        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="isForSet">控制fields</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update<TClass>(List<TClass> entities, params string[] columns) where TClass : class, new()
        {
            if (entities?.Count > 0)
            {
                var tableInfo = GetTableInfo<TClass>();
                var whereStr = BuildEntityWhere(sqlent, entities[0], null, tableInfo?.Item1);
                if (whereStr.IsNullOrEmpty())
                {
                    whereStr.ArgumentNullException("更新数据时，未设置过滤条件。@" + tableInfo?.Item3);
                    //return false;
                }
                sqlent.Sql = whereStr;
                return Update(entities, sqlent, true, columns, tableInfo);
            }
            return false;
        }

        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update<TClass>(TClass entity, IEnumerable<string> IgnoreColumns) where TClass : class, new()
        {
            return Update(new List<TClass> { entity }, IgnoreColumns);
        }
        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update<TClass>(List<TClass> entities, IEnumerable<string> IgnoreColumns) where TClass : class, new()
        {
            if (entities?.Count > 0)
            {
                var tableInfo = GetTableInfo<TClass>();
                var whereStr = BuildEntityWhere(sqlent, entities[0], null, tableInfo?.Item1);
                if (whereStr.IsNullOrEmpty())
                {
                    whereStr.ArgumentNullException("更新数据时，未设置过滤条件。@" + tableInfo?.Item3);
                    //return false;
                }
                sqlent.Sql = whereStr;
                return Update(entities, sqlent, false, IgnoreColumns, tableInfo);
            }
            return false;
        }
        #endregion
        #region Update where
        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update<TClass>(TClass entity, Expression<Func<TClass, bool>> where, params string[] columns) where TClass : class, new()
        {
            return Update(new List<TClass> { entity }, where, columns);
        }

        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update<TClass>(List<TClass> entities, Expression<Func<TClass, bool>> where, params string[] columns) where TClass : class, new()
        {
            var whereStr = BuildWhereExp(sqlent, where, null);
            if (whereStr.IsNullOrEmpty())
            {
                return Update(entities, columns);
            }
            sqlent.Sql = whereStr;
            return Update(entities, sqlent, true, columns);
        }

        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update<TClass>(TClass entity, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns = null) where TClass : class, new()
        {
            return Update(new List<TClass> { entity }, where, IgnoreColumns);
        }

        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update<TClass>(List<TClass> entities, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns) where TClass : class, new()
        {
            var whereStr = BuildWhereExp(sqlent, where, null);
            if (whereStr.IsNullOrEmpty())
            {
                return Update(entities, IgnoreColumns);
            }
            sqlent.Sql = whereStr;
            return Update(entities, sqlent, false, IgnoreColumns);
        }

        /// <summary>
        /// 通用更新方法
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="entities"></param>
        /// <param name="whereSqle"></param>
        /// <param name="isForSet"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        protected bool Update<TClass>(List<TClass> entities, SqlEntity whereSqle, bool isForSet, IEnumerable<string> columns
            , Tuple<DbTableInfo, string, string> tableInfo = null) where TClass : class, new()
        {
            if (!entities.IsNullOrEmpty_())
            {
                if (tableInfo == null)
                    tableInfo = GetTableInfo<TClass>();
                sqlList.Clear();
                entities.ForEach(entity =>
                {
                    var sqle = BuildUpdate(entity, isForSet, columns?.CastToList<string>(), whereSqle, tableInfo, false);
                    if (sqle != null)
                    {
                        sqlList.Add(sqle);
                    }
                });
                sqlent?.Parameters.Clear();
                sqlent.Sql = "";
                return Exec;
            }
            return false;
        }
        #endregion
        #region Update Expression fields
        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="fields">设置多个更新字段</param>
        /// <param name="where">添加条件表达式</param>
        /// <returns></returns>
        public bool Update<TClass>(Expression<Func<TClass, TClass>> fields, Expression<Func<TClass, bool>> where) where TClass : class, new()
        {
            Set<TClass>(fields);
            Where<TClass>(where);

            return Exec;
        }
        #endregion
        //---------------------------------------------------------------------------------------------------------------
        #region Set/ Expression fields
        /// <summary>
        /// 设置多个更新字段
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public IUpdateSqlBuilder Set<TClass>(Expression<Func<TClass, TClass>> fields) where TClass : class, new()
        {
            dicfields = BuildFields(fields, (column) => column.IsNullOrEmpty() || column.IsIdentity == true || column.IsPrimaryKey == true);
            return this;
        }
        /// <summary>
        /// 设置单个更新字段值
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <typeparam name="TField"></typeparam>
        /// <param name="field"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public IUpdateSqlBuilder Set<TClass, TValue>(Expression<Func<TClass, TValue>> field, TValue value) where TClass : class, new()
        {
            var fieds = new List<string>(ExpressionHelper.GetMemberNames(field?.Body));
            if (fieds.Count > 0)
            {
                var tableInfo = GetTableInfo<TClass>();
                var col = tableInfo.Item1.GetColumn(fieds[0]);
                if (col != null && col.IsIdentity == false && col.IsPrimaryKey == false)
                {
                    var colname = $"{tableInfo.Item2}{suffixLeft}{col.Name}{suffixRigh}";
                    dicfields[col.Name] = Tuple.Create(colname, (object)value, col?.PropertyInfo.PropertyType.GetUnderType());
                }
            }

            return this;
        }
        #endregion

        #region Where
        /// <summary>
        /// 添加条件表达式
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public IUpdateSqlBuilder Where<TClass>(Expression<Func<TClass, bool>> where, bool isAnd = true) where TClass : class, new()
        {
            BuildWhere(sqlent, where, isAnd);
            return this;
        }
        #endregion
        //---------------------------------------------------------------------------------------------------------------
        #region UpdateOrInsert
        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool UpdateOrInsert<TClass>(TClass entity, Expression<Func<TClass, bool>> where, params string[] columns) where TClass : class, new()
        {
            var whereStr = BuildWhereExp(sqlent, where, null);
            if (whereStr.IsNullOrEmpty())
            {
                return false;
            }
            sqlent.Sql = whereStr;
            return UpdateOrInsertBase(new List<TClass> { entity }, sqlent, false, columns);
        }

        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool UpdateOrInsert<TClass>(List<TClass> entities, params string[] columns) where TClass : class, new()
        {
            sqlent?.Parameters.Clear();
            sqlent.Sql = "";
            return UpdateOrInsertBase(entities, sqlent, true, columns);
        }

        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool UpdateOrInsert<TClass>(TClass entity, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns) where TClass : class, new()
        {
            var whereStr = BuildWhereExp(sqlent, where, null);
            if (whereStr.IsNullOrEmpty())
            {
                return false;
            }
            sqlent.Sql = whereStr;
            return UpdateOrInsertBase(new List<TClass> { entity }, sqlent, false, IgnoreColumns);
        }
        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <typeparam name="TClass">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool UpdateOrInsert<TClass>(List<TClass> entities, IEnumerable<string> IgnoreColumns = null) where TClass : class, new()
        {
            sqlent?.Parameters.Clear();
            sqlent.Sql = "";
            return UpdateOrInsertBase(entities, sqlent, false, IgnoreColumns);
        }

        /// <summary>
        /// 保存数据（自动新增或更新动作）
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="fields">设置多个更新字段</param>
        /// <param name="where">条件表达式</param>
        /// <returns></returns>
        public bool UpdateOrInsert<TClass>(Expression<Func<TClass, TClass>> fields, Expression<Func<TClass, bool>> where) where TClass : class, new()
        {
            var tableInfo = GetTableInfo<TClass>();
            return UpdateOrInsertBase(fields, where, tableInfo);
        }
        #endregion
        //-------------------------------------------------------------------------------------------------------------------------------
        #region Exists
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Exists<TClass>(TClass entity) where TClass : class, new()
        {
            return base.Exists(entity);
        }
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Exists<TClass>(Expression<Func<TClass, bool>> where) where TClass : class, new()
        {
            return base.Exists(where);
        }
        #endregion
        //-------------------------------------------------------------------------------------------------------------------------------
        #region override
        /// <summary>
        /// 返回sql脚本实体
        /// </summary>
        /// <returns></returns>
        public override SqlEntity GetSql(bool isMerge = false) { Build(isMerge); return sqlent; }
        //-----------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 开始
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <returns></returns>
        protected override Tuple<DbTableInfo, string, string> From<TClass>(string tableAlias = "")
        {
            tupleTableInfo = GetTableInfo<TClass>(tableAlias);
            fromStr = $"{tupleTableInfo.Item3}";
            return tupleTableInfo;
        }
        protected override bool Build(bool isMerge = false)
        {
            if (fromStr.IsNullOrEmpty())
                return false;
            base.Build();
            //-----------------------------------------------------------------------------------------------------------------
            var whereStr = "";
            if (isMerge == false)
            {
                whereStr = "1=2";
                if (WhereList.Count > 0)
                {
                    whereStr = string.Join(" ", WhereList).Trim()
                        .ReplaceIgnoreCase("WHERE", "").TrimPrefixName("AND").TrimPrefixName("OR");
                }
            }
            //-----------------------------------------------------------------------------------------------------------------
            isBuilded = true;
            return BuildUpdateSQL(sqlent, this.dicfields, whereStr, tupleTableInfo, isMerge);
        }
        #endregion
    }
    #endregion
    #region UpdateSqlBuilder<TClass>
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TClass">实体类型</typeparam>
    internal class UpdateSqlBuilder<TClass> : UpdateSqlBuilder, IUpdateSqlBuilder<TClass> where TClass : class, new()
    {
        public UpdateSqlBuilder(IDbHelper _db) : base(_db)
        {
            From();
        }

        #region Update
        /// <summary>
        /// 更新一条数据,自动对比新老实体对象属性差异来决定更新字段
        /// </summary>
        /// <param name="newEntity">新实体</param>
        /// <param name="oldEntity">老实体</param>
        /// <returns></returns>
        public bool Update(TClass newEntity, TClass oldEntity)
        {
            return base.Update(newEntity, oldEntity);
        }

        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update(TClass entity, params string[] columns)
        {
            return base.Update(entity, columns);
        }

        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entities">数据列表</param>
        /// <param name="isForSet">控制fields</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update(List<TClass> entities, params string[] columns)
        {
            return base.Update(entities, columns);
        }

        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update(TClass entity, IEnumerable<string> IgnoreColumns) 
        {
            return base.Update(entity, IgnoreColumns);
        }
        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entities">数据列表</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update(List<TClass> entities, IEnumerable<string> IgnoreColumns) 
        {
            return base.Update(entities, IgnoreColumns);
        }
        #endregion
        #region Update where
        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update(TClass entity, Expression<Func<TClass, bool>> where, params string[] columns)
        {
            return base.Update(entity, where, columns);
        }

        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entities">数据列表</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool Update(List<TClass> entities, Expression<Func<TClass, bool>> where, params string[] columns)
        {
            return base.Update(entities, where, columns);
        }

        /// <summary>
        /// 更新一条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update(TClass entity, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns = null)
        {
            return base.Update(entity, where, IgnoreColumns);
        }

        /// <summary>
        /// 更新多条数据,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entities">数据列表</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool Update(List<TClass> entities, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns)
        {
            return base.Update(entities, where, IgnoreColumns);
        }
        #endregion
        #region Update Expression fields
        /// <summary>
        /// 更新一条数据
        /// </summary>
        /// <param name="fields">设置多个更新字段</param>
        /// <param name="where">添加条件表达式</param>
        /// <returns></returns>
        public bool Update(Expression<Func<TClass,TClass>> fields, Expression<Func<TClass, bool>> where)
        {
            return base.Update(fields, where);
        }
        #endregion
        //---------------------------------------------------------------------------------------------------------------
        #region Set Expression fields
        /// <summary>
        /// 设置多个更新字段
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public IUpdateSqlBuilder<TClass> Set(Expression<Func<TClass,TClass>> fields)
        {
            base.Set(fields);
            return this;
        }
        /// <summary>
        /// 设置单个更新字段值
        /// </summary>
        /// <typeparam name="TField"></typeparam>
        /// <param name="field"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public IUpdateSqlBuilder<TClass> Set<TValue>(Expression<Func<TClass, TValue>> field, TValue value)
        {
            base.Set(field, value);
            return this;
        }
        #endregion
        #region Where
        /// <summary>
        /// 添加条件表达式
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public IUpdateSqlBuilder<TClass> Where(Expression<Func<TClass, bool>> where, bool isAnd = true)
        {
            base.Where(where, isAnd);
            return this;
        }
        #endregion
        //---------------------------------------------------------------------------------------------------------------
        #region UpdateOrInsert
        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool UpdateOrInsert(TClass entity, Expression<Func<TClass, bool>> where, params string[] columns)
        {
            return base.UpdateOrInsert(entity, where, columns);
        }

        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entities">数据列表</param>
        /// <param name="where">条件表达式</param>
        /// <param name="columns">需要更新的字段</param>
        public bool UpdateOrInsert(List<TClass> entities, params string[] columns)
        {
            return base.UpdateOrInsert(entities,columns);
        }

        /// <summary>
        /// 保存数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="where">条件表达式</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool UpdateOrInsert(TClass entity, Expression<Func<TClass, bool>> where, IEnumerable<string> IgnoreColumns)
        {
            return base.UpdateOrInsert(entity, where, IgnoreColumns);
        }
        /// <summary>
        /// 保存数据多条数据（自动新增或更新动作）,某些属性,默认更新实体列表，所有字段
        /// </summary>
        /// <param name="entities">数据列表</param>
        /// <param name="IgnoreColumns">不需要更新的字段</param>
        public bool UpdateOrInsert(List<TClass> entities, IEnumerable<string> IgnoreColumns = null) 
        {
            return base.UpdateOrInsert(entities, IgnoreColumns);
        }

        /// <summary>
        /// 保存数据（自动新增或更新动作）
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="fields">设置多个更新字段</param>
        /// <param name="where">条件表达式</param>
        /// <returns></returns>
        public bool UpdateOrInsert(Expression<Func<TClass,TClass>> fields, Expression<Func<TClass, bool>> where)
        {
            return base.UpdateOrInsert(fields, where);
        }
        #endregion
        //-------------------------------------------------------------------------------------------------------------------------------
        #region Exists
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <returns></returns>
        public bool Exists(TClass entity) 
        {
            return base.Exists(entity);
        }
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Exists(Expression<Func<TClass, bool>> where)
        {
            return base.Exists(where);
        }
        #endregion
        //-------------------------------------------------------------------------------------------------------------------------------
        #region override
        /// <summary>
        /// 返回sql脚本实体
        /// </summary>
        /// <returns></returns>
        public override SqlEntity GetSql(bool isMerge = false) { Build(isMerge); return sqlent; }
        //-----------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 开始
        /// </summary>
        /// <returns></returns>
        protected virtual Tuple<DbTableInfo, string, string> From()
        {
            tupleTableInfo = GetTableInfo<TClass>();
            fromStr = $"{tupleTableInfo.Item3}";
            return tupleTableInfo;
        }
        protected override bool Build(bool isMerge = false)
        {
            if (fromStr.IsNullOrEmpty())
                return false;
            base.Build();
            //-----------------------------------------------------------------------------------------------------------------
            var whereStr = "";
            if (isMerge == false)
            {
                whereStr = "1=2";
                if (WhereList.Count > 0)
                {
                    whereStr = string.Join(" ", WhereList).Trim()
                        .ReplaceIgnoreCase("WHERE", "").TrimPrefixName("AND").TrimPrefixName("OR");
                }
            }
            //-----------------------------------------------------------------------------------------------------------------
            isBuilded = true;
            return BuildUpdateSQL(sqlent, this.dicfields, whereStr, tupleTableInfo, isMerge);
        }
        #endregion
    }
    #endregion

}
