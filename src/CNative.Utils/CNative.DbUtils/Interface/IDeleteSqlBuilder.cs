﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CNative.DbUtils
{
    #region IDeleteSqlBuilder
    public partial interface IDeleteSqlBuilder : IExecuteSql
    {
        /// <summary>
        /// 添加条件表达式
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        IDeleteSqlBuilder Where<TClass>(Expression<Func<TClass, bool>> where, bool isAnd = true) where TClass : class, new();

        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        IDeleteSqlBuilder Delete<TClass>(TClass entity) where TClass : class, new();

        /// <summary>
        /// 删除多条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        IDeleteSqlBuilder Delete<TClass>(List<TClass> entities) where TClass : class, new();

        /// <summary>
        /// 通过条件表达式删除
        /// </summary> 
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        bool Delete<TClass>(Expression<Func<TClass, bool>> where) where TClass : class, new();
        //---------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists<TClass>(TClass entity) where TClass : class, new();
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists<TClass>(Expression<Func<TClass, bool>> where) where TClass : class, new();
    }
    #endregion
    #region IDeleteSqlBuilder<TClass>
    public partial interface IDeleteSqlBuilder<TClass> : IExecuteSql where TClass : class, new()
    {
        /// <summary>
        /// 添加条件表达式
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        IDeleteSqlBuilder<TClass> Where(Expression<Func<TClass, bool>> where, bool isAnd = true);

        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entity">实体对象</param>
        IDeleteSqlBuilder<TClass> Delete(TClass entity);

        /// <summary>
        /// 删除多条数据
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="entities">数据列表</param>
        IDeleteSqlBuilder<TClass> Delete(List<TClass> entities);
        /// <summary>
        /// 通过条件表达式删除
        /// </summary> 
        /// <typeparam name="TClass"></typeparam>
        /// <param name="where"></param>
        bool Delete(Expression<Func<TClass, bool>> where);
        //---------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists(TClass entity);
        /// <summary>
        /// 通过主键，判断实体是否存在表中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool Exists(Expression<Func<TClass, bool>> where);

    }
    #endregion
}
