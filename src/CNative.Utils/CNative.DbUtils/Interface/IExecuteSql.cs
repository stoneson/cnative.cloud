﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CNative.DbUtils
{
    public partial interface IExecuteSql : IDisposable
    {
        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        bool Execute();
#if !NET40
        /// <summary>
        /// 异步执行脚本
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        Task<bool> ExecuteAsync();
#endif
        /// <summary>
        /// 执行脚本,等同于Execute()
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        bool Exec { get; }
        //---------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 返回sql脚本实体
        /// </summary>
        /// <returns></returns>
        SqlEntity GetSql(bool isMerge = false);

        /// <summary>
        /// 返回sql脚本实体集合
        /// </summary>
        /// <returns></returns>
        List<SqlEntity> GetSqls();
    }
}
