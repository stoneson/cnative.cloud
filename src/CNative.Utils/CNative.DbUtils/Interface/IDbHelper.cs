﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CNative.DbUtils
{
    public interface IDbHelper
    {
        /// <summary>
        /// 当前数据库连接字符串
        /// </summary>
        string ConnectString { get; set; }
        /// <summary>
        /// 是否启用主从分离模式
        /// </summary>
        bool IsUseMasterSlaveSeparation { get; set; }
        /// <summary>
        /// 主数据库连接字符串
        /// </summary>
        string MasterConnectString { get; set; }
        /// <summary>
        /// 从数据库连接字符串集合
        /// </summary>
        List<string> SlaveConnectStrings { set; get; }
        int? CommandTimeout { get; set; }
        /// <summary>
        /// 数据库类型
        /// </summary>
        CNative.DbUtils.DatabaseType DBType { get; }
        /// <summary>
        /// 数据库提供者
        /// </summary>
        BaseProvider SqlDbProvider { get; }

        /// <summary>
        /// 执行脚本
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        bool Execute(SqlEntity sql);
        /// <summary>
        /// 批量执行脚本
        /// </summary>
        /// <param name="sqlList"></param>
        /// <returns></returns>
        bool Execute(List<SqlEntity> sqlList);
        /// <summary>
        /// 查寻返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        DataSet QueryDataSet(SqlEntity sql);
        /// <summary>
        /// 查寻返回DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        DataTable QueryDataTable(SqlEntity sql);
        /// <summary>
        /// 查寻返回实体集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        List<T> Query<T>(SqlEntity sql) where T : class;
        /// <summary>
        /// 查寻返回单个字段值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        T GetSingle<T>(SqlEntity sql);
        /// <summary>
        /// 查寻返回单行实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        T GetSingleRow<T>(SqlEntity sql) where T : class;
        /// <summary>
        /// 执行脚本无返回值
        /// </summary>
        /// <param name="sql"></param>
        void ExecuteOneWay(SqlEntity sql);

        /// <summary>
        /// 开始单库事物
        /// </summary>
        /// </summary>
        /// <param name="isol"></param>
        /// <returns></returns>
        System.Data.IDbTransaction BeginTransaction(System.Data.IsolationLevel isol = IsolationLevel.ReadCommitted);
        /// <summary>
        /// 结束事物提交
        /// </summary>
        void Commit();
        /// <summary>
        /// 创建脚本对象
        /// </summary>
        /// <returns></returns>
        SqlEntity CreateSqlEntity();

        /// <summary>
        /// 判断当前连接字符串是否有效
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="isThrow"></param>
        /// <returns></returns>
        bool Ping(string connectionString = "", bool isThrow = false);
    }
}
