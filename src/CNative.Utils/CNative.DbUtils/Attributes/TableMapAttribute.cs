﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNative.DbUtils
{
    /// <summary>
    /// 指定类将映射到的数据库表
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TableMapAttribute : Attribute
    {
        /// <summary>
        /// 指定类将映射到的数据库表
        /// </summary>
        public TableMapAttribute()
        {
        }
        /// <summary>
        /// 表的类名称
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 类映射到的表的架构,对应数据库名
        /// </summary>
        public string Schema { get; set; }
    }
}
