﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNative.DbUtils
{
    /// <summary>
    /// 表示一个属性或字段的映射特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class DbFieldMapAttribute : Attribute
    {
        public DbFieldMapAttribute()
        {
        }
        /// <summary>
        /// 字段名
        /// </summary>
        public string CloumnName { get; set; }

        /// <summary>
        /// 字段类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 字段长度
        /// </summary>
        public int MaxLength { get; set; }

        /// <summary>
        /// 是否为主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// 是否为自增类型
        /// </summary>
        public bool IsIdentity { get; set; } = false;

        /// <summary>
        /// 是否为空
        /// </summary>
        public bool IsNullable { get; set; }

        /// <summary>
        /// 字段描述说明
        /// </summary>
        public string Description
        {
            get
            {
                return _description.IsNullOrEmpty() ? CloumnName : _description;
            }
            set
            {
                _description = value;
            }
        }

        private string _description { get; set; }
    }
}
