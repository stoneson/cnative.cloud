﻿using System;
using System.Configuration;

namespace CNative.DbUtils
{
    public class DbFactory
    {
        public static ISqlBuilder CreateSqlBuilder(string dbName)
        {
            return new SqlBuilder(dbName);
        }

        public static IDbHelper CreateDb(string dbName)
        {
            return new DbHelper(dbName);
        }
#if !NET40
        public static MongoDbHelper CreateMongoDb(string mongoConnStr, string dbName, bool autoCreateDb = false, bool autoCreateCollection = false)
        {
            return new MongoDbHelper(mongoConnStr, dbName, autoCreateDb, autoCreateCollection);
        }
        public static MongoDbHelper CreateMongoDb(string DBname, bool autoCreateDb = false, bool autoCreateCollection = false)
        {
            string mongoConnStr = string.Empty;
            //#if NETSTANDARD2_0
            if (CNative.Utilities.ConfigurationHelper.ConnectionStrings == null || CNative.Utilities.ConfigurationHelper.ConnectionStrings.Count == 0)
                throw new Exception("appsettings.json文件中未找到[ConnectionStrings]配置节点");
            if (!CNative.Utilities.ConfigurationHelper.ConnectionStrings.ContainsKey(DBname))
                throw new Exception("未找到名称为[" + DBname + "]的连接字符串");
            var configSettings = CNative.Utilities.ConfigurationHelper.ConnectionStrings[DBname];
            if (configSettings == null)
                throw new Exception("未找到名称为[" + DBname + "]的连接字符串");
            if (configSettings != null)
            {
                mongoConnStr = configSettings.ConnectionString;
            }
            else
            {
                throw new Exception("未找到名称为" + DBname + "的数据库配置节点");
            }
            return new MongoDbHelper(mongoConnStr, DBname, autoCreateDb, autoCreateCollection);
        }
#endif
    }

}
