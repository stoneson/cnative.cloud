﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using CNative.Utilities;

namespace CNative.DbUtils
{
    /// <summary>
    /// CNative DbParameter
    /// </summary>
    public class CNDbParameter : DbParameter
    {
        public bool IsRefCursor { get; set; }
        public CNDbParameter(string name, object value)
        {
            this.Value = value;
            this.ParameterName = name;
            if (value != null)
            {
                SettingDataType(value.GetType());
            }
        }
        public CNDbParameter(string name, object value, Type type)
        {
            this.Value = value;
            this.ParameterName = name;
            SettingDataType(type);
        }
        public CNDbParameter(string name, object value, Type type, ParameterDirection direction)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            SettingDataType(type);
        }
        public CNDbParameter(string name, object value, Type type, ParameterDirection direction, int size)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            this.Size = size;
            SettingDataType(type);
        }


        public CNDbParameter(string name, object value, System.Data.DbType type)
        {
            this.Value = value;
            this.ParameterName = name;
            this.DbType = type;
        }
        public CNDbParameter(string name, DataTable value, string SqlServerTypeName)
        {
            this.Value = value;
            this.ParameterName = name;
            this.TypeName = SqlServerTypeName;
        }
        public CNDbParameter(string name, object value, System.Data.DbType type, ParameterDirection direction)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            this.DbType = type;
        }
        public CNDbParameter(string name, object value, System.Data.DbType type, ParameterDirection direction, int size)
        {
            this.Value = value;
            this.ParameterName = name;
            this.Direction = direction;
            this.Size = size;
            this.DbType = type;
        }

        private void SettingDataType(Type type)
        {
            if (type == Constants.ByteArrayType)
            {
                this.DbType = System.Data.DbType.Binary;
            }
            else if (type == Constants.GuidType)
            {
                this.DbType = System.Data.DbType.Guid;
            }
            else if (type == Constants.IntType)
            {
                this.DbType = System.Data.DbType.Int32;
            }
            else if (type == Constants.ShortType)
            {
                this.DbType = System.Data.DbType.Int16;
            }
            else if (type == Constants.LongType)
            {
                this.DbType = System.Data.DbType.Int64;
            }
            else if (type == Constants.DateType)
            {
                this.DbType = System.Data.DbType.DateTime;
            }
            else if (type == Constants.DobType)
            {
                this.DbType = System.Data.DbType.Double;
            }
            else if (type == Constants.DecType)
            {
                this.DbType = System.Data.DbType.Decimal;
            }
            else if (type == Constants.ByteType)
            {
                this.DbType = System.Data.DbType.Byte;
            }
            else if (type == Constants.FloatType)
            {
                this.DbType = System.Data.DbType.Single;
            }
            else if (type == Constants.BoolType)
            {
                this.DbType = System.Data.DbType.Boolean;
            }
            else if (type == Constants.StringType)
            {
                this.DbType = System.Data.DbType.String;
            }
            else if (type == Constants.DateTimeOffsetType)
            {
                this.DbType = System.Data.DbType.DateTimeOffset;
            }
            else if (type == Constants.TimeSpanType)
            {
                if (this.Value != null)
                    this.Value = this.Value.ToString();
            }
            else if (type != null && type.IsEnum)
            {
                this.DbType = System.Data.DbType.Int64;
            }

        }
        public CNDbParameter(string name, object value, bool isOutput)
        {
            this.Value = value;
            this.ParameterName = name;
            if (isOutput)
            {
                this.Direction = ParameterDirection.Output;
            }
        }
        public override System.Data.DbType DbType
        {
            get; set;
        }

        public override ParameterDirection Direction
        {
            get; set;
        }

        public override bool IsNullable
        {
            get; set;
        }

        public override string ParameterName
        {
            get; set;
        }

        public int _Size;

        public override int Size
        {
            get
            {
                if (_Size == 0 && Value != null)
                {
                    var isByteArray = Value.GetType() == Constants.ByteArrayType;
                    if (isByteArray)
                        _Size = -1;
                    else
                    {
                        var length = Value.ToString().Length;
                        _Size = length < 4000 ? 4000 : -1;

                    }
                }
                if (_Size == 0)
                    _Size = 4000;
                return _Size;
            }
            set
            {
                _Size = value;
            }
        }

        public override string SourceColumn
        {
            get; set;
        }

        public override bool SourceColumnNullMapping
        {
            get; set;
        }
        public string UdtTypeName
        {
            get;
            set;
        }

        public override object Value
        {
            get; set;
        }

        public Dictionary<string, object> TempDate
        {
            get; set;
        }

        /// <summary>
        /// 如果类库是.NET 4.5请删除该属性
        /// If the SqlSugar library is.NET 4.5, delete the property
        /// </summary>
        public override DataRowVersion SourceVersion
        {
            get; set;
        }

        public override void ResetDbType()
        {
            this.DbType = System.Data.DbType.String;
        }


        public string TypeName { get; set; }
        public bool IsJson { get; set; }
        public bool IsArray { get; set; }
    }
}
