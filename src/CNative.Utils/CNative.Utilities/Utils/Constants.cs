﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace CNative.Utilities
{
  public  static class Constants
    {
        public const string Dot = ".";
        public const char DotChar = '.';
        public const string Space = " ";
        public const char SpaceChar = ' ';

        /// <summary>
        /// typeof(int)
        /// </summary>
        public static Type IntType = typeof(int);
        /// <summary>
        /// typeof(long)
        /// </summary>
        public static Type LongType = typeof(long);
        /// <summary>
        /// typeof(Guid)
        /// </summary>
        public static Type GuidType = typeof(Guid);
        /// <summary>
        /// typeof(bool)
        /// </summary>
        public static Type BoolType = typeof(bool);
        /// <summary>
        /// typeof(bool?)
        /// </summary>
        public static Type BoolTypeNull = typeof(bool?);
        /// <summary>
        /// typeof(Byte)
        /// </summary>
        public static Type ByteType = typeof(Byte);
        /// <summary>
        /// typeof(object)
        /// </summary>
        public static Type ObjType = typeof(object);
        /// <summary>
        /// typeof(double)
        /// </summary>
        public static Type DobType = typeof(double);
        public static Type FloatType = typeof(float);
        public static Type ShortType = typeof(short);
        /// <summary>
        /// typeof(decimal)
        /// </summary>
        public static Type DecType = typeof(decimal);
        public static Type StringType = typeof(string);
        /// <summary>
        /// typeof(DateTime)
        /// </summary>
        public static Type DateType = typeof(DateTime);
        /// <summary>
        /// typeof(DateTimeOffset)
        /// </summary>
        public static Type DateTimeOffsetType = typeof(DateTimeOffset);
        /// <summary>
        ///  typeof(TimeSpan)
        /// </summary>
        public static Type TimeSpanType = typeof(TimeSpan);
        /// <summary>
        ///  typeof(byte[])
        /// </summary>
        public static Type ByteArrayType = typeof(byte[]);
        /// <summary>
        /// typeof(ExpandoObject)
        /// </summary>
        public static Type DynamicType = typeof(ExpandoObject);
        /// <summary>
        /// typeof(KeyValuePair<int, int>)
        /// </summary>
        public static Type Dicii = typeof(KeyValuePair<int, int>);
        /// <summary>
        /// typeof(KeyValuePair<int, string>)
        /// </summary>
        public static Type DicIS = typeof(KeyValuePair<int, string>);
        /// <summary>
        /// typeof(KeyValuePair<string, int>)
        /// </summary>
        public static Type DicSi = typeof(KeyValuePair<string, int>);
        /// <summary>
        /// typeof(KeyValuePair<string, string>)
        /// </summary>
        public static Type DicSS = typeof(KeyValuePair<string, string>);
        /// <summary>
        /// typeof(KeyValuePair<object, object>)
        /// </summary>
        public static Type DicOO = typeof(KeyValuePair<object, object>);
        /// <summary>
        ///  typeof(KeyValuePair<string, object>)
        /// </summary>
        public static Type DicSo = typeof(KeyValuePair<string, object>);
        /// <summary>
        ///  typeof(Dictionary<string, string>)
        /// </summary>
        public static Type DicArraySS = typeof(Dictionary<string, string>);
        /// <summary>
        /// typeof(Dictionary<string, object>)
        /// </summary>
        public static Type DicArraySO = typeof(Dictionary<string, object>);

        /// <summary>
        /// 数字类型集合
        /// </summary>
        public static Type[] NumericalTypes = new Type[]
        {
            typeof(uint),
            typeof(sbyte),
            typeof(ulong),
            typeof(ushort),
            ByteType,
            ShortType,
            IntType,
            LongType,
            DecType,
            DobType,
            FloatType,
        };
    }
}
