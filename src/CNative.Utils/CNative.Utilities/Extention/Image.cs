﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace CNative
{
    public static partial class Extention
    {
        /// <summary>
        /// 将Image转换成Base64 
        /// </summary>
        /// <param name="imgPhoto"></param>
        /// <returns></returns>
        public static string Image2Base64(this System.Drawing.Image imgPhoto)
        {
            return Convert.ToBase64String(imgPhoto.ToBytes());
        }

        /// <summary>
        /// Base64转Image，返回值是Image对象
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public static System.Drawing.Image Base642Image(string base64String)
        {
            return ToImage(Convert.FromBase64String(base64String));
        }

        /// <summary>
        /// 将Image转换成流数据byte[] 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this System.Drawing.Image image)
        {
            ImageFormat format = image.RawFormat;
            using (MemoryStream ms = new MemoryStream())
            {
                if (format.Equals(ImageFormat.Jpeg))
                {
                    image.Save(ms, ImageFormat.Jpeg);
                }
                else if (format.Equals(ImageFormat.Png))
                {
                    image.Save(ms, ImageFormat.Png);
                }
                else if (format.Equals(ImageFormat.Bmp))
                {
                    image.Save(ms, ImageFormat.Bmp);
                }
                else if (format.Equals(ImageFormat.Gif))
                {
                    image.Save(ms, ImageFormat.Gif);
                }
                else if (format.Equals(ImageFormat.Icon))
                {
                    image.Save(ms, ImageFormat.Icon);
                }
                else
                {
                    image.Save(ms, ImageFormat.Bmp);
                }

                byte[] buffer = new byte[ms.Length];
                //Image.Save()会改变MemoryStream的Position，需要重新Seek到Begin
                ms.Seek(0, SeekOrigin.Begin);
                ms.Read(buffer, 0, buffer.Length);
                return buffer;
            }
        }

        /// <summary>
        /// Byte[]类型转Image，返回值是Image对象
        /// </summary> 
        /// <param name="streamByte"></param>
        /// <returns></returns>
        public static System.Drawing.Image ToImage(this byte[] streamByte)
        {
            var ms = new System.IO.MemoryStream(streamByte);
            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            return img;
        }

        /// <summary>
        /// 按照指定大小缩放图片
        /// </summary>
        /// <param name="srcImage">图片</param>
        /// <param name="iWidth">目标宽度</param>
        /// <param name="iHeight">目标长度</param>
        /// <returns></returns>
        public static Image GetThumbnail(this Image srcImage, int iWidth, int iHeight)
        {
            try
            {
                // 要保存到的图片
                Bitmap b = new Bitmap(iWidth, iHeight);
                Graphics g = Graphics.FromImage(b);
                // 插值算法的质量
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.DrawImage(srcImage, new Rectangle(0, 0, iWidth, iHeight), new Rectangle(0, 0, srcImage.Width, srcImage.Height), GraphicsUnit.Pixel);
                g.Dispose();
                return b;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}