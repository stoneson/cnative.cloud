﻿using System;

namespace CNative
{
    public static partial class Utils
    {
        /// <summary>
        /// jsGetTime转为DateTime
        /// </summary>
        /// <param name="jsGetTime">js中Date.getTime()</param>
        /// <returns></returns>
        public static DateTime ToDateTime_From_JsGetTime(this long jsGetTime)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(jsGetTime + "0000");  //说明下，时间格式为13位后面补加4个"0"，如果时间格式为10位则后面补加7个"0",至于为什么我也不太清楚，也是仿照人家写的代码转换的
            TimeSpan toNow = new TimeSpan(lTime);
            DateTime dtResult = dtStart.Add(toNow); //得到转换后的时间

            return dtResult;
        }

        /// <summary>
        /// 将数字转为对应的字节数组
        /// </summary>
        /// <param name="num">数字</param>
        /// <returns></returns>
        public static byte[] ToBytes(this int num)
        {
            return BitConverter.GetBytes(num);
        }

        /// <summary>
        ///   将一个或多个枚举常数的名称或数字值的字符串表示转换成等效的枚举对象。
        /// </summary>
        /// <param name="value">要转换的枚举名称或基础值的字符串表示形式。</param>
        /// <typeparam name="TEnum">
        ///   要将 <paramref name="value" /> 转换到的枚举类型。
        /// </typeparam>
        /// <returns>
        ///   当此方法返回时，如果分析操作成功，<paramref name="value" /> 将包含值由 <paramref name="value" /> 表示的 <paramref name="TEnum" /> 类型的对象。
        ///    如果分析操作失败，<paramref name="value" /> 包括 <paramref name="TEnum" /> 的基础类型的默认值。
        ///    请注意，此值无需为 <paramref name="TEnum" /> 枚举的成员。
        ///    此参数未经初始化即被传递。
        /// </returns>
        public static TEnum ToEnum<TEnum>(this int value) where TEnum : struct
        {
            return value.ToString().ToEnum<TEnum>();
        }
    }
}
