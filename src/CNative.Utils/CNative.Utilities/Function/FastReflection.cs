﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CNative.Utilities
{
	public class Dict<TKey, TValue> : System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue>
	{
		/// <summary>初始化一个新 <see cref="Dict{TKey, TValue}"/> 实例。</summary>
		public Dict() : base()
		{
		}
		/// <summary>初始化一个新 <see cref="Dict{TKey, TValue}"/> 实例。</summary>
		/// <param name="capacity">可包含的初始元素数</param>
		public Dict(int capacity) : base(1, capacity)
		{
		}

		/// <summary>初始化一个新 <see cref="Dict{TKey, TValue}"/> 实例。</summary>
		/// <param name="comparer">比较键时要使用的比较器</param>
		public Dict(IEqualityComparer<TKey> comparer) : base(comparer)
		{
		}

		/// <summary>初始化一个新 <see cref="Dict{TKey, TValue}"/> 实例。</summary>
		/// <param name="capacity">可包含的初始元素数</param>
		/// <param name="comparer">比较键时要使用的比较器</param>
		public Dict(int capacity, IEqualityComparer<TKey> comparer) : base(1, capacity, comparer)
		{
		}
		/// <summary>获取值</summary>
		/// <param name="key">键名</param>
		/// <param name="func">用于在值不存在时添加值的委托</param>
		/// <returns>值</returns>
		public TValue Get(TKey key, Func<TValue> func)
		{
			TValue val;
			if (base.TryGetValue(key, out val))
				return val;

			val = func();
			if (!val.IsNullOrEmpty_<TValue>())
				base[key] = val;
			return val;
		}

		/// <summary>移除指定键的值</summary>
		/// <param name="key">要移除的键</param>
		/// <returns>返回是否移除成功</returns>
		public bool Del(TKey key)
		{
			return base.TryRemove(key, out TValue val);
		}

	}
	/// <summary>访问器类型</summary>
	public enum AccessorType
	{
		/// <summary>属性</summary>
		Property,
		/// <summary>字段</summary>
		Field
	}
	/// <summary>字段/属性访问器</summary>
	public class Accessor
	{
		/// <summary>访问器名称</summary>
		public string Name;
		/// <summary>成员信息</summary>
		public MemberInfo Member;
		/// <summary>访问器数据类型</summary>
		public Type DataType;
		/// <summary>访问器类型</summary>
		public AccessorType AccessorType;
		/// <summary>是否为可读属性</summary>
		public bool CanRade;
		/// <summary>是否为可写属性</summary>
		public bool CanWrite;
		/// <summary>是否为虚属性</summary>
		public bool IsVirtual;

		/// <summary>设置访问器的值</summary>
		/// <param name="instance">要设置的对象实例</param>
		/// <param name="value">要设置的值</param>
		public void SetValue(object instance, object value)
		{
			switch (this.AccessorType)
			{
				case AccessorType.Property:
					((PropertyInfo)this.Member).FastSetValue(instance, value);
					break;
				case AccessorType.Field:
					((FieldInfo)this.Member).FastSetValue(instance, value);
					break;
			}
		}

		/// <summary>获取访问器的值</summary>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>访问器的值</returns>
		public object GetValue(object instance)
		{
			object val = null;
			switch (this.AccessorType)
			{
				case AccessorType.Property:
					val = ((PropertyInfo)this.Member).FastGetValue(instance);
					break;
				case AccessorType.Field:
					val = ((FieldInfo)this.Member).FastGetValue(instance);
					break;
			}
			return val;
		}

		/// <summary>获取访问器的值</summary>
		/// <typeparam name="T">值的数据类型</typeparam>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>访问器的值</returns>
		public T GetValue<T>(object instance)
		{
			return (T)this.GetValue(instance);
		}
	}
	/// <summary>
	/// 快速反射类
	/// </summary>
	public static class FastReflection
	{
		#region 创建实例
		private static Dict<ConstructorInfo, Func<object[], object>> OC;

		/// <summary>通过类的构造器信息创建对象实例</summary>
		/// <param name="constructorInfo">构造器信息</param>
		/// <param name="parameters">参数</param>
		/// <returns>新对象实例</returns>
		public static object FastInstance(this ConstructorInfo constructorInfo, params object[] parameters)
		{
			if (OC == null) OC = new Dict<ConstructorInfo, Func<object[], object>>();

			var exec = OC.Get(constructorInfo, () =>
			{
				var parametersParameter = Expression.Parameter(typeof(object[]), "parameters");
				var parameterExpressions = new List<Expression>();
				var paramInfos = constructorInfo.GetParameters();

				for (int i = 0, l = paramInfos.Length; i < l; i++)
				{
					var valueObj = Expression.ArrayIndex(parametersParameter, Expression.Constant(i));
					var valueCast = Expression.Convert(valueObj, paramInfos[i].ParameterType);

					parameterExpressions.Add(valueCast);
				}

				var instanceCreate = Expression.New(constructorInfo, parameterExpressions);
				var instanceCreateCast = Expression.Convert(instanceCreate, typeof(object));
				var lambda = Expression.Lambda<Func<object[], object>>(instanceCreateCast, parametersParameter);

				return lambda.Compile();
			});
			return exec(parameters);
		}

		/// <summary>通过类的构造器信息创建对象实例</summary>
		/// <typeparam name="T">返回对象类型</typeparam>
		/// <param name="constructorInfo">构造器信息</param>
		/// <param name="parameters">参数</param>
		/// <returns>新对象实例</returns>
		public static T FastInstance<T>(this ConstructorInfo constructorInfo, params object[] parameters)
		{
			return (T)constructorInfo.FastInstance(parameters);
		}

		/// <summary>通过对象类型创建对象实例</summary>
		/// <param name="type">对象类型</param>
		/// <param name="parameters">参数</param>
		/// <returns>新对象实例</returns>
		public static object FastInstance(this Type type, params object[] parameters)
		{
			//var paramLen = parameters.Length;
			//var types = new Type[paramLen];
			//for(int i = 0; i < paramLen; i++)
			//	types[i] = parameters[i].GetType();
			var consInfo = GetConstructor(type, parameters);// type.GetConstructor(types);
			return consInfo.FastInstance(parameters);
		}

		/// <summary>通过对象类型创建对象实例</summary>
		/// <typeparam name="T">返回对象类型</typeparam>
		/// <param name="type">对象类型</param>
		/// <param name="parameters">参数</param>
		/// <returns>新对象实例</returns>
		public static T FastInstance<T>(this Type type, params object[] parameters)
		{
			return (T)type.FastInstance(parameters);
		}

		/// <summary>创建对象实例</summary>
		/// <typeparam name="T">返回对象类型</typeparam>
		/// <param name="parameters">参数</param>
		/// <returns>新对象实例</returns>
		public static T FastInstance<T>(params object[] parameters)
		{
			return FastInstance<T>(typeof(T), parameters);
		}

		/// <summary>通过对象类型名称创建对象实例</summary>
		/// <param name="typeName">对象类型名称，格式为“类型名, 程序集名”，如：NetRube.Plugin.Mail, NetRube.Plugin</param>
		/// <param name="parameters">参数</param>
		/// <returns>新对象实例</returns>
		public static object FastInstance(this string typeName, params object[] parameters)
		{
			return FastInstance(FastGetType(typeName), parameters);
		}

		/// <summary>通过对象类型名称创建对象实例</summary>
		/// <typeparam name="T">返回对象类型</typeparam>
		/// <param name="typeName">对象类型名称，格式为“类型名, 程序集名”，如：NetRube.Plugin.Mail, NetRube.Plugin</param>
		/// <param name="parameters">参数</param>
		/// <returns>新对象实例</returns>
		public static T FastInstance<T>(string typeName, params object[] parameters)
		{
			return (T)FastInstance(typeName, parameters);
		}
		#endregion

		#region 方法
		private static Dict<MethodInfo, Func<object, object[], object>> MC;

		/// <summary>快速调用对象实例的方法</summary>
		/// <param name="methodInfo">要调用的方法</param>
		/// <param name="instance">对象实例</param>
		/// <param name="parameters">参数</param>
		/// <returns>方法执行的结果</returns>
		public static object FastInvoke(this MethodInfo methodInfo, object instance, params object[] parameters)
		{
			if (methodInfo == null) return null;

			if (MC == null) MC = new Dict<MethodInfo, Func<object, object[], object>>();
			var exec = MC.Get(methodInfo, () =>
			{
				var objectType = typeof(object);
				var objectsType = typeof(object[]);

				var instanceParameter = Expression.Parameter(objectType, "instance");
				var parametersParameter = Expression.Parameter(objectsType, "parameters");

				var parameterExpressions = new List<Expression>();
				var paramInfos = methodInfo.GetParameters();
				for (int i = 0, l = paramInfos.Length; i < l; i++)
				{
					var valueObj = Expression.ArrayIndex(parametersParameter, Expression.Constant(i));
					var valueCast = Expression.Convert(valueObj, paramInfos[i].ParameterType);

					parameterExpressions.Add(valueCast);
				}

				var instanceCast = methodInfo.IsStatic ? null : Expression.Convert(instanceParameter, methodInfo.ReflectedType);
				var methodCall = Expression.Call(instanceCast, methodInfo, parameterExpressions);

				if (methodCall.Type == typeof(void))
				{
					var lambda = Expression.Lambda<Action<object, object[]>>(methodCall, instanceParameter, parametersParameter);

					var act = lambda.Compile();
					return (inst, para) =>
					{
						act(inst, para);
						return null;
					};
				}
				else
				{
					var castMethodCall = Expression.Convert(methodCall, objectType);
					var lambda = Expression.Lambda<Func<object, object[], object>>(
						castMethodCall, instanceParameter, parametersParameter);

					return lambda.Compile();
				}
			});
			return exec(instance, parameters);
		}

		/// <summary>通过方法名称快速调用对象实例的方法</summary>
		/// <param name="methodName">方法名称</param>
		/// <param name="instance">对象实例</param>
		/// <param name="parameters">参数</param>
		/// <returns>方法执行的结果</returns>
		public static object FastInvoke(this object instance, string methodName, params object[] parameters)
		{
			if (instance == null)
				throw new Exception("执行对象 instance 不能为NULL");
			if (methodName.IsNullOrEmpty_()) return null;
			//int paramsLen = parameters.Length;
			//var typeArray = new Type[paramsLen];
			//for(int i = 0; i < paramsLen; i++)
			//	typeArray[i] = parameters[i].GetType();
			//var method = instance.GetType().GetMethod(methodName, typeArray);
			var method = GetMethodInfo(instance.GetType(), methodName, parameters);

			var parameterinfos = method?.GetParameters();
			if (parameterinfos?.Length == 1 && parameterinfos[0].ParameterType == typeof(object[]))
				parameters = new object[] { parameters };

			return method?.FastInvoke(instance, parameters);
		}
		/// <summary>
		/// 反射对象类型,执行对象内部方法
		/// </summary>
		/// <param name="type">对象类型</param>
		/// <param name="methodName">方法名</param>
		/// <param name="paramters">参数</param>
		/// <returns>object</returns>
		public static object FastInvoke(this Type type, string methodName, params object[] paramters)
		{
			if (type == null)
				throw new Exception("对象类型 type 不能为NULL");
			var target = FastInstance(type);//Activator.CreateInstance(type);
			return FastInvoke(target, methodName, paramters);
		}
		/// <summary>
		/// 反射对象类型,执行对象内部方法
		/// </summary>
		/// <param name="type">对象类型</param>
		/// <param name="methodName">方法名</param>
		/// <param name="paramters">参数</param>
		/// <returns>object</returns>
		public static object FastInvoke(this string typeName, string methodName, params object[] paramters)
		{
			if (typeName.IsNullOrEmpty())
				throw new Exception("对象类型 type 不能为NULL");
			var target = FastInstance(typeName);// Activator.CreateInstance(type);
			return FastInvoke(target, methodName, paramters);
		}
		/// <summary>
		/// 反射对象类型,执行对象内部方法
		/// </summary>
		/// <param name="type">对象类型</param>
		/// <param name="methodName">方法名</param>
		/// <param name="paramters">参数</param>
		/// <returns>object</returns>
		public static object FastInvoke(this MethodInfo methodInfo, Type type, params object[] paramters)
		{
			if (type == null)
				throw new Exception("对象类型 type 不能为NULL");
			var target = FastInstance(type);//Activator.CreateInstance(type);
			return FastInvoke(methodInfo, target, paramters);
		}
		#endregion

		#region 属性
		#region 获取
		private static Dict<PropertyInfo, Func<object, object>> PGC;

		/// <summary>快速获取对象属性值</summary>
		/// <param name="propertyInfo">要获取的对象属性</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static object FastGetValue(this PropertyInfo propertyInfo, object instance)
		{
			if (propertyInfo == null) return null;
			if (!propertyInfo.CanRead) return null;

			if (PGC == null) PGC = new Dict<PropertyInfo, Func<object, object>>();
			var exec = PGC.Get(propertyInfo, () =>
			{
				if (propertyInfo.GetIndexParameters().Length > 0)
				{
					// 对索引属性直接返回默认值
					return (inst) =>
					{
						return null;
					};
				}
				else
				{
					var objType = typeof(object);
					var instanceParameter = Expression.Parameter(objType, "instance");
					var instanceCast = propertyInfo.GetGetMethod(true).IsStatic ? null : Expression.Convert(instanceParameter, propertyInfo.ReflectedType);
					var propertyAccess = Expression.Property(instanceCast, propertyInfo);
					var castPropertyValue = Expression.Convert(propertyAccess, objType);
					var lambda = Expression.Lambda<Func<object, object>>(castPropertyValue, instanceParameter);

					return lambda.Compile();
				}
			});
			return exec(instance);
		}

		/// <summary>通过属性名称快速获取对象属性值</summary>
		/// <param name="propertyName">要获取的对象属性名称</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static object FastGetPropertyValue(string propertyName, object instance)
		{
			if (propertyName.IsNullOrEmpty_()) return null;
			var propertyInfo = instance.GetType().GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
			if (propertyInfo != null)
				return propertyInfo.FastGetValue(instance);
			return null;
		}

		/// <summary>快速获取对象属性值</summary>
		/// <typeparam name="T">返回的数据类型</typeparam>
		/// <param name="propertyInfo">要获取的对象属性</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static T FastGetValue<T>(this PropertyInfo propertyInfo, object instance)
		{
			return (T)propertyInfo.FastGetValue(instance);
		}

		/// <summary>通过属性名称快速获取对象属性值</summary>
		/// <typeparam name="T">返回的数据类型</typeparam>
		/// <param name="propertyName">要获取的对象属性名称</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static T FastGetPropertyValue<T>(string propertyName, object instance)
		{
			return (T)FastGetValue(propertyName, instance);
		}

		public static object FastGetValue(string propertyName, object instance)
		{
			if (propertyName.IsNullOrEmpty_() || instance.IsNullOrEmpty_()) return null;

			var bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;
			var propertyInfo = instance.GetType().GetProperty(propertyName, bindingFlags);
			if (propertyInfo != null)
				return propertyInfo.FastGetValue(instance);

			var fieldInfo = instance.GetType().GetField(propertyName, bindingFlags);
			if (fieldInfo != null)
				return fieldInfo.FastGetValue(instance);
			return null;
		}

		private static Dict<PropertyInfo, Func<object>> SPGC;
		/// <summary>
		/// 快速获取对象静态属性值
		/// </summary>
		/// <param name="propertyInfo"></param>
		/// <returns></returns>
		public static object FastGetStaticValue(this PropertyInfo propertyInfo)
		{
			if (propertyInfo == null) return null;
			if (!propertyInfo.CanRead) return null;

			if (SPGC == null) SPGC = new Dict<PropertyInfo, Func<object>>();
			var exec = SPGC.Get(propertyInfo, () =>
			{
				var fieldExp = Expression.Property(null, propertyInfo);
				var lambda = Expression.Lambda<Func<object>>(
					Expression.Convert(fieldExp, typeof(object))
				);
				return lambda.Compile();
			});
			return exec();
		}
		/// <summary>通过属性名称快速获取对象静态属性值</summary>
		/// <typeparam name="T">返回的数据类型</typeparam>
		/// <param name="propertyName">要获取的对象属性名称</param>
		/// <returns>属性值</returns>
		public static object FastGetStaticPropertyValue(string propertyName, Type type)
		{
			if (propertyName.IsNullOrEmpty_() || type.IsNullOrEmpty_()) return null;
			var propertyInfo = type.GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
			return FastGetStaticValue(propertyInfo);
		}
		/// <summary>通过属性名称快速获取对象静态属性值</summary>
		/// <typeparam name="T">返回的数据类型</typeparam>
		/// <param name="propertyName">要获取的对象属性名称</param>
		/// <returns>属性值</returns>
		public static object FastGetStaticPropertyValue<T>(string propertyName)
		{
			return FastGetStaticPropertyValue(propertyName, typeof(T));
		}
		/// <summary>通过属性名称快速获取对象静态属性值</summary>
		/// <param name="typeName">返回的数据类型</typeparam>
		/// <param name="propertyName">要获取的对象属性名称</param>
		/// <returns>属性值</returns>
		public static object FastGetStaticPropertyValue(string propertyName, string typeName)
		{
			return FastGetStaticPropertyValue(propertyName, FastGetType(typeName));
		}
		public static object FastGetStaticValue(string propertyName, string typeName)
		{
			if (propertyName.IsNullOrEmpty_() || typeName.IsNullOrEmpty_()) return null;

			var bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;
			var type = FastGetType(typeName);
			if (type == null) return null;
			var propertyInfo = type.GetProperty(propertyName, bindingFlags);
			if (propertyInfo != null)
				return propertyInfo.FastGetStaticValue();

			var fieldInfo = type.GetField(propertyName, bindingFlags);
			if (fieldInfo != null)
				return fieldInfo.FastGetStaticValue();
			return null;
		}
		#endregion

		#region 设置
		private static Dict<PropertyInfo, Action<object, object>> PSC;

		/// <summary>快速设置对象属性值</summary>
		/// <param name="propertyInfo">要设置的对象属性</param>
		/// <param name="instance">要设置的对象实例</param>
		/// <param name="value">要设置的值</param>
		public static void FastSetValue(this PropertyInfo propertyInfo, object instance, object value)
		{
			if (propertyInfo == null) return;
			if (!propertyInfo.CanWrite) return;

			if (PSC == null) PSC = new Dict<PropertyInfo, Action<object, object>>();
			var exec = PSC.Get(propertyInfo, () =>
			{
				if (propertyInfo.GetIndexParameters().Length > 0)
				{
					// 对索引属性直接无视
					return (inst, para) => { };
				}
				else
				{
					var objType = typeof(object);
					var instanceParameter = Expression.Parameter(objType, "instance");
					var valueParameter = Expression.Parameter(objType, "value");
					var instanceCast = propertyInfo.GetSetMethod(true).IsStatic ? null : Expression.Convert(instanceParameter, propertyInfo.ReflectedType);
					var valueCast = Expression.Convert(valueParameter, propertyInfo.PropertyType);
					var propertyAccess = Expression.Property(instanceCast, propertyInfo);
					var propertyAssign = Expression.Assign(propertyAccess, valueCast);
					var lambda = Expression.Lambda<Action<object, object>>(propertyAssign, instanceParameter, valueParameter);

					return lambda.Compile();
				}
			});
			exec(instance, value);
		}

		/// <summary>通过属性名称快速设置对象属性值</summary>
		/// <param name="propertyName">要设置的对象属性名称</param>
		/// <param name="instance">要设置的对象实例</param>
		/// <param name="value">要设置的值</param>
		public static void FastSetPropertyValue(string propertyName, object instance, object value)
		{
			if (propertyName.IsNullOrEmpty_()) return;
			var propertyInfo = instance.GetType().GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
			if (propertyInfo != null)
				propertyInfo.FastSetValue(instance, value);
		}

		/// <summary>通过属性名称快速设置对象属性值</summary>
		/// <param name="propertyName">要设置的对象属性名称</param>
		/// <param name="instance">要设置的对象实例</param>
		/// <param name="value">要设置的值</param>
		public static void FastSetValue(string propertyName, object instance, object value)
		{
			var bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;

			if (propertyName.IsNullOrEmpty_()) return;
			var propertyInfo = instance.GetType().GetProperty(propertyName, bindingFlags);
			if (propertyInfo != null)
			{
				propertyInfo.FastSetValue(instance, value);
				return;
			}
			var fieldInfo = instance.GetType().GetField(propertyName, bindingFlags);
			if (fieldInfo != null)
				fieldInfo.FastSetValue(instance, value);
		}
		public static void FastSetValue(string propertyName, string typeName, object value)
		{
			if (propertyName.IsNullOrEmpty_() || typeName.IsNullOrEmpty_()) return;

			var bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;
			var type = FastGetType(typeName);
			if (type == null) return;
			var obj = type.FastInstance();
			var propertyInfo = type.GetProperty(propertyName, bindingFlags);
			if (propertyInfo != null)
				propertyInfo.FastSetValue(obj, value);

			var fieldInfo = type.GetField(propertyName, bindingFlags);
			if (fieldInfo != null)
				fieldInfo.FastSetValue(obj, value);
		}
		#endregion
		#endregion

		#region 字段
		#region 读取
		private static Dict<FieldInfo, Func<object, object>> FGC;

		/// <summary>快速获取对象字段值</summary>
		/// <param name="fieldInfo">要获取的对象字段</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static object FastGetValue(this FieldInfo fieldInfo, object instance)
		{
			if (fieldInfo == null) return null;

			if (FGC == null) FGC = new Dict<FieldInfo, Func<object, object>>();
			var exec = FGC.Get(fieldInfo, () =>
			{
				var objType = typeof(object);
				var instanceParameter = Expression.Parameter(objType, "instance");
				var instanceCast = fieldInfo.IsStatic ? null : Expression.Convert(instanceParameter, fieldInfo.ReflectedType);
				var fieldAccess = Expression.Field(instanceCast, fieldInfo);
				var castFieldValue = Expression.Convert(fieldAccess, objType);
				var lambda = Expression.Lambda<Func<object, object>>(castFieldValue, instanceParameter);

				return lambda.Compile();
			});
			return exec(instance);
		}

		/// <summary>通过字段名称快速获取对象字段值</summary>
		/// <param name="fieldName">要获取的对象字段名称</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static object FastGetFieldValue(string fieldName, object instance)
		{
			if (fieldName.IsNullOrEmpty_()) return null;
			var fieldInfo = instance.GetType().GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
			if (fieldInfo != null)
				return fieldInfo.FastGetValue(instance);
			return null;
		}

		/// <summary>快速获取对象字段值</summary>
		/// <typeparam name="T">返回的数据类型</typeparam>
		/// <param name="fieldInfo">要获取的对象字段</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static T FastGetValue<T>(this FieldInfo fieldInfo, object instance)
		{
			return (T)fieldInfo.FastGetValue(instance);
		}

		/// <summary>通过字段名称快速获取对象字段值</summary>
		/// <typeparam name="T">返回的数据类型</typeparam>
		/// <param name="fieldName">要获取的对象字段名称</param>
		/// <param name="instance">要获取的对象实例</param>
		/// <returns>属性值</returns>
		public static T FastGetFieldValue<T>(string fieldName, object instance)
		{
			return (T)FastGetFieldValue(fieldName, instance);
		}

		private static Dict<FieldInfo, Func<object>> SFGC;
		/// <summary>
		/// 快速获取对象静态字段值
		/// </summary>
		/// <param name="propertyInfo"></param>
		/// <returns></returns>
		public static object FastGetStaticValue(this FieldInfo field)
		{
			if (field == null) return null;

			if (SFGC == null) SFGC = new Dict<FieldInfo, Func<object>>();
			var exec = SFGC.Get(field, () =>
			{
				var fieldExp = Expression.Field(null, field);
				var lambda = Expression.Lambda<Func<object>>(
					Expression.Convert(fieldExp, typeof(object))
				);
				return lambda.Compile();
			});
			return exec();
		}
		/// <summary>通过字段名称快速获取对象静态字段值</summary>
		/// <typeparam name="T">返回的数据类型</typeparam>
		/// <param name="fieldInfo">要获取的对象字段</param>
		/// <returns>属性值</returns>
		public static object FastGetStaticFieldValue<T>(string fieldName)
		{
			return FastGetStaticFieldValue(fieldName, typeof(T));
		}
		/// <summary>通过字段名称快速获取对象静态字段值</summary>
		/// <param name="type">返回的数据类型</typeparam>
		/// <param name="fieldInfo">要获取的对象字段</param>
		/// <returns>属性值</returns>
		public static object FastGetStaticFieldValue(string fieldName, Type type)
		{
			if (fieldName.IsNullOrEmpty_() || type.IsNullOrEmpty_()) return null;
			var field = type.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
			return FastGetStaticValue(field);
		}
		/// <summary>通过字段名称快速获取对象静态字段值</summary>
		/// <param name="typeName">返回的数据类型</typeparam>
		/// <param name="fieldInfo">要获取的对象字段</param>
		/// <returns>属性值</returns>
		public static object FastGetStaticFieldValue(string fieldName, string typeName)
		{
			return FastGetStaticFieldValue(fieldName, FastGetType(typeName));
		}
		#endregion

		#region 设置
		private static Dict<FieldInfo, Action<object, object>> FSC;

		/// <summary>快速设置对象字段值</summary>
		/// <param name="fieldInfo">要设置的对象字段</param>
		/// <param name="instance">要设置的对象实例</param>
		/// <param name="value">要设置的值</param>
		public static void FastSetValue(this FieldInfo fieldInfo, object instance, object value)
		{
			if (fieldInfo == null) return;

			if (FSC == null) FSC = new Dict<FieldInfo, Action<object, object>>();
			var exec = FSC.Get(fieldInfo, () =>
			{
				var objType = typeof(object);
				var instanceParameter = Expression.Parameter(objType, "instance");
				var valueParameter = Expression.Parameter(objType, "value");
				var instanceCast = fieldInfo.IsStatic ? null : Expression.Convert(instanceParameter, fieldInfo.ReflectedType);
				var valueCast = Expression.Convert(valueParameter, fieldInfo.FieldType);
				var fieldAccess = Expression.Field(instanceCast, fieldInfo);
				var fieldAssign = Expression.Assign(fieldAccess, valueCast);
				var lambda = Expression.Lambda<Action<object, object>>(fieldAssign, instanceParameter, valueParameter);

				return lambda.Compile();
			});
			exec(instance, value);
		}

		/// <summary>通过字段名称快速设置对象字段值</summary>
		/// <param name="fieldName">要设置的对象字段名称</param>
		/// <param name="instance">要设置的对象实例</param>
		/// <param name="value">要设置的值</param>
		public static void FastSetFieldValue(string fieldName, object instance, object value)
		{
			if (fieldName.IsNullOrEmpty_()) return;
			var fieldInfo = instance.GetType().GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
			if (fieldInfo != null)
				fieldInfo.FastSetValue(instance, value);
		}
		#endregion
		#endregion

		#region 类型相关
		#region 获取类型
		private static Dict<string, Type> NTC;

		/// <summary>通过类型的程序集限定名称获取类型</summary>
		/// <param name="typeName">要获取的类型的程序集限定名称</param>
		/// <returns>类型</returns>
		/// <exception cref="ArgumentNullOrEmptyException">类型的程序集限定名称为 null 或为空</exception>
		public static Type FastGetType(this string typeName)
		{
			if (typeName.IsNullOrEmpty_())
				throw new ArgumentException("值不能爲 null 或 empty。");

			if (NTC == null) NTC = new Dict<string, Type>(StringComparer.OrdinalIgnoreCase);
			typeName = typeName.Trim();
			return NTC.Get(typeName, () =>
			{
				try
				{
					var type = GetTypeByString(typeName);
					if (type != null)
					{
						return type;
					}
					type = Type.GetType(typeName, true, true);
					if (type == null) type = GetType(typeName);
					return type;
				}
				catch { var type = GetType(typeName); return type; }
			});
		}
		private static Type GetType(string typeName)
		{
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (var assembly in assemblies)
			{
				var allTypes = assembly.GetTypes();
				var type = allTypes.FirstOrDefault(t => t.Name == typeName || t.FullName.Contains(typeName));
				if (type != null)
				{
					return type;
				}
			}
			throw new KeyNotFoundException("Can't find type " + typeName);
		}
		private static Type GetTypeByString(string type)
		{
			switch (type.ToLower())
			{
				case "bool":
					return Type.GetType("System.Boolean", false, true);
				case "byte":
					return Type.GetType("System.Byte", false, true);
				case "sbyte":
					return Type.GetType("System.SByte", false, true);
				case "char":
					return Type.GetType("System.Char", false, true);
				case "decimal":
					return Type.GetType("System.Decimal", false, true);
				case "double":
					return Type.GetType("System.Double", false, true);
				case "single":
				case "float":
					return Type.GetType("System.Single", false, true);
				case "int32":
				case "int":
					return Type.GetType("System.Int32", false, true);
				case "uint32":
				case "uint":
					return Type.GetType("System.UInt32", false, true);
				case "int64":
				case "long":
					return Type.GetType("System.Int64", false, true);
				case "uint64":
				case "ulong":
					return Type.GetType("System.UInt64", false, true);
				case "object":
					return Type.GetType("System.Object", false, true);
				case "int16":
				case "short":
					return Type.GetType("System.Int16", false, true);
				case "uint16":
				case "ushort":
					return Type.GetType("System.UInt16", false, true);
				case "string":
					return Type.GetType("System.String", false, true);
				case "date":
				case "datetime":
					return Type.GetType("System.DateTime", false, true);
				case "guid":
					return Type.GetType("System.Guid", false, true);
				default:
					return Type.GetType(type, false, true);
			}
		}
		#endregion

		#region 获取类型属性
		private static Dict<string, Dict<string, PropertyInfo>> PI;

		/// <summary>获取类型的属性集合</summary>
		/// <param name="type">要获取的类型</param>
		/// <returns>类型的属性集合</returns>
		public static Dict<string, PropertyInfo> FastGetPropertys(this Type type)
		{
			if (PI == null) PI = new Dict<string, Dict<string, PropertyInfo>>(StringComparer.OrdinalIgnoreCase);

			var typeName = type.FullName;
			return PI.Get(typeName, () =>
			{
				var ls = new Dict<string, PropertyInfo>(StringComparer.OrdinalIgnoreCase);
				var props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
				foreach (var p in props) ls[p.Name] = p;
				return ls;
			});
		}

		/// <summary>获取类型的属性集合</summary>
		/// <typeparam name="T">要获取的类型</typeparam>
		/// <returns>类型的属性集合</returns>
		public static Dict<string, PropertyInfo> FastGetPropertys<T>()
		{
			return typeof(T).FastGetPropertys();
		}
		/// <summary>获取类型的属性集合</summary>
		/// <typeparam name="T">要获取的类型</typeparam>
		/// <returns>类型的属性集合</returns>
		public static List<PropertyInfo> FastGetPropertyList<T>()
		{
			return FastGetPropertyList(typeof(T));
		}
		/// <summary>获取类型的属性集合</summary>
		/// <typeparam name="T">要获取的类型</typeparam>
		/// <returns>类型的属性集合</returns>
		public static List<PropertyInfo> FastGetPropertyList(Type ctype)
		{
			var dic = ctype.FastGetPropertys();
			return dic?.Select(s => s.Value).ToList();
		}
		#endregion

		#region 获取类型公共字段
		private static Dict<string, Dictionary<string, FieldInfo>> FI;

		/// <summary>获取类型的公共字段集合</summary>
		/// <param name="type">要获取的类型</param>
		/// <returns>类型的公共字段集合</returns>
		public static Dictionary<string, FieldInfo> FastGetFields(this Type type)
		{
			if (FI == null) FI = new Dict<string, Dictionary<string, FieldInfo>>(StringComparer.OrdinalIgnoreCase);

			var typeName = type.FullName;
			return FI.Get(typeName, () =>
			{
				var ls = new Dictionary<string, FieldInfo>(StringComparer.OrdinalIgnoreCase);
				var props = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
				foreach (var f in props) ls[f.Name] = f;
				return ls;
			});
		}

		/// <summary>获取类型的公共字段集合</summary>
		/// <typeparam name="T">要获取的类型</typeparam>
		/// <returns>类型的公共字段集合</returns>
		public static Dictionary<string, FieldInfo> FastGetFields<T>()
		{
			return typeof(T).FastGetFields();
		}

		/// <summary>获取类型的公共字段集合</summary>
		/// <typeparam name="T">要获取的类型</typeparam>
		/// <returns>类型的公共字段集合</returns>
		public static List<FieldInfo> FastGetFieldList<T>()
		{
			var dic = typeof(T).FastGetFields();
			return dic?.Select(s => s.Value).ToList();
		}
		#endregion

		#region 获取类型访问器
		private static Dict<string, Dictionary<string, Accessor>> AC;

		/// <summary>获取类型的访问器集合</summary>
		/// <param name="type">要获取的类型</param>
		/// <returns>类型的访问器集合</returns>
		public static Dictionary<string, Accessor> FastGetAccessors(this Type type)
		{
			if (AC == null) AC = new Dict<string, Dictionary<string, Accessor>>(StringComparer.OrdinalIgnoreCase);

			var typeName = type.FullName;
			return AC.Get(typeName, () =>
			{
				var ls = new Dictionary<string, Accessor>(StringComparer.OrdinalIgnoreCase);
				var props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
				var name = string.Empty;
				foreach (var p in props)
				{
					name = p.Name;
					var a = new Accessor
					{
						Name = name,
						Member = p,
						DataType = p.PropertyType,
						AccessorType = AccessorType.Property,
						CanRade = p.CanRead,
						CanWrite = p.CanWrite,
						IsVirtual = p.IsVirtual()
					};
					ls.Add(name, a);
				}

				var fis = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
				foreach (var f in fis)
				{
					name = f.Name;
					var a = new Accessor
					{
						Name = name,
						Member = f,
						DataType = f.FieldType,
						AccessorType = AccessorType.Field,
						CanRade = true,
						CanWrite = true,
						IsVirtual = false
					};
					ls.Add(name, a);
				}

				return ls;
			});
		}

		/// <summary>获取类型的访问器集合</summary>
		/// <typeparam name="T">要获取的类型</typeparam>
		/// <returns>类型的访问器集合</returns>
		public static Dictionary<string, Accessor> FastGetAccessors<T>()
		{
			return typeof(T).FastGetAccessors();
		}
		#endregion
		#endregion

		#region GetConstructor
		/// <summary>
		/// 获取构造函数
		/// </summary>
		/// <param name="_Type"></param>
		/// <param name="paramters"></param>
		/// <returns></returns>
		public static ConstructorInfo GetConstructor(this Type _Type, object[] paramters)
		{
			bool argExistsNull;

			Type[] argsTypes = GetTypeArray(paramters, out argExistsNull);
			try
			{
				var gct = _Type.GetConstructor(argsTypes);
				if (gct == null)
				{
					var bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
					gct = _Type.GetConstructors(bindingAttr)?.FirstOrDefault(m => m.GetParameters()?.Length == argsTypes.Length);
				}
				return gct;
			}
			catch (ArgumentNullException)
			{
				return GetConstructor(_Type, argsTypes, argExistsNull);
			}
			catch (ArgumentException)
			{
				return GetConstructor(_Type, argsTypes, argExistsNull);
			}
		}
		/// <summary>
		/// 获取构造函数
		/// </summary>
		/// <param name="_Type"></param>
		/// <param name="argsTypes"></param>
		/// <param name="argExistsNull"></param>
		/// <returns></returns>
		public static ConstructorInfo GetConstructor(this Type _Type, Type[] types, bool argExistsNull = false)
		{
			int _iParamCount = types == null ? 0 : types.Length;
			var bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
			if (argExistsNull)//如果入参存在空值
			{
				//通过名称获取所有配置的方法
				var _listConstructorInfo = _Type.GetConstructors(bindingAttr).ToList();
				if (_listConstructorInfo?.Count == 1)//唯一直接返回
				{
					return _listConstructorInfo[0];
				}
				else if (_listConstructorInfo?.Count > 1)
				{
					var _listConstructorInfoc = _listConstructorInfo.FindAll(p => p.GetParameters().Length.Equals(_iParamCount));
					if (_listConstructorInfoc?.Count == 1)//通过入参数量来配置方法，唯一直接返回
					{
						return _listConstructorInfoc[0];
					}
					else if (_listConstructorInfoc?.Count > 1)
					{
						//寻找参数类型匹配最多的方法
						var mathCount = new List<int>(new int[_listConstructorInfoc.Count]);
						for (var i = 0; i < _listConstructorInfoc.Count; i++)
						{
							var prs = _listConstructorInfoc[i].GetParameters();
							mathCount[i] = 0;
							for (var k = 0; k < prs.Length; k++)
							{
								if ((types[k] != null
										 && (types[k] == prs[k].ParameterType || types[k] == Nullable.GetUnderlyingType(prs[k].ParameterType)))
									 || (types[k] == null
										 && (!prs[k].ParameterType.IsValueType || prs[k].ParameterType.IsNullable())))
									mathCount[i]++;
							}
						}
						return _listConstructorInfoc[mathCount.IndexOf(mathCount.Max())];
					}
				}
				return null;
			}
			//------------------------------------------------------------------------------------------------------------------
			if (_iParamCount > 0)
			{
				return _Type.GetConstructor(bindingAttr,//筛选条件
						 Type.DefaultBinder,//绑定
						 types,//参数类型
						 new ParameterModifier[] { new ParameterModifier(_iParamCount) }//参数个数
						  );
			}
			else
			{
				try
				{
					return _Type.GetConstructor(types);
				}
				catch (AmbiguousMatchException)
				{
					return _Type.GetConstructors(bindingAttr)?.FirstOrDefault(m => m.GetParameters()?.Length == 0);
				}
			}
		}
		#endregion

		#region GetMethodInfo
		///// <summary>
		///// 移除控件eventhandler方法
		///// </summary>
		///// <param name="target"></param>
		///// <param name="eventName"></param>
		//public static void RemoveEventHandler(this Control target, string eventName)
		//{
		//	if (target == null) return;
		//	if (string.IsNullOrEmpty(eventName)) return;

		//	BindingFlags bPropertyFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic;//筛选
		//	BindingFlags fieldFlags = BindingFlags.Static | BindingFlags.NonPublic;
		//	Type ctrlType = typeof(System.Windows.Forms.Control);
		//	PropertyInfo pInfo = ctrlType.GetProperty("Events", bPropertyFlags);
		//	EventHandlerList evtHLst = (EventHandlerList)pInfo.GetValue(target, null);//事件列表
		//	FieldInfo fieldInfo = (typeof(Control).GetField("Event" + eventName, fieldFlags));
		//	Delegate d = evtHLst[fieldInfo.GetValue(target)];

		//	if (d == null) return;
		//	EventInfo eventInfo = ctrlType.GetEvent(eventName);

		//	foreach (Delegate dx in d.GetInvocationList())
		//	{
		//		//移除已订阅的eventName类型事件
		//		eventInfo.RemoveEventHandler(target, dx);
		//	}
		//}

		/// <summary>
		/// 获取对象方法
		/// </summary>
		/// <param name="_Type"></param>
		/// <param name="_sMethodName"></param>
		/// <param name="paramters"></param>
		/// <returns></returns>
		public static MethodInfo GetMethodInfo(this Type _Type, string _sMethodName, params object[] paramters)
		{
			try
			{
				var bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public;
				var md = _Type.GetMethod(_sMethodName, bindingAttr);
				if (md == null)
				{
					bool argExistsNull;
					Type[] argsTypes = GetTypeArray(paramters, out argExistsNull);
					return GetMethodInfo(_Type, _sMethodName, argsTypes, true);
				}
				else
				{
					return md;
				}
			}
			catch (AmbiguousMatchException)
			{
				bool argExistsNull;
				Type[] argsTypes = GetTypeArray(paramters, out argExistsNull);
				return GetMethodInfo(_Type, _sMethodName, argsTypes, argExistsNull);
			}
		}

		/// <summary>
		/// 获取指定数组中对象的类型
		/// </summary>
		/// <param name="paramters"></param>
		/// <param name="argExistsNull"></param>
		/// <returns></returns>
		public static Type[] GetTypeArray(object[] paramters, out bool argExistsNull)
		{
			argExistsNull = false;
			Type[] argsTypes = new Type[0];
			try
			{
				if (paramters?.Length > 0)
				{
					argsTypes = new Type[paramters.Length]; //Type.GetTypeArray(paramters);
					for (var i = 0; i < paramters.Length; i++)
					{
						if (paramters[i] != null)
						{
							argsTypes[i] = paramters[i].GetType();
						}
						else
						{
							argsTypes[i] = null;
							argExistsNull = true;
						}
					}
				}
			}
			catch
			{
				argsTypes = new Type[0];
			}
			return argsTypes;
		}
		/// <summary>
		/// 获取对象方法
		/// </summary>
		/// <param name="_Type"></param>
		/// <param name="_sMethodName"></param>
		/// <param name="argsTypes"></param>
		/// <param name="argExistsNull"></param>
		/// <returns></returns>
		public static MethodInfo GetMethodInfo(Type _Type, string _sMethodName, Type[] argsTypes, bool argExistsNull = false)
		{
			int _iParamCount = argsTypes == null ? 0 : argsTypes.Length;
			var bindingAttr = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public;
			if (argExistsNull)//如果入参存在空值
			{
				//通过名称获取所有配置的方法
				var _listMethodInfo = _Type.GetMethods(bindingAttr).ToList().FindAll(p => p.Name.Equals(_sMethodName, StringComparison.OrdinalIgnoreCase));
				if (_listMethodInfo?.Count == 1)//唯一直接返回
				{
					return _listMethodInfo[0];
				}
				else if (_listMethodInfo?.Count > 1)
				{
					var _listMethodInfoc = _listMethodInfo.FindAll(p => p.GetParameters().Length.Equals(_iParamCount));
					if (_listMethodInfoc?.Count == 1)//通过入参数量来配置方法，唯一直接返回
					{
						return _listMethodInfoc[0];
					}
					else if (_listMethodInfoc?.Count > 1)
					{
						//寻找参数类型匹配最多的方法
						var mathCount = new List<int>(new int[_listMethodInfoc.Count]);
						for (var i = 0; i < _listMethodInfoc.Count; i++)
						{
							var prs = _listMethodInfoc[i].GetParameters();
							mathCount[i] = 0;
							for (var k = 0; k < prs.Length; k++)
							{
								if ((argsTypes[k] != null
										 && (argsTypes[k] == prs[k].ParameterType || argsTypes[k] == Nullable.GetUnderlyingType(prs[k].ParameterType)))
									 || (argsTypes[k] == null
										 && (!prs[k].ParameterType.IsValueType || prs[k].ParameterType.IsNullable())))
									mathCount[i]++;
							}
						}
						return _listMethodInfoc[mathCount.IndexOf(mathCount.Max())];
					}
				}
				return null;
			}
			//------------------------------------------------------------------------------------------------------------------
			if (_iParamCount > 0)
			{
				return _Type.GetMethod(_sMethodName, bindingAttr,//筛选条件
						 Type.DefaultBinder,//绑定
						 argsTypes,//参数类型
						 new ParameterModifier[] { new ParameterModifier(_iParamCount) }//参数个数
						  );
			}
			else
			{
				try
				{
					return _Type.GetMethod(_sMethodName, bindingAttr);
				}
				catch (AmbiguousMatchException)
				{
					return _Type.GetMethods(bindingAttr)?.FirstOrDefault(m => m.Name.Equals(_sMethodName, StringComparison.OrdinalIgnoreCase) && m.GetParameters()?.Length == 0);
				}
			}
		}
		#endregion

	}
}