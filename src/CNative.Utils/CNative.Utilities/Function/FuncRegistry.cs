﻿using System;

namespace CNative.Utilities.Function
{
#if NET45 && NET40
    
using Microsoft.Win32;
    /// <summary>
    /// 注册表操作类,此类不是通用的注册表操作类，只操作路径为：Computer\HKEY_CURRENT_USER\Software\CNative下的节点信息
    /// </summary>
    public class FuncRegistry:IDisposable
    {
       private RegistryKey _HCenterRegistryKey;
        public FuncRegistry()
        {
           _HCenterRegistryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE", true).CreateSubKey("CNative");
        }

        /// <summary>
        /// 写入注册表信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void WriteValue(string name, string value)
        {
          _HCenterRegistryKey.SetValue(name, value);
        }

        /// <summary>
        /// 清除CNative节点下所有的注册表信息
        /// </summary>
        public void Clear()
        {
           Registry.CurrentUser.OpenSubKey(@"SOFTWARE", true).DeleteSubKey("CNative");
        }

        /// <summary>
        /// 获取注册表信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="nullValue"></param>
        /// <returns></returns>
        public string GetValue(string name,string nullValue=null)
        {
            var val = _HCenterRegistryKey.GetValue(name);
            if (val == null && nullValue != null)
                return nullValue;
            return val.NullToStr();

        }

        public void Dispose()
        {
            _HCenterRegistryKey?.Dispose();
        }
    }
#endif
}
