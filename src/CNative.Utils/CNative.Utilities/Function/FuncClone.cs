﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
//using CNative.Network.Common;

namespace CNative.Utilities.Function
{
    public static class FuncClone<TIn>
    {
        private static Func<TIn, TIn> GetFunc(Func<string, bool> excludeExpress)
        {
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TIn), "p");
            List<MemberBinding> memberBindingList = new List<MemberBinding>();

            foreach (var item in typeof(TIn).GetProperties())
            {
                if (!item.CanWrite)
                    continue;
                if (excludeExpress != null && !excludeExpress(item.Name))
                    continue;
                MemberExpression property =
                    Expression.Property(parameterExpression, typeof(TIn).GetProperty(item.Name));
                MemberBinding memberBinding = Expression.Bind(item, property);
                memberBindingList.Add(memberBinding);
            }

            MemberInitExpression memberInitExpression =
                Expression.MemberInit(Expression.New(typeof(TIn)), memberBindingList.ToArray());
            Expression<Func<TIn, TIn>> lambda = Expression.Lambda<Func<TIn, TIn>>(memberInitExpression,
                new ParameterExpression[] { parameterExpression });

            return lambda.Compile();
        }

        public static IEnumerable<TIn> Clone(List<TIn> tIn, Func<string, bool> excludeExpress = null)
        {
            var cache = GetFunc(excludeExpress);
            foreach (TIn tin in tIn)
            {
                yield return cache(tin);
            }
        }


        public static TIn Clone(TIn tIn, Func<string, bool> excludeExpress = null)
        {
            var cache = GetFunc(excludeExpress);
            return cache(tIn);
        }

        public static TIn CloneByJson(TIn tIn)
        {
            var jsonStr = JsonHelper.SerializeObject(tIn);
            return JsonHelper.DeserializeObject<TIn>(jsonStr);
        }

        public static TOut CloneByJson<TIn,TOut>(TIn tIn)
        {
            var jsonStr = JsonHelper.SerializeObject(tIn);
            return JsonHelper.DeserializeObject<TOut>(jsonStr);
        }

    }

    public static class FuncClone<TIn, TOut>
    {
        private static Func<TIn, TOut> GetFunc(Func<string, bool> excludeExpress)
        {
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TIn), "p");
            List<MemberBinding> memberBindingList = new List<MemberBinding>();

            foreach (var item in typeof(TIn).GetProperties())
            {
                if (!item.CanWrite)
                    continue;
                if (excludeExpress != null && !excludeExpress(item.Name))
                    continue;
                if (typeof(TIn).GetProperties().All(a => a.Name.ToUpper() != item.Name.ToUpper()))
                    continue;
                var tOutProperty = typeof(TOut).GetProperty(item.Name);
                if(tOutProperty==null || tOutProperty.PropertyType!=item.PropertyType)
                    continue;
                MemberExpression property = Expression.Property(parameterExpression, item);
                MemberBinding memberBinding = Expression.Bind(tOutProperty, property);
                memberBindingList.Add(memberBinding);
            }

            MemberInitExpression memberInitExpression =
                Expression.MemberInit(Expression.New(typeof(TOut)), memberBindingList.ToArray());
            Expression<Func<TIn, TOut>> lambda = Expression.Lambda<Func<TIn, TOut>>(memberInitExpression,
                new ParameterExpression[] { parameterExpression });

            return lambda.Compile();
        }

        public static IEnumerable<TOut> Clone(List<TIn> tIn, Func<string, bool> excludeExpress = null)
        {
            var cache = GetFunc(excludeExpress);
            foreach (TIn tin in tIn)
            {
                yield return cache(tin);
            }
        }
        public static TOut Clone(TIn tIn, Func<string, bool> excludeExpress = null)
        {
            var cache = GetFunc(excludeExpress);
            return cache(tIn);
        }


        public static IEnumerable<TOut> CloneByJson(List<TIn> tIn)
        {
            var jsonStr = JsonHelper.SerializeObject(tIn);
            return JsonHelper.DeserializeObject<IEnumerable<TOut>>(jsonStr);
        }
        public static TOut CloneByJson(TIn tIn)
        {
            var jsonStr = JsonHelper.SerializeObject(tIn);
            return JsonHelper.DeserializeObject<TOut>(jsonStr);
        }
    }
}