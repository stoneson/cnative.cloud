﻿
using System;

namespace CNative.Utilities.Log4Net
{
#if !net40
    using Microsoft.Extensions.Logging;
    public static class Log4NetProviderExtensions
    {
        public static ILogger CreateLogger<T>(this ILoggerProvider provider) where T : class
        {
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (!provider.GetType().IsAssignableFrom(typeof(Log4NetProvider)))
            {
                throw new ArgumentOutOfRangeException(nameof(provider), "The ILoggerProvider should be of type Log4NetProvider.");
            }

            return provider.CreateLogger(typeof(T).FullName);
        }
    }
#endif
}
