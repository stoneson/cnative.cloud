﻿#if !net40
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Utilities.Nlog
{
    public static class LoggerFactoryExtensions
    {
        public static ILoggerFactory AddNlogFileLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(new FileLoggerProvider());
            return factory;
        }

#if !net461

        public static ILoggingBuilder AddNlogFileLogger(this ILoggingBuilder builder)
        {
            builder.AddProvider(new FileLoggerProvider());
            return builder;
        }
#endif
    }
}
#endif

