﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace CNative.Utilities
{
    public class StackTraceHelper
    {
        public static List<string> GetStackTrace()
        {
            StackTrace stackTrace = new StackTrace(true);
            int frameCount = stackTrace.FrameCount;
            List<string> list = new List<string>(frameCount);
            for (int i = 1; i < frameCount; i++)
            {
                StackFrame frame = stackTrace.GetFrame(i);
                MethodBase method = frame.GetMethod();
                list.Add(a(method));
            }
            list.Reverse();
            return list;
        }

        private static string a(MethodBase A_0)
        {
            string value;
            if (A_0.DeclaringType == null)
            {
                value = A_0.Name;
            }
            else
            {
                value = A_0.DeclaringType.FullName + '.' + A_0.Name;
            }
            StringBuilder stringBuilder = new StringBuilder(value);
            stringBuilder.Append('(');
            foreach (ParameterInfo parameterInfo in A_0.GetParameters())
            {
                Type parameterType = parameterInfo.ParameterType;
                string text = string.Empty;
                if (parameterType.IsArray)
                {
                    text = a(parameterType);
                }
                stringBuilder.Append(string.Concat(new object[]
                {
                    parameterType.Name,
                    text,
                    ' ',
                    parameterInfo.Name
                }));
            }
            stringBuilder.Append(')');
            return stringBuilder.ToString();
        }

        private static string a(Type A_0)
        {
            int arrayRank = A_0.GetArrayRank();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append('[');
            stringBuilder.Append(',', arrayRank - 1);
            stringBuilder.Append(']');
            return stringBuilder.ToString();
        }
    }
}
