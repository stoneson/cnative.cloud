﻿using System;
using System.Runtime.InteropServices;

namespace CNative.Utilities
{
    /// <summary>
    /// 内存清理压缩到虚拟内存，避免内存过大，思路：将目前不用的内存数据转移
    /// </summary>
    public class MemoryClear
    {
        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize")]
        public static extern int SetProcessWorkingSetSize(IntPtr process, int minSize, int maxSize);

        /// <summary>
        /// 释放内存
        /// </summary>
        public static void Clear()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
        }
    }
}
